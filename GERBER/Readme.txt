Generates Extended Gerber Format
You will get five gerber files that contain data for:
Top Copper - *.GTL
2nd Copper- *.G2L 
3rd Copper- *.G3L 
Bottom Copper - *.GBL
Top Silkscreen - *.GTO
Bottom Silkscreen - *.GBO
Top Soldermask - *.GTS
Bottom Soldermask - *.GBS
Top Solderpaste - *.GTP
Bottom Solderpaste - *.GBP
Milling File - *.GML
Drill File - *.TXT
SMD top component location - *.mnt
SMD bottom component location - *.mnb

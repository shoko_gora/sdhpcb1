<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.3.2">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="framebase" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="case" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="Stiffner" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="mPads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="mVias" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="mUnrouted" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="mDimension" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="mtStop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="mbStop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="PART_" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Marcin_LIB">
<packages>
<package name="TSSOP28">
<description>&lt;b&gt;Thin Shrink Small Outline Plastic 28&lt;/b&gt;&lt;p&gt;
MAX3223-MAX3243.pdf</description>
<wire x1="-4.7896" y1="-2.2828" x2="4.7896" y2="-2.2828" width="0.1524" layer="21"/>
<wire x1="4.7896" y1="2.2828" x2="4.7896" y2="-2.2828" width="0.1524" layer="21"/>
<wire x1="4.7896" y1="2.2828" x2="-4.7896" y2="2.2828" width="0.1524" layer="21"/>
<wire x1="-4.7896" y1="-2.2828" x2="-4.7896" y2="2.2828" width="0.1524" layer="21"/>
<wire x1="-4.561" y1="-2.0542" x2="4.561" y2="-2.0542" width="0.0508" layer="21"/>
<wire x1="4.561" y1="2.0542" x2="4.561" y2="-2.0542" width="0.0508" layer="21"/>
<wire x1="4.561" y1="2.0542" x2="-4.561" y2="2.0542" width="0.0508" layer="21"/>
<wire x1="-4.561" y1="-2.0542" x2="-4.561" y2="2.0542" width="0.0508" layer="21"/>
<circle x="-3.5756" y="-1.2192" radius="0.4572" width="0.1524" layer="21"/>
<smd name="1" x="-4.225" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-3.575" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="-2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="-1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="-0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="-0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="3.575" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="4.225" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="15" x="4.225" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="16" x="3.575" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="17" x="2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="18" x="2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="19" x="1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="20" x="0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="21" x="0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="22" x="-0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="23" x="-0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="24" x="-1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="25" x="-2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="26" x="-2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="27" x="-3.575" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="28" x="-4.225" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<text x="-5.1706" y="-2.0828" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.1612" y="-2.0828" size="1.016" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.3266" y1="-3.121" x2="-4.1234" y2="-2.2828" layer="51"/>
<rectangle x1="-3.6766" y1="-3.121" x2="-3.4734" y2="-2.2828" layer="51"/>
<rectangle x1="-3.0266" y1="-3.121" x2="-2.8234" y2="-2.2828" layer="51"/>
<rectangle x1="-2.3766" y1="-3.121" x2="-2.1734" y2="-2.2828" layer="51"/>
<rectangle x1="-1.7266" y1="-3.121" x2="-1.5234" y2="-2.2828" layer="51"/>
<rectangle x1="-1.0766" y1="-3.121" x2="-0.8734" y2="-2.2828" layer="51"/>
<rectangle x1="-0.4266" y1="-3.121" x2="-0.2234" y2="-2.2828" layer="51"/>
<rectangle x1="0.2234" y1="-3.121" x2="0.4266" y2="-2.2828" layer="51"/>
<rectangle x1="0.8734" y1="-3.121" x2="1.0766" y2="-2.2828" layer="51"/>
<rectangle x1="1.5234" y1="-3.121" x2="1.7266" y2="-2.2828" layer="51"/>
<rectangle x1="2.1734" y1="-3.121" x2="2.3766" y2="-2.2828" layer="51"/>
<rectangle x1="2.8234" y1="-3.121" x2="3.0266" y2="-2.2828" layer="51"/>
<rectangle x1="3.4734" y1="-3.121" x2="3.6766" y2="-2.2828" layer="51"/>
<rectangle x1="4.1234" y1="-3.121" x2="4.3266" y2="-2.2828" layer="51"/>
<rectangle x1="4.1234" y1="2.2828" x2="4.3266" y2="3.121" layer="51"/>
<rectangle x1="3.4734" y1="2.2828" x2="3.6766" y2="3.121" layer="51"/>
<rectangle x1="2.8234" y1="2.2828" x2="3.0266" y2="3.121" layer="51"/>
<rectangle x1="2.1734" y1="2.2828" x2="2.3766" y2="3.121" layer="51"/>
<rectangle x1="1.5234" y1="2.2828" x2="1.7266" y2="3.121" layer="51"/>
<rectangle x1="0.8734" y1="2.2828" x2="1.0766" y2="3.121" layer="51"/>
<rectangle x1="0.2234" y1="2.2828" x2="0.4266" y2="3.121" layer="51"/>
<rectangle x1="-0.4266" y1="2.2828" x2="-0.2234" y2="3.121" layer="51"/>
<rectangle x1="-1.0766" y1="2.2828" x2="-0.8734" y2="3.121" layer="51"/>
<rectangle x1="-1.7266" y1="2.2828" x2="-1.5234" y2="3.121" layer="51"/>
<rectangle x1="-2.3766" y1="2.2828" x2="-2.1734" y2="3.121" layer="51"/>
<rectangle x1="-3.0266" y1="2.2828" x2="-2.8234" y2="3.121" layer="51"/>
<rectangle x1="-3.6766" y1="2.2828" x2="-3.4734" y2="3.121" layer="51"/>
<rectangle x1="-4.3266" y1="2.2828" x2="-4.1234" y2="3.121" layer="51"/>
</package>
<package name="SO-16LD">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt; 300 x 400 mil</description>
<wire x1="-5.08" y1="-3.7084" x2="5.08" y2="-3.7084" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-3.7084" x2="5.08" y2="3.7084" width="0.1524" layer="21"/>
<wire x1="5.08" y1="3.7084" x2="-5.08" y2="3.7084" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.7084" x2="-5.08" y2="-3.7084" width="0.1524" layer="21"/>
<circle x="-4.2164" y="-2.8194" radius="0.5334" width="0.1524" layer="21"/>
<smd name="1" x="-4.445" y="-4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="2" x="-3.175" y="-4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="3" x="-1.905" y="-4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="4" x="-0.635" y="-4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="5" x="0.635" y="-4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="6" x="1.905" y="-4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="7" x="3.175" y="-4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="8" x="4.445" y="-4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="9" x="4.445" y="4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="10" x="3.175" y="4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="11" x="1.905" y="4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="12" x="0.635" y="4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="13" x="-0.635" y="4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="14" x="-1.905" y="4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="15" x="-3.175" y="4.7752" dx="0.762" dy="1.524" layer="1"/>
<smd name="16" x="-4.445" y="4.7752" dx="0.762" dy="1.524" layer="1"/>
<text x="-5.461" y="-3.175" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.81" y="-1.016" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.2418" y1="3.7592" x2="4.6482" y2="5.08" layer="51"/>
<rectangle x1="2.9718" y1="3.7592" x2="3.3782" y2="5.08" layer="51"/>
<rectangle x1="1.7018" y1="3.7592" x2="2.1082" y2="5.08" layer="51"/>
<rectangle x1="0.4318" y1="3.7592" x2="0.8382" y2="5.08" layer="51"/>
<rectangle x1="-0.8382" y1="3.7592" x2="-0.4318" y2="5.08" layer="51"/>
<rectangle x1="-2.1082" y1="3.7592" x2="-1.7018" y2="5.08" layer="51"/>
<rectangle x1="-3.3782" y1="3.7592" x2="-2.9718" y2="5.08" layer="51"/>
<rectangle x1="-4.6482" y1="3.7592" x2="-4.2418" y2="5.08" layer="51"/>
<rectangle x1="4.2418" y1="-5.08" x2="4.6482" y2="-3.7592" layer="51"/>
<rectangle x1="2.9718" y1="-5.08" x2="3.3782" y2="-3.7592" layer="51"/>
<rectangle x1="1.7018" y1="-5.08" x2="2.1082" y2="-3.7592" layer="51"/>
<rectangle x1="0.4318" y1="-5.08" x2="0.8382" y2="-3.7592" layer="51"/>
<rectangle x1="-0.8382" y1="-5.08" x2="-0.4318" y2="-3.7592" layer="51"/>
<rectangle x1="-2.1082" y1="-5.08" x2="-1.7018" y2="-3.7592" layer="51"/>
<rectangle x1="-3.3782" y1="-5.08" x2="-2.9718" y2="-3.7592" layer="51"/>
<rectangle x1="-4.6482" y1="-5.08" x2="-4.2418" y2="-3.7592" layer="51"/>
</package>
<package name="WSOF6I">
<wire x1="-1.3" y1="0.8" x2="1.3" y2="0.8" width="0.1" layer="51"/>
<wire x1="1.3" y1="0.8" x2="1.3" y2="-0.8" width="0.1" layer="51"/>
<wire x1="1.3" y1="-0.8" x2="-1.3" y2="-0.8" width="0.1" layer="51"/>
<wire x1="-1.3" y1="-0.8" x2="-1.3" y2="0.8" width="0.1" layer="51"/>
<smd name="1" x="-1.4" y="0.525" dx="0.3" dy="0.5" layer="1" rot="R90"/>
<circle x="0" y="0" radius="0.4" width="0.1" layer="51"/>
<rectangle x1="-0.2" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<smd name="2" x="-1.4" y="0" dx="0.25" dy="0.5" layer="1" rot="R90"/>
<smd name="3" x="-1.4" y="-0.525" dx="0.25" dy="0.5" layer="1" rot="R90"/>
<smd name="4" x="1.4" y="-0.525" dx="0.25" dy="0.5" layer="1" rot="R90"/>
<smd name="5" x="1.4" y="0" dx="0.25" dy="0.5" layer="1" rot="R90"/>
<smd name="6" x="1.4" y="0.525" dx="0.25" dy="0.5" layer="1" rot="R90"/>
<wire x1="-1.3" y1="0.8" x2="1.3" y2="0.8" width="0.1" layer="21"/>
<wire x1="1.3" y1="0.8" x2="1.3" y2="-0.8" width="0.1" layer="21"/>
<wire x1="1.3" y1="-0.8" x2="-1.3" y2="-0.8" width="0.1" layer="21"/>
<wire x1="-1.3" y1="-0.8" x2="-1.3" y2="0.8" width="0.1" layer="21"/>
<circle x="0" y="0" radius="0.4" width="0.1" layer="21"/>
<rectangle x1="-0.2" y1="-0.1" x2="0.2" y2="0.1" layer="21"/>
<text x="-1" y="1" size="1" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1" layer="27">&gt;MPN</text>
<circle x="-1.4" y="1.1" radius="0.14141875" width="0.127" layer="21"/>
</package>
<package name="BME280">
<wire x1="-1.25" y1="1.25" x2="1.25" y2="1.25" width="0.127" layer="21"/>
<wire x1="1.25" y1="1.25" x2="1.25" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.25" y1="-1.25" x2="-1.25" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-1.25" y1="-1.25" x2="-1.25" y2="1.25" width="0.127" layer="21"/>
<smd name="8" x="-1.025" y="0.975" dx="0.5" dy="0.35" layer="1"/>
<smd name="7" x="-1.025" y="0.325" dx="0.5" dy="0.35" layer="1"/>
<smd name="6" x="-1.025" y="-0.325" dx="0.5" dy="0.35" layer="1"/>
<smd name="5" x="-1.025" y="-0.975" dx="0.5" dy="0.35" layer="1"/>
<smd name="4" x="1.025" y="-0.975" dx="0.5" dy="0.35" layer="1"/>
<smd name="3" x="1.025" y="-0.325" dx="0.5" dy="0.35" layer="1"/>
<smd name="2" x="1.025" y="0.325" dx="0.5" dy="0.35" layer="1"/>
<smd name="1" x="1.025" y="0.975" dx="0.5" dy="0.35" layer="1"/>
<circle x="0.8" y="1.6" radius="0.1" width="0.127" layer="21"/>
<wire x1="0" y1="1.3" x2="-1" y2="1.3" width="0.05" layer="51"/>
<wire x1="-1" y1="1.3" x2="-1.2" y2="1.1" width="0.05" layer="51" curve="90"/>
<wire x1="-1.2" y1="1.1" x2="-1.2" y2="0" width="0.05" layer="51"/>
<wire x1="0" y1="1.2" x2="-1" y2="1.2" width="0.05" layer="51"/>
<wire x1="-1" y1="1.2" x2="-1.1" y2="1.1" width="0.05" layer="51" curve="90"/>
<wire x1="-1.1" y1="1.1" x2="-1.1" y2="0" width="0.05" layer="51"/>
<wire x1="0" y1="1" x2="-0.6" y2="1" width="0.05" layer="51"/>
<wire x1="-0.6" y1="1" x2="-0.9" y2="0.7" width="0.05" layer="51" curve="90"/>
<wire x1="-0.9" y1="0.7" x2="-0.9" y2="0" width="0.05" layer="51"/>
<wire x1="0" y1="-1.3" x2="-1" y2="-1.3" width="0.05" layer="51"/>
<wire x1="-1" y1="-1.3" x2="-1.2" y2="-1.1" width="0.05" layer="51" curve="-90"/>
<wire x1="-1.2" y1="-1.1" x2="-1.2" y2="0" width="0.05" layer="51"/>
<wire x1="0" y1="-1.2" x2="-1" y2="-1.2" width="0.05" layer="51"/>
<wire x1="-1" y1="-1.2" x2="-1.1" y2="-1.1" width="0.05" layer="51" curve="-90"/>
<wire x1="-1.1" y1="-1.1" x2="-1.1" y2="0" width="0.05" layer="51"/>
<wire x1="0" y1="-1" x2="-0.6" y2="-1" width="0.05" layer="51"/>
<wire x1="-0.6" y1="-1" x2="-0.9" y2="-0.7" width="0.05" layer="51" curve="-90"/>
<wire x1="-0.9" y1="-0.7" x2="-0.9" y2="0" width="0.05" layer="51"/>
<wire x1="0" y1="1.3" x2="1" y2="1.3" width="0.05" layer="51"/>
<wire x1="1" y1="1.3" x2="1.2" y2="1.1" width="0.05" layer="51" curve="-90"/>
<wire x1="1.2" y1="1.1" x2="1.2" y2="0" width="0.05" layer="51"/>
<wire x1="0" y1="1.2" x2="1" y2="1.2" width="0.05" layer="51"/>
<wire x1="1" y1="1.2" x2="1.1" y2="1.1" width="0.05" layer="51" curve="-90"/>
<wire x1="1.1" y1="1.1" x2="1.1" y2="0" width="0.05" layer="51"/>
<wire x1="0" y1="1" x2="0.6" y2="1" width="0.05" layer="51"/>
<wire x1="0.6" y1="1" x2="0.9" y2="0.7" width="0.05" layer="51" curve="-90"/>
<wire x1="0.9" y1="0.7" x2="0.9" y2="0" width="0.05" layer="51"/>
<wire x1="0" y1="-1.3" x2="1" y2="-1.3" width="0.05" layer="51"/>
<wire x1="1" y1="-1.3" x2="1.2" y2="-1.1" width="0.05" layer="51" curve="90"/>
<wire x1="1.2" y1="-1.1" x2="1.2" y2="0" width="0.05" layer="51"/>
<wire x1="0" y1="-1.2" x2="1" y2="-1.2" width="0.05" layer="51"/>
<wire x1="1" y1="-1.2" x2="1.1" y2="-1.1" width="0.05" layer="51" curve="90"/>
<wire x1="1.1" y1="-1.1" x2="1.1" y2="0" width="0.05" layer="51"/>
<wire x1="0" y1="-1" x2="0.6" y2="-1" width="0.05" layer="51"/>
<wire x1="0.6" y1="-1" x2="0.9" y2="-0.7" width="0.05" layer="51" curve="90"/>
<wire x1="0.9" y1="-0.7" x2="0.9" y2="0" width="0.05" layer="51"/>
<circle x="0" y="0.6" radius="0.15" width="0.05" layer="51"/>
<text x="-1.1" y="2" size="1" layer="25">&gt;NAME</text>
<text x="-1.3" y="-2.4" size="1" layer="27">&gt;MPN</text>
</package>
<package name="DCN-R-PDSO-G8">
<description>&lt;b&gt;DCN (R-PDSO-G8) PLASTIC SMALL-OUTLINE PACKAGE (DIE DOWN)&lt;/b&gt;&lt;p&gt;
Source: tps62120.pdf (www.ti.com)</description>
<smd name="1" x="-0.975" y="-1.35" dx="0.45" dy="0.8" layer="1" stop="no" cream="no"/>
<smd name="2" x="-0.325" y="-1.35" dx="0.45" dy="0.8" layer="1" stop="no" cream="no"/>
<smd name="3" x="0.325" y="-1.35" dx="0.45" dy="0.8" layer="1" stop="no" cream="no"/>
<smd name="4" x="0.975" y="-1.35" dx="0.45" dy="0.8" layer="1" stop="no" cream="no"/>
<rectangle x1="-1.225" y1="-1.825" x2="-0.725" y2="-0.875" layer="29"/>
<rectangle x1="-1.175" y1="-1.725" x2="-0.775" y2="-0.975" layer="31"/>
<rectangle x1="-0.575" y1="-1.825" x2="-0.075" y2="-0.875" layer="29"/>
<rectangle x1="-0.525" y1="-1.725" x2="-0.125" y2="-0.975" layer="31"/>
<rectangle x1="0.075" y1="-1.825" x2="0.575" y2="-0.875" layer="29"/>
<rectangle x1="0.125" y1="-1.725" x2="0.525" y2="-0.975" layer="31"/>
<rectangle x1="0.725" y1="-1.825" x2="1.225" y2="-0.875" layer="29"/>
<rectangle x1="0.775" y1="-1.725" x2="1.175" y2="-0.975" layer="31"/>
<smd name="5" x="0.975" y="1.35" dx="0.45" dy="0.8" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="6" x="0.325" y="1.35" dx="0.45" dy="0.8" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="7" x="-0.325" y="1.35" dx="0.45" dy="0.8" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="8" x="-0.975" y="1.35" dx="0.45" dy="0.8" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="0.725" y1="0.875" x2="1.225" y2="1.825" layer="29" rot="R180"/>
<rectangle x1="0.775" y1="0.975" x2="1.175" y2="1.725" layer="31" rot="R180"/>
<rectangle x1="0.075" y1="0.875" x2="0.575" y2="1.825" layer="29" rot="R180"/>
<rectangle x1="0.125" y1="0.975" x2="0.525" y2="1.725" layer="31" rot="R180"/>
<rectangle x1="-0.575" y1="0.875" x2="-0.075" y2="1.825" layer="29" rot="R180"/>
<rectangle x1="-0.525" y1="0.975" x2="-0.125" y2="1.725" layer="31" rot="R180"/>
<rectangle x1="-1.225" y1="0.875" x2="-0.725" y2="1.825" layer="29" rot="R180"/>
<rectangle x1="-1.175" y1="0.975" x2="-0.775" y2="1.725" layer="31" rot="R180"/>
<rectangle x1="-1.15" y1="-1.5" x2="-0.8" y2="-0.825" layer="51"/>
<wire x1="-1.4" y1="0.775" x2="1.4" y2="0.775" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.775" x2="1.4" y2="-0.775" width="0.2032" layer="21"/>
<wire x1="1.4" y1="-0.775" x2="-1.4" y2="-0.775" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="-0.775" x2="-1.4" y2="0.775" width="0.2032" layer="21"/>
<rectangle x1="-1.4" y1="-0.75" x2="0" y2="0" layer="21"/>
<rectangle x1="-0.5" y1="-1.5" x2="-0.15" y2="-0.825" layer="51"/>
<rectangle x1="0.15" y1="-1.5" x2="0.5" y2="-0.825" layer="51"/>
<rectangle x1="0.8" y1="-1.5" x2="1.15" y2="-0.825" layer="51"/>
<rectangle x1="0.725" y1="0.875" x2="1.225" y2="1.825" layer="29" rot="R180"/>
<rectangle x1="0.075" y1="0.875" x2="0.575" y2="1.825" layer="29" rot="R180"/>
<rectangle x1="-0.575" y1="0.875" x2="-0.075" y2="1.825" layer="29" rot="R180"/>
<rectangle x1="-1.225" y1="0.875" x2="-0.725" y2="1.825" layer="29" rot="R180"/>
<rectangle x1="0.8" y1="0.825" x2="1.15" y2="1.5" layer="51" rot="R180"/>
<rectangle x1="0.15" y1="0.825" x2="0.5" y2="1.5" layer="51" rot="R180"/>
<rectangle x1="-0.5" y1="0.825" x2="-0.15" y2="1.5" layer="51" rot="R180"/>
<rectangle x1="-1.15" y1="0.825" x2="-0.8" y2="1.5" layer="51" rot="R180"/>
<text x="-1.6" y="-1.775" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.9" y="-1.75" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="UBLOX_NEO-6/7">
<description>NEO-6 Series - u-blox 6 NEO Series GALILEO-Ready GPS Modules

The NEO-6 module series brings the high performance, 50-channel u-blox 6 positioning engine, to the miniature NEO form factor.

The u-blox NEO-6 series modules measure 12.2 x 16.0 x 2.4 mm and are the optimal choice for applications requiring greater integration and high performance GPS in weak signal areas.

http://www.u-blox.com/images/downloads/Product_Docs/LEA-6_NEO-6_MAX_HardwareIntegrationManual_%28GPS.G6-HW-09007%29.pdf
http://www.u-blox.com/images/downloads/Product_Docs/NEO-6_DataSheet_%28GPS.G6-HW-09005%29.pdf</description>
<wire x1="-4.43" y1="7.5" x2="5.23" y2="7.5" width="0.127" layer="21"/>
<wire x1="-4.43" y1="7.5" x2="-4.43" y2="-8.5" width="0.127" layer="21"/>
<wire x1="-4.43" y1="-8.5" x2="5.23" y2="-8.5" width="0.127" layer="21"/>
<wire x1="5.23" y1="-8.5" x2="5.23" y2="7.5" width="0.127" layer="21"/>
<circle x="4.14" y="-7.39" radius="0.25" width="0.4" layer="21"/>
<smd name="13_GND" x="-5.6" y="6.6" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="14_MOSI/CFG_COM0" x="-5.6" y="5.5" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="15_MISO/CFG_COM1" x="-5.6" y="4.4" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="16_CFG_CPS0/SCK" x="-5.6" y="3.3" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="17_RESERVED" x="-5.6" y="2.2" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="18_SDA2" x="-5.6" y="-0.8" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="19_SCL2" x="-5.6" y="-1.9" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="20_TXD1" x="-5.6" y="-3" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="21_RXD1" x="-5.6" y="-4.1" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="22_V_BCKP" x="-5.6" y="-5.2" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="23_VCC" x="-5.6" y="-6.3" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="24_GND" x="-5.6" y="-7.4" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="12_GND" x="6.4" y="6.6" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="11_RF_IN" x="6.4" y="5.5" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="10_GND" x="6.4" y="4.4" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="9_VCC_RF" x="6.4" y="3.3" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="8_RESERVED" x="6.4" y="2.2" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="7_VDDUSB" x="6.4" y="-0.8" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="6_USB_DP" x="6.4" y="-1.9" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="5_USB_DM" x="6.4" y="-3" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="4_EXTINT0" x="6.4" y="-4.1" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="3_TIMEPULSE" x="6.4" y="-5.2" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="2_SS_N" x="6.4" y="-6.3" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<smd name="1_RESERVED" x="6.4" y="-7.4" dx="0.8" dy="1.8" layer="1" rot="R90"/>
<text x="4.29" y="0.9" size="1.016" layer="21" rot="R180">ublox NEO-6</text>
</package>
<package name="L0402">
<wire x1="-0.762" y1="-0.4445" x2="-0.889" y2="-0.3175" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.889" y1="-0.3175" x2="-0.889" y2="0.3175" width="0.0762" layer="21"/>
<wire x1="-0.889" y1="0.3175" x2="-0.762" y2="0.4445" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.762" y1="0.4445" x2="0.762" y2="0.4445" width="0.0762" layer="21"/>
<wire x1="0.762" y1="0.4445" x2="0.889" y2="0.3175" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.889" y1="0.3175" x2="0.889" y2="-0.3175" width="0.0762" layer="21"/>
<wire x1="0.889" y1="-0.3175" x2="0.762" y2="-0.4445" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.762" y1="-0.4445" x2="-0.762" y2="-0.4445" width="0.0762" layer="21"/>
<smd name="1" x="-0.4445" y="0" dx="0.635" dy="0.635" layer="1" roundness="50"/>
<smd name="2" x="0.4445" y="0" dx="0.635" dy="0.635" layer="1" roundness="50"/>
<text x="-1.27" y="0.635" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.27" y="-1.524" size="0.889" layer="27" font="vector" ratio="11">&gt;VALUE</text>
</package>
<package name="0603">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="IRM_05/10">
<wire x1="-22.85" y1="-12.7" x2="-22.85" y2="12.7" width="0.127" layer="21"/>
<wire x1="-22.85" y1="12.7" x2="22.85" y2="12.7" width="0.127" layer="21"/>
<wire x1="22.85" y1="12.7" x2="22.85" y2="-12.7" width="0.127" layer="21"/>
<wire x1="22.85" y1="-12.7" x2="-22.85" y2="-12.7" width="0.127" layer="21"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<pad name="AC/L" x="-19.25" y="-9.25" drill="1.3" diameter="2.54"/>
<pad name="AC/N" x="-19.25" y="1.5" drill="1.3" diameter="2.54"/>
<pad name="V-" x="19.25" y="-9.25" drill="1.3" diameter="2.54"/>
<pad name="V+" x="19.25" y="-1.25" drill="1.3" diameter="2.54"/>
<text x="-15.24" y="-10.16" size="1.27" layer="21">AC/L</text>
<text x="-15.24" y="1.27" size="1.27" layer="21">AC/N</text>
<text x="12.7" y="-1.27" size="1.27" layer="21">V+</text>
<text x="12.7" y="-10.16" size="1.27" layer="21">V-</text>
</package>
<package name="OKI-78SRH">
<description>OKI-78SR-3.3/1.5W36H&lt;br&gt;
OKI-78SR-5/1.5W36H</description>
<wire x1="5.08" y1="-1.27" x2="-5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="15.24" width="0.127" layer="21"/>
<wire x1="-5.08" y1="15.24" x2="5.08" y2="15.24" width="0.127" layer="21"/>
<wire x1="5.08" y1="15.24" x2="5.08" y2="10.16" width="0.127" layer="21"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-5.08" y1="10.16" x2="-2.54" y2="12.7" width="0.127" layer="21"/>
<wire x1="-2.54" y1="12.7" x2="2.54" y2="12.7" width="0.127" layer="21"/>
<wire x1="2.54" y1="12.7" x2="5.08" y2="10.16" width="0.127" layer="21"/>
<wire x1="5.08" y1="10.16" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="5.08" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="10.16" width="0.127" layer="21"/>
<pad name="3" x="-2.54" y="0" drill="1.016"/>
<pad name="2" x="0" y="0" drill="1.016"/>
<pad name="1" x="2.54" y="0" drill="1.016"/>
<text x="-1.016" y="-2.794" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-5.08" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="5.08" width="0.127" layer="51"/>
<wire x1="5.08" y1="10.16" x2="5.08" y2="15.24" width="0.127" layer="51"/>
<wire x1="5.08" y1="15.24" x2="-5.08" y2="15.24" width="0.127" layer="51"/>
<wire x1="-5.08" y1="15.24" x2="-5.08" y2="10.16" width="0.127" layer="51"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="5.08" width="0.127" layer="51"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-5.08" y1="10.16" x2="-2.54" y2="12.7" width="0.127" layer="51"/>
<wire x1="-2.54" y1="12.7" x2="2.54" y2="12.7" width="0.127" layer="51"/>
<wire x1="2.54" y1="12.7" x2="5.08" y2="10.16" width="0.127" layer="51"/>
<wire x1="5.08" y1="10.16" x2="5.08" y2="5.08" width="0.127" layer="51"/>
<wire x1="5.08" y1="5.08" x2="2.54" y2="2.54" width="0.127" layer="51"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.127" layer="51"/>
<wire x1="-2.54" y1="2.54" x2="-5.08" y2="5.08" width="0.127" layer="51"/>
</package>
<package name="SOT223">
<description>&lt;b&gt;Smal Outline Transistor&lt;/b&gt;</description>
<wire x1="-3.124" y1="1.731" x2="-3.124" y2="-1.729" width="0.1524" layer="21"/>
<wire x1="3.124" y1="-1.729" x2="3.124" y2="1.731" width="0.1524" layer="21"/>
<wire x1="-3.124" y1="1.731" x2="3.124" y2="1.731" width="0.1524" layer="21"/>
<wire x1="3.124" y1="-1.729" x2="-3.124" y2="-1.729" width="0.1524" layer="21"/>
<smd name="1" x="-2.2606" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="2" x="0.0254" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="3" x="2.3114" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="4" x="0" y="3.1496" dx="3.81" dy="2.0066" layer="1"/>
<text x="-2.54" y="4.318" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.794" y="-5.842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="1.778" x2="1.524" y2="3.302" layer="51"/>
<rectangle x1="-2.667" y1="-3.302" x2="-1.905" y2="-1.778" layer="51"/>
<rectangle x1="1.905" y1="-3.302" x2="2.667" y2="-1.778" layer="51"/>
<rectangle x1="-0.381" y1="-3.302" x2="0.381" y2="-1.778" layer="51"/>
</package>
<package name="HC-SR501">
<description>&lt;b&gt;PIR Motion Sensor Module DYP-ME003&lt;/b&gt; based on &lt;b&gt;BISS0001&lt;/b&gt; pyroelectic detector</description>
<wire x1="-16.256" y1="12.192" x2="16.256" y2="12.192" width="0.127" layer="21"/>
<wire x1="16.256" y1="12.192" x2="16.256" y2="-12.192" width="0.127" layer="21"/>
<wire x1="16.256" y1="-12.192" x2="-16.256" y2="-12.192" width="0.127" layer="21"/>
<wire x1="-16.256" y1="-12.192" x2="-16.256" y2="12.192" width="0.127" layer="21"/>
<hole x="-14.1" y="0" drill="1.8"/>
<hole x="14.1" y="0" drill="1.8"/>
<circle x="0" y="0" radius="11.5" width="0.127" layer="21"/>
<wire x1="-11.5" y1="11.5" x2="11.5" y2="11.5" width="0.127" layer="21"/>
<wire x1="11.5" y1="11.5" x2="11.5" y2="-11.5" width="0.127" layer="21"/>
<wire x1="11.5" y1="-11.5" x2="-11.5" y2="-11.5" width="0.127" layer="21"/>
<wire x1="-11.5" y1="-11.5" x2="-11.5" y2="11.5" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="-10.16" drill="1" shape="square"/>
<pad name="2" x="0" y="-10.16" drill="1"/>
<pad name="3" x="2.54" y="-10.16" drill="1"/>
<wire x1="-3.81" y1="-9.525" x2="-3.175" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-8.89" x2="-1.905" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-8.89" x2="-1.27" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-9.525" x2="-0.635" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-8.89" x2="0.635" y2="-8.89" width="0.127" layer="21"/>
<wire x1="0.635" y1="-8.89" x2="1.27" y2="-9.525" width="0.127" layer="21"/>
<wire x1="1.27" y1="-9.525" x2="1.905" y2="-8.89" width="0.127" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="3.175" y2="-8.89" width="0.127" layer="21"/>
<wire x1="3.175" y1="-8.89" x2="3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="3.81" y1="-9.525" x2="3.81" y2="-10.795" width="0.127" layer="21"/>
<wire x1="3.81" y1="-10.795" x2="3.175" y2="-11.43" width="0.127" layer="21"/>
<wire x1="3.175" y1="-11.43" x2="1.905" y2="-11.43" width="0.127" layer="21"/>
<wire x1="1.905" y1="-11.43" x2="1.27" y2="-10.795" width="0.127" layer="21"/>
<wire x1="1.27" y1="-10.795" x2="0.635" y2="-11.43" width="0.127" layer="21"/>
<wire x1="0.635" y1="-11.43" x2="-0.635" y2="-11.43" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="-1.27" y2="-10.795" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-10.795" x2="-1.905" y2="-11.43" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-11.43" x2="-3.175" y2="-11.43" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-11.43" x2="-3.81" y2="-10.795" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-10.795" x2="-3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.127" layer="21"/>
<text x="0" y="13.335" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-13.335" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LED_1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MSOP10">
<description>&lt;b&gt;10-Lead Mini Small Outline Package [MSOP]&lt;/b&gt; (RM-10)&lt;p&gt;
Source: http://www.analog.com/UploadedFiles/Data_Sheets/35641221898805SSM2167_b.pdf&lt;br&gt;
COMPLIANT TO JEDEC STANDARDS MO-187BA</description>
<wire x1="-1.4" y1="1.4" x2="1.4" y2="1.4" width="0.2032" layer="21"/>
<wire x1="1.4" y1="1.4" x2="1.4" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="1.4" y1="-1.4" x2="-1.4" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="-1.4" x2="-1.4" y2="1.4" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="-1.1" x2="-0.8" y2="-0.5" width="0.2032" layer="21" curve="-180"/>
<wire x1="-0.8" y1="-0.5" x2="-0.8" y2="-1.1" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-2.1131" dx="0.25" dy="1" layer="1"/>
<smd name="2" x="-0.5" y="-2.1131" dx="0.25" dy="1" layer="1"/>
<smd name="3" x="0" y="-2.1131" dx="0.25" dy="1" layer="1"/>
<smd name="4" x="0.5" y="-2.1131" dx="0.25" dy="1" layer="1"/>
<smd name="5" x="1" y="-2.1131" dx="0.25" dy="1" layer="1"/>
<smd name="6" x="1" y="2.1131" dx="0.25" dy="1" layer="1" rot="R180"/>
<smd name="7" x="0.5" y="2.1131" dx="0.25" dy="1" layer="1" rot="R180"/>
<smd name="8" x="0" y="2.1131" dx="0.25" dy="1" layer="1" rot="R180"/>
<smd name="9" x="-0.5" y="2.1131" dx="0.25" dy="1" layer="1" rot="R180"/>
<smd name="10" x="-1" y="2.1131" dx="0.25" dy="1" layer="1" rot="R180"/>
<text x="-2.032" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.302" y="-2.54" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.1244" y1="-2.5" x2="-0.8744" y2="-1.5" layer="51"/>
<rectangle x1="-0.6244" y1="-2.5" x2="-0.3744" y2="-1.5" layer="51"/>
<rectangle x1="-0.1244" y1="-2.5" x2="0.1256" y2="-1.5" layer="51"/>
<rectangle x1="0.3756" y1="-2.5" x2="0.6256" y2="-1.5" layer="51"/>
<rectangle x1="0.8756" y1="-2.5" x2="1.1256" y2="-1.5" layer="51"/>
<rectangle x1="0.8744" y1="1.5" x2="1.1244" y2="2.5" layer="51" rot="R180"/>
<rectangle x1="0.3744" y1="1.5" x2="0.6244" y2="2.5" layer="51" rot="R180"/>
<rectangle x1="-0.1256" y1="1.5" x2="0.1244" y2="2.5" layer="51" rot="R180"/>
<rectangle x1="-0.6256" y1="1.5" x2="-0.3756" y2="2.5" layer="51" rot="R180"/>
<rectangle x1="-1.1256" y1="1.5" x2="-0.8756" y2="2.5" layer="51" rot="R180"/>
</package>
<package name="RTRIM3364W">
<description>&lt;b&gt;Trimm resistor&lt;/b&gt; BOURNS&lt;p&gt;
SMD Cermet trimmer</description>
<wire x1="-2.3" y1="-1.85" x2="-2.3" y2="1.85" width="0.254" layer="51"/>
<wire x1="-2.3" y1="1.85" x2="2.3" y2="1.85" width="0.254" layer="51"/>
<wire x1="2.3" y1="1.85" x2="2.3" y2="-1.85" width="0.254" layer="51"/>
<wire x1="2.3" y1="-1.85" x2="-2.3" y2="-1.85" width="0.254" layer="51"/>
<wire x1="-2.3" y1="-1.85" x2="-2.3" y2="1.85" width="0.254" layer="21"/>
<wire x1="-2.3" y1="1.85" x2="-1.3" y2="1.85" width="0.254" layer="21"/>
<wire x1="2.3" y1="-1.85" x2="2.3" y2="1.85" width="0.254" layer="21"/>
<wire x1="2.3" y1="1.85" x2="1.3" y2="1.85" width="0.254" layer="21"/>
<wire x1="-0.4" y1="-1.85" x2="0.4" y2="-1.85" width="0.254" layer="21"/>
<circle x="1.2" y="0.65" radius="0.7" width="0.1016" layer="51"/>
<smd name="A" x="-1.275" y="-1.45" dx="1.3" dy="1.6" layer="1"/>
<smd name="E" x="1.275" y="-1.45" dx="1.3" dy="1.6" layer="1"/>
<smd name="S" x="0" y="2.085" dx="2" dy="1.6" layer="1"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.55" y1="0.55" x2="1.85" y2="0.75" layer="51"/>
<rectangle x1="-1.8" y1="-2.1" x2="-0.75" y2="-1.95" layer="51"/>
<rectangle x1="0.75" y1="-2.1" x2="1.8" y2="-1.95" layer="51"/>
<rectangle x1="-0.75" y1="1.95" x2="0.75" y2="2.1" layer="51"/>
</package>
<package name="RM51-3021-85-1012">
<pad name="A1" x="-6.1" y="6" drill="1.3" diameter="2.1"/>
<pad name="NO" x="6.1" y="6" drill="1.3" diameter="2.1"/>
<pad name="A2" x="-6.1" y="-6" drill="1.3" diameter="2.1"/>
<pad name="NC" x="6.1" y="-6" drill="1.3" diameter="2.1"/>
<pad name="COM" x="-8.1" y="0" drill="1.3" diameter="2.1"/>
<text x="-9.525" y="8.255" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-9.525" y="-9.525" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<wire x1="-9.5" y1="-7.75" x2="9.5" y2="-7.75" width="0.127" layer="21"/>
<wire x1="9.5" y1="-7.75" x2="9.5" y2="7.75" width="0.127" layer="21"/>
<wire x1="9.5" y1="7.75" x2="-9.5" y2="7.75" width="0.127" layer="21"/>
<wire x1="-9.5" y1="7.75" x2="-9.5" y2="-7.75" width="0.127" layer="21"/>
<rectangle x1="-9.5" y1="-7.75" x2="9.5" y2="7.75" layer="39"/>
<wire x1="-4" y1="6" x2="-2" y2="6" width="0.127" layer="21"/>
<wire x1="-2" y1="6" x2="-2" y2="3.5" width="0.127" layer="21"/>
<wire x1="-2" y1="3.5" x2="-2" y2="1" width="0.127" layer="21"/>
<wire x1="-2" y1="1" x2="-4" y2="1" width="0.127" layer="21"/>
<wire x1="-4" y1="1" x2="-4" y2="-1" width="0.127" layer="21"/>
<wire x1="-4" y1="-1" x2="-3" y2="-1" width="0.127" layer="21"/>
<wire x1="-3" y1="-1" x2="-2" y2="-1" width="0.127" layer="21"/>
<wire x1="-2" y1="-1" x2="0" y2="-1" width="0.127" layer="21"/>
<wire x1="0" y1="-1" x2="0" y2="1" width="0.127" layer="21"/>
<wire x1="0" y1="1" x2="-1" y2="1" width="0.127" layer="21"/>
<wire x1="-1" y1="1" x2="-2" y2="1" width="0.127" layer="21"/>
<wire x1="-2" y1="-1" x2="-2" y2="-6" width="0.127" layer="21"/>
<wire x1="-2" y1="-6" x2="-4" y2="-6" width="0.127" layer="21"/>
<wire x1="-3" y1="-1" x2="-1" y2="1" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0" x2="-5" y2="0" width="0.127" layer="21"/>
<wire x1="-5" y1="0" x2="-5" y2="0.5" width="0.127" layer="21"/>
<wire x1="-5" y1="0.5" x2="-2" y2="3.5" width="0.127" layer="21" curve="-90"/>
<wire x1="-2" y1="3.5" x2="1" y2="0" width="0.127" layer="21" curve="-98.797386"/>
<wire x1="1" y1="0" x2="4.5" y2="0" width="0.127" layer="21"/>
<wire x1="4.5" y1="0" x2="8.5" y2="-4" width="0.127" layer="21"/>
<wire x1="7.5" y1="4.5" x2="7.5" y2="-0.5" width="0.127" layer="21"/>
</package>
<package name="SOT-23">
<description>&lt;b&gt;SOT23&lt;/b&gt;</description>
<wire x1="-0.1905" y1="-0.635" x2="0.1905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.4605" y1="-0.254" x2="1.4605" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.4605" y1="0.635" x2="0.6985" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="0.635" x2="-1.4605" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.4605" y1="0.635" x2="-1.4605" y2="-0.254" width="0.127" layer="21"/>
<smd name="3" x="0" y="1.016" dx="1.016" dy="1.143" layer="1"/>
<smd name="2" x="0.889" y="-1.016" dx="1.016" dy="1.143" layer="1"/>
<smd name="1" x="-0.889" y="-1.016" dx="1.016" dy="1.143" layer="1" rot="R180"/>
<text x="-1.905" y="1.905" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<rectangle x1="-1.524" y1="-1.651" x2="1.524" y2="1.651" layer="39"/>
</package>
<package name="RPI_ZERO">
<description>Raspberry Pi board model B+, full outline with position of big connectors &amp;amp; drill holes</description>
<circle x="3.5" y="3.5" radius="3.1" width="0.127" layer="51"/>
<circle x="61.5" y="3.5" radius="3.1" width="0.127" layer="51"/>
<circle x="61.5" y="26.5" radius="3.1" width="0.127" layer="51"/>
<circle x="3.5" y="26.5" radius="3.1" width="0.127" layer="51"/>
<wire x1="0" y1="3" x2="3" y2="0" width="0.254" layer="51" curve="90"/>
<wire x1="3" y1="0" x2="6.85" y2="0" width="0.254" layer="51"/>
<wire x1="6.85" y1="0" x2="14.35" y2="0" width="0.254" layer="51"/>
<wire x1="14.35" y1="0" x2="62" y2="0" width="0.254" layer="51"/>
<wire x1="62" y1="0" x2="65" y2="3" width="0.254" layer="51" curve="90"/>
<wire x1="65" y1="3" x2="65" y2="27" width="0.254" layer="51"/>
<wire x1="65" y1="27" x2="62" y2="30" width="0.254" layer="51" curve="90"/>
<wire x1="62" y1="30" x2="3" y2="30" width="0.254" layer="51"/>
<wire x1="3" y1="30" x2="0" y2="27" width="0.254" layer="51" curve="90"/>
<wire x1="0" y1="27" x2="0" y2="3" width="0.254" layer="51"/>
<wire x1="7.1" y1="29.04" x2="7.1" y2="26.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="26.5" x2="7.1" y2="23.96" width="0.127" layer="21"/>
<wire x1="7.1" y1="23.96" x2="8.0525" y2="23.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="23.96" x2="8.6875" y2="23.96" width="0.127" layer="21"/>
<wire x1="8.6875" y1="23.96" x2="9.64" y2="23.96" width="0.127" layer="21"/>
<wire x1="9.64" y1="23.96" x2="57.9" y2="23.96" width="0.127" layer="21"/>
<wire x1="57.9" y1="23.96" x2="57.9" y2="29.04" width="0.127" layer="21"/>
<wire x1="57.9" y1="29.04" x2="7.1" y2="29.04" width="0.127" layer="21"/>
<wire x1="7.1" y1="26.5" x2="9.64" y2="26.5" width="0.127" layer="21"/>
<wire x1="9.64" y1="26.5" x2="9.64" y2="23.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="23.96" x2="8.0525" y2="23.6425" width="0.127" layer="21"/>
<wire x1="8.0525" y1="23.6425" x2="8.6875" y2="23.6425" width="0.127" layer="21"/>
<wire x1="8.6875" y1="23.6425" x2="8.6875" y2="23.96" width="0.127" layer="21"/>
<wire x1="6.85" y1="0" x2="6.85" y2="5.75" width="0.127" layer="51"/>
<wire x1="6.85" y1="5.75" x2="14.35" y2="5.75" width="0.127" layer="51"/>
<wire x1="14.35" y1="5.75" x2="14.35" y2="0" width="0.127" layer="51"/>
<wire x1="6.85" y1="0" x2="6.6" y2="-0.5" width="0.127" layer="51"/>
<wire x1="6.6" y1="-0.5" x2="14.6" y2="-0.5" width="0.127" layer="51"/>
<wire x1="14.6" y1="-0.5" x2="14.35" y2="0" width="0.127" layer="51"/>
<pad name="1" x="8.37" y="25.23" drill="1" diameter="1.778" shape="square"/>
<pad name="2" x="8.37" y="27.77" drill="1" diameter="1.778"/>
<pad name="3" x="10.91" y="25.23" drill="1" diameter="1.778"/>
<pad name="4" x="10.91" y="27.77" drill="1" diameter="1.778"/>
<pad name="5" x="13.45" y="25.23" drill="1" diameter="1.778"/>
<pad name="6" x="13.45" y="27.77" drill="1" diameter="1.778"/>
<pad name="7" x="15.99" y="25.23" drill="1" diameter="1.778"/>
<pad name="8" x="15.99" y="27.77" drill="1" diameter="1.778"/>
<pad name="9" x="18.53" y="25.23" drill="1" diameter="1.778"/>
<pad name="10" x="18.53" y="27.77" drill="1" diameter="1.778"/>
<pad name="11" x="21.07" y="25.23" drill="1" diameter="1.778"/>
<pad name="12" x="21.07" y="27.77" drill="1" diameter="1.778"/>
<pad name="13" x="23.61" y="25.23" drill="1" diameter="1.778"/>
<pad name="14" x="23.61" y="27.77" drill="1" diameter="1.778"/>
<pad name="15" x="26.15" y="25.23" drill="1" diameter="1.778"/>
<pad name="16" x="26.15" y="27.77" drill="1" diameter="1.778"/>
<pad name="17" x="28.69" y="25.23" drill="1" diameter="1.778"/>
<pad name="18" x="28.69" y="27.77" drill="1" diameter="1.778"/>
<pad name="19" x="31.23" y="25.23" drill="1" diameter="1.778"/>
<pad name="20" x="31.23" y="27.77" drill="1" diameter="1.778"/>
<pad name="21" x="33.77" y="25.23" drill="1" diameter="1.778"/>
<pad name="22" x="33.77" y="27.77" drill="1" diameter="1.778"/>
<pad name="23" x="36.31" y="25.23" drill="1" diameter="1.778"/>
<pad name="24" x="36.31" y="27.77" drill="1" diameter="1.778"/>
<pad name="25" x="38.85" y="25.23" drill="1" diameter="1.778"/>
<pad name="26" x="38.85" y="27.77" drill="1" diameter="1.778"/>
<pad name="27" x="41.39" y="25.23" drill="1" diameter="1.778"/>
<pad name="28" x="41.39" y="27.77" drill="1" diameter="1.778"/>
<pad name="29" x="43.93" y="25.23" drill="1" diameter="1.778"/>
<pad name="30" x="43.93" y="27.77" drill="1" diameter="1.778"/>
<pad name="31" x="46.47" y="25.23" drill="1" diameter="1.778"/>
<pad name="32" x="46.47" y="27.77" drill="1" diameter="1.778"/>
<pad name="33" x="49.01" y="25.23" drill="1" diameter="1.778"/>
<pad name="34" x="49.01" y="27.77" drill="1" diameter="1.778"/>
<pad name="35" x="51.55" y="25.23" drill="1" diameter="1.778"/>
<pad name="36" x="51.55" y="27.77" drill="1" diameter="1.778"/>
<pad name="37" x="54.09" y="25.23" drill="1" diameter="1.778"/>
<pad name="38" x="54.09" y="27.77" drill="1" diameter="1.778"/>
<pad name="39" x="56.63" y="25.23" drill="1" diameter="1.778"/>
<pad name="40" x="56.63" y="27.77" drill="1" diameter="1.778"/>
<text x="8.0525" y="22.3725" size="1.016" layer="21" font="vector" ratio="10">1</text>
<hole x="3.5" y="3.5" drill="2.75"/>
<hole x="3.5" y="26.5" drill="2.75"/>
<hole x="61.5" y="3.5" drill="2.75"/>
<hole x="61.5" y="26.5" drill="2.75"/>
<wire x1="36.9" y1="0" x2="37.75" y2="0" width="0.254" layer="51"/>
<wire x1="37.75" y1="0" x2="45.25" y2="0" width="0.254" layer="51"/>
<wire x1="37.75" y1="0" x2="37.75" y2="5.75" width="0.127" layer="51"/>
<wire x1="37.75" y1="5.75" x2="45.25" y2="5.75" width="0.127" layer="51"/>
<wire x1="45.25" y1="5.75" x2="45.25" y2="0" width="0.127" layer="51"/>
<wire x1="37.75" y1="0" x2="37.5" y2="-0.5" width="0.127" layer="51"/>
<wire x1="37.5" y1="-0.5" x2="45.5" y2="-0.5" width="0.127" layer="51"/>
<wire x1="45.5" y1="-0.5" x2="45.25" y2="0" width="0.127" layer="51"/>
<wire x1="49.5" y1="0" x2="50.35" y2="0" width="0.254" layer="51"/>
<wire x1="50.35" y1="0" x2="57.85" y2="0" width="0.254" layer="51"/>
<wire x1="50.35" y1="0" x2="50.35" y2="5.75" width="0.127" layer="51"/>
<wire x1="50.35" y1="5.75" x2="57.85" y2="5.75" width="0.127" layer="51"/>
<wire x1="57.85" y1="5.75" x2="57.85" y2="0" width="0.127" layer="51"/>
<wire x1="50.35" y1="0" x2="50.1" y2="-0.5" width="0.127" layer="51"/>
<wire x1="50.1" y1="-0.5" x2="58.1" y2="-0.5" width="0.127" layer="51"/>
<wire x1="58.1" y1="-0.5" x2="57.85" y2="0" width="0.127" layer="51"/>
<circle x="3.5" y="3.5" radius="3.1" width="0.127" layer="21"/>
<circle x="61.5" y="3.5" radius="3.1" width="0.127" layer="21"/>
<circle x="61.5" y="26.5" radius="3.1" width="0.127" layer="21"/>
<circle x="3.5" y="26.5" radius="3.1" width="0.127" layer="21"/>
<wire x1="0" y1="3" x2="3" y2="0" width="0.254" layer="21" curve="90"/>
<wire x1="3" y1="0" x2="6.85" y2="0" width="0.254" layer="21"/>
<wire x1="6.85" y1="0" x2="14.35" y2="0" width="0.254" layer="21"/>
<wire x1="14.35" y1="0" x2="62" y2="0" width="0.254" layer="21"/>
<wire x1="62" y1="0" x2="65" y2="3" width="0.254" layer="21" curve="90"/>
<wire x1="65" y1="3" x2="65" y2="27" width="0.254" layer="21"/>
<wire x1="65" y1="27" x2="62" y2="30" width="0.254" layer="21" curve="90"/>
<wire x1="62" y1="30" x2="3" y2="30" width="0.254" layer="21"/>
<wire x1="3" y1="30" x2="0" y2="27" width="0.254" layer="21" curve="90"/>
<wire x1="0" y1="27" x2="0" y2="3" width="0.254" layer="21"/>
<wire x1="37.75" y1="0" x2="45.25" y2="0" width="0.254" layer="21"/>
<wire x1="49.5" y1="0" x2="50.35" y2="0" width="0.254" layer="21"/>
</package>
<package name="SWITCH_DTSM6">
<wire x1="-3.3" y1="3" x2="3.3" y2="3" width="0.127" layer="21"/>
<wire x1="3.3" y1="3" x2="3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="3.3" y1="-3" x2="-3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-3" x2="-3.3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.5033" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1" width="0.127" layer="21"/>
<smd name="T1" x="-3.4" y="2.3" dx="3.2" dy="1.2" layer="1"/>
<smd name="T2" x="3.4" y="2.3" dx="3.2" dy="1.2" layer="1"/>
<smd name="T4" x="3.4" y="-2.3" dx="3.2" dy="1.2" layer="1"/>
<smd name="T3" x="-3.4" y="-2.3" dx="3.2" dy="1.2" layer="1"/>
<text x="-3" y="3.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-4.8" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="PCA9685">
<pin name="A1" x="-15.24" y="0" length="middle" direction="in"/>
<pin name="A2" x="-15.24" y="-2.54" length="middle" direction="in"/>
<pin name="A3" x="-15.24" y="-5.08" length="middle" direction="in"/>
<pin name="A4" x="-15.24" y="-7.62" length="middle" direction="in"/>
<pin name="A0" x="-15.24" y="2.54" length="middle" direction="in"/>
<pin name="LED0" x="15.24" y="17.78" length="middle" direction="out" rot="R180"/>
<pin name="LED1" x="15.24" y="15.24" length="middle" direction="out" rot="R180"/>
<pin name="LED2" x="15.24" y="12.7" length="middle" direction="out" rot="R180"/>
<pin name="LED3" x="15.24" y="10.16" length="middle" direction="out" rot="R180"/>
<pin name="LED4" x="15.24" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="LED5" x="15.24" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="LED6" x="15.24" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="LED7" x="15.24" y="0" length="middle" direction="out" rot="R180"/>
<pin name="GND" x="-15.24" y="-20.32" length="middle" direction="sup"/>
<pin name="LED8" x="15.24" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="LED9" x="15.24" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="LED10" x="15.24" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="LED11" x="15.24" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="LED12" x="15.24" y="-12.7" length="middle" direction="out" rot="R180"/>
<pin name="LED13" x="15.24" y="-15.24" length="middle" direction="out" rot="R180"/>
<pin name="LED14" x="15.24" y="-17.78" length="middle" direction="out" rot="R180"/>
<pin name="LED15" x="15.24" y="-20.32" length="middle" direction="out" rot="R180"/>
<pin name="!OE!" x="-15.24" y="-17.78" length="middle" direction="in"/>
<pin name="A5" x="-15.24" y="-10.16" length="middle" direction="in"/>
<pin name="EXTCLK" x="-15.24" y="-15.24" length="middle" direction="in"/>
<pin name="SCL" x="-15.24" y="7.62" length="middle"/>
<pin name="SDA" x="-15.24" y="10.16" length="middle"/>
<pin name="VCC" x="-15.24" y="17.78" length="middle" direction="sup"/>
<wire x1="-10.16" y1="20.32" x2="10.16" y2="20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="20.32" x2="10.16" y2="-22.86" width="0.254" layer="94"/>
<wire x1="10.16" y1="-22.86" x2="-10.16" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-22.86" x2="-10.16" y2="20.32" width="0.254" layer="94"/>
<text x="-10.16" y="-25.4" size="1.5" layer="96">&gt;MPN</text>
<text x="-10.16" y="22.86" size="2" layer="95">&gt;NAME</text>
</symbol>
<symbol name="PCF8574">
<pin name="A1" x="-15.24" y="2.54" length="middle" direction="in"/>
<pin name="A2" x="-15.24" y="0" length="middle" direction="in"/>
<pin name="A0" x="-15.24" y="5.08" length="middle" direction="in"/>
<pin name="P0" x="15.24" y="17.78" length="middle" rot="R180"/>
<pin name="P1" x="15.24" y="15.24" length="middle" rot="R180"/>
<pin name="P2" x="15.24" y="12.7" length="middle" rot="R180"/>
<pin name="P3" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="GND" x="-15.24" y="-5.08" length="middle" direction="sup"/>
<pin name="SCL" x="-15.24" y="10.16" length="middle"/>
<pin name="SDA" x="-15.24" y="12.7" length="middle"/>
<pin name="VCC" x="-15.24" y="17.78" length="middle" direction="sup"/>
<wire x1="-10.16" y1="20.32" x2="10.16" y2="20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="20.32" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="20.32" width="0.254" layer="94"/>
<text x="-10.16" y="-10.16" size="1.5" layer="96">&gt;MPN</text>
<text x="-10.16" y="20.32" size="2" layer="95">&gt;NAME</text>
<pin name="P4" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="P5" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="P6" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="P7" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="!INT!" x="15.24" y="-5.08" length="middle" direction="out" rot="R180"/>
</symbol>
<symbol name="BH1750FVI">
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<circle x="0" y="0" radius="5.00321875" width="0.254" layer="94"/>
<wire x1="-6.096" y1="-3.302" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="0" y1="0" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="0" y1="0" x2="-0.508" y2="0" width="0.6096" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.762" y2="0" width="0.6096" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.254" y2="-0.508" width="0.6096" layer="94"/>
<wire x1="-0.254" y1="-0.508" x2="0" y2="-0.762" width="0.6096" layer="94"/>
<wire x1="0" y1="-0.762" x2="0" y2="-0.508" width="0.6096" layer="94"/>
<wire x1="0" y1="-0.508" x2="0" y2="-0.254" width="0.254" layer="94"/>
<wire x1="0" y1="-0.254" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-0.254" x2="-0.254" y2="-0.254" width="0.6096" layer="94"/>
<wire x1="-0.254" y1="-0.254" x2="-0.508" y2="0" width="0.6096" layer="94"/>
<wire x1="-0.254" y1="-0.508" x2="0" y2="-0.508" width="0.6096" layer="94"/>
<pin name="VCC" x="-15.24" y="7.62" length="middle" direction="pwr"/>
<pin name="ADDR" x="-15.24" y="5.08" length="middle" direction="in"/>
<pin name="GND" x="-15.24" y="-7.62" length="middle" direction="pwr"/>
<pin name="SDA" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="DVI" x="15.24" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="SCL" x="15.24" y="5.08" length="middle" rot="R180"/>
<text x="-10.16" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-12.7" size="1.778" layer="96">&gt;MPN</text>
</symbol>
<symbol name="BME280">
<wire x1="-17.78" y1="10.16" x2="-17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-10.16" x2="20.32" y2="-10.16" width="0.254" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="20.32" y2="10.16" width="0.254" layer="94"/>
<wire x1="20.32" y1="10.16" x2="-17.78" y2="10.16" width="0.254" layer="94"/>
<pin name="VDD" x="-22.86" y="7.62" length="middle" direction="pwr"/>
<pin name="CSB" x="25.4" y="0" length="middle" direction="in" rot="R180"/>
<pin name="GND" x="-22.86" y="-5.08" length="middle" direction="pwr"/>
<pin name="SDA" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="SDO" x="25.4" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="SCL" x="25.4" y="5.08" length="middle" rot="R180"/>
<text x="-17.68" y="11.06" size="1.778" layer="95">&gt;NAME</text>
<text x="-17.78" y="-12.7" size="1.778" layer="96">&gt;MPN</text>
<wire x1="-4.956" y1="9.266" x2="-7.496" y2="5.202" width="0.254" layer="94"/>
<wire x1="-7.496" y1="5.202" x2="-4.956" y2="0.884" width="0.254" layer="94" curve="127.893014"/>
<wire x1="-4.956" y1="9.266" x2="-2.67" y2="4.948" width="0.254" layer="94"/>
<wire x1="-6.988" y1="4.186" x2="-6.988" y2="3.678" width="0.254" layer="94"/>
<wire x1="-6.988" y1="3.678" x2="-4.956" y2="1.646" width="0.254" layer="94" curve="90"/>
<text x="-3.94" y="0.63" size="1.778" layer="94">RH</text>
<wire x1="3.596" y1="1.646" x2="7.406" y2="1.646" width="0.254" layer="94"/>
<wire x1="10.962" y1="4.186" x2="9.184" y2="4.948" width="0.254" layer="94" curve="59.862918"/>
<wire x1="3.596" y1="1.646" x2="2.326" y2="5.456" width="0.254" layer="94" curve="-143.130119"/>
<wire x1="2.326" y1="5.456" x2="4.866" y2="5.456" width="0.254" layer="94" curve="-73.739761"/>
<wire x1="4.866" y1="5.456" x2="6.39" y2="6.98" width="0.254" layer="94" curve="-90"/>
<wire x1="6.39" y1="6.98" x2="7.152" y2="6.98" width="0.254" layer="94"/>
<wire x1="9.184" y1="4.948" x2="7.152" y2="6.98" width="0.254" layer="94" curve="90"/>
<wire x1="2.326" y1="5.456" x2="6.136" y2="6.98" width="0.254" layer="94" curve="-180"/>
<wire x1="1.818" y1="6.98" x2="0.802" y2="6.98" width="0.254" layer="94"/>
<wire x1="2.58" y1="8.25" x2="1.818" y2="8.758" width="0.254" layer="94"/>
<wire x1="4.104" y1="8.758" x2="4.104" y2="9.52" width="0.254" layer="94"/>
<wire x1="5.374" y1="8.504" x2="6.136" y2="9.266" width="0.254" layer="94"/>
<circle x="9.438" y="1.646" radius="1.93440625" width="0.254" layer="94"/>
<wire x1="10.962" y1="4.186" x2="10.962" y2="2.916" width="0.254" layer="94" curve="-73.739863"/>
<text x="8.93" y="0.884" size="1.778" layer="94">P</text>
<circle x="-2.416" y="1.646" radius="1.93440625" width="0.254" layer="94"/>
<wire x1="-4.956" y1="0.884" x2="-4.448" y2="1.138" width="0.254" layer="94" curve="44.306266"/>
<wire x1="-2.67" y1="4.948" x2="-2.416" y2="3.678" width="0.254" layer="94" curve="-33.174699"/>
<wire x1="1.126" y1="-1.424" x2="1.126" y2="-5.996" width="0.254" layer="94"/>
<wire x1="1.126" y1="-1.424" x2="2.396" y2="-1.424" width="0.254" layer="94" curve="-180"/>
<wire x1="2.396" y1="-1.424" x2="2.396" y2="-5.996" width="0.254" layer="94"/>
<circle x="1.734" y="-7.012" radius="0.254" width="0.254" layer="94"/>
<circle x="1.7" y="-7" radius="0.1" width="0.254" layer="94"/>
<wire x1="-0.4" y1="-3.3" x2="4" y2="-3.3" width="0.254" layer="94"/>
<text x="-0.7" y="-3.2" size="1.778" layer="94">+</text>
<text x="-0.7" y="-5.1" size="1.778" layer="94">-</text>
<wire x1="2.4" y1="-2.2" x2="2" y2="-2.2" width="0.254" layer="94"/>
<wire x1="2.3" y1="-4.6" x2="1.9" y2="-4.6" width="0.254" layer="94"/>
<circle x="4.578" y="-7.414" radius="1.93440625" width="0.254" layer="94"/>
<wire x1="2.6" y1="-7.8" x2="1.1" y2="-6" width="0.254" layer="94" curve="-186.879944"/>
<wire x1="2.4" y1="-6" x2="2.9" y2="-6.5" width="0.254" layer="94" curve="-90"/>
<text x="4.07" y="-8.316" size="1.778" layer="94">T</text>
<pin name="VDDIO" x="-22.86" y="5.08" length="middle" direction="pwr"/>
<pin name="GND@1" x="-22.86" y="-7.62" length="middle" direction="pwr"/>
</symbol>
<symbol name="INA219">
<pin name="VIN+" x="-2.54" y="15.24" length="middle" direction="in" rot="R270"/>
<pin name="VIN-" x="5.08" y="15.24" length="middle" direction="in" rot="R270"/>
<pin name="VS" x="-17.78" y="7.62" length="middle" direction="pwr"/>
<pin name="SDA" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="GND" x="-17.78" y="0" length="middle" direction="pwr"/>
<wire x1="-12.7" y1="10.16" x2="15.24" y2="10.16" width="0.254" layer="94"/>
<wire x1="15.24" y1="10.16" x2="15.24" y2="-5.08" width="0.254" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="-12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<text x="-12.446" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-7.62" size="1.778" layer="96">&gt;MPN</text>
<pin name="A0" x="-17.78" y="5.08" length="middle" direction="in"/>
<pin name="A1" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="SCL" x="20.32" y="-2.54" length="middle" rot="R180"/>
</symbol>
<symbol name="UBLOX_NEO-6/7">
<wire x1="-22.86" y1="27.94" x2="-22.86" y2="-33.02" width="0.254" layer="95"/>
<wire x1="-22.86" y1="-33.02" x2="22.86" y2="-33.02" width="0.254" layer="95"/>
<wire x1="22.86" y1="-33.02" x2="22.86" y2="27.94" width="0.254" layer="95"/>
<wire x1="22.86" y1="27.94" x2="-22.86" y2="27.94" width="0.254" layer="95"/>
<text x="-5.08" y="2.54" size="1.27" layer="95">UBLOX NEO-6/7</text>
<pin name="13_GND" x="-27.94" y="-27.94" visible="pin" length="middle"/>
<pin name="14_MOSI/CFG_COM0" x="27.94" y="12.7" visible="pin" length="middle" rot="R180"/>
<pin name="15_MISO/CFG_COM1" x="27.94" y="10.16" visible="pin" length="middle" rot="R180"/>
<pin name="16_CFG_CPS0/SCK" x="27.94" y="15.24" visible="pin" length="middle" rot="R180"/>
<pin name="17_RESERVED" x="27.94" y="25.4" visible="pin" length="middle" rot="R180"/>
<pin name="18_SDA2" x="-27.94" y="2.54" visible="pin" length="middle"/>
<pin name="19_SCL2" x="-27.94" y="0" visible="pin" length="middle"/>
<pin name="20_TXD1" x="27.94" y="-25.4" visible="pin" length="middle" rot="R180"/>
<pin name="21_RXD1" x="27.94" y="-27.94" visible="pin" length="middle" rot="R180"/>
<pin name="22_V_BCKP" x="27.94" y="-12.7" visible="pin" length="middle" rot="R180"/>
<pin name="23_VCC" x="-27.94" y="25.4" visible="pin" length="middle"/>
<pin name="24_GND" x="-27.94" y="-30.48" visible="pin" length="middle"/>
<pin name="12_GND" x="-27.94" y="-25.4" visible="pin" length="middle"/>
<pin name="11_RF_IN" x="-27.94" y="-15.24" visible="pin" length="middle"/>
<pin name="10_GND" x="-27.94" y="-22.86" visible="pin" length="middle"/>
<pin name="9_VCC_RF" x="-27.94" y="-12.7" visible="pin" length="middle"/>
<pin name="8_RESERVED" x="-27.94" y="-10.16" visible="pin" length="middle"/>
<pin name="7_VDDUSB" x="27.94" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="6_USB_DP" x="27.94" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="5_USB_DM" x="27.94" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="4_EXTINT0" x="27.94" y="-15.24" visible="pin" length="middle" rot="R180"/>
<pin name="3_TIMEPULSE" x="-27.94" y="15.24" visible="pin" length="middle"/>
<pin name="2_SS_N" x="27.94" y="17.78" visible="pin" length="middle" rot="R180"/>
<pin name="1_RESERVED" x="27.94" y="22.86" visible="pin" length="middle" rot="R180"/>
<rectangle x1="-4.5847" y1="-9.0297" x2="4.6101" y2="-9.0043" layer="94"/>
<rectangle x1="-4.6609" y1="-9.0043" x2="4.6863" y2="-8.9789" layer="94"/>
<rectangle x1="-4.7371" y1="-8.9789" x2="4.7371" y2="-8.9535" layer="94"/>
<rectangle x1="-4.7879" y1="-8.9535" x2="4.7879" y2="-8.9281" layer="94"/>
<rectangle x1="-4.8133" y1="-8.9281" x2="4.8387" y2="-8.9027" layer="94"/>
<rectangle x1="-4.8641" y1="-8.9027" x2="4.8641" y2="-8.8773" layer="94"/>
<rectangle x1="-4.8895" y1="-8.8773" x2="4.9149" y2="-8.8519" layer="94"/>
<rectangle x1="-4.9403" y1="-8.8519" x2="4.9403" y2="-8.8265" layer="94"/>
<rectangle x1="-4.9657" y1="-8.8265" x2="4.9657" y2="-8.8011" layer="94"/>
<rectangle x1="-4.9911" y1="-8.8011" x2="4.9911" y2="-8.7757" layer="94"/>
<rectangle x1="-5.0165" y1="-8.7757" x2="5.0165" y2="-8.7503" layer="94"/>
<rectangle x1="-5.0419" y1="-8.7503" x2="5.0419" y2="-8.7249" layer="94"/>
<rectangle x1="-5.0673" y1="-8.7249" x2="5.0673" y2="-8.6995" layer="94"/>
<rectangle x1="-5.0673" y1="-8.6995" x2="5.0927" y2="-8.6741" layer="94"/>
<rectangle x1="-5.0927" y1="-8.6741" x2="5.1181" y2="-8.6487" layer="94"/>
<rectangle x1="-5.1181" y1="-8.6487" x2="5.1181" y2="-8.6233" layer="94"/>
<rectangle x1="-5.1181" y1="-8.6233" x2="5.1435" y2="-8.5979" layer="94"/>
<rectangle x1="-5.1435" y1="-8.5979" x2="5.1435" y2="-8.5725" layer="94"/>
<rectangle x1="-5.1435" y1="-8.5725" x2="5.1689" y2="-8.5471" layer="94"/>
<rectangle x1="-5.1689" y1="-8.5471" x2="5.1689" y2="-8.5217" layer="94"/>
<rectangle x1="-5.1689" y1="-8.5217" x2="-2.8575" y2="-8.4963" layer="94"/>
<rectangle x1="-1.7907" y1="-8.5217" x2="1.8161" y2="-8.4963" layer="94"/>
<rectangle x1="2.8575" y1="-8.5217" x2="5.1943" y2="-8.4963" layer="94"/>
<rectangle x1="-5.1943" y1="-8.4963" x2="-3.0353" y2="-8.4709" layer="94"/>
<rectangle x1="-1.5621" y1="-8.4963" x2="-0.9525" y2="-8.4709" layer="94"/>
<rectangle x1="-0.4191" y1="-8.4963" x2="1.6129" y2="-8.4709" layer="94"/>
<rectangle x1="3.1115" y1="-8.4963" x2="5.1943" y2="-8.4709" layer="94"/>
<rectangle x1="-5.1943" y1="-8.4709" x2="-3.1369" y2="-8.4455" layer="94"/>
<rectangle x1="-1.4605" y1="-8.4709" x2="-0.9525" y2="-8.4455" layer="94"/>
<rectangle x1="-0.4191" y1="-8.4709" x2="1.5367" y2="-8.4455" layer="94"/>
<rectangle x1="3.2131" y1="-8.4709" x2="5.2197" y2="-8.4455" layer="94"/>
<rectangle x1="-5.1943" y1="-8.4455" x2="-3.1877" y2="-8.4201" layer="94"/>
<rectangle x1="-1.4097" y1="-8.4455" x2="-0.9525" y2="-8.4201" layer="94"/>
<rectangle x1="-0.4191" y1="-8.4455" x2="1.4859" y2="-8.4201" layer="94"/>
<rectangle x1="3.2639" y1="-8.4455" x2="5.2197" y2="-8.4201" layer="94"/>
<rectangle x1="-5.2197" y1="-8.4201" x2="-3.2385" y2="-8.3947" layer="94"/>
<rectangle x1="-1.3843" y1="-8.4201" x2="-0.9525" y2="-8.3947" layer="94"/>
<rectangle x1="-0.4191" y1="-8.4201" x2="1.4605" y2="-8.3947" layer="94"/>
<rectangle x1="3.3147" y1="-8.4201" x2="5.2197" y2="-8.3947" layer="94"/>
<rectangle x1="-5.2197" y1="-8.3947" x2="-3.2639" y2="-8.3693" layer="94"/>
<rectangle x1="-1.3589" y1="-8.3947" x2="-0.9525" y2="-8.3693" layer="94"/>
<rectangle x1="-0.4191" y1="-8.3947" x2="1.4097" y2="-8.3693" layer="94"/>
<rectangle x1="3.3401" y1="-8.3947" x2="5.2451" y2="-8.3693" layer="94"/>
<rectangle x1="-5.2197" y1="-8.3693" x2="-3.2893" y2="-8.3439" layer="94"/>
<rectangle x1="-1.3335" y1="-8.3693" x2="-0.9525" y2="-8.3439" layer="94"/>
<rectangle x1="-0.4191" y1="-8.3693" x2="1.4097" y2="-8.3439" layer="94"/>
<rectangle x1="3.3655" y1="-8.3693" x2="5.2451" y2="-8.3439" layer="94"/>
<rectangle x1="-5.2451" y1="-8.3439" x2="-3.3147" y2="-8.3185" layer="94"/>
<rectangle x1="-1.3081" y1="-8.3439" x2="-0.9525" y2="-8.3185" layer="94"/>
<rectangle x1="-0.4191" y1="-8.3439" x2="1.3843" y2="-8.3185" layer="94"/>
<rectangle x1="3.3909" y1="-8.3439" x2="5.2451" y2="-8.3185" layer="94"/>
<rectangle x1="-5.2451" y1="-8.3185" x2="-3.3401" y2="-8.2931" layer="94"/>
<rectangle x1="-1.2827" y1="-8.3185" x2="-0.9525" y2="-8.2931" layer="94"/>
<rectangle x1="-0.4191" y1="-8.3185" x2="1.3589" y2="-8.2931" layer="94"/>
<rectangle x1="3.3909" y1="-8.3185" x2="5.2451" y2="-8.2931" layer="94"/>
<rectangle x1="-5.2451" y1="-8.2931" x2="-3.3655" y2="-8.2677" layer="94"/>
<rectangle x1="-1.2827" y1="-8.2931" x2="-0.9525" y2="-8.2677" layer="94"/>
<rectangle x1="-0.4191" y1="-8.2931" x2="1.3589" y2="-8.2677" layer="94"/>
<rectangle x1="3.4163" y1="-8.2931" x2="5.2451" y2="-8.2677" layer="94"/>
<rectangle x1="-5.2451" y1="-8.2677" x2="-3.3655" y2="-8.2423" layer="94"/>
<rectangle x1="-1.2827" y1="-8.2677" x2="-0.9525" y2="-8.2423" layer="94"/>
<rectangle x1="-0.4191" y1="-8.2677" x2="1.3589" y2="-8.2423" layer="94"/>
<rectangle x1="3.4163" y1="-8.2677" x2="5.2451" y2="-8.2423" layer="94"/>
<rectangle x1="-5.2451" y1="-8.2423" x2="-3.3909" y2="-8.2169" layer="94"/>
<rectangle x1="-1.2573" y1="-8.2423" x2="-0.9525" y2="-8.2169" layer="94"/>
<rectangle x1="-0.4191" y1="-8.2423" x2="1.3335" y2="-8.2169" layer="94"/>
<rectangle x1="3.4417" y1="-8.2423" x2="5.2451" y2="-8.2169" layer="94"/>
<rectangle x1="-5.2451" y1="-8.2169" x2="-3.3909" y2="-8.1915" layer="94"/>
<rectangle x1="-1.2573" y1="-8.2169" x2="-0.9525" y2="-8.1915" layer="94"/>
<rectangle x1="-0.4191" y1="-8.2169" x2="1.3335" y2="-8.1915" layer="94"/>
<rectangle x1="3.4417" y1="-8.2169" x2="5.2451" y2="-8.1915" layer="94"/>
<rectangle x1="-5.2451" y1="-8.1915" x2="-3.4163" y2="-8.1661" layer="94"/>
<rectangle x1="-1.2573" y1="-8.1915" x2="-0.9525" y2="-8.1661" layer="94"/>
<rectangle x1="-0.4191" y1="-8.1915" x2="1.3335" y2="-8.1661" layer="94"/>
<rectangle x1="3.4417" y1="-8.1915" x2="5.2451" y2="-8.1661" layer="94"/>
<rectangle x1="-5.2451" y1="-8.1661" x2="-3.4163" y2="-8.1407" layer="94"/>
<rectangle x1="-1.2573" y1="-8.1661" x2="-0.9525" y2="-8.1407" layer="94"/>
<rectangle x1="-0.4191" y1="-8.1661" x2="1.3335" y2="-8.1407" layer="94"/>
<rectangle x1="3.4417" y1="-8.1661" x2="5.2451" y2="-8.1407" layer="94"/>
<rectangle x1="-5.2451" y1="-8.1407" x2="-3.4163" y2="-8.1153" layer="94"/>
<rectangle x1="-1.2573" y1="-8.1407" x2="-0.9525" y2="-8.1153" layer="94"/>
<rectangle x1="-0.4191" y1="-8.1407" x2="1.3335" y2="-8.1153" layer="94"/>
<rectangle x1="1.9939" y1="-8.1407" x2="2.7813" y2="-8.1153" layer="94"/>
<rectangle x1="3.4417" y1="-8.1407" x2="5.2451" y2="-8.1153" layer="94"/>
<rectangle x1="-5.2451" y1="-8.1153" x2="-3.4163" y2="-8.0899" layer="94"/>
<rectangle x1="-2.6797" y1="-8.1153" x2="-2.6543" y2="-8.0899" layer="94"/>
<rectangle x1="-2.6289" y1="-8.1153" x2="-2.6035" y2="-8.0899" layer="94"/>
<rectangle x1="-2.5781" y1="-8.1153" x2="-2.5527" y2="-8.0899" layer="94"/>
<rectangle x1="-2.5273" y1="-8.1153" x2="-2.5019" y2="-8.0899" layer="94"/>
<rectangle x1="-2.4765" y1="-8.1153" x2="-2.4511" y2="-8.0899" layer="94"/>
<rectangle x1="-2.4257" y1="-8.1153" x2="-2.4003" y2="-8.0899" layer="94"/>
<rectangle x1="-2.3749" y1="-8.1153" x2="-2.3495" y2="-8.0899" layer="94"/>
<rectangle x1="-2.3241" y1="-8.1153" x2="-2.2987" y2="-8.0899" layer="94"/>
<rectangle x1="-2.2733" y1="-8.1153" x2="-2.2479" y2="-8.0899" layer="94"/>
<rectangle x1="-2.2225" y1="-8.1153" x2="-2.1971" y2="-8.0899" layer="94"/>
<rectangle x1="-2.1717" y1="-8.1153" x2="-2.1463" y2="-8.0899" layer="94"/>
<rectangle x1="-2.1209" y1="-8.1153" x2="-2.0955" y2="-8.0899" layer="94"/>
<rectangle x1="-2.0701" y1="-8.1153" x2="-2.0447" y2="-8.0899" layer="94"/>
<rectangle x1="-2.0193" y1="-8.1153" x2="-1.9939" y2="-8.0899" layer="94"/>
<rectangle x1="-1.9685" y1="-8.1153" x2="-1.9431" y2="-8.0899" layer="94"/>
<rectangle x1="-1.2319" y1="-8.1153" x2="-0.9525" y2="-8.0899" layer="94"/>
<rectangle x1="-0.4191" y1="-8.1153" x2="1.3335" y2="-8.0899" layer="94"/>
<rectangle x1="1.8923" y1="-8.1153" x2="2.8575" y2="-8.0899" layer="94"/>
<rectangle x1="3.4417" y1="-8.1153" x2="5.2451" y2="-8.0899" layer="94"/>
<rectangle x1="-5.2451" y1="-8.0899" x2="-3.4163" y2="-8.0645" layer="94"/>
<rectangle x1="-2.7813" y1="-8.0899" x2="-1.8669" y2="-8.0645" layer="94"/>
<rectangle x1="-1.2319" y1="-8.0899" x2="-0.9525" y2="-8.0645" layer="94"/>
<rectangle x1="-0.4191" y1="-8.0899" x2="1.3335" y2="-8.0645" layer="94"/>
<rectangle x1="1.8669" y1="-8.0899" x2="2.8829" y2="-8.0645" layer="94"/>
<rectangle x1="3.4417" y1="-8.0899" x2="5.2451" y2="-8.0645" layer="94"/>
<rectangle x1="-5.2451" y1="-8.0645" x2="-3.4417" y2="-8.0391" layer="94"/>
<rectangle x1="-2.8321" y1="-8.0645" x2="-1.8161" y2="-8.0391" layer="94"/>
<rectangle x1="-1.2319" y1="-8.0645" x2="-0.9525" y2="-8.0391" layer="94"/>
<rectangle x1="0.7747" y1="-8.0645" x2="1.3335" y2="-8.0391" layer="94"/>
<rectangle x1="1.8669" y1="-8.0645" x2="2.9083" y2="-8.0391" layer="94"/>
<rectangle x1="3.4417" y1="-8.0645" x2="5.2451" y2="-8.0391" layer="94"/>
<rectangle x1="-5.2451" y1="-8.0391" x2="-3.4417" y2="-8.0137" layer="94"/>
<rectangle x1="-2.8575" y1="-8.0391" x2="-1.7907" y2="-8.0137" layer="94"/>
<rectangle x1="-1.2319" y1="-8.0391" x2="-0.9525" y2="-8.0137" layer="94"/>
<rectangle x1="0.8255" y1="-8.0391" x2="1.3081" y2="-8.0137" layer="94"/>
<rectangle x1="1.8669" y1="-8.0391" x2="2.9083" y2="-8.0137" layer="94"/>
<rectangle x1="3.4417" y1="-8.0391" x2="5.2451" y2="-8.0137" layer="94"/>
<rectangle x1="-5.2451" y1="-8.0137" x2="-3.4417" y2="-7.9883" layer="94"/>
<rectangle x1="-2.8575" y1="-8.0137" x2="-1.7653" y2="-7.9883" layer="94"/>
<rectangle x1="-1.2319" y1="-8.0137" x2="-0.9525" y2="-7.9883" layer="94"/>
<rectangle x1="0.8763" y1="-8.0137" x2="1.3335" y2="-7.9883" layer="94"/>
<rectangle x1="1.8415" y1="-8.0137" x2="2.9083" y2="-7.9883" layer="94"/>
<rectangle x1="3.4671" y1="-8.0137" x2="5.2451" y2="-7.9883" layer="94"/>
<rectangle x1="-5.2451" y1="-7.9883" x2="-3.4417" y2="-7.9629" layer="94"/>
<rectangle x1="-2.8829" y1="-7.9883" x2="-1.7653" y2="-7.9629" layer="94"/>
<rectangle x1="-1.2319" y1="-7.9883" x2="-0.9525" y2="-7.9629" layer="94"/>
<rectangle x1="0.9017" y1="-7.9883" x2="1.3081" y2="-7.9629" layer="94"/>
<rectangle x1="1.8415" y1="-7.9883" x2="2.9083" y2="-7.9629" layer="94"/>
<rectangle x1="3.4417" y1="-7.9883" x2="5.2451" y2="-7.9629" layer="94"/>
<rectangle x1="-5.2451" y1="-7.9629" x2="-3.4417" y2="-7.9375" layer="94"/>
<rectangle x1="-2.8829" y1="-7.9629" x2="-1.7653" y2="-7.9375" layer="94"/>
<rectangle x1="-1.2319" y1="-7.9629" x2="-0.9525" y2="-7.9375" layer="94"/>
<rectangle x1="0.9271" y1="-7.9629" x2="1.3335" y2="-7.9375" layer="94"/>
<rectangle x1="1.8415" y1="-7.9629" x2="2.9083" y2="-7.9375" layer="94"/>
<rectangle x1="3.4417" y1="-7.9629" x2="5.2451" y2="-7.9375" layer="94"/>
<rectangle x1="-5.2451" y1="-7.9375" x2="-3.4417" y2="-7.9121" layer="94"/>
<rectangle x1="-2.8829" y1="-7.9375" x2="-1.7653" y2="-7.9121" layer="94"/>
<rectangle x1="-1.2319" y1="-7.9375" x2="-0.9525" y2="-7.9121" layer="94"/>
<rectangle x1="0.9525" y1="-7.9375" x2="2.9083" y2="-7.9121" layer="94"/>
<rectangle x1="3.4417" y1="-7.9375" x2="5.2451" y2="-7.9121" layer="94"/>
<rectangle x1="-5.2451" y1="-7.9121" x2="-3.4417" y2="-7.8867" layer="94"/>
<rectangle x1="-2.8829" y1="-7.9121" x2="-1.7653" y2="-7.8867" layer="94"/>
<rectangle x1="-1.2319" y1="-7.9121" x2="-0.9525" y2="-7.8867" layer="94"/>
<rectangle x1="0.9779" y1="-7.9121" x2="2.8829" y2="-7.8867" layer="94"/>
<rectangle x1="3.4671" y1="-7.9121" x2="5.2451" y2="-7.8867" layer="94"/>
<rectangle x1="-5.2451" y1="-7.8867" x2="-3.4417" y2="-7.8613" layer="94"/>
<rectangle x1="-2.9083" y1="-7.8867" x2="-2.4003" y2="-7.8613" layer="94"/>
<rectangle x1="-1.2319" y1="-7.8867" x2="-0.9525" y2="-7.8613" layer="94"/>
<rectangle x1="1.0033" y1="-7.8867" x2="2.8321" y2="-7.8613" layer="94"/>
<rectangle x1="3.4417" y1="-7.8867" x2="5.2451" y2="-7.8613" layer="94"/>
<rectangle x1="-5.2451" y1="-7.8613" x2="-3.4417" y2="-7.8359" layer="94"/>
<rectangle x1="-2.9083" y1="-7.8613" x2="-2.4003" y2="-7.8359" layer="94"/>
<rectangle x1="-1.2319" y1="-7.8613" x2="-0.9525" y2="-7.8359" layer="94"/>
<rectangle x1="1.0033" y1="-7.8613" x2="2.7051" y2="-7.8359" layer="94"/>
<rectangle x1="3.4417" y1="-7.8613" x2="5.2451" y2="-7.8359" layer="94"/>
<rectangle x1="-5.2451" y1="-7.8359" x2="-3.4417" y2="-7.8105" layer="94"/>
<rectangle x1="-2.9083" y1="-7.8359" x2="-2.4003" y2="-7.8105" layer="94"/>
<rectangle x1="-1.2319" y1="-7.8359" x2="-0.9525" y2="-7.8105" layer="94"/>
<rectangle x1="1.0287" y1="-7.8359" x2="1.7653" y2="-7.8105" layer="94"/>
<rectangle x1="3.4417" y1="-7.8359" x2="5.2451" y2="-7.8105" layer="94"/>
<rectangle x1="-5.2451" y1="-7.8105" x2="-3.4417" y2="-7.7851" layer="94"/>
<rectangle x1="-2.9083" y1="-7.8105" x2="-2.4003" y2="-7.7851" layer="94"/>
<rectangle x1="-1.2319" y1="-7.8105" x2="-0.9525" y2="-7.7851" layer="94"/>
<rectangle x1="1.0287" y1="-7.8105" x2="1.6129" y2="-7.7851" layer="94"/>
<rectangle x1="3.4417" y1="-7.8105" x2="5.2451" y2="-7.7851" layer="94"/>
<rectangle x1="-5.2451" y1="-7.7851" x2="-3.4417" y2="-7.7597" layer="94"/>
<rectangle x1="-2.9083" y1="-7.7851" x2="-2.4003" y2="-7.7597" layer="94"/>
<rectangle x1="-1.2319" y1="-7.7851" x2="-0.9525" y2="-7.7597" layer="94"/>
<rectangle x1="1.0287" y1="-7.7851" x2="1.5367" y2="-7.7597" layer="94"/>
<rectangle x1="3.4417" y1="-7.7851" x2="5.2451" y2="-7.7597" layer="94"/>
<rectangle x1="-5.2451" y1="-7.7597" x2="-3.4417" y2="-7.7343" layer="94"/>
<rectangle x1="-2.9083" y1="-7.7597" x2="-2.4003" y2="-7.7343" layer="94"/>
<rectangle x1="-1.2319" y1="-7.7597" x2="-0.9525" y2="-7.7343" layer="94"/>
<rectangle x1="1.0541" y1="-7.7597" x2="1.4859" y2="-7.7343" layer="94"/>
<rectangle x1="3.4417" y1="-7.7597" x2="5.2451" y2="-7.7343" layer="94"/>
<rectangle x1="-5.2451" y1="-7.7343" x2="-3.4417" y2="-7.7089" layer="94"/>
<rectangle x1="-2.9083" y1="-7.7343" x2="-2.4003" y2="-7.7089" layer="94"/>
<rectangle x1="-1.2319" y1="-7.7343" x2="-0.9525" y2="-7.7089" layer="94"/>
<rectangle x1="1.0541" y1="-7.7343" x2="1.4351" y2="-7.7089" layer="94"/>
<rectangle x1="3.4163" y1="-7.7343" x2="5.2451" y2="-7.7089" layer="94"/>
<rectangle x1="-5.2451" y1="-7.7089" x2="-3.4417" y2="-7.6835" layer="94"/>
<rectangle x1="-2.9083" y1="-7.7089" x2="-2.4003" y2="-7.6835" layer="94"/>
<rectangle x1="-1.2319" y1="-7.7089" x2="-0.9525" y2="-7.6835" layer="94"/>
<rectangle x1="1.0541" y1="-7.7089" x2="1.4097" y2="-7.6835" layer="94"/>
<rectangle x1="3.4163" y1="-7.7089" x2="5.2451" y2="-7.6835" layer="94"/>
<rectangle x1="-5.2451" y1="-7.6835" x2="-3.4417" y2="-7.6581" layer="94"/>
<rectangle x1="-2.9083" y1="-7.6835" x2="-2.4003" y2="-7.6581" layer="94"/>
<rectangle x1="-1.2319" y1="-7.6835" x2="-0.9525" y2="-7.6581" layer="94"/>
<rectangle x1="1.0541" y1="-7.6835" x2="1.3843" y2="-7.6581" layer="94"/>
<rectangle x1="3.4163" y1="-7.6835" x2="5.2451" y2="-7.6581" layer="94"/>
<rectangle x1="-5.2451" y1="-7.6581" x2="-3.4417" y2="-7.6327" layer="94"/>
<rectangle x1="-2.9083" y1="-7.6581" x2="-2.4003" y2="-7.6327" layer="94"/>
<rectangle x1="-1.2319" y1="-7.6581" x2="-0.9525" y2="-7.6327" layer="94"/>
<rectangle x1="-0.4191" y1="-7.6581" x2="0.4191" y2="-7.6327" layer="94"/>
<rectangle x1="1.0795" y1="-7.6581" x2="1.3843" y2="-7.6327" layer="94"/>
<rectangle x1="3.3909" y1="-7.6581" x2="5.2451" y2="-7.6327" layer="94"/>
<rectangle x1="-5.2451" y1="-7.6327" x2="-3.4417" y2="-7.6073" layer="94"/>
<rectangle x1="-2.9083" y1="-7.6327" x2="-2.4003" y2="-7.6073" layer="94"/>
<rectangle x1="-1.2319" y1="-7.6327" x2="-0.9525" y2="-7.6073" layer="94"/>
<rectangle x1="-0.4191" y1="-7.6327" x2="0.4699" y2="-7.6073" layer="94"/>
<rectangle x1="1.0795" y1="-7.6327" x2="1.3589" y2="-7.6073" layer="94"/>
<rectangle x1="3.3655" y1="-7.6327" x2="5.2451" y2="-7.6073" layer="94"/>
<rectangle x1="-5.2451" y1="-7.6073" x2="-3.4417" y2="-7.5819" layer="94"/>
<rectangle x1="-2.9083" y1="-7.6073" x2="-2.4003" y2="-7.5819" layer="94"/>
<rectangle x1="-1.2319" y1="-7.6073" x2="-0.9525" y2="-7.5819" layer="94"/>
<rectangle x1="-0.4191" y1="-7.6073" x2="0.4953" y2="-7.5819" layer="94"/>
<rectangle x1="1.0795" y1="-7.6073" x2="1.3589" y2="-7.5819" layer="94"/>
<rectangle x1="3.3655" y1="-7.6073" x2="5.2451" y2="-7.5819" layer="94"/>
<rectangle x1="-5.2451" y1="-7.5819" x2="-3.4417" y2="-7.5565" layer="94"/>
<rectangle x1="-2.9083" y1="-7.5819" x2="-2.4003" y2="-7.5565" layer="94"/>
<rectangle x1="-1.2319" y1="-7.5819" x2="-0.9525" y2="-7.5565" layer="94"/>
<rectangle x1="-0.4191" y1="-7.5819" x2="0.5207" y2="-7.5565" layer="94"/>
<rectangle x1="1.0795" y1="-7.5819" x2="1.3589" y2="-7.5565" layer="94"/>
<rectangle x1="3.3147" y1="-7.5819" x2="5.2451" y2="-7.5565" layer="94"/>
<rectangle x1="-5.2451" y1="-7.5565" x2="-3.4417" y2="-7.5311" layer="94"/>
<rectangle x1="-2.9083" y1="-7.5565" x2="-2.4003" y2="-7.5311" layer="94"/>
<rectangle x1="-1.2319" y1="-7.5565" x2="-0.9525" y2="-7.5311" layer="94"/>
<rectangle x1="-0.4191" y1="-7.5565" x2="0.5207" y2="-7.5311" layer="94"/>
<rectangle x1="1.0795" y1="-7.5565" x2="1.3335" y2="-7.5311" layer="94"/>
<rectangle x1="3.2893" y1="-7.5565" x2="5.2451" y2="-7.5311" layer="94"/>
<rectangle x1="-5.2451" y1="-7.5311" x2="-3.4417" y2="-7.5057" layer="94"/>
<rectangle x1="-2.9083" y1="-7.5311" x2="-0.9525" y2="-7.5057" layer="94"/>
<rectangle x1="-0.4191" y1="-7.5311" x2="0.5461" y2="-7.5057" layer="94"/>
<rectangle x1="1.0795" y1="-7.5311" x2="1.3335" y2="-7.5057" layer="94"/>
<rectangle x1="3.2385" y1="-7.5311" x2="5.2451" y2="-7.5057" layer="94"/>
<rectangle x1="-5.2451" y1="-7.5057" x2="-3.4417" y2="-7.4803" layer="94"/>
<rectangle x1="-2.9083" y1="-7.5057" x2="-0.9525" y2="-7.4803" layer="94"/>
<rectangle x1="-0.4191" y1="-7.5057" x2="0.5461" y2="-7.4803" layer="94"/>
<rectangle x1="1.0795" y1="-7.5057" x2="1.3335" y2="-7.4803" layer="94"/>
<rectangle x1="3.1623" y1="-7.5057" x2="5.2451" y2="-7.4803" layer="94"/>
<rectangle x1="-5.2451" y1="-7.4803" x2="-3.4417" y2="-7.4549" layer="94"/>
<rectangle x1="-2.9083" y1="-7.4803" x2="-0.9525" y2="-7.4549" layer="94"/>
<rectangle x1="-0.4191" y1="-7.4803" x2="0.5461" y2="-7.4549" layer="94"/>
<rectangle x1="1.0795" y1="-7.4803" x2="1.3335" y2="-7.4549" layer="94"/>
<rectangle x1="3.0353" y1="-7.4803" x2="5.2451" y2="-7.4549" layer="94"/>
<rectangle x1="-5.2451" y1="-7.4549" x2="-3.4417" y2="-7.4295" layer="94"/>
<rectangle x1="-2.9083" y1="-7.4549" x2="-0.9525" y2="-7.4295" layer="94"/>
<rectangle x1="-0.4191" y1="-7.4549" x2="0.5461" y2="-7.4295" layer="94"/>
<rectangle x1="1.0795" y1="-7.4549" x2="1.3335" y2="-7.4295" layer="94"/>
<rectangle x1="2.0955" y1="-7.4549" x2="2.1209" y2="-7.4295" layer="94"/>
<rectangle x1="2.1463" y1="-7.4549" x2="2.1717" y2="-7.4295" layer="94"/>
<rectangle x1="2.1971" y1="-7.4549" x2="2.2225" y2="-7.4295" layer="94"/>
<rectangle x1="2.2479" y1="-7.4549" x2="2.2733" y2="-7.4295" layer="94"/>
<rectangle x1="2.2987" y1="-7.4549" x2="2.3241" y2="-7.4295" layer="94"/>
<rectangle x1="2.3495" y1="-7.4549" x2="2.3749" y2="-7.4295" layer="94"/>
<rectangle x1="2.4003" y1="-7.4549" x2="2.4257" y2="-7.4295" layer="94"/>
<rectangle x1="2.4511" y1="-7.4549" x2="2.4765" y2="-7.4295" layer="94"/>
<rectangle x1="2.5019" y1="-7.4549" x2="2.5273" y2="-7.4295" layer="94"/>
<rectangle x1="2.5527" y1="-7.4549" x2="2.5781" y2="-7.4295" layer="94"/>
<rectangle x1="2.6035" y1="-7.4549" x2="2.6289" y2="-7.4295" layer="94"/>
<rectangle x1="2.6543" y1="-7.4549" x2="2.6797" y2="-7.4295" layer="94"/>
<rectangle x1="2.7051" y1="-7.4549" x2="5.2451" y2="-7.4295" layer="94"/>
<rectangle x1="-5.2451" y1="-7.4295" x2="-3.4417" y2="-7.4041" layer="94"/>
<rectangle x1="-2.9083" y1="-7.4295" x2="-0.9525" y2="-7.4041" layer="94"/>
<rectangle x1="-0.4191" y1="-7.4295" x2="0.5461" y2="-7.4041" layer="94"/>
<rectangle x1="1.0795" y1="-7.4295" x2="1.3335" y2="-7.4041" layer="94"/>
<rectangle x1="1.9177" y1="-7.4295" x2="5.2451" y2="-7.4041" layer="94"/>
<rectangle x1="-5.2451" y1="-7.4041" x2="-3.4417" y2="-7.3787" layer="94"/>
<rectangle x1="-2.8829" y1="-7.4041" x2="-0.9525" y2="-7.3787" layer="94"/>
<rectangle x1="-0.4191" y1="-7.4041" x2="0.5461" y2="-7.3787" layer="94"/>
<rectangle x1="1.0795" y1="-7.4041" x2="1.3335" y2="-7.3787" layer="94"/>
<rectangle x1="1.8669" y1="-7.4041" x2="5.2451" y2="-7.3787" layer="94"/>
<rectangle x1="-5.2451" y1="-7.3787" x2="-3.4417" y2="-7.3533" layer="94"/>
<rectangle x1="-2.8829" y1="-7.3787" x2="-1.7653" y2="-7.3533" layer="94"/>
<rectangle x1="-1.2573" y1="-7.3787" x2="-0.9525" y2="-7.3533" layer="94"/>
<rectangle x1="-0.4191" y1="-7.3787" x2="0.5461" y2="-7.3533" layer="94"/>
<rectangle x1="1.0795" y1="-7.3787" x2="1.3335" y2="-7.3533" layer="94"/>
<rectangle x1="1.8669" y1="-7.3787" x2="5.2451" y2="-7.3533" layer="94"/>
<rectangle x1="-5.2451" y1="-7.3533" x2="-3.4417" y2="-7.3279" layer="94"/>
<rectangle x1="-2.8829" y1="-7.3533" x2="-1.7653" y2="-7.3279" layer="94"/>
<rectangle x1="-1.2573" y1="-7.3533" x2="-0.9525" y2="-7.3279" layer="94"/>
<rectangle x1="-0.4191" y1="-7.3533" x2="0.5207" y2="-7.3279" layer="94"/>
<rectangle x1="1.0795" y1="-7.3533" x2="1.3335" y2="-7.3279" layer="94"/>
<rectangle x1="1.8415" y1="-7.3533" x2="2.8575" y2="-7.3279" layer="94"/>
<rectangle x1="3.3909" y1="-7.3533" x2="5.2451" y2="-7.3279" layer="94"/>
<rectangle x1="-5.2451" y1="-7.3279" x2="-3.4417" y2="-7.3025" layer="94"/>
<rectangle x1="-2.8829" y1="-7.3279" x2="-1.7653" y2="-7.3025" layer="94"/>
<rectangle x1="-1.2573" y1="-7.3279" x2="-0.9525" y2="-7.3025" layer="94"/>
<rectangle x1="-0.4191" y1="-7.3279" x2="0.5207" y2="-7.3025" layer="94"/>
<rectangle x1="1.0795" y1="-7.3279" x2="1.3335" y2="-7.3025" layer="94"/>
<rectangle x1="1.8415" y1="-7.3279" x2="2.8575" y2="-7.3025" layer="94"/>
<rectangle x1="3.3909" y1="-7.3279" x2="5.2451" y2="-7.3025" layer="94"/>
<rectangle x1="-5.2451" y1="-7.3025" x2="-3.4417" y2="-7.2771" layer="94"/>
<rectangle x1="-2.8575" y1="-7.3025" x2="-1.7653" y2="-7.2771" layer="94"/>
<rectangle x1="-1.2573" y1="-7.3025" x2="-0.9525" y2="-7.2771" layer="94"/>
<rectangle x1="-0.4191" y1="-7.3025" x2="0.4953" y2="-7.2771" layer="94"/>
<rectangle x1="1.0795" y1="-7.3025" x2="1.3335" y2="-7.2771" layer="94"/>
<rectangle x1="1.8415" y1="-7.3025" x2="2.8575" y2="-7.2771" layer="94"/>
<rectangle x1="3.3909" y1="-7.3025" x2="5.2451" y2="-7.2771" layer="94"/>
<rectangle x1="-5.2451" y1="-7.2771" x2="-3.4417" y2="-7.2517" layer="94"/>
<rectangle x1="-2.8321" y1="-7.2771" x2="-1.7907" y2="-7.2517" layer="94"/>
<rectangle x1="-1.2573" y1="-7.2771" x2="-0.9525" y2="-7.2517" layer="94"/>
<rectangle x1="-0.4191" y1="-7.2771" x2="0.4699" y2="-7.2517" layer="94"/>
<rectangle x1="1.0795" y1="-7.2771" x2="1.3335" y2="-7.2517" layer="94"/>
<rectangle x1="1.8415" y1="-7.2771" x2="2.8575" y2="-7.2517" layer="94"/>
<rectangle x1="3.3909" y1="-7.2771" x2="5.2451" y2="-7.2517" layer="94"/>
<rectangle x1="-5.2451" y1="-7.2517" x2="-3.4417" y2="-7.2263" layer="94"/>
<rectangle x1="-2.8067" y1="-7.2517" x2="-1.8161" y2="-7.2263" layer="94"/>
<rectangle x1="-1.2573" y1="-7.2517" x2="-0.9525" y2="-7.2263" layer="94"/>
<rectangle x1="-0.4191" y1="-7.2517" x2="0.4191" y2="-7.2263" layer="94"/>
<rectangle x1="1.0795" y1="-7.2517" x2="1.3335" y2="-7.2263" layer="94"/>
<rectangle x1="1.8669" y1="-7.2517" x2="2.8575" y2="-7.2263" layer="94"/>
<rectangle x1="3.3909" y1="-7.2517" x2="5.2451" y2="-7.2263" layer="94"/>
<rectangle x1="-5.2451" y1="-7.2263" x2="-3.4163" y2="-7.2009" layer="94"/>
<rectangle x1="-2.7305" y1="-7.2263" x2="-1.8669" y2="-7.2009" layer="94"/>
<rectangle x1="-1.2573" y1="-7.2263" x2="-0.9525" y2="-7.2009" layer="94"/>
<rectangle x1="1.0541" y1="-7.2263" x2="1.3335" y2="-7.2009" layer="94"/>
<rectangle x1="1.8923" y1="-7.2263" x2="2.8321" y2="-7.2009" layer="94"/>
<rectangle x1="3.3909" y1="-7.2263" x2="5.2451" y2="-7.2009" layer="94"/>
<rectangle x1="-5.2451" y1="-7.2009" x2="-3.4163" y2="-7.1755" layer="94"/>
<rectangle x1="-1.2573" y1="-7.2009" x2="-0.9525" y2="-7.1755" layer="94"/>
<rectangle x1="1.0541" y1="-7.2009" x2="1.3335" y2="-7.1755" layer="94"/>
<rectangle x1="1.9431" y1="-7.2009" x2="2.7813" y2="-7.1755" layer="94"/>
<rectangle x1="3.3909" y1="-7.2009" x2="5.2451" y2="-7.1755" layer="94"/>
<rectangle x1="-5.2451" y1="-7.1755" x2="-3.4163" y2="-7.1501" layer="94"/>
<rectangle x1="-1.2573" y1="-7.1755" x2="-0.9525" y2="-7.1501" layer="94"/>
<rectangle x1="1.0541" y1="-7.1755" x2="1.3335" y2="-7.1501" layer="94"/>
<rectangle x1="3.3909" y1="-7.1755" x2="5.2451" y2="-7.1501" layer="94"/>
<rectangle x1="-5.2451" y1="-7.1501" x2="-3.4163" y2="-7.1247" layer="94"/>
<rectangle x1="-1.2573" y1="-7.1501" x2="-0.9525" y2="-7.1247" layer="94"/>
<rectangle x1="1.0541" y1="-7.1501" x2="1.3335" y2="-7.1247" layer="94"/>
<rectangle x1="3.3655" y1="-7.1501" x2="5.2451" y2="-7.1247" layer="94"/>
<rectangle x1="-5.2451" y1="-7.1247" x2="-3.3909" y2="-7.0993" layer="94"/>
<rectangle x1="-1.2573" y1="-7.1247" x2="-0.9525" y2="-7.0993" layer="94"/>
<rectangle x1="1.0287" y1="-7.1247" x2="1.3335" y2="-7.0993" layer="94"/>
<rectangle x1="3.3655" y1="-7.1247" x2="5.2451" y2="-7.0993" layer="94"/>
<rectangle x1="-5.2451" y1="-7.0993" x2="-3.3909" y2="-7.0739" layer="94"/>
<rectangle x1="-1.2827" y1="-7.0993" x2="-0.9525" y2="-7.0739" layer="94"/>
<rectangle x1="1.0287" y1="-7.0993" x2="1.3335" y2="-7.0739" layer="94"/>
<rectangle x1="3.3655" y1="-7.0993" x2="5.2451" y2="-7.0739" layer="94"/>
<rectangle x1="-5.2451" y1="-7.0739" x2="-3.3909" y2="-7.0485" layer="94"/>
<rectangle x1="-1.2827" y1="-7.0739" x2="-0.9525" y2="-7.0485" layer="94"/>
<rectangle x1="1.0033" y1="-7.0739" x2="1.3589" y2="-7.0485" layer="94"/>
<rectangle x1="3.3655" y1="-7.0739" x2="5.2451" y2="-7.0485" layer="94"/>
<rectangle x1="-5.2451" y1="-7.0485" x2="-3.3655" y2="-7.0231" layer="94"/>
<rectangle x1="-1.2827" y1="-7.0485" x2="-0.9525" y2="-7.0231" layer="94"/>
<rectangle x1="1.0033" y1="-7.0485" x2="1.3589" y2="-7.0231" layer="94"/>
<rectangle x1="3.3655" y1="-7.0485" x2="5.2451" y2="-7.0231" layer="94"/>
<rectangle x1="-5.2451" y1="-7.0231" x2="-3.3401" y2="-6.9977" layer="94"/>
<rectangle x1="-1.3081" y1="-7.0231" x2="-0.9525" y2="-6.9977" layer="94"/>
<rectangle x1="0.9779" y1="-7.0231" x2="1.3843" y2="-6.9977" layer="94"/>
<rectangle x1="3.3401" y1="-7.0231" x2="5.2451" y2="-6.9977" layer="94"/>
<rectangle x1="-5.2451" y1="-6.9977" x2="-3.3401" y2="-6.9723" layer="94"/>
<rectangle x1="-1.3335" y1="-6.9977" x2="-0.9525" y2="-6.9723" layer="94"/>
<rectangle x1="0.9779" y1="-6.9977" x2="1.3843" y2="-6.9723" layer="94"/>
<rectangle x1="3.3401" y1="-6.9977" x2="5.2451" y2="-6.9723" layer="94"/>
<rectangle x1="-5.2451" y1="-6.9723" x2="-3.3147" y2="-6.9469" layer="94"/>
<rectangle x1="-1.3335" y1="-6.9723" x2="-0.9525" y2="-6.9469" layer="94"/>
<rectangle x1="0.9525" y1="-6.9723" x2="1.4097" y2="-6.9469" layer="94"/>
<rectangle x1="3.3147" y1="-6.9723" x2="5.2451" y2="-6.9469" layer="94"/>
<rectangle x1="-5.2451" y1="-6.9469" x2="-3.2893" y2="-6.9215" layer="94"/>
<rectangle x1="-1.3589" y1="-6.9469" x2="-0.9525" y2="-6.9215" layer="94"/>
<rectangle x1="0.9271" y1="-6.9469" x2="1.4351" y2="-6.9215" layer="94"/>
<rectangle x1="3.2893" y1="-6.9469" x2="5.2451" y2="-6.9215" layer="94"/>
<rectangle x1="-5.2451" y1="-6.9215" x2="-3.2385" y2="-6.8961" layer="94"/>
<rectangle x1="-1.3843" y1="-6.9215" x2="-0.9525" y2="-6.8961" layer="94"/>
<rectangle x1="0.8763" y1="-6.9215" x2="1.4605" y2="-6.8961" layer="94"/>
<rectangle x1="3.2639" y1="-6.9215" x2="5.2451" y2="-6.8961" layer="94"/>
<rectangle x1="-5.2451" y1="-6.8961" x2="-3.2131" y2="-6.8707" layer="94"/>
<rectangle x1="-1.4351" y1="-6.8961" x2="-0.9525" y2="-6.8707" layer="94"/>
<rectangle x1="0.8509" y1="-6.8961" x2="1.4859" y2="-6.8707" layer="94"/>
<rectangle x1="3.2385" y1="-6.8961" x2="5.2451" y2="-6.8707" layer="94"/>
<rectangle x1="-5.2451" y1="-6.8707" x2="-3.1623" y2="-6.8453" layer="94"/>
<rectangle x1="-1.4859" y1="-6.8707" x2="-0.9525" y2="-6.8453" layer="94"/>
<rectangle x1="0.8001" y1="-6.8707" x2="1.5367" y2="-6.8453" layer="94"/>
<rectangle x1="3.1877" y1="-6.8707" x2="5.2451" y2="-6.8453" layer="94"/>
<rectangle x1="-5.2451" y1="-6.8453" x2="-3.0861" y2="-6.8199" layer="94"/>
<rectangle x1="-1.5367" y1="-6.8453" x2="-0.9525" y2="-6.8199" layer="94"/>
<rectangle x1="0.7239" y1="-6.8453" x2="1.6129" y2="-6.8199" layer="94"/>
<rectangle x1="3.1115" y1="-6.8453" x2="5.2451" y2="-6.8199" layer="94"/>
<rectangle x1="-5.2451" y1="-6.8199" x2="-2.9591" y2="-6.7945" layer="94"/>
<rectangle x1="-1.6637" y1="-6.8199" x2="-0.9271" y2="-6.7945" layer="94"/>
<rectangle x1="-0.9017" y1="-6.8199" x2="-0.8763" y2="-6.7945" layer="94"/>
<rectangle x1="-0.8509" y1="-6.8199" x2="-0.8255" y2="-6.7945" layer="94"/>
<rectangle x1="-0.8001" y1="-6.8199" x2="-0.7747" y2="-6.7945" layer="94"/>
<rectangle x1="-0.7493" y1="-6.8199" x2="-0.7239" y2="-6.7945" layer="94"/>
<rectangle x1="-0.6985" y1="-6.8199" x2="-0.6731" y2="-6.7945" layer="94"/>
<rectangle x1="-0.6477" y1="-6.8199" x2="-0.6223" y2="-6.7945" layer="94"/>
<rectangle x1="-0.5969" y1="-6.8199" x2="-0.5715" y2="-6.7945" layer="94"/>
<rectangle x1="-0.5461" y1="-6.8199" x2="-0.5207" y2="-6.7945" layer="94"/>
<rectangle x1="-0.4953" y1="-6.8199" x2="-0.4699" y2="-6.7945" layer="94"/>
<rectangle x1="-0.4445" y1="-6.8199" x2="-0.4191" y2="-6.7945" layer="94"/>
<rectangle x1="-0.3937" y1="-6.8199" x2="-0.3683" y2="-6.7945" layer="94"/>
<rectangle x1="-0.3429" y1="-6.8199" x2="-0.3175" y2="-6.7945" layer="94"/>
<rectangle x1="-0.2921" y1="-6.8199" x2="-0.2667" y2="-6.7945" layer="94"/>
<rectangle x1="-0.2413" y1="-6.8199" x2="-0.2159" y2="-6.7945" layer="94"/>
<rectangle x1="-0.1905" y1="-6.8199" x2="-0.1651" y2="-6.7945" layer="94"/>
<rectangle x1="-0.1397" y1="-6.8199" x2="-0.1143" y2="-6.7945" layer="94"/>
<rectangle x1="-0.0889" y1="-6.8199" x2="-0.0635" y2="-6.7945" layer="94"/>
<rectangle x1="-0.0381" y1="-6.8199" x2="-0.0127" y2="-6.7945" layer="94"/>
<rectangle x1="0.0127" y1="-6.8199" x2="0.0381" y2="-6.7945" layer="94"/>
<rectangle x1="0.0635" y1="-6.8199" x2="0.0889" y2="-6.7945" layer="94"/>
<rectangle x1="0.1143" y1="-6.8199" x2="0.1397" y2="-6.7945" layer="94"/>
<rectangle x1="0.1651" y1="-6.8199" x2="0.1905" y2="-6.7945" layer="94"/>
<rectangle x1="0.2159" y1="-6.8199" x2="0.2413" y2="-6.7945" layer="94"/>
<rectangle x1="0.2667" y1="-6.8199" x2="0.2921" y2="-6.7945" layer="94"/>
<rectangle x1="0.3175" y1="-6.8199" x2="0.3429" y2="-6.7945" layer="94"/>
<rectangle x1="0.3683" y1="-6.8199" x2="0.3937" y2="-6.7945" layer="94"/>
<rectangle x1="0.4191" y1="-6.8199" x2="0.4445" y2="-6.7945" layer="94"/>
<rectangle x1="0.4699" y1="-6.8199" x2="0.4953" y2="-6.7945" layer="94"/>
<rectangle x1="0.5207" y1="-6.8199" x2="0.5461" y2="-6.7945" layer="94"/>
<rectangle x1="0.5715" y1="-6.8199" x2="1.7399" y2="-6.7945" layer="94"/>
<rectangle x1="3.0099" y1="-6.8199" x2="5.2451" y2="-6.7945" layer="94"/>
<rectangle x1="-5.2451" y1="-6.7945" x2="5.2451" y2="-6.7691" layer="94"/>
<rectangle x1="-5.2451" y1="-6.7691" x2="5.2451" y2="-6.7437" layer="94"/>
<rectangle x1="-5.2451" y1="-6.7437" x2="5.2451" y2="-6.7183" layer="94"/>
<rectangle x1="-5.2451" y1="-6.7183" x2="5.2451" y2="-6.6929" layer="94"/>
<rectangle x1="-5.2451" y1="-6.6929" x2="5.2451" y2="-6.6675" layer="94"/>
<rectangle x1="-5.2451" y1="-6.6675" x2="5.2451" y2="-6.6421" layer="94"/>
<rectangle x1="-5.2451" y1="-6.6421" x2="5.2451" y2="-6.6167" layer="94"/>
<rectangle x1="-5.2451" y1="-6.6167" x2="5.2451" y2="-6.5913" layer="94"/>
<rectangle x1="-5.2451" y1="-6.5913" x2="5.2451" y2="-6.5659" layer="94"/>
<rectangle x1="-5.2451" y1="-6.5659" x2="5.2451" y2="-6.5405" layer="94"/>
<rectangle x1="-5.2451" y1="-6.5405" x2="5.2451" y2="-6.5151" layer="94"/>
<rectangle x1="-5.2451" y1="-6.5151" x2="5.2451" y2="-6.4897" layer="94"/>
<rectangle x1="-5.2451" y1="-6.4897" x2="5.2451" y2="-6.4643" layer="94"/>
<rectangle x1="-5.2451" y1="-6.4643" x2="5.2451" y2="-6.4389" layer="94"/>
<rectangle x1="-5.2451" y1="-6.4389" x2="5.2451" y2="-6.4135" layer="94"/>
<rectangle x1="-5.2451" y1="-6.4135" x2="5.2451" y2="-6.3881" layer="94"/>
<rectangle x1="-5.2451" y1="-6.3881" x2="5.2451" y2="-6.3627" layer="94"/>
<rectangle x1="-5.2451" y1="-6.3627" x2="5.2451" y2="-6.3373" layer="94"/>
<rectangle x1="-5.2451" y1="-6.3373" x2="5.2451" y2="-6.3119" layer="94"/>
<rectangle x1="-5.2451" y1="-6.3119" x2="5.2451" y2="-6.2865" layer="94"/>
<rectangle x1="-5.2451" y1="-6.2865" x2="5.2451" y2="-6.2611" layer="94"/>
<rectangle x1="-5.2451" y1="-6.2611" x2="5.2451" y2="-6.2357" layer="94"/>
<rectangle x1="-5.2451" y1="-6.2357" x2="5.2451" y2="-6.2103" layer="94"/>
<rectangle x1="-5.2451" y1="-6.2103" x2="5.2451" y2="-6.1849" layer="94"/>
<rectangle x1="-5.2451" y1="-6.1849" x2="5.2451" y2="-6.1595" layer="94"/>
<rectangle x1="-5.2451" y1="-6.1595" x2="5.2451" y2="-6.1341" layer="94"/>
<rectangle x1="-5.2451" y1="-6.1341" x2="5.2451" y2="-6.1087" layer="94"/>
<rectangle x1="-5.2451" y1="-6.1087" x2="5.2451" y2="-6.0833" layer="94"/>
<rectangle x1="-5.2451" y1="-6.0833" x2="-3.2893" y2="-6.0579" layer="94"/>
<rectangle x1="-3.2131" y1="-6.0833" x2="5.2451" y2="-6.0579" layer="94"/>
<rectangle x1="-5.2451" y1="-6.0579" x2="-3.4417" y2="-6.0325" layer="94"/>
<rectangle x1="-3.0861" y1="-6.0579" x2="5.2451" y2="-6.0325" layer="94"/>
<rectangle x1="-5.2451" y1="-6.0325" x2="-3.4925" y2="-6.0071" layer="94"/>
<rectangle x1="-3.0099" y1="-6.0325" x2="5.2451" y2="-6.0071" layer="94"/>
<rectangle x1="-5.2451" y1="-6.0071" x2="-3.5687" y2="-5.9817" layer="94"/>
<rectangle x1="-2.9591" y1="-6.0071" x2="5.2451" y2="-5.9817" layer="94"/>
<rectangle x1="-5.2451" y1="-5.9817" x2="-3.6195" y2="-5.9563" layer="94"/>
<rectangle x1="-2.9337" y1="-5.9817" x2="5.2451" y2="-5.9563" layer="94"/>
<rectangle x1="-5.2451" y1="-5.9563" x2="-3.6703" y2="-5.9309" layer="94"/>
<rectangle x1="-2.9083" y1="-5.9563" x2="3.0353" y2="-5.9309" layer="94"/>
<rectangle x1="3.1115" y1="-5.9563" x2="5.2451" y2="-5.9309" layer="94"/>
<rectangle x1="-5.2451" y1="-5.9309" x2="-3.7465" y2="-5.9055" layer="94"/>
<rectangle x1="-2.8575" y1="-5.9309" x2="2.9845" y2="-5.9055" layer="94"/>
<rectangle x1="3.1877" y1="-5.9309" x2="5.2451" y2="-5.9055" layer="94"/>
<rectangle x1="-5.2451" y1="-5.9055" x2="-3.7973" y2="-5.8801" layer="94"/>
<rectangle x1="-2.8575" y1="-5.9055" x2="2.9591" y2="-5.8801" layer="94"/>
<rectangle x1="3.2131" y1="-5.9055" x2="5.2451" y2="-5.8801" layer="94"/>
<rectangle x1="-5.2451" y1="-5.8801" x2="-3.8735" y2="-5.8547" layer="94"/>
<rectangle x1="-2.8321" y1="-5.8801" x2="2.9591" y2="-5.8547" layer="94"/>
<rectangle x1="3.2385" y1="-5.8801" x2="5.2451" y2="-5.8547" layer="94"/>
<rectangle x1="-5.2451" y1="-5.8547" x2="-3.9243" y2="-5.8293" layer="94"/>
<rectangle x1="-2.8067" y1="-5.8547" x2="2.9337" y2="-5.8293" layer="94"/>
<rectangle x1="3.2639" y1="-5.8547" x2="5.2451" y2="-5.8293" layer="94"/>
<rectangle x1="-5.2451" y1="-5.8293" x2="-3.9751" y2="-5.8039" layer="94"/>
<rectangle x1="-2.7813" y1="-5.8293" x2="2.9337" y2="-5.8039" layer="94"/>
<rectangle x1="3.2893" y1="-5.8293" x2="5.2451" y2="-5.8039" layer="94"/>
<rectangle x1="-5.2451" y1="-5.8039" x2="-4.0513" y2="-5.7785" layer="94"/>
<rectangle x1="-2.7813" y1="-5.8039" x2="2.9337" y2="-5.7785" layer="94"/>
<rectangle x1="3.3147" y1="-5.8039" x2="5.2451" y2="-5.7785" layer="94"/>
<rectangle x1="-5.2451" y1="-5.7785" x2="-4.1021" y2="-5.7531" layer="94"/>
<rectangle x1="-2.7559" y1="-5.7785" x2="2.9337" y2="-5.7531" layer="94"/>
<rectangle x1="3.3401" y1="-5.7785" x2="5.2451" y2="-5.7531" layer="94"/>
<rectangle x1="-5.2451" y1="-5.7531" x2="-4.1529" y2="-5.7277" layer="94"/>
<rectangle x1="-2.7559" y1="-5.7531" x2="2.9337" y2="-5.7277" layer="94"/>
<rectangle x1="3.3655" y1="-5.7531" x2="5.2451" y2="-5.7277" layer="94"/>
<rectangle x1="-5.2451" y1="-5.7277" x2="-4.2291" y2="-5.7023" layer="94"/>
<rectangle x1="-2.7305" y1="-5.7277" x2="2.9591" y2="-5.7023" layer="94"/>
<rectangle x1="3.3909" y1="-5.7277" x2="5.2451" y2="-5.7023" layer="94"/>
<rectangle x1="-5.2451" y1="-5.7023" x2="-4.2799" y2="-5.6769" layer="94"/>
<rectangle x1="-2.7305" y1="-5.7023" x2="2.9845" y2="-5.6769" layer="94"/>
<rectangle x1="3.4163" y1="-5.7023" x2="5.2451" y2="-5.6769" layer="94"/>
<rectangle x1="-5.2451" y1="-5.6769" x2="-4.3307" y2="-5.6515" layer="94"/>
<rectangle x1="-2.7051" y1="-5.6769" x2="1.6637" y2="-5.6515" layer="94"/>
<rectangle x1="1.8161" y1="-5.6769" x2="3.0099" y2="-5.6515" layer="94"/>
<rectangle x1="3.4163" y1="-5.6769" x2="5.2451" y2="-5.6515" layer="94"/>
<rectangle x1="-5.2451" y1="-5.6515" x2="-4.3561" y2="-5.6261" layer="94"/>
<rectangle x1="-2.7051" y1="-5.6515" x2="1.6383" y2="-5.6261" layer="94"/>
<rectangle x1="1.8669" y1="-5.6515" x2="3.0353" y2="-5.6261" layer="94"/>
<rectangle x1="3.4417" y1="-5.6515" x2="5.2451" y2="-5.6261" layer="94"/>
<rectangle x1="-5.2451" y1="-5.6261" x2="-4.4069" y2="-5.6007" layer="94"/>
<rectangle x1="-3.3401" y1="-5.6261" x2="-3.1877" y2="-5.6007" layer="94"/>
<rectangle x1="-2.7051" y1="-5.6261" x2="1.6129" y2="-5.6007" layer="94"/>
<rectangle x1="1.8923" y1="-5.6261" x2="3.0607" y2="-5.6007" layer="94"/>
<rectangle x1="3.4671" y1="-5.6261" x2="5.2451" y2="-5.6007" layer="94"/>
<rectangle x1="-5.2451" y1="-5.6007" x2="-4.4323" y2="-5.5753" layer="94"/>
<rectangle x1="-3.3909" y1="-5.6007" x2="-3.1623" y2="-5.5753" layer="94"/>
<rectangle x1="-2.6797" y1="-5.6007" x2="1.5875" y2="-5.5753" layer="94"/>
<rectangle x1="1.9177" y1="-5.6007" x2="3.0861" y2="-5.5753" layer="94"/>
<rectangle x1="3.4671" y1="-5.6007" x2="5.2451" y2="-5.5753" layer="94"/>
<rectangle x1="-5.2451" y1="-5.5753" x2="-4.4577" y2="-5.5499" layer="94"/>
<rectangle x1="-3.4417" y1="-5.5753" x2="-3.1623" y2="-5.5499" layer="94"/>
<rectangle x1="-2.6797" y1="-5.5753" x2="1.5875" y2="-5.5499" layer="94"/>
<rectangle x1="1.9431" y1="-5.5753" x2="3.1115" y2="-5.5499" layer="94"/>
<rectangle x1="3.4925" y1="-5.5753" x2="5.2451" y2="-5.5499" layer="94"/>
<rectangle x1="-5.2451" y1="-5.5499" x2="-4.4831" y2="-5.5245" layer="94"/>
<rectangle x1="-3.5179" y1="-5.5499" x2="-3.1369" y2="-5.5245" layer="94"/>
<rectangle x1="-2.6543" y1="-5.5499" x2="1.5875" y2="-5.5245" layer="94"/>
<rectangle x1="1.9939" y1="-5.5499" x2="3.1369" y2="-5.5245" layer="94"/>
<rectangle x1="3.4925" y1="-5.5499" x2="5.2451" y2="-5.5245" layer="94"/>
<rectangle x1="-5.2451" y1="-5.5245" x2="-4.4831" y2="-5.4991" layer="94"/>
<rectangle x1="-3.5687" y1="-5.5245" x2="-3.1369" y2="-5.4991" layer="94"/>
<rectangle x1="-2.6543" y1="-5.5245" x2="1.5875" y2="-5.4991" layer="94"/>
<rectangle x1="2.0193" y1="-5.5245" x2="3.1623" y2="-5.4991" layer="94"/>
<rectangle x1="3.5179" y1="-5.5245" x2="5.2451" y2="-5.4991" layer="94"/>
<rectangle x1="-5.2451" y1="-5.4991" x2="-4.5085" y2="-5.4737" layer="94"/>
<rectangle x1="-3.6449" y1="-5.4991" x2="-3.1115" y2="-5.4737" layer="94"/>
<rectangle x1="-2.6289" y1="-5.4991" x2="0.0635" y2="-5.4737" layer="94"/>
<rectangle x1="0.1905" y1="-5.4991" x2="1.5875" y2="-5.4737" layer="94"/>
<rectangle x1="2.0447" y1="-5.4991" x2="3.1623" y2="-5.4737" layer="94"/>
<rectangle x1="3.5179" y1="-5.4991" x2="5.2451" y2="-5.4737" layer="94"/>
<rectangle x1="-5.2451" y1="-5.4737" x2="-4.5339" y2="-5.4483" layer="94"/>
<rectangle x1="-3.6957" y1="-5.4737" x2="-3.1115" y2="-5.4483" layer="94"/>
<rectangle x1="-2.6289" y1="-5.4737" x2="0.0381" y2="-5.4483" layer="94"/>
<rectangle x1="0.2413" y1="-5.4737" x2="1.6129" y2="-5.4483" layer="94"/>
<rectangle x1="2.0447" y1="-5.4737" x2="3.1877" y2="-5.4483" layer="94"/>
<rectangle x1="3.5433" y1="-5.4737" x2="5.2451" y2="-5.4483" layer="94"/>
<rectangle x1="-5.2451" y1="-5.4483" x2="-4.5339" y2="-5.4229" layer="94"/>
<rectangle x1="-3.7465" y1="-5.4483" x2="-3.0861" y2="-5.4229" layer="94"/>
<rectangle x1="-2.6289" y1="-5.4483" x2="0.0127" y2="-5.4229" layer="94"/>
<rectangle x1="0.2667" y1="-5.4483" x2="1.6383" y2="-5.4229" layer="94"/>
<rectangle x1="2.0701" y1="-5.4483" x2="3.2131" y2="-5.4229" layer="94"/>
<rectangle x1="3.5433" y1="-5.4483" x2="5.2451" y2="-5.4229" layer="94"/>
<rectangle x1="-5.2451" y1="-5.4229" x2="-4.5593" y2="-5.3975" layer="94"/>
<rectangle x1="-3.8227" y1="-5.4229" x2="-3.0861" y2="-5.3975" layer="94"/>
<rectangle x1="-2.6035" y1="-5.4229" x2="-0.0127" y2="-5.3975" layer="94"/>
<rectangle x1="0.3175" y1="-5.4229" x2="1.6637" y2="-5.3975" layer="94"/>
<rectangle x1="2.0955" y1="-5.4229" x2="3.2131" y2="-5.3975" layer="94"/>
<rectangle x1="3.5687" y1="-5.4229" x2="5.2451" y2="-5.3975" layer="94"/>
<rectangle x1="-5.2451" y1="-5.3975" x2="-4.5593" y2="-5.3721" layer="94"/>
<rectangle x1="-3.8735" y1="-5.3975" x2="-3.0861" y2="-5.3721" layer="94"/>
<rectangle x1="-2.6035" y1="-5.3975" x2="-0.0127" y2="-5.3721" layer="94"/>
<rectangle x1="0.3429" y1="-5.3975" x2="1.6891" y2="-5.3721" layer="94"/>
<rectangle x1="2.1209" y1="-5.3975" x2="3.2385" y2="-5.3721" layer="94"/>
<rectangle x1="3.5687" y1="-5.3975" x2="5.2451" y2="-5.3721" layer="94"/>
<rectangle x1="-5.2451" y1="-5.3721" x2="-4.5593" y2="-5.3467" layer="94"/>
<rectangle x1="-3.9243" y1="-5.3721" x2="-3.0607" y2="-5.3467" layer="94"/>
<rectangle x1="-2.5781" y1="-5.3721" x2="-0.0127" y2="-5.3467" layer="94"/>
<rectangle x1="0.3683" y1="-5.3721" x2="1.7145" y2="-5.3467" layer="94"/>
<rectangle x1="2.1463" y1="-5.3721" x2="3.2639" y2="-5.3467" layer="94"/>
<rectangle x1="3.5941" y1="-5.3721" x2="5.2451" y2="-5.3467" layer="94"/>
<rectangle x1="-5.2451" y1="-5.3467" x2="-4.5847" y2="-5.3213" layer="94"/>
<rectangle x1="-4.0005" y1="-5.3467" x2="-3.0607" y2="-5.3213" layer="94"/>
<rectangle x1="-2.5781" y1="-5.3467" x2="-0.0127" y2="-5.3213" layer="94"/>
<rectangle x1="0.3937" y1="-5.3467" x2="1.7399" y2="-5.3213" layer="94"/>
<rectangle x1="2.1717" y1="-5.3467" x2="3.2639" y2="-5.3213" layer="94"/>
<rectangle x1="3.5941" y1="-5.3467" x2="5.2451" y2="-5.3213" layer="94"/>
<rectangle x1="-5.2451" y1="-5.3213" x2="-4.5847" y2="-5.2959" layer="94"/>
<rectangle x1="-4.0513" y1="-5.3213" x2="-3.0353" y2="-5.2959" layer="94"/>
<rectangle x1="-2.5781" y1="-5.3213" x2="-0.0127" y2="-5.2959" layer="94"/>
<rectangle x1="0.4191" y1="-5.3213" x2="1.7653" y2="-5.2959" layer="94"/>
<rectangle x1="2.1717" y1="-5.3213" x2="3.2639" y2="-5.2959" layer="94"/>
<rectangle x1="3.5941" y1="-5.3213" x2="5.2451" y2="-5.2959" layer="94"/>
<rectangle x1="-5.2451" y1="-5.2959" x2="-4.5847" y2="-5.2705" layer="94"/>
<rectangle x1="-4.1021" y1="-5.2959" x2="-3.0353" y2="-5.2705" layer="94"/>
<rectangle x1="-2.5527" y1="-5.2959" x2="0.0127" y2="-5.2705" layer="94"/>
<rectangle x1="0.4445" y1="-5.2959" x2="1.7907" y2="-5.2705" layer="94"/>
<rectangle x1="2.1971" y1="-5.2959" x2="3.2893" y2="-5.2705" layer="94"/>
<rectangle x1="3.6195" y1="-5.2959" x2="5.2451" y2="-5.2705" layer="94"/>
<rectangle x1="-5.2451" y1="-5.2705" x2="-4.5847" y2="-5.2451" layer="94"/>
<rectangle x1="-4.1275" y1="-5.2705" x2="-3.0099" y2="-5.2451" layer="94"/>
<rectangle x1="-2.5527" y1="-5.2705" x2="0.0127" y2="-5.2451" layer="94"/>
<rectangle x1="0.4699" y1="-5.2705" x2="1.8161" y2="-5.2451" layer="94"/>
<rectangle x1="2.2225" y1="-5.2705" x2="3.2893" y2="-5.2451" layer="94"/>
<rectangle x1="3.6195" y1="-5.2705" x2="5.2451" y2="-5.2451" layer="94"/>
<rectangle x1="-5.2451" y1="-5.2451" x2="-4.5847" y2="-5.2197" layer="94"/>
<rectangle x1="-4.1529" y1="-5.2451" x2="-3.0099" y2="-5.2197" layer="94"/>
<rectangle x1="-2.5273" y1="-5.2451" x2="0.0381" y2="-5.2197" layer="94"/>
<rectangle x1="0.4953" y1="-5.2451" x2="1.8415" y2="-5.2197" layer="94"/>
<rectangle x1="2.2225" y1="-5.2451" x2="3.3147" y2="-5.2197" layer="94"/>
<rectangle x1="3.6195" y1="-5.2451" x2="5.2451" y2="-5.2197" layer="94"/>
<rectangle x1="-5.2451" y1="-5.2197" x2="-4.5847" y2="-5.1943" layer="94"/>
<rectangle x1="-4.1529" y1="-5.2197" x2="-3.0099" y2="-5.1943" layer="94"/>
<rectangle x1="-2.5273" y1="-5.2197" x2="0.0635" y2="-5.1943" layer="94"/>
<rectangle x1="0.5207" y1="-5.2197" x2="1.8669" y2="-5.1943" layer="94"/>
<rectangle x1="2.2479" y1="-5.2197" x2="3.3147" y2="-5.1943" layer="94"/>
<rectangle x1="3.6449" y1="-5.2197" x2="5.2451" y2="-5.1943" layer="94"/>
<rectangle x1="-5.2451" y1="-5.1943" x2="-4.6101" y2="-5.1689" layer="94"/>
<rectangle x1="-4.1529" y1="-5.1943" x2="-2.9845" y2="-5.1689" layer="94"/>
<rectangle x1="-2.5019" y1="-5.1943" x2="0.1143" y2="-5.1689" layer="94"/>
<rectangle x1="0.5461" y1="-5.1943" x2="1.8923" y2="-5.1689" layer="94"/>
<rectangle x1="2.2733" y1="-5.1943" x2="3.3147" y2="-5.1689" layer="94"/>
<rectangle x1="3.6449" y1="-5.1943" x2="5.2451" y2="-5.1689" layer="94"/>
<rectangle x1="-5.2451" y1="-5.1689" x2="-4.5847" y2="-5.1435" layer="94"/>
<rectangle x1="-4.1529" y1="-5.1689" x2="-2.9845" y2="-5.1435" layer="94"/>
<rectangle x1="-2.5019" y1="-5.1689" x2="0.1397" y2="-5.1435" layer="94"/>
<rectangle x1="0.5715" y1="-5.1689" x2="1.9177" y2="-5.1435" layer="94"/>
<rectangle x1="2.2733" y1="-5.1689" x2="3.3401" y2="-5.1435" layer="94"/>
<rectangle x1="3.6449" y1="-5.1689" x2="5.2451" y2="-5.1435" layer="94"/>
<rectangle x1="-5.2451" y1="-5.1435" x2="-4.5847" y2="-5.1181" layer="94"/>
<rectangle x1="-4.1529" y1="-5.1435" x2="-2.9591" y2="-5.1181" layer="94"/>
<rectangle x1="-2.5019" y1="-5.1435" x2="0.1651" y2="-5.1181" layer="94"/>
<rectangle x1="0.5969" y1="-5.1435" x2="1.9431" y2="-5.1181" layer="94"/>
<rectangle x1="2.2987" y1="-5.1435" x2="3.3401" y2="-5.1181" layer="94"/>
<rectangle x1="3.6449" y1="-5.1435" x2="5.2451" y2="-5.1181" layer="94"/>
<rectangle x1="-5.2451" y1="-5.1181" x2="-4.5847" y2="-5.0927" layer="94"/>
<rectangle x1="-4.1275" y1="-5.1181" x2="-2.9591" y2="-5.0927" layer="94"/>
<rectangle x1="-2.4765" y1="-5.1181" x2="0.1905" y2="-5.0927" layer="94"/>
<rectangle x1="0.6223" y1="-5.1181" x2="1.9431" y2="-5.0927" layer="94"/>
<rectangle x1="2.3241" y1="-5.1181" x2="3.3401" y2="-5.0927" layer="94"/>
<rectangle x1="3.6703" y1="-5.1181" x2="5.2451" y2="-5.0927" layer="94"/>
<rectangle x1="-5.2451" y1="-5.0927" x2="-4.5847" y2="-5.0673" layer="94"/>
<rectangle x1="-4.1275" y1="-5.0927" x2="-2.9591" y2="-5.0673" layer="94"/>
<rectangle x1="-2.4765" y1="-5.0927" x2="0.2159" y2="-5.0673" layer="94"/>
<rectangle x1="0.6477" y1="-5.0927" x2="1.9685" y2="-5.0673" layer="94"/>
<rectangle x1="2.3241" y1="-5.0927" x2="3.3655" y2="-5.0673" layer="94"/>
<rectangle x1="3.6703" y1="-5.0927" x2="5.2451" y2="-5.0673" layer="94"/>
<rectangle x1="-5.2451" y1="-5.0673" x2="-4.5847" y2="-5.0419" layer="94"/>
<rectangle x1="-4.1021" y1="-5.0673" x2="-2.9337" y2="-5.0419" layer="94"/>
<rectangle x1="-2.4511" y1="-5.0673" x2="0.2413" y2="-5.0419" layer="94"/>
<rectangle x1="0.6477" y1="-5.0673" x2="1.9939" y2="-5.0419" layer="94"/>
<rectangle x1="2.3495" y1="-5.0673" x2="3.3655" y2="-5.0419" layer="94"/>
<rectangle x1="3.6703" y1="-5.0673" x2="5.2451" y2="-5.0419" layer="94"/>
<rectangle x1="-5.2451" y1="-5.0419" x2="-4.5847" y2="-5.0165" layer="94"/>
<rectangle x1="-4.1021" y1="-5.0419" x2="-2.9337" y2="-5.0165" layer="94"/>
<rectangle x1="-2.4511" y1="-5.0419" x2="0.2667" y2="-5.0165" layer="94"/>
<rectangle x1="0.6731" y1="-5.0419" x2="1.9939" y2="-5.0165" layer="94"/>
<rectangle x1="2.3495" y1="-5.0419" x2="3.3655" y2="-5.0165" layer="94"/>
<rectangle x1="3.6703" y1="-5.0419" x2="5.2451" y2="-5.0165" layer="94"/>
<rectangle x1="-5.2451" y1="-5.0165" x2="-4.5593" y2="-4.9911" layer="94"/>
<rectangle x1="-4.0767" y1="-5.0165" x2="-2.9083" y2="-4.9911" layer="94"/>
<rectangle x1="-2.4257" y1="-5.0165" x2="0.2921" y2="-4.9911" layer="94"/>
<rectangle x1="0.6985" y1="-5.0165" x2="2.0193" y2="-4.9911" layer="94"/>
<rectangle x1="2.3749" y1="-5.0165" x2="3.3655" y2="-4.9911" layer="94"/>
<rectangle x1="3.6703" y1="-5.0165" x2="5.2451" y2="-4.9911" layer="94"/>
<rectangle x1="-5.2451" y1="-4.9911" x2="-4.5593" y2="-4.9657" layer="94"/>
<rectangle x1="-4.0767" y1="-4.9911" x2="-2.9083" y2="-4.9657" layer="94"/>
<rectangle x1="-2.4257" y1="-4.9911" x2="0.3175" y2="-4.9657" layer="94"/>
<rectangle x1="0.7239" y1="-4.9911" x2="2.0193" y2="-4.9657" layer="94"/>
<rectangle x1="2.3749" y1="-4.9911" x2="3.3655" y2="-4.9657" layer="94"/>
<rectangle x1="3.6703" y1="-4.9911" x2="5.2451" y2="-4.9657" layer="94"/>
<rectangle x1="-5.2451" y1="-4.9657" x2="-4.5339" y2="-4.9403" layer="94"/>
<rectangle x1="-4.0767" y1="-4.9657" x2="-2.8829" y2="-4.9403" layer="94"/>
<rectangle x1="-2.4257" y1="-4.9657" x2="0.3429" y2="-4.9403" layer="94"/>
<rectangle x1="0.7239" y1="-4.9657" x2="2.0447" y2="-4.9403" layer="94"/>
<rectangle x1="2.4003" y1="-4.9657" x2="3.3909" y2="-4.9403" layer="94"/>
<rectangle x1="3.6703" y1="-4.9657" x2="5.2451" y2="-4.9403" layer="94"/>
<rectangle x1="-5.2451" y1="-4.9403" x2="-4.5339" y2="-4.9149" layer="94"/>
<rectangle x1="-4.0513" y1="-4.9403" x2="-2.8829" y2="-4.9149" layer="94"/>
<rectangle x1="-2.4003" y1="-4.9403" x2="0.3683" y2="-4.9149" layer="94"/>
<rectangle x1="0.7493" y1="-4.9403" x2="2.0701" y2="-4.9149" layer="94"/>
<rectangle x1="2.4003" y1="-4.9403" x2="3.3655" y2="-4.9149" layer="94"/>
<rectangle x1="3.6957" y1="-4.9403" x2="5.2451" y2="-4.9149" layer="94"/>
<rectangle x1="-5.2451" y1="-4.9149" x2="-4.5339" y2="-4.8895" layer="94"/>
<rectangle x1="-4.0513" y1="-4.9149" x2="-2.8575" y2="-4.8895" layer="94"/>
<rectangle x1="-2.4003" y1="-4.9149" x2="0.3937" y2="-4.8895" layer="94"/>
<rectangle x1="0.7747" y1="-4.9149" x2="2.0701" y2="-4.8895" layer="94"/>
<rectangle x1="2.4003" y1="-4.9149" x2="3.3909" y2="-4.8895" layer="94"/>
<rectangle x1="3.6957" y1="-4.9149" x2="5.2451" y2="-4.8895" layer="94"/>
<rectangle x1="-5.2451" y1="-4.8895" x2="-4.5085" y2="-4.8641" layer="94"/>
<rectangle x1="-4.0259" y1="-4.8895" x2="-2.8575" y2="-4.8641" layer="94"/>
<rectangle x1="-2.3749" y1="-4.8895" x2="0.3937" y2="-4.8641" layer="94"/>
<rectangle x1="0.7747" y1="-4.8895" x2="2.0955" y2="-4.8641" layer="94"/>
<rectangle x1="2.4257" y1="-4.8895" x2="3.3909" y2="-4.8641" layer="94"/>
<rectangle x1="3.6957" y1="-4.8895" x2="5.2451" y2="-4.8641" layer="94"/>
<rectangle x1="-5.2451" y1="-4.8641" x2="-4.5085" y2="-4.8387" layer="94"/>
<rectangle x1="-4.0259" y1="-4.8641" x2="-2.8575" y2="-4.8387" layer="94"/>
<rectangle x1="-2.3749" y1="-4.8641" x2="0.4191" y2="-4.8387" layer="94"/>
<rectangle x1="0.8001" y1="-4.8641" x2="2.0955" y2="-4.8387" layer="94"/>
<rectangle x1="2.4257" y1="-4.8641" x2="3.3909" y2="-4.8387" layer="94"/>
<rectangle x1="3.6957" y1="-4.8641" x2="5.2451" y2="-4.8387" layer="94"/>
<rectangle x1="-5.2451" y1="-4.8387" x2="-4.4831" y2="-4.8133" layer="94"/>
<rectangle x1="-4.0005" y1="-4.8387" x2="-2.8321" y2="-4.8133" layer="94"/>
<rectangle x1="-2.3495" y1="-4.8387" x2="0.4445" y2="-4.8133" layer="94"/>
<rectangle x1="0.8255" y1="-4.8387" x2="2.1209" y2="-4.8133" layer="94"/>
<rectangle x1="2.4511" y1="-4.8387" x2="3.3909" y2="-4.8133" layer="94"/>
<rectangle x1="3.6957" y1="-4.8387" x2="5.2451" y2="-4.8133" layer="94"/>
<rectangle x1="-5.2451" y1="-4.8133" x2="-4.4831" y2="-4.7879" layer="94"/>
<rectangle x1="-4.0005" y1="-4.8133" x2="-2.8321" y2="-4.7879" layer="94"/>
<rectangle x1="-2.3495" y1="-4.8133" x2="0.4699" y2="-4.7879" layer="94"/>
<rectangle x1="0.8255" y1="-4.8133" x2="2.1209" y2="-4.7879" layer="94"/>
<rectangle x1="2.4511" y1="-4.8133" x2="3.3909" y2="-4.7879" layer="94"/>
<rectangle x1="3.6957" y1="-4.8133" x2="5.2451" y2="-4.7879" layer="94"/>
<rectangle x1="-5.2451" y1="-4.7879" x2="-4.4577" y2="-4.7625" layer="94"/>
<rectangle x1="-4.0005" y1="-4.7879" x2="-2.8067" y2="-4.7625" layer="94"/>
<rectangle x1="-2.3495" y1="-4.7879" x2="0.4953" y2="-4.7625" layer="94"/>
<rectangle x1="0.8509" y1="-4.7879" x2="2.1209" y2="-4.7625" layer="94"/>
<rectangle x1="2.4511" y1="-4.7879" x2="3.3909" y2="-4.7625" layer="94"/>
<rectangle x1="3.6957" y1="-4.7879" x2="5.2451" y2="-4.7625" layer="94"/>
<rectangle x1="-5.2451" y1="-4.7625" x2="-4.4577" y2="-4.7371" layer="94"/>
<rectangle x1="-3.9751" y1="-4.7625" x2="-2.8067" y2="-4.7371" layer="94"/>
<rectangle x1="-2.3241" y1="-4.7625" x2="0.4953" y2="-4.7371" layer="94"/>
<rectangle x1="0.8763" y1="-4.7625" x2="2.1463" y2="-4.7371" layer="94"/>
<rectangle x1="2.4765" y1="-4.7625" x2="3.3909" y2="-4.7371" layer="94"/>
<rectangle x1="3.6957" y1="-4.7625" x2="5.2451" y2="-4.7371" layer="94"/>
<rectangle x1="-5.2451" y1="-4.7371" x2="-4.4577" y2="-4.7117" layer="94"/>
<rectangle x1="-3.9751" y1="-4.7371" x2="-2.8067" y2="-4.7117" layer="94"/>
<rectangle x1="-2.3241" y1="-4.7371" x2="0.5207" y2="-4.7117" layer="94"/>
<rectangle x1="0.8763" y1="-4.7371" x2="2.1463" y2="-4.7117" layer="94"/>
<rectangle x1="2.4765" y1="-4.7371" x2="3.3909" y2="-4.7117" layer="94"/>
<rectangle x1="3.6957" y1="-4.7371" x2="5.2451" y2="-4.7117" layer="94"/>
<rectangle x1="-5.2451" y1="-4.7117" x2="-4.4323" y2="-4.6863" layer="94"/>
<rectangle x1="-3.9497" y1="-4.7117" x2="-2.7813" y2="-4.6863" layer="94"/>
<rectangle x1="-2.2987" y1="-4.7117" x2="0.5461" y2="-4.6863" layer="94"/>
<rectangle x1="0.9017" y1="-4.7117" x2="2.1717" y2="-4.6863" layer="94"/>
<rectangle x1="2.5019" y1="-4.7117" x2="3.3655" y2="-4.6863" layer="94"/>
<rectangle x1="3.6703" y1="-4.7117" x2="5.2451" y2="-4.6863" layer="94"/>
<rectangle x1="-5.2451" y1="-4.6863" x2="-4.4323" y2="-4.6609" layer="94"/>
<rectangle x1="-3.9497" y1="-4.6863" x2="-2.7813" y2="-4.6609" layer="94"/>
<rectangle x1="-2.2987" y1="-4.6863" x2="0.5461" y2="-4.6609" layer="94"/>
<rectangle x1="0.9017" y1="-4.6863" x2="2.1717" y2="-4.6609" layer="94"/>
<rectangle x1="2.5019" y1="-4.6863" x2="3.3655" y2="-4.6609" layer="94"/>
<rectangle x1="3.6703" y1="-4.6863" x2="5.2451" y2="-4.6609" layer="94"/>
<rectangle x1="-5.2451" y1="-4.6609" x2="-4.4069" y2="-4.6355" layer="94"/>
<rectangle x1="-3.9497" y1="-4.6609" x2="-2.7559" y2="-4.6355" layer="94"/>
<rectangle x1="-2.2733" y1="-4.6609" x2="0.5715" y2="-4.6355" layer="94"/>
<rectangle x1="0.9271" y1="-4.6609" x2="2.1717" y2="-4.6355" layer="94"/>
<rectangle x1="2.5019" y1="-4.6609" x2="3.3655" y2="-4.6355" layer="94"/>
<rectangle x1="3.6703" y1="-4.6609" x2="5.2451" y2="-4.6355" layer="94"/>
<rectangle x1="-5.2451" y1="-4.6355" x2="-4.4069" y2="-4.6101" layer="94"/>
<rectangle x1="-3.9243" y1="-4.6355" x2="-2.7559" y2="-4.6101" layer="94"/>
<rectangle x1="-2.2733" y1="-4.6355" x2="0.5715" y2="-4.6101" layer="94"/>
<rectangle x1="0.9271" y1="-4.6355" x2="2.1971" y2="-4.6101" layer="94"/>
<rectangle x1="2.5019" y1="-4.6355" x2="3.3655" y2="-4.6101" layer="94"/>
<rectangle x1="3.6703" y1="-4.6355" x2="5.2451" y2="-4.6101" layer="94"/>
<rectangle x1="-5.2451" y1="-4.6101" x2="-4.3815" y2="-4.5847" layer="94"/>
<rectangle x1="-3.9243" y1="-4.6101" x2="-2.7305" y2="-4.5847" layer="94"/>
<rectangle x1="-2.2733" y1="-4.6101" x2="0.5969" y2="-4.5847" layer="94"/>
<rectangle x1="0.9525" y1="-4.6101" x2="2.1971" y2="-4.5847" layer="94"/>
<rectangle x1="2.5273" y1="-4.6101" x2="3.3655" y2="-4.5847" layer="94"/>
<rectangle x1="3.6703" y1="-4.6101" x2="5.2451" y2="-4.5847" layer="94"/>
<rectangle x1="-5.2451" y1="-4.5847" x2="-4.3815" y2="-4.5593" layer="94"/>
<rectangle x1="-3.8989" y1="-4.5847" x2="-2.7305" y2="-4.5593" layer="94"/>
<rectangle x1="-2.2479" y1="-4.5847" x2="0.6223" y2="-4.5593" layer="94"/>
<rectangle x1="0.9525" y1="-4.5847" x2="2.1971" y2="-4.5593" layer="94"/>
<rectangle x1="2.5273" y1="-4.5847" x2="3.3655" y2="-4.5593" layer="94"/>
<rectangle x1="3.6703" y1="-4.5847" x2="5.2451" y2="-4.5593" layer="94"/>
<rectangle x1="-5.2451" y1="-4.5593" x2="-4.3815" y2="-4.5339" layer="94"/>
<rectangle x1="-3.8989" y1="-4.5593" x2="-2.7305" y2="-4.5339" layer="94"/>
<rectangle x1="-2.2479" y1="-4.5593" x2="0.6223" y2="-4.5339" layer="94"/>
<rectangle x1="0.9779" y1="-4.5593" x2="2.2225" y2="-4.5339" layer="94"/>
<rectangle x1="2.5273" y1="-4.5593" x2="3.3655" y2="-4.5339" layer="94"/>
<rectangle x1="3.6449" y1="-4.5593" x2="5.2451" y2="-4.5339" layer="94"/>
<rectangle x1="-5.2451" y1="-4.5339" x2="-4.3561" y2="-4.5085" layer="94"/>
<rectangle x1="-3.8735" y1="-4.5339" x2="-2.7051" y2="-4.5085" layer="94"/>
<rectangle x1="-2.2225" y1="-4.5339" x2="0.6477" y2="-4.5085" layer="94"/>
<rectangle x1="0.9779" y1="-4.5339" x2="2.2225" y2="-4.5085" layer="94"/>
<rectangle x1="2.5273" y1="-4.5339" x2="3.3655" y2="-4.5085" layer="94"/>
<rectangle x1="3.6449" y1="-4.5339" x2="5.2451" y2="-4.5085" layer="94"/>
<rectangle x1="-5.2451" y1="-4.5085" x2="-4.3561" y2="-4.4831" layer="94"/>
<rectangle x1="-3.8735" y1="-4.5085" x2="-2.7051" y2="-4.4831" layer="94"/>
<rectangle x1="-2.2225" y1="-4.5085" x2="0.6477" y2="-4.4831" layer="94"/>
<rectangle x1="1.0033" y1="-4.5085" x2="2.2225" y2="-4.4831" layer="94"/>
<rectangle x1="2.5527" y1="-4.5085" x2="3.3909" y2="-4.4831" layer="94"/>
<rectangle x1="3.6195" y1="-4.5085" x2="5.2451" y2="-4.4831" layer="94"/>
<rectangle x1="-5.2451" y1="-4.4831" x2="-4.3307" y2="-4.4577" layer="94"/>
<rectangle x1="-3.8735" y1="-4.4831" x2="-2.6797" y2="-4.4577" layer="94"/>
<rectangle x1="-2.1971" y1="-4.4831" x2="0.6731" y2="-4.4577" layer="94"/>
<rectangle x1="1.0033" y1="-4.4831" x2="2.2479" y2="-4.4577" layer="94"/>
<rectangle x1="2.5527" y1="-4.4831" x2="3.4163" y2="-4.4577" layer="94"/>
<rectangle x1="3.5941" y1="-4.4831" x2="5.2451" y2="-4.4577" layer="94"/>
<rectangle x1="-5.2451" y1="-4.4577" x2="-4.3307" y2="-4.4323" layer="94"/>
<rectangle x1="-3.8481" y1="-4.4577" x2="-2.6797" y2="-4.4323" layer="94"/>
<rectangle x1="-2.1971" y1="-4.4577" x2="0.6731" y2="-4.4323" layer="94"/>
<rectangle x1="1.0287" y1="-4.4577" x2="2.2479" y2="-4.4323" layer="94"/>
<rectangle x1="2.5527" y1="-4.4577" x2="3.4671" y2="-4.4323" layer="94"/>
<rectangle x1="3.5433" y1="-4.4577" x2="5.2451" y2="-4.4323" layer="94"/>
<rectangle x1="-5.2451" y1="-4.4323" x2="-4.3307" y2="-4.4069" layer="94"/>
<rectangle x1="-3.8481" y1="-4.4323" x2="-2.6543" y2="-4.4069" layer="94"/>
<rectangle x1="-2.1971" y1="-4.4323" x2="0.6985" y2="-4.4069" layer="94"/>
<rectangle x1="1.0287" y1="-4.4323" x2="2.2479" y2="-4.4069" layer="94"/>
<rectangle x1="2.5527" y1="-4.4323" x2="5.2451" y2="-4.4069" layer="94"/>
<rectangle x1="-5.2451" y1="-4.4069" x2="-4.3053" y2="-4.3815" layer="94"/>
<rectangle x1="-3.8227" y1="-4.4069" x2="-2.6543" y2="-4.3815" layer="94"/>
<rectangle x1="-2.1717" y1="-4.4069" x2="0.6985" y2="-4.3815" layer="94"/>
<rectangle x1="1.0287" y1="-4.4069" x2="2.2479" y2="-4.3815" layer="94"/>
<rectangle x1="2.5527" y1="-4.4069" x2="5.2451" y2="-4.3815" layer="94"/>
<rectangle x1="-5.2451" y1="-4.3815" x2="-4.3053" y2="-4.3561" layer="94"/>
<rectangle x1="-3.8227" y1="-4.3815" x2="-2.6543" y2="-4.3561" layer="94"/>
<rectangle x1="-2.1717" y1="-4.3815" x2="0.7239" y2="-4.3561" layer="94"/>
<rectangle x1="1.0541" y1="-4.3815" x2="2.2479" y2="-4.3561" layer="94"/>
<rectangle x1="2.5781" y1="-4.3815" x2="5.2451" y2="-4.3561" layer="94"/>
<rectangle x1="-5.2451" y1="-4.3561" x2="-4.2799" y2="-4.3307" layer="94"/>
<rectangle x1="-3.7973" y1="-4.3561" x2="-2.6289" y2="-4.3307" layer="94"/>
<rectangle x1="-2.1463" y1="-4.3561" x2="0.7239" y2="-4.3307" layer="94"/>
<rectangle x1="1.0541" y1="-4.3561" x2="2.2733" y2="-4.3307" layer="94"/>
<rectangle x1="2.5781" y1="-4.3561" x2="5.2451" y2="-4.3307" layer="94"/>
<rectangle x1="-5.2451" y1="-4.3307" x2="-4.2799" y2="-4.3053" layer="94"/>
<rectangle x1="-3.7973" y1="-4.3307" x2="-2.6289" y2="-4.3053" layer="94"/>
<rectangle x1="-2.1463" y1="-4.3307" x2="0.7493" y2="-4.3053" layer="94"/>
<rectangle x1="1.0795" y1="-4.3307" x2="2.2733" y2="-4.3053" layer="94"/>
<rectangle x1="2.5781" y1="-4.3307" x2="5.2451" y2="-4.3053" layer="94"/>
<rectangle x1="-5.2451" y1="-4.3053" x2="-4.2545" y2="-4.2799" layer="94"/>
<rectangle x1="-3.7973" y1="-4.3053" x2="-2.6035" y2="-4.2799" layer="94"/>
<rectangle x1="-2.1463" y1="-4.3053" x2="0.7493" y2="-4.2799" layer="94"/>
<rectangle x1="1.0795" y1="-4.3053" x2="2.2733" y2="-4.2799" layer="94"/>
<rectangle x1="2.5781" y1="-4.3053" x2="5.2451" y2="-4.2799" layer="94"/>
<rectangle x1="-5.2451" y1="-4.2799" x2="-4.2545" y2="-4.2545" layer="94"/>
<rectangle x1="-3.7719" y1="-4.2799" x2="-2.6035" y2="-4.2545" layer="94"/>
<rectangle x1="-2.1209" y1="-4.2799" x2="0.7493" y2="-4.2545" layer="94"/>
<rectangle x1="1.0795" y1="-4.2799" x2="2.2733" y2="-4.2545" layer="94"/>
<rectangle x1="2.5781" y1="-4.2799" x2="5.2451" y2="-4.2545" layer="94"/>
<rectangle x1="-5.2451" y1="-4.2545" x2="-4.2545" y2="-4.2291" layer="94"/>
<rectangle x1="-3.7719" y1="-4.2545" x2="-2.5781" y2="-4.2291" layer="94"/>
<rectangle x1="-2.1209" y1="-4.2545" x2="0.7747" y2="-4.2291" layer="94"/>
<rectangle x1="1.1049" y1="-4.2545" x2="2.2733" y2="-4.2291" layer="94"/>
<rectangle x1="2.5781" y1="-4.2545" x2="5.2451" y2="-4.2291" layer="94"/>
<rectangle x1="-5.2451" y1="-4.2291" x2="-4.2291" y2="-4.2037" layer="94"/>
<rectangle x1="-3.7465" y1="-4.2291" x2="-2.5781" y2="-4.2037" layer="94"/>
<rectangle x1="-2.1209" y1="-4.2291" x2="0.7747" y2="-4.2037" layer="94"/>
<rectangle x1="1.1049" y1="-4.2291" x2="2.2733" y2="-4.2037" layer="94"/>
<rectangle x1="2.5781" y1="-4.2291" x2="5.2451" y2="-4.2037" layer="94"/>
<rectangle x1="-5.2451" y1="-4.2037" x2="-4.2291" y2="-4.1783" layer="94"/>
<rectangle x1="-3.7465" y1="-4.2037" x2="-2.5781" y2="-4.1783" layer="94"/>
<rectangle x1="-2.1209" y1="-4.2037" x2="-1.0541" y2="-4.1783" layer="94"/>
<rectangle x1="-1.0287" y1="-4.2037" x2="0.8001" y2="-4.1783" layer="94"/>
<rectangle x1="1.1049" y1="-4.2037" x2="2.2733" y2="-4.1783" layer="94"/>
<rectangle x1="2.5781" y1="-4.2037" x2="5.2451" y2="-4.1783" layer="94"/>
<rectangle x1="-5.2451" y1="-4.1783" x2="-4.2037" y2="-4.1529" layer="94"/>
<rectangle x1="-3.7211" y1="-4.1783" x2="-2.5527" y2="-4.1529" layer="94"/>
<rectangle x1="-2.1209" y1="-4.1783" x2="-1.1049" y2="-4.1529" layer="94"/>
<rectangle x1="-1.0033" y1="-4.1783" x2="0.8001" y2="-4.1529" layer="94"/>
<rectangle x1="1.1303" y1="-4.1783" x2="2.2733" y2="-4.1529" layer="94"/>
<rectangle x1="2.5781" y1="-4.1783" x2="5.2451" y2="-4.1529" layer="94"/>
<rectangle x1="-5.2451" y1="-4.1529" x2="-4.2037" y2="-4.1275" layer="94"/>
<rectangle x1="-3.7211" y1="-4.1529" x2="-2.5527" y2="-4.1275" layer="94"/>
<rectangle x1="-2.1209" y1="-4.1529" x2="-1.1557" y2="-4.1275" layer="94"/>
<rectangle x1="-1.0033" y1="-4.1529" x2="0.8001" y2="-4.1275" layer="94"/>
<rectangle x1="1.1303" y1="-4.1529" x2="2.2987" y2="-4.1275" layer="94"/>
<rectangle x1="2.6035" y1="-4.1529" x2="5.2451" y2="-4.1275" layer="94"/>
<rectangle x1="-5.2451" y1="-4.1275" x2="-4.1783" y2="-4.1021" layer="94"/>
<rectangle x1="-3.7211" y1="-4.1275" x2="-2.5527" y2="-4.1021" layer="94"/>
<rectangle x1="-2.1209" y1="-4.1275" x2="-1.1811" y2="-4.1021" layer="94"/>
<rectangle x1="-1.0033" y1="-4.1275" x2="0.8255" y2="-4.1021" layer="94"/>
<rectangle x1="1.1303" y1="-4.1275" x2="2.2987" y2="-4.1021" layer="94"/>
<rectangle x1="2.5781" y1="-4.1275" x2="5.2451" y2="-4.1021" layer="94"/>
<rectangle x1="-5.2451" y1="-4.1021" x2="-4.1783" y2="-4.0767" layer="94"/>
<rectangle x1="-3.6957" y1="-4.1021" x2="-2.5527" y2="-4.0767" layer="94"/>
<rectangle x1="-2.1209" y1="-4.1021" x2="-1.2065" y2="-4.0767" layer="94"/>
<rectangle x1="-0.9779" y1="-4.1021" x2="0.8255" y2="-4.0767" layer="94"/>
<rectangle x1="1.1557" y1="-4.1021" x2="2.2987" y2="-4.0767" layer="94"/>
<rectangle x1="2.5781" y1="-4.1021" x2="5.2451" y2="-4.0767" layer="94"/>
<rectangle x1="-5.2451" y1="-4.0767" x2="-4.1783" y2="-4.0513" layer="94"/>
<rectangle x1="-3.6957" y1="-4.0767" x2="-2.5781" y2="-4.0513" layer="94"/>
<rectangle x1="-2.1209" y1="-4.0767" x2="-1.2573" y2="-4.0513" layer="94"/>
<rectangle x1="-0.9779" y1="-4.0767" x2="0.8255" y2="-4.0513" layer="94"/>
<rectangle x1="1.1557" y1="-4.0767" x2="2.2733" y2="-4.0513" layer="94"/>
<rectangle x1="2.5781" y1="-4.0767" x2="5.2451" y2="-4.0513" layer="94"/>
<rectangle x1="-5.2451" y1="-4.0513" x2="-4.1529" y2="-4.0259" layer="94"/>
<rectangle x1="-3.6703" y1="-4.0513" x2="-2.6035" y2="-4.0259" layer="94"/>
<rectangle x1="-2.1209" y1="-4.0513" x2="-1.2827" y2="-4.0259" layer="94"/>
<rectangle x1="-0.9525" y1="-4.0513" x2="0.8509" y2="-4.0259" layer="94"/>
<rectangle x1="1.1557" y1="-4.0513" x2="2.2987" y2="-4.0259" layer="94"/>
<rectangle x1="2.6035" y1="-4.0513" x2="5.2451" y2="-4.0259" layer="94"/>
<rectangle x1="-5.2451" y1="-4.0259" x2="-4.1529" y2="-4.0005" layer="94"/>
<rectangle x1="-3.6703" y1="-4.0259" x2="-2.6543" y2="-4.0005" layer="94"/>
<rectangle x1="-2.1209" y1="-4.0259" x2="-1.3081" y2="-4.0005" layer="94"/>
<rectangle x1="-0.9525" y1="-4.0259" x2="0.8509" y2="-4.0005" layer="94"/>
<rectangle x1="1.1811" y1="-4.0259" x2="2.2987" y2="-4.0005" layer="94"/>
<rectangle x1="2.5781" y1="-4.0259" x2="5.2451" y2="-4.0005" layer="94"/>
<rectangle x1="-5.2451" y1="-4.0005" x2="-4.1275" y2="-3.9751" layer="94"/>
<rectangle x1="-3.6449" y1="-4.0005" x2="-2.7305" y2="-3.9751" layer="94"/>
<rectangle x1="-2.1209" y1="-4.0005" x2="-1.3081" y2="-3.9751" layer="94"/>
<rectangle x1="-0.9271" y1="-4.0005" x2="0.8509" y2="-3.9751" layer="94"/>
<rectangle x1="1.1811" y1="-4.0005" x2="2.2987" y2="-3.9751" layer="94"/>
<rectangle x1="2.5781" y1="-4.0005" x2="5.2451" y2="-3.9751" layer="94"/>
<rectangle x1="-5.2451" y1="-3.9751" x2="-4.1275" y2="-3.9497" layer="94"/>
<rectangle x1="-3.6449" y1="-3.9751" x2="-2.7813" y2="-3.9497" layer="94"/>
<rectangle x1="-2.1463" y1="-3.9751" x2="-1.3335" y2="-3.9497" layer="94"/>
<rectangle x1="-0.9271" y1="-3.9751" x2="0.8763" y2="-3.9497" layer="94"/>
<rectangle x1="1.1811" y1="-3.9751" x2="2.2733" y2="-3.9497" layer="94"/>
<rectangle x1="2.5781" y1="-3.9751" x2="5.2451" y2="-3.9497" layer="94"/>
<rectangle x1="-5.2451" y1="-3.9497" x2="-4.1021" y2="-3.9243" layer="94"/>
<rectangle x1="-3.6449" y1="-3.9497" x2="-2.8321" y2="-3.9243" layer="94"/>
<rectangle x1="-2.1463" y1="-3.9497" x2="-1.3589" y2="-3.9243" layer="94"/>
<rectangle x1="-0.9017" y1="-3.9497" x2="0.8763" y2="-3.9243" layer="94"/>
<rectangle x1="1.1811" y1="-3.9497" x2="2.2987" y2="-3.9243" layer="94"/>
<rectangle x1="2.6035" y1="-3.9497" x2="5.2451" y2="-3.9243" layer="94"/>
<rectangle x1="-5.2451" y1="-3.9243" x2="-4.1021" y2="-3.8989" layer="94"/>
<rectangle x1="-3.6195" y1="-3.9243" x2="-2.9083" y2="-3.8989" layer="94"/>
<rectangle x1="-2.1717" y1="-3.9243" x2="-1.3843" y2="-3.8989" layer="94"/>
<rectangle x1="-0.9017" y1="-3.9243" x2="0.8763" y2="-3.8989" layer="94"/>
<rectangle x1="1.2065" y1="-3.9243" x2="2.2733" y2="-3.8989" layer="94"/>
<rectangle x1="2.5781" y1="-3.9243" x2="5.2451" y2="-3.8989" layer="94"/>
<rectangle x1="-5.2451" y1="-3.8989" x2="-4.1021" y2="-3.8735" layer="94"/>
<rectangle x1="-3.6195" y1="-3.8989" x2="-2.9591" y2="-3.8735" layer="94"/>
<rectangle x1="-2.1717" y1="-3.8989" x2="-1.3843" y2="-3.8735" layer="94"/>
<rectangle x1="-0.8763" y1="-3.8989" x2="0.8763" y2="-3.8735" layer="94"/>
<rectangle x1="1.2065" y1="-3.8989" x2="2.2733" y2="-3.8735" layer="94"/>
<rectangle x1="2.5781" y1="-3.8989" x2="5.2451" y2="-3.8735" layer="94"/>
<rectangle x1="-5.2451" y1="-3.8735" x2="-4.0767" y2="-3.8481" layer="94"/>
<rectangle x1="-3.5941" y1="-3.8735" x2="-3.0099" y2="-3.8481" layer="94"/>
<rectangle x1="-2.1971" y1="-3.8735" x2="-1.4097" y2="-3.8481" layer="94"/>
<rectangle x1="-0.8763" y1="-3.8735" x2="0.9017" y2="-3.8481" layer="94"/>
<rectangle x1="1.2065" y1="-3.8735" x2="2.2733" y2="-3.8481" layer="94"/>
<rectangle x1="2.5781" y1="-3.8735" x2="5.2451" y2="-3.8481" layer="94"/>
<rectangle x1="-5.2451" y1="-3.8481" x2="-4.0767" y2="-3.8227" layer="94"/>
<rectangle x1="-3.5941" y1="-3.8481" x2="-3.0861" y2="-3.8227" layer="94"/>
<rectangle x1="-2.1971" y1="-3.8481" x2="-1.4097" y2="-3.8227" layer="94"/>
<rectangle x1="-0.8763" y1="-3.8481" x2="0.9017" y2="-3.8227" layer="94"/>
<rectangle x1="1.2065" y1="-3.8481" x2="2.2733" y2="-3.8227" layer="94"/>
<rectangle x1="2.5781" y1="-3.8481" x2="5.2451" y2="-3.8227" layer="94"/>
<rectangle x1="-5.2451" y1="-3.8227" x2="-4.0513" y2="-3.7973" layer="94"/>
<rectangle x1="-3.5687" y1="-3.8227" x2="-3.1369" y2="-3.7973" layer="94"/>
<rectangle x1="-2.2225" y1="-3.8227" x2="-1.4351" y2="-3.7973" layer="94"/>
<rectangle x1="-0.8509" y1="-3.8227" x2="0.9017" y2="-3.7973" layer="94"/>
<rectangle x1="1.2065" y1="-3.8227" x2="2.2733" y2="-3.7973" layer="94"/>
<rectangle x1="2.5781" y1="-3.8227" x2="5.2451" y2="-3.7973" layer="94"/>
<rectangle x1="-5.2451" y1="-3.7973" x2="-4.0513" y2="-3.7719" layer="94"/>
<rectangle x1="-3.5687" y1="-3.7973" x2="-3.2131" y2="-3.7719" layer="94"/>
<rectangle x1="-2.2479" y1="-3.7973" x2="-1.4351" y2="-3.7719" layer="94"/>
<rectangle x1="-0.8509" y1="-3.7973" x2="0.9017" y2="-3.7719" layer="94"/>
<rectangle x1="1.2319" y1="-3.7973" x2="2.2733" y2="-3.7719" layer="94"/>
<rectangle x1="2.5781" y1="-3.7973" x2="5.2451" y2="-3.7719" layer="94"/>
<rectangle x1="-5.2451" y1="-3.7719" x2="-4.0259" y2="-3.7465" layer="94"/>
<rectangle x1="-3.5687" y1="-3.7719" x2="-3.2639" y2="-3.7465" layer="94"/>
<rectangle x1="-2.2479" y1="-3.7719" x2="-1.4605" y2="-3.7465" layer="94"/>
<rectangle x1="-0.8255" y1="-3.7719" x2="0.9271" y2="-3.7465" layer="94"/>
<rectangle x1="1.2319" y1="-3.7719" x2="2.2479" y2="-3.7465" layer="94"/>
<rectangle x1="2.5781" y1="-3.7719" x2="5.2451" y2="-3.7465" layer="94"/>
<rectangle x1="-5.2451" y1="-3.7465" x2="-4.0259" y2="-3.7211" layer="94"/>
<rectangle x1="-3.5433" y1="-3.7465" x2="-3.3147" y2="-3.7211" layer="94"/>
<rectangle x1="-2.2733" y1="-3.7465" x2="-1.4605" y2="-3.7211" layer="94"/>
<rectangle x1="-0.8255" y1="-3.7465" x2="0.9271" y2="-3.7211" layer="94"/>
<rectangle x1="1.2319" y1="-3.7465" x2="2.2479" y2="-3.7211" layer="94"/>
<rectangle x1="2.5527" y1="-3.7465" x2="5.2451" y2="-3.7211" layer="94"/>
<rectangle x1="-5.2451" y1="-3.7211" x2="-4.0259" y2="-3.6957" layer="94"/>
<rectangle x1="-3.5179" y1="-3.7211" x2="-3.3909" y2="-3.6957" layer="94"/>
<rectangle x1="-2.3241" y1="-3.7211" x2="-1.4859" y2="-3.6957" layer="94"/>
<rectangle x1="-0.8001" y1="-3.7211" x2="0.9271" y2="-3.6957" layer="94"/>
<rectangle x1="1.2319" y1="-3.7211" x2="2.2479" y2="-3.6957" layer="94"/>
<rectangle x1="2.5527" y1="-3.7211" x2="5.2451" y2="-3.6957" layer="94"/>
<rectangle x1="-5.2451" y1="-3.6957" x2="-4.0005" y2="-3.6703" layer="94"/>
<rectangle x1="-2.3495" y1="-3.6957" x2="-1.4859" y2="-3.6703" layer="94"/>
<rectangle x1="-0.8001" y1="-3.6957" x2="0.9271" y2="-3.6703" layer="94"/>
<rectangle x1="1.2319" y1="-3.6957" x2="2.2479" y2="-3.6703" layer="94"/>
<rectangle x1="2.5527" y1="-3.6957" x2="5.2451" y2="-3.6703" layer="94"/>
<rectangle x1="-5.2451" y1="-3.6703" x2="-4.0005" y2="-3.6449" layer="94"/>
<rectangle x1="-2.3749" y1="-3.6703" x2="-1.4859" y2="-3.6449" layer="94"/>
<rectangle x1="-0.8001" y1="-3.6703" x2="0.9271" y2="-3.6449" layer="94"/>
<rectangle x1="1.2319" y1="-3.6703" x2="2.2479" y2="-3.6449" layer="94"/>
<rectangle x1="2.5527" y1="-3.6703" x2="5.2451" y2="-3.6449" layer="94"/>
<rectangle x1="-5.2451" y1="-3.6449" x2="-3.9751" y2="-3.6195" layer="94"/>
<rectangle x1="-2.4257" y1="-3.6449" x2="-1.4859" y2="-3.6195" layer="94"/>
<rectangle x1="-0.7747" y1="-3.6449" x2="0.9525" y2="-3.6195" layer="94"/>
<rectangle x1="1.2573" y1="-3.6449" x2="2.2479" y2="-3.6195" layer="94"/>
<rectangle x1="2.5273" y1="-3.6449" x2="5.2451" y2="-3.6195" layer="94"/>
<rectangle x1="-5.2451" y1="-3.6195" x2="-3.9751" y2="-3.5941" layer="94"/>
<rectangle x1="-2.5019" y1="-3.6195" x2="-1.5113" y2="-3.5941" layer="94"/>
<rectangle x1="-0.7747" y1="-3.6195" x2="0.9525" y2="-3.5941" layer="94"/>
<rectangle x1="1.2573" y1="-3.6195" x2="2.2733" y2="-3.5941" layer="94"/>
<rectangle x1="2.5273" y1="-3.6195" x2="5.2451" y2="-3.5941" layer="94"/>
<rectangle x1="-5.2451" y1="-3.5941" x2="-3.9497" y2="-3.5687" layer="94"/>
<rectangle x1="-2.5527" y1="-3.5941" x2="-1.5113" y2="-3.5687" layer="94"/>
<rectangle x1="-0.7493" y1="-3.5941" x2="0.9525" y2="-3.5687" layer="94"/>
<rectangle x1="1.2573" y1="-3.5941" x2="2.2733" y2="-3.5687" layer="94"/>
<rectangle x1="2.5019" y1="-3.5941" x2="5.2451" y2="-3.5687" layer="94"/>
<rectangle x1="-5.2451" y1="-3.5687" x2="-3.9497" y2="-3.5433" layer="94"/>
<rectangle x1="-2.6035" y1="-3.5687" x2="-1.5113" y2="-3.5433" layer="94"/>
<rectangle x1="-0.7493" y1="-3.5687" x2="0.9525" y2="-3.5433" layer="94"/>
<rectangle x1="1.2573" y1="-3.5687" x2="2.3241" y2="-3.5433" layer="94"/>
<rectangle x1="2.4765" y1="-3.5687" x2="5.2451" y2="-3.5433" layer="94"/>
<rectangle x1="-5.2451" y1="-3.5433" x2="-3.9243" y2="-3.5179" layer="94"/>
<rectangle x1="-2.6797" y1="-3.5433" x2="-1.5113" y2="-3.5179" layer="94"/>
<rectangle x1="-0.7239" y1="-3.5433" x2="0.9525" y2="-3.5179" layer="94"/>
<rectangle x1="1.2573" y1="-3.5433" x2="5.2451" y2="-3.5179" layer="94"/>
<rectangle x1="-5.2451" y1="-3.5179" x2="-3.9243" y2="-3.4925" layer="94"/>
<rectangle x1="-2.7305" y1="-3.5179" x2="-1.5113" y2="-3.4925" layer="94"/>
<rectangle x1="-0.7239" y1="-3.5179" x2="0.9525" y2="-3.4925" layer="94"/>
<rectangle x1="1.2573" y1="-3.5179" x2="5.2451" y2="-3.4925" layer="94"/>
<rectangle x1="-5.2451" y1="-3.4925" x2="-3.8989" y2="-3.4671" layer="94"/>
<rectangle x1="-2.7813" y1="-3.4925" x2="-1.5113" y2="-3.4671" layer="94"/>
<rectangle x1="-0.6985" y1="-3.4925" x2="0.9525" y2="-3.4671" layer="94"/>
<rectangle x1="1.2573" y1="-3.4925" x2="5.2451" y2="-3.4671" layer="94"/>
<rectangle x1="-5.2451" y1="-3.4671" x2="-3.8735" y2="-3.4417" layer="94"/>
<rectangle x1="-2.8575" y1="-3.4671" x2="-1.5113" y2="-3.4417" layer="94"/>
<rectangle x1="-0.6985" y1="-3.4671" x2="0.9525" y2="-3.4417" layer="94"/>
<rectangle x1="1.2573" y1="-3.4671" x2="5.2451" y2="-3.4417" layer="94"/>
<rectangle x1="-5.2451" y1="-3.4417" x2="-3.8735" y2="-3.4163" layer="94"/>
<rectangle x1="-2.9083" y1="-3.4417" x2="-1.5113" y2="-3.4163" layer="94"/>
<rectangle x1="-0.6985" y1="-3.4417" x2="0.9525" y2="-3.4163" layer="94"/>
<rectangle x1="1.2573" y1="-3.4417" x2="5.2451" y2="-3.4163" layer="94"/>
<rectangle x1="-5.2451" y1="-3.4163" x2="-3.8481" y2="-3.3909" layer="94"/>
<rectangle x1="-2.9845" y1="-3.4163" x2="-1.5113" y2="-3.3909" layer="94"/>
<rectangle x1="-0.6731" y1="-3.4163" x2="0.9525" y2="-3.3909" layer="94"/>
<rectangle x1="1.2573" y1="-3.4163" x2="5.2451" y2="-3.3909" layer="94"/>
<rectangle x1="-5.2451" y1="-3.3909" x2="-3.7973" y2="-3.3655" layer="94"/>
<rectangle x1="-3.0353" y1="-3.3909" x2="-1.5113" y2="-3.3655" layer="94"/>
<rectangle x1="-0.6731" y1="-3.3909" x2="0.9779" y2="-3.3655" layer="94"/>
<rectangle x1="1.2573" y1="-3.3909" x2="5.2451" y2="-3.3655" layer="94"/>
<rectangle x1="-5.2451" y1="-3.3655" x2="-3.7719" y2="-3.3401" layer="94"/>
<rectangle x1="-3.0861" y1="-3.3655" x2="-1.5113" y2="-3.3401" layer="94"/>
<rectangle x1="-0.6477" y1="-3.3655" x2="0.9779" y2="-3.3401" layer="94"/>
<rectangle x1="1.2573" y1="-3.3655" x2="5.2451" y2="-3.3401" layer="94"/>
<rectangle x1="-5.2451" y1="-3.3401" x2="-3.7465" y2="-3.3147" layer="94"/>
<rectangle x1="-3.1623" y1="-3.3401" x2="-1.5113" y2="-3.3147" layer="94"/>
<rectangle x1="-0.6477" y1="-3.3401" x2="0.9779" y2="-3.3147" layer="94"/>
<rectangle x1="1.2827" y1="-3.3401" x2="5.2451" y2="-3.3147" layer="94"/>
<rectangle x1="-5.2451" y1="-3.3147" x2="-3.6957" y2="-3.2893" layer="94"/>
<rectangle x1="-3.2131" y1="-3.3147" x2="-2.5781" y2="-3.2893" layer="94"/>
<rectangle x1="-2.4003" y1="-3.3147" x2="-1.5113" y2="-3.2893" layer="94"/>
<rectangle x1="-0.6223" y1="-3.3147" x2="0.9525" y2="-3.2893" layer="94"/>
<rectangle x1="1.2573" y1="-3.3147" x2="5.2451" y2="-3.2893" layer="94"/>
<rectangle x1="-5.2451" y1="-3.2893" x2="-3.6195" y2="-3.2639" layer="94"/>
<rectangle x1="-3.2893" y1="-3.2893" x2="-2.7051" y2="-3.2639" layer="94"/>
<rectangle x1="-2.2479" y1="-3.2893" x2="-1.4859" y2="-3.2639" layer="94"/>
<rectangle x1="-0.6223" y1="-3.2893" x2="0.9779" y2="-3.2639" layer="94"/>
<rectangle x1="1.2573" y1="-3.2893" x2="5.2451" y2="-3.2639" layer="94"/>
<rectangle x1="-5.2451" y1="-3.2639" x2="-3.4925" y2="-3.2385" layer="94"/>
<rectangle x1="-3.4671" y1="-3.2639" x2="-3.4417" y2="-3.2385" layer="94"/>
<rectangle x1="-3.4163" y1="-3.2639" x2="-2.7813" y2="-3.2385" layer="94"/>
<rectangle x1="-2.1717" y1="-3.2639" x2="-1.4859" y2="-3.2385" layer="94"/>
<rectangle x1="-0.5969" y1="-3.2639" x2="0.9779" y2="-3.2385" layer="94"/>
<rectangle x1="1.2573" y1="-3.2639" x2="5.2451" y2="-3.2385" layer="94"/>
<rectangle x1="-5.2451" y1="-3.2385" x2="-2.8321" y2="-3.2131" layer="94"/>
<rectangle x1="-2.1209" y1="-3.2385" x2="-1.4859" y2="-3.2131" layer="94"/>
<rectangle x1="-0.5969" y1="-3.2385" x2="0.9779" y2="-3.2131" layer="94"/>
<rectangle x1="1.2827" y1="-3.2385" x2="5.2451" y2="-3.2131" layer="94"/>
<rectangle x1="-5.2451" y1="-3.2131" x2="-2.8829" y2="-3.1877" layer="94"/>
<rectangle x1="-2.0701" y1="-3.2131" x2="-1.4859" y2="-3.1877" layer="94"/>
<rectangle x1="-0.5969" y1="-3.2131" x2="0.9525" y2="-3.1877" layer="94"/>
<rectangle x1="1.2573" y1="-3.2131" x2="5.2451" y2="-3.1877" layer="94"/>
<rectangle x1="-5.2451" y1="-3.1877" x2="-2.9337" y2="-3.1623" layer="94"/>
<rectangle x1="-2.0447" y1="-3.1877" x2="-1.4605" y2="-3.1623" layer="94"/>
<rectangle x1="-0.5715" y1="-3.1877" x2="0.9779" y2="-3.1623" layer="94"/>
<rectangle x1="1.2827" y1="-3.1877" x2="5.2451" y2="-3.1623" layer="94"/>
<rectangle x1="-5.2451" y1="-3.1623" x2="-2.9591" y2="-3.1369" layer="94"/>
<rectangle x1="-1.9939" y1="-3.1623" x2="-1.4605" y2="-3.1369" layer="94"/>
<rectangle x1="-0.5715" y1="-3.1623" x2="0.9779" y2="-3.1369" layer="94"/>
<rectangle x1="1.2573" y1="-3.1623" x2="5.2451" y2="-3.1369" layer="94"/>
<rectangle x1="-5.2451" y1="-3.1369" x2="-3.0099" y2="-3.1115" layer="94"/>
<rectangle x1="-1.9685" y1="-3.1369" x2="-1.4351" y2="-3.1115" layer="94"/>
<rectangle x1="-0.5461" y1="-3.1369" x2="0.9525" y2="-3.1115" layer="94"/>
<rectangle x1="1.2573" y1="-3.1369" x2="5.2451" y2="-3.1115" layer="94"/>
<rectangle x1="-5.2451" y1="-3.1115" x2="-3.0353" y2="-3.0861" layer="94"/>
<rectangle x1="-1.9431" y1="-3.1115" x2="-1.4351" y2="-3.0861" layer="94"/>
<rectangle x1="-0.5461" y1="-3.1115" x2="0.9525" y2="-3.0861" layer="94"/>
<rectangle x1="1.2573" y1="-3.1115" x2="5.2451" y2="-3.0861" layer="94"/>
<rectangle x1="-5.2451" y1="-3.0861" x2="-3.0607" y2="-3.0607" layer="94"/>
<rectangle x1="-1.9177" y1="-3.0861" x2="-1.4097" y2="-3.0607" layer="94"/>
<rectangle x1="-0.5207" y1="-3.0861" x2="0.9525" y2="-3.0607" layer="94"/>
<rectangle x1="1.2573" y1="-3.0861" x2="5.2451" y2="-3.0607" layer="94"/>
<rectangle x1="-5.2451" y1="-3.0607" x2="-3.0861" y2="-3.0353" layer="94"/>
<rectangle x1="-1.8923" y1="-3.0607" x2="-1.4097" y2="-3.0353" layer="94"/>
<rectangle x1="-0.5207" y1="-3.0607" x2="0.9525" y2="-3.0353" layer="94"/>
<rectangle x1="1.2573" y1="-3.0607" x2="5.2451" y2="-3.0353" layer="94"/>
<rectangle x1="-5.2451" y1="-3.0353" x2="-3.1115" y2="-3.0099" layer="94"/>
<rectangle x1="-1.8669" y1="-3.0353" x2="-1.3843" y2="-3.0099" layer="94"/>
<rectangle x1="-0.4953" y1="-3.0353" x2="0.9525" y2="-3.0099" layer="94"/>
<rectangle x1="1.2573" y1="-3.0353" x2="5.2451" y2="-3.0099" layer="94"/>
<rectangle x1="-5.2451" y1="-3.0099" x2="-3.1369" y2="-2.9845" layer="94"/>
<rectangle x1="-1.8415" y1="-3.0099" x2="-1.3843" y2="-2.9845" layer="94"/>
<rectangle x1="-0.4953" y1="-3.0099" x2="0.9525" y2="-2.9845" layer="94"/>
<rectangle x1="1.2573" y1="-3.0099" x2="5.2451" y2="-2.9845" layer="94"/>
<rectangle x1="-5.2451" y1="-2.9845" x2="-3.1369" y2="-2.9591" layer="94"/>
<rectangle x1="-1.8161" y1="-2.9845" x2="-1.3589" y2="-2.9591" layer="94"/>
<rectangle x1="-0.4953" y1="-2.9845" x2="0.9525" y2="-2.9591" layer="94"/>
<rectangle x1="1.2573" y1="-2.9845" x2="5.2451" y2="-2.9591" layer="94"/>
<rectangle x1="-5.2451" y1="-2.9591" x2="-3.1623" y2="-2.9337" layer="94"/>
<rectangle x1="-1.8161" y1="-2.9591" x2="-1.3335" y2="-2.9337" layer="94"/>
<rectangle x1="-0.4699" y1="-2.9591" x2="0.9525" y2="-2.9337" layer="94"/>
<rectangle x1="1.2573" y1="-2.9591" x2="5.2451" y2="-2.9337" layer="94"/>
<rectangle x1="-5.2451" y1="-2.9337" x2="-3.1877" y2="-2.9083" layer="94"/>
<rectangle x1="-1.7907" y1="-2.9337" x2="-1.3081" y2="-2.9083" layer="94"/>
<rectangle x1="-0.4699" y1="-2.9337" x2="0.9271" y2="-2.9083" layer="94"/>
<rectangle x1="1.2573" y1="-2.9337" x2="5.2451" y2="-2.9083" layer="94"/>
<rectangle x1="-5.2451" y1="-2.9083" x2="-3.1877" y2="-2.8829" layer="94"/>
<rectangle x1="-1.7653" y1="-2.9083" x2="-1.2827" y2="-2.8829" layer="94"/>
<rectangle x1="-0.4445" y1="-2.9083" x2="0.9271" y2="-2.8829" layer="94"/>
<rectangle x1="1.2319" y1="-2.9083" x2="5.2451" y2="-2.8829" layer="94"/>
<rectangle x1="-5.2451" y1="-2.8829" x2="-3.2131" y2="-2.8575" layer="94"/>
<rectangle x1="-1.7653" y1="-2.8829" x2="-1.2573" y2="-2.8575" layer="94"/>
<rectangle x1="-0.4445" y1="-2.8829" x2="0.9271" y2="-2.8575" layer="94"/>
<rectangle x1="1.2319" y1="-2.8829" x2="5.2451" y2="-2.8575" layer="94"/>
<rectangle x1="-5.2451" y1="-2.8575" x2="-3.2131" y2="-2.8321" layer="94"/>
<rectangle x1="-1.7399" y1="-2.8575" x2="-1.2319" y2="-2.8321" layer="94"/>
<rectangle x1="-0.4191" y1="-2.8575" x2="0.9271" y2="-2.8321" layer="94"/>
<rectangle x1="1.2319" y1="-2.8575" x2="5.2451" y2="-2.8321" layer="94"/>
<rectangle x1="-5.2451" y1="-2.8321" x2="-3.2385" y2="-2.8067" layer="94"/>
<rectangle x1="-1.7399" y1="-2.8321" x2="-1.2065" y2="-2.8067" layer="94"/>
<rectangle x1="-0.4191" y1="-2.8321" x2="0.9271" y2="-2.8067" layer="94"/>
<rectangle x1="1.2319" y1="-2.8321" x2="5.2451" y2="-2.8067" layer="94"/>
<rectangle x1="-5.2451" y1="-2.8067" x2="-3.2385" y2="-2.7813" layer="94"/>
<rectangle x1="-1.7145" y1="-2.8067" x2="-1.1811" y2="-2.7813" layer="94"/>
<rectangle x1="-0.3937" y1="-2.8067" x2="0.9017" y2="-2.7813" layer="94"/>
<rectangle x1="1.2319" y1="-2.8067" x2="5.2451" y2="-2.7813" layer="94"/>
<rectangle x1="-5.2451" y1="-2.7813" x2="-3.2385" y2="-2.7559" layer="94"/>
<rectangle x1="-1.7145" y1="-2.7813" x2="-1.1303" y2="-2.7559" layer="94"/>
<rectangle x1="-0.3937" y1="-2.7813" x2="0.9017" y2="-2.7559" layer="94"/>
<rectangle x1="1.2065" y1="-2.7813" x2="5.2451" y2="-2.7559" layer="94"/>
<rectangle x1="-5.2451" y1="-2.7559" x2="-3.2639" y2="-2.7305" layer="94"/>
<rectangle x1="-1.7145" y1="-2.7559" x2="-1.1049" y2="-2.7305" layer="94"/>
<rectangle x1="-0.3937" y1="-2.7559" x2="0.9017" y2="-2.7305" layer="94"/>
<rectangle x1="1.2065" y1="-2.7559" x2="5.2451" y2="-2.7305" layer="94"/>
<rectangle x1="-5.2451" y1="-2.7305" x2="-3.2639" y2="-2.7051" layer="94"/>
<rectangle x1="-1.6891" y1="-2.7305" x2="-1.0541" y2="-2.7051" layer="94"/>
<rectangle x1="-0.3683" y1="-2.7305" x2="0.9017" y2="-2.7051" layer="94"/>
<rectangle x1="1.2065" y1="-2.7305" x2="5.2451" y2="-2.7051" layer="94"/>
<rectangle x1="-5.2451" y1="-2.7051" x2="-3.2639" y2="-2.6797" layer="94"/>
<rectangle x1="-1.6891" y1="-2.7051" x2="-0.9779" y2="-2.6797" layer="94"/>
<rectangle x1="-0.4191" y1="-2.7051" x2="0.9017" y2="-2.6797" layer="94"/>
<rectangle x1="1.2065" y1="-2.7051" x2="5.2451" y2="-2.6797" layer="94"/>
<rectangle x1="-5.2451" y1="-2.6797" x2="-3.2893" y2="-2.6543" layer="94"/>
<rectangle x1="-1.6891" y1="-2.6797" x2="-0.9017" y2="-2.6543" layer="94"/>
<rectangle x1="-0.4953" y1="-2.6797" x2="0.9271" y2="-2.6543" layer="94"/>
<rectangle x1="1.1811" y1="-2.6797" x2="5.2451" y2="-2.6543" layer="94"/>
<rectangle x1="-5.2451" y1="-2.6543" x2="-3.2893" y2="-2.6289" layer="94"/>
<rectangle x1="-1.6891" y1="-2.6543" x2="-0.7493" y2="-2.6289" layer="94"/>
<rectangle x1="-0.7239" y1="-2.6543" x2="-0.6985" y2="-2.6289" layer="94"/>
<rectangle x1="-0.6731" y1="-2.6543" x2="0.9525" y2="-2.6289" layer="94"/>
<rectangle x1="1.1557" y1="-2.6543" x2="5.2451" y2="-2.6289" layer="94"/>
<rectangle x1="-5.2451" y1="-2.6289" x2="-3.2893" y2="-2.6035" layer="94"/>
<rectangle x1="-1.6891" y1="-2.6289" x2="0.9779" y2="-2.6035" layer="94"/>
<rectangle x1="1.1303" y1="-2.6289" x2="5.2451" y2="-2.6035" layer="94"/>
<rectangle x1="-5.2451" y1="-2.6035" x2="-3.2893" y2="-2.5781" layer="94"/>
<rectangle x1="-1.6891" y1="-2.6035" x2="5.2451" y2="-2.5781" layer="94"/>
<rectangle x1="-5.2451" y1="-2.5781" x2="-3.2893" y2="-2.5527" layer="94"/>
<rectangle x1="-1.6637" y1="-2.5781" x2="5.2451" y2="-2.5527" layer="94"/>
<rectangle x1="-5.2451" y1="-2.5527" x2="-3.2893" y2="-2.5273" layer="94"/>
<rectangle x1="-1.6637" y1="-2.5527" x2="5.2451" y2="-2.5273" layer="94"/>
<rectangle x1="-5.2451" y1="-2.5273" x2="-3.2893" y2="-2.5019" layer="94"/>
<rectangle x1="-1.6637" y1="-2.5273" x2="5.2451" y2="-2.5019" layer="94"/>
<rectangle x1="-5.2451" y1="-2.5019" x2="-3.2893" y2="-2.4765" layer="94"/>
<rectangle x1="-1.6637" y1="-2.5019" x2="5.2451" y2="-2.4765" layer="94"/>
<rectangle x1="-5.2451" y1="-2.4765" x2="-3.2893" y2="-2.4511" layer="94"/>
<rectangle x1="-1.6637" y1="-2.4765" x2="5.2451" y2="-2.4511" layer="94"/>
<rectangle x1="-5.2451" y1="-2.4511" x2="-3.2893" y2="-2.4257" layer="94"/>
<rectangle x1="-1.6637" y1="-2.4511" x2="5.2451" y2="-2.4257" layer="94"/>
<rectangle x1="-5.2451" y1="-2.4257" x2="-3.2893" y2="-2.4003" layer="94"/>
<rectangle x1="-1.6637" y1="-2.4257" x2="5.2451" y2="-2.4003" layer="94"/>
<rectangle x1="-5.2451" y1="-2.4003" x2="-3.2893" y2="-2.3749" layer="94"/>
<rectangle x1="-1.6891" y1="-2.4003" x2="5.2451" y2="-2.3749" layer="94"/>
<rectangle x1="-5.2451" y1="-2.3749" x2="-3.2893" y2="-2.3495" layer="94"/>
<rectangle x1="-1.6891" y1="-2.3749" x2="5.2451" y2="-2.3495" layer="94"/>
<rectangle x1="-5.2451" y1="-2.3495" x2="-3.2893" y2="-2.3241" layer="94"/>
<rectangle x1="-1.6891" y1="-2.3495" x2="5.2451" y2="-2.3241" layer="94"/>
<rectangle x1="-5.2451" y1="-2.3241" x2="-3.2639" y2="-2.2987" layer="94"/>
<rectangle x1="-1.6891" y1="-2.3241" x2="5.2451" y2="-2.2987" layer="94"/>
<rectangle x1="-5.2451" y1="-2.2987" x2="-3.2639" y2="-2.2733" layer="94"/>
<rectangle x1="-1.6891" y1="-2.2987" x2="5.2451" y2="-2.2733" layer="94"/>
<rectangle x1="-5.2451" y1="-2.2733" x2="-3.2639" y2="-2.2479" layer="94"/>
<rectangle x1="-1.7145" y1="-2.2733" x2="5.2451" y2="-2.2479" layer="94"/>
<rectangle x1="-5.2451" y1="-2.2479" x2="-3.2639" y2="-2.2225" layer="94"/>
<rectangle x1="-1.7145" y1="-2.2479" x2="5.2451" y2="-2.2225" layer="94"/>
<rectangle x1="-5.2451" y1="-2.2225" x2="-3.2385" y2="-2.1971" layer="94"/>
<rectangle x1="-1.7145" y1="-2.2225" x2="5.2451" y2="-2.1971" layer="94"/>
<rectangle x1="-5.2451" y1="-2.1971" x2="-3.2385" y2="-2.1717" layer="94"/>
<rectangle x1="-1.7399" y1="-2.1971" x2="5.2451" y2="-2.1717" layer="94"/>
<rectangle x1="-5.2451" y1="-2.1717" x2="-3.2131" y2="-2.1463" layer="94"/>
<rectangle x1="-1.7399" y1="-2.1717" x2="5.2451" y2="-2.1463" layer="94"/>
<rectangle x1="-5.2451" y1="-2.1463" x2="-3.2131" y2="-2.1209" layer="94"/>
<rectangle x1="-1.7653" y1="-2.1463" x2="5.2451" y2="-2.1209" layer="94"/>
<rectangle x1="-5.2451" y1="-2.1209" x2="-3.1877" y2="-2.0955" layer="94"/>
<rectangle x1="-1.7653" y1="-2.1209" x2="5.2451" y2="-2.0955" layer="94"/>
<rectangle x1="-5.2451" y1="-2.0955" x2="-3.1877" y2="-2.0701" layer="94"/>
<rectangle x1="-1.7907" y1="-2.0955" x2="5.2451" y2="-2.0701" layer="94"/>
<rectangle x1="-5.2451" y1="-2.0701" x2="-3.1623" y2="-2.0447" layer="94"/>
<rectangle x1="-1.7907" y1="-2.0701" x2="5.2451" y2="-2.0447" layer="94"/>
<rectangle x1="-5.2451" y1="-2.0447" x2="-3.1623" y2="-2.0193" layer="94"/>
<rectangle x1="-1.8161" y1="-2.0447" x2="5.2451" y2="-2.0193" layer="94"/>
<rectangle x1="-5.2451" y1="-2.0193" x2="-3.1369" y2="-1.9939" layer="94"/>
<rectangle x1="-1.8415" y1="-2.0193" x2="5.2451" y2="-1.9939" layer="94"/>
<rectangle x1="-5.2451" y1="-1.9939" x2="-3.1115" y2="-1.9685" layer="94"/>
<rectangle x1="-1.8415" y1="-1.9939" x2="5.2451" y2="-1.9685" layer="94"/>
<rectangle x1="-5.2451" y1="-1.9685" x2="-3.0861" y2="-1.9431" layer="94"/>
<rectangle x1="-1.8669" y1="-1.9685" x2="5.2451" y2="-1.9431" layer="94"/>
<rectangle x1="-5.2451" y1="-1.9431" x2="-3.0607" y2="-1.9177" layer="94"/>
<rectangle x1="-1.8923" y1="-1.9431" x2="5.2451" y2="-1.9177" layer="94"/>
<rectangle x1="-5.2451" y1="-1.9177" x2="-3.0353" y2="-1.8923" layer="94"/>
<rectangle x1="-1.9177" y1="-1.9177" x2="5.2451" y2="-1.8923" layer="94"/>
<rectangle x1="-5.2451" y1="-1.8923" x2="-3.0099" y2="-1.8669" layer="94"/>
<rectangle x1="-1.9431" y1="-1.8923" x2="5.2451" y2="-1.8669" layer="94"/>
<rectangle x1="-5.2451" y1="-1.8669" x2="-2.9845" y2="-1.8415" layer="94"/>
<rectangle x1="-1.9685" y1="-1.8669" x2="5.2451" y2="-1.8415" layer="94"/>
<rectangle x1="-5.2451" y1="-1.8415" x2="-2.9591" y2="-1.8161" layer="94"/>
<rectangle x1="-2.0193" y1="-1.8415" x2="5.2451" y2="-1.8161" layer="94"/>
<rectangle x1="-5.2451" y1="-1.8161" x2="-2.9083" y2="-1.7907" layer="94"/>
<rectangle x1="-2.0447" y1="-1.8161" x2="5.2451" y2="-1.7907" layer="94"/>
<rectangle x1="-5.2451" y1="-1.7907" x2="-2.8575" y2="-1.7653" layer="94"/>
<rectangle x1="-2.0955" y1="-1.7907" x2="5.2451" y2="-1.7653" layer="94"/>
<rectangle x1="-5.2451" y1="-1.7653" x2="-2.8067" y2="-1.7399" layer="94"/>
<rectangle x1="-2.1463" y1="-1.7653" x2="-1.5621" y2="-1.7399" layer="94"/>
<rectangle x1="-1.3081" y1="-1.7653" x2="5.2451" y2="-1.7399" layer="94"/>
<rectangle x1="-5.2451" y1="-1.7399" x2="-2.7559" y2="-1.7145" layer="94"/>
<rectangle x1="-2.2225" y1="-1.7399" x2="-1.6637" y2="-1.7145" layer="94"/>
<rectangle x1="-1.2319" y1="-1.7399" x2="5.2451" y2="-1.7145" layer="94"/>
<rectangle x1="-5.2451" y1="-1.7145" x2="-2.6797" y2="-1.6891" layer="94"/>
<rectangle x1="-2.2987" y1="-1.7145" x2="-1.7145" y2="-1.6891" layer="94"/>
<rectangle x1="-1.1811" y1="-1.7145" x2="5.2451" y2="-1.6891" layer="94"/>
<rectangle x1="-5.2451" y1="-1.6891" x2="-1.7653" y2="-1.6637" layer="94"/>
<rectangle x1="-1.1303" y1="-1.6891" x2="5.2451" y2="-1.6637" layer="94"/>
<rectangle x1="-5.2451" y1="-1.6637" x2="-1.8415" y2="-1.6383" layer="94"/>
<rectangle x1="-1.1049" y1="-1.6637" x2="5.2451" y2="-1.6383" layer="94"/>
<rectangle x1="-5.2451" y1="-1.6383" x2="-1.8923" y2="-1.6129" layer="94"/>
<rectangle x1="-1.0541" y1="-1.6383" x2="5.2451" y2="-1.6129" layer="94"/>
<rectangle x1="-5.2451" y1="-1.6129" x2="-1.9431" y2="-1.5875" layer="94"/>
<rectangle x1="-1.0287" y1="-1.6129" x2="5.2451" y2="-1.5875" layer="94"/>
<rectangle x1="-5.2451" y1="-1.5875" x2="-2.0193" y2="-1.5621" layer="94"/>
<rectangle x1="-1.0287" y1="-1.5875" x2="5.2451" y2="-1.5621" layer="94"/>
<rectangle x1="-5.2451" y1="-1.5621" x2="-2.0701" y2="-1.5367" layer="94"/>
<rectangle x1="-1.0033" y1="-1.5621" x2="5.2451" y2="-1.5367" layer="94"/>
<rectangle x1="-5.2451" y1="-1.5367" x2="-2.1463" y2="-1.5113" layer="94"/>
<rectangle x1="-0.9779" y1="-1.5367" x2="5.2451" y2="-1.5113" layer="94"/>
<rectangle x1="-5.2451" y1="-1.5113" x2="-2.1971" y2="-1.4859" layer="94"/>
<rectangle x1="-0.9525" y1="-1.5113" x2="5.2451" y2="-1.4859" layer="94"/>
<rectangle x1="-5.2451" y1="-1.4859" x2="-2.2479" y2="-1.4605" layer="94"/>
<rectangle x1="-0.9525" y1="-1.4859" x2="5.2451" y2="-1.4605" layer="94"/>
<rectangle x1="-5.2451" y1="-1.4605" x2="-2.3241" y2="-1.4351" layer="94"/>
<rectangle x1="-0.9271" y1="-1.4605" x2="5.2451" y2="-1.4351" layer="94"/>
<rectangle x1="-5.2451" y1="-1.4351" x2="-2.3749" y2="-1.4097" layer="94"/>
<rectangle x1="-0.9271" y1="-1.4351" x2="5.2451" y2="-1.4097" layer="94"/>
<rectangle x1="-5.2451" y1="-1.4097" x2="-2.4257" y2="-1.3843" layer="94"/>
<rectangle x1="-0.9271" y1="-1.4097" x2="5.2451" y2="-1.3843" layer="94"/>
<rectangle x1="-5.2451" y1="-1.3843" x2="-2.5019" y2="-1.3589" layer="94"/>
<rectangle x1="-0.9017" y1="-1.3843" x2="5.2451" y2="-1.3589" layer="94"/>
<rectangle x1="-5.2451" y1="-1.3589" x2="-2.5273" y2="-1.3335" layer="94"/>
<rectangle x1="-0.9017" y1="-1.3589" x2="5.2451" y2="-1.3335" layer="94"/>
<rectangle x1="-5.2451" y1="-1.3335" x2="-2.5781" y2="-1.3081" layer="94"/>
<rectangle x1="-1.4859" y1="-1.3335" x2="-1.4097" y2="-1.3081" layer="94"/>
<rectangle x1="-0.8763" y1="-1.3335" x2="5.2451" y2="-1.3081" layer="94"/>
<rectangle x1="-5.2451" y1="-1.3081" x2="-2.6035" y2="-1.2827" layer="94"/>
<rectangle x1="-1.5367" y1="-1.3081" x2="-1.3589" y2="-1.2827" layer="94"/>
<rectangle x1="-0.8763" y1="-1.3081" x2="5.2451" y2="-1.2827" layer="94"/>
<rectangle x1="-5.2451" y1="-1.2827" x2="-2.6289" y2="-1.2573" layer="94"/>
<rectangle x1="-1.6129" y1="-1.2827" x2="-1.3335" y2="-1.2573" layer="94"/>
<rectangle x1="-0.8509" y1="-1.2827" x2="5.2451" y2="-1.2573" layer="94"/>
<rectangle x1="-5.2451" y1="-1.2573" x2="-2.6543" y2="-1.2319" layer="94"/>
<rectangle x1="-1.6637" y1="-1.2573" x2="-1.3335" y2="-1.2319" layer="94"/>
<rectangle x1="-0.8509" y1="-1.2573" x2="5.2451" y2="-1.2319" layer="94"/>
<rectangle x1="-5.2451" y1="-1.2319" x2="-2.6797" y2="-1.2065" layer="94"/>
<rectangle x1="-1.7399" y1="-1.2319" x2="-1.3081" y2="-1.2065" layer="94"/>
<rectangle x1="-0.8509" y1="-1.2319" x2="5.2451" y2="-1.2065" layer="94"/>
<rectangle x1="-5.2451" y1="-1.2065" x2="-2.6797" y2="-1.1811" layer="94"/>
<rectangle x1="-1.7907" y1="-1.2065" x2="-1.3081" y2="-1.1811" layer="94"/>
<rectangle x1="-0.8255" y1="-1.2065" x2="5.2451" y2="-1.1811" layer="94"/>
<rectangle x1="-5.2451" y1="-1.1811" x2="-2.7051" y2="-1.1557" layer="94"/>
<rectangle x1="-1.8415" y1="-1.1811" x2="-1.3081" y2="-1.1557" layer="94"/>
<rectangle x1="-0.8255" y1="-1.1811" x2="5.2451" y2="-1.1557" layer="94"/>
<rectangle x1="-5.2451" y1="-1.1557" x2="-2.7051" y2="-1.1303" layer="94"/>
<rectangle x1="-1.9177" y1="-1.1557" x2="-1.2827" y2="-1.1303" layer="94"/>
<rectangle x1="-0.8001" y1="-1.1557" x2="5.2451" y2="-1.1303" layer="94"/>
<rectangle x1="-5.2451" y1="-1.1303" x2="-2.7305" y2="-1.1049" layer="94"/>
<rectangle x1="-1.9685" y1="-1.1303" x2="-1.2827" y2="-1.1049" layer="94"/>
<rectangle x1="-0.8001" y1="-1.1303" x2="5.2451" y2="-1.1049" layer="94"/>
<rectangle x1="-5.2451" y1="-1.1049" x2="-2.7305" y2="-1.0795" layer="94"/>
<rectangle x1="-2.0193" y1="-1.1049" x2="-1.2573" y2="-1.0795" layer="94"/>
<rectangle x1="-0.7747" y1="-1.1049" x2="5.2451" y2="-1.0795" layer="94"/>
<rectangle x1="-5.2451" y1="-1.0795" x2="-2.7559" y2="-1.0541" layer="94"/>
<rectangle x1="-2.0955" y1="-1.0795" x2="-1.2573" y2="-1.0541" layer="94"/>
<rectangle x1="-0.7747" y1="-1.0795" x2="5.2451" y2="-1.0541" layer="94"/>
<rectangle x1="-5.2451" y1="-1.0541" x2="-2.7559" y2="-1.0287" layer="94"/>
<rectangle x1="-2.1463" y1="-1.0541" x2="-1.2319" y2="-1.0287" layer="94"/>
<rectangle x1="-0.7493" y1="-1.0541" x2="5.2451" y2="-1.0287" layer="94"/>
<rectangle x1="-5.2451" y1="-1.0287" x2="-2.7559" y2="-1.0033" layer="94"/>
<rectangle x1="-2.1971" y1="-1.0287" x2="-1.2319" y2="-1.0033" layer="94"/>
<rectangle x1="-0.7493" y1="-1.0287" x2="5.2451" y2="-1.0033" layer="94"/>
<rectangle x1="-5.2451" y1="-1.0033" x2="-2.7813" y2="-0.9779" layer="94"/>
<rectangle x1="-2.2733" y1="-1.0033" x2="-1.2319" y2="-0.9779" layer="94"/>
<rectangle x1="-0.7493" y1="-1.0033" x2="5.2451" y2="-0.9779" layer="94"/>
<rectangle x1="-5.2451" y1="-0.9779" x2="-2.7813" y2="-0.9525" layer="94"/>
<rectangle x1="-2.2987" y1="-0.9779" x2="-1.2065" y2="-0.9525" layer="94"/>
<rectangle x1="-0.7239" y1="-0.9779" x2="5.2451" y2="-0.9525" layer="94"/>
<rectangle x1="-5.2451" y1="-0.9525" x2="-2.7813" y2="-0.9271" layer="94"/>
<rectangle x1="-2.3241" y1="-0.9525" x2="-1.2065" y2="-0.9271" layer="94"/>
<rectangle x1="-0.7239" y1="-0.9525" x2="5.2451" y2="-0.9271" layer="94"/>
<rectangle x1="-5.2451" y1="-0.9271" x2="-2.7813" y2="-0.9017" layer="94"/>
<rectangle x1="-2.3495" y1="-0.9271" x2="-1.1811" y2="-0.9017" layer="94"/>
<rectangle x1="-0.6985" y1="-0.9271" x2="5.2451" y2="-0.9017" layer="94"/>
<rectangle x1="-5.2451" y1="-0.9017" x2="-2.7813" y2="-0.8763" layer="94"/>
<rectangle x1="-2.3495" y1="-0.9017" x2="-1.1811" y2="-0.8763" layer="94"/>
<rectangle x1="-0.6985" y1="-0.9017" x2="5.2451" y2="-0.8763" layer="94"/>
<rectangle x1="-5.2451" y1="-0.8763" x2="-2.7813" y2="-0.8509" layer="94"/>
<rectangle x1="-2.3495" y1="-0.8763" x2="-1.1557" y2="-0.8509" layer="94"/>
<rectangle x1="-0.6731" y1="-0.8763" x2="5.2451" y2="-0.8509" layer="94"/>
<rectangle x1="-5.2451" y1="-0.8509" x2="-2.7813" y2="-0.8255" layer="94"/>
<rectangle x1="-2.3241" y1="-0.8509" x2="-1.1557" y2="-0.8255" layer="94"/>
<rectangle x1="-0.6731" y1="-0.8509" x2="5.2451" y2="-0.8255" layer="94"/>
<rectangle x1="-5.2451" y1="-0.8255" x2="-2.7813" y2="-0.8001" layer="94"/>
<rectangle x1="-2.3241" y1="-0.8255" x2="-1.1303" y2="-0.8001" layer="94"/>
<rectangle x1="-0.6731" y1="-0.8255" x2="5.2451" y2="-0.8001" layer="94"/>
<rectangle x1="-5.2451" y1="-0.8001" x2="-2.7813" y2="-0.7747" layer="94"/>
<rectangle x1="-2.2987" y1="-0.8001" x2="-1.1303" y2="-0.7747" layer="94"/>
<rectangle x1="-0.6477" y1="-0.8001" x2="5.2451" y2="-0.7747" layer="94"/>
<rectangle x1="-5.2451" y1="-0.7747" x2="-2.7559" y2="-0.7493" layer="94"/>
<rectangle x1="-2.2987" y1="-0.7747" x2="-1.1303" y2="-0.7493" layer="94"/>
<rectangle x1="-0.6477" y1="-0.7747" x2="5.2451" y2="-0.7493" layer="94"/>
<rectangle x1="-5.2451" y1="-0.7493" x2="-2.7559" y2="-0.7239" layer="94"/>
<rectangle x1="-2.2987" y1="-0.7493" x2="-1.1049" y2="-0.7239" layer="94"/>
<rectangle x1="-0.6223" y1="-0.7493" x2="5.2451" y2="-0.7239" layer="94"/>
<rectangle x1="-5.2451" y1="-0.7239" x2="-2.7559" y2="-0.6985" layer="94"/>
<rectangle x1="-2.2733" y1="-0.7239" x2="-1.1049" y2="-0.6985" layer="94"/>
<rectangle x1="-0.6223" y1="-0.7239" x2="5.2451" y2="-0.6985" layer="94"/>
<rectangle x1="-5.2451" y1="-0.6985" x2="-2.7559" y2="-0.6731" layer="94"/>
<rectangle x1="-2.2733" y1="-0.6985" x2="-1.0795" y2="-0.6731" layer="94"/>
<rectangle x1="-0.5969" y1="-0.6985" x2="5.2451" y2="-0.6731" layer="94"/>
<rectangle x1="-5.2451" y1="-0.6731" x2="-2.7305" y2="-0.6477" layer="94"/>
<rectangle x1="-2.2479" y1="-0.6731" x2="-1.0795" y2="-0.6477" layer="94"/>
<rectangle x1="-0.5969" y1="-0.6731" x2="5.2451" y2="-0.6477" layer="94"/>
<rectangle x1="-5.2451" y1="-0.6477" x2="-2.7305" y2="-0.6223" layer="94"/>
<rectangle x1="-2.2479" y1="-0.6477" x2="-1.0541" y2="-0.6223" layer="94"/>
<rectangle x1="-0.5969" y1="-0.6477" x2="5.2451" y2="-0.6223" layer="94"/>
<rectangle x1="-5.2451" y1="-0.6223" x2="-2.7051" y2="-0.5969" layer="94"/>
<rectangle x1="-2.2225" y1="-0.6223" x2="-1.0541" y2="-0.5969" layer="94"/>
<rectangle x1="-0.5715" y1="-0.6223" x2="5.2451" y2="-0.5969" layer="94"/>
<rectangle x1="-5.2451" y1="-0.5969" x2="-2.7051" y2="-0.5715" layer="94"/>
<rectangle x1="-2.2225" y1="-0.5969" x2="-1.0541" y2="-0.5715" layer="94"/>
<rectangle x1="-0.5715" y1="-0.5969" x2="5.2451" y2="-0.5715" layer="94"/>
<rectangle x1="-5.2451" y1="-0.5715" x2="-2.6797" y2="-0.5461" layer="94"/>
<rectangle x1="-2.2225" y1="-0.5715" x2="-1.0287" y2="-0.5461" layer="94"/>
<rectangle x1="-0.5461" y1="-0.5715" x2="5.2451" y2="-0.5461" layer="94"/>
<rectangle x1="-5.2451" y1="-0.5461" x2="-2.6797" y2="-0.5207" layer="94"/>
<rectangle x1="-2.1971" y1="-0.5461" x2="-1.0287" y2="-0.5207" layer="94"/>
<rectangle x1="-0.5461" y1="-0.5461" x2="5.2451" y2="-0.5207" layer="94"/>
<rectangle x1="-5.2451" y1="-0.5207" x2="-2.6797" y2="-0.4953" layer="94"/>
<rectangle x1="-2.1971" y1="-0.5207" x2="-1.0033" y2="-0.4953" layer="94"/>
<rectangle x1="-0.5207" y1="-0.5207" x2="5.2451" y2="-0.4953" layer="94"/>
<rectangle x1="-5.2451" y1="-0.4953" x2="-2.6543" y2="-0.4699" layer="94"/>
<rectangle x1="-2.1717" y1="-0.4953" x2="-1.0033" y2="-0.4699" layer="94"/>
<rectangle x1="-0.5207" y1="-0.4953" x2="5.2451" y2="-0.4699" layer="94"/>
<rectangle x1="-5.2451" y1="-0.4699" x2="-2.6543" y2="-0.4445" layer="94"/>
<rectangle x1="-2.1717" y1="-0.4699" x2="-0.9779" y2="-0.4445" layer="94"/>
<rectangle x1="-0.5207" y1="-0.4699" x2="5.2451" y2="-0.4445" layer="94"/>
<rectangle x1="-5.2451" y1="-0.4445" x2="-2.6289" y2="-0.4191" layer="94"/>
<rectangle x1="-2.1463" y1="-0.4445" x2="-0.9779" y2="-0.4191" layer="94"/>
<rectangle x1="-0.4953" y1="-0.4445" x2="5.2451" y2="-0.4191" layer="94"/>
<rectangle x1="-5.2451" y1="-0.4191" x2="-2.6289" y2="-0.3937" layer="94"/>
<rectangle x1="-2.1463" y1="-0.4191" x2="-0.9779" y2="-0.3937" layer="94"/>
<rectangle x1="-0.4953" y1="-0.4191" x2="5.2451" y2="-0.3937" layer="94"/>
<rectangle x1="-5.2451" y1="-0.3937" x2="-2.6035" y2="-0.3683" layer="94"/>
<rectangle x1="-2.1463" y1="-0.3937" x2="-0.9525" y2="-0.3683" layer="94"/>
<rectangle x1="-0.4699" y1="-0.3937" x2="5.2451" y2="-0.3683" layer="94"/>
<rectangle x1="-5.2451" y1="-0.3683" x2="-2.6035" y2="-0.3429" layer="94"/>
<rectangle x1="-2.1209" y1="-0.3683" x2="-0.9525" y2="-0.3429" layer="94"/>
<rectangle x1="-0.4699" y1="-0.3683" x2="5.2451" y2="-0.3429" layer="94"/>
<rectangle x1="-5.2451" y1="-0.3429" x2="-2.5781" y2="-0.3175" layer="94"/>
<rectangle x1="-2.1209" y1="-0.3429" x2="-0.9271" y2="-0.3175" layer="94"/>
<rectangle x1="-0.4445" y1="-0.3429" x2="5.2451" y2="-0.3175" layer="94"/>
<rectangle x1="-5.2451" y1="-0.3175" x2="-2.5781" y2="-0.2921" layer="94"/>
<rectangle x1="-2.0955" y1="-0.3175" x2="-0.9271" y2="-0.2921" layer="94"/>
<rectangle x1="-0.4445" y1="-0.3175" x2="5.2451" y2="-0.2921" layer="94"/>
<rectangle x1="-5.2451" y1="-0.2921" x2="-2.5781" y2="-0.2667" layer="94"/>
<rectangle x1="-2.0955" y1="-0.2921" x2="-0.9017" y2="-0.2667" layer="94"/>
<rectangle x1="-0.4445" y1="-0.2921" x2="5.2451" y2="-0.2667" layer="94"/>
<rectangle x1="-5.2451" y1="-0.2667" x2="-2.5527" y2="-0.2413" layer="94"/>
<rectangle x1="-2.0701" y1="-0.2667" x2="-0.9017" y2="-0.2413" layer="94"/>
<rectangle x1="-0.4191" y1="-0.2667" x2="5.2451" y2="-0.2413" layer="94"/>
<rectangle x1="-5.2451" y1="-0.2413" x2="-2.5527" y2="-0.2159" layer="94"/>
<rectangle x1="-2.0701" y1="-0.2413" x2="-0.9017" y2="-0.2159" layer="94"/>
<rectangle x1="-0.4191" y1="-0.2413" x2="5.2451" y2="-0.2159" layer="94"/>
<rectangle x1="-5.2451" y1="-0.2159" x2="-2.5273" y2="-0.1905" layer="94"/>
<rectangle x1="-2.0447" y1="-0.2159" x2="-0.8763" y2="-0.1905" layer="94"/>
<rectangle x1="-0.3937" y1="-0.2159" x2="5.2451" y2="-0.1905" layer="94"/>
<rectangle x1="-5.2451" y1="-0.1905" x2="-2.5273" y2="-0.1651" layer="94"/>
<rectangle x1="-2.0447" y1="-0.1905" x2="-0.8763" y2="-0.1651" layer="94"/>
<rectangle x1="-0.3937" y1="-0.1905" x2="5.2451" y2="-0.1651" layer="94"/>
<rectangle x1="-5.2451" y1="-0.1651" x2="-2.5273" y2="-0.1397" layer="94"/>
<rectangle x1="-2.0447" y1="-0.1651" x2="-0.8509" y2="-0.1397" layer="94"/>
<rectangle x1="-0.3683" y1="-0.1651" x2="5.2451" y2="-0.1397" layer="94"/>
<rectangle x1="-5.2451" y1="-0.1397" x2="-2.5019" y2="-0.1143" layer="94"/>
<rectangle x1="-2.0193" y1="-0.1397" x2="-0.8509" y2="-0.1143" layer="94"/>
<rectangle x1="-0.3683" y1="-0.1397" x2="5.2451" y2="-0.1143" layer="94"/>
<rectangle x1="-5.2451" y1="-0.1143" x2="-2.5019" y2="-0.0889" layer="94"/>
<rectangle x1="-2.0193" y1="-0.1143" x2="-0.8255" y2="-0.0889" layer="94"/>
<rectangle x1="-0.3683" y1="-0.1143" x2="5.2451" y2="-0.0889" layer="94"/>
<rectangle x1="-5.2451" y1="-0.0889" x2="-2.4765" y2="-0.0635" layer="94"/>
<rectangle x1="-1.9939" y1="-0.0889" x2="-0.8255" y2="-0.0635" layer="94"/>
<rectangle x1="-0.3683" y1="-0.0889" x2="5.2451" y2="-0.0635" layer="94"/>
<rectangle x1="-5.2451" y1="-0.0635" x2="-2.4765" y2="-0.0381" layer="94"/>
<rectangle x1="-1.9939" y1="-0.0635" x2="-0.8255" y2="-0.0381" layer="94"/>
<rectangle x1="-0.3429" y1="-0.0635" x2="5.2451" y2="-0.0381" layer="94"/>
<rectangle x1="-5.2451" y1="-0.0381" x2="-2.4511" y2="-0.0127" layer="94"/>
<rectangle x1="-1.9685" y1="-0.0381" x2="-0.8001" y2="-0.0127" layer="94"/>
<rectangle x1="-0.3429" y1="-0.0381" x2="5.2451" y2="-0.0127" layer="94"/>
<rectangle x1="-5.2451" y1="-0.0127" x2="-2.4511" y2="0.0127" layer="94"/>
<rectangle x1="-1.9685" y1="-0.0127" x2="-0.8001" y2="0.0127" layer="94"/>
<rectangle x1="-0.3429" y1="-0.0127" x2="5.2451" y2="0.0127" layer="94"/>
<rectangle x1="-5.2451" y1="0.0127" x2="-2.4257" y2="0.0381" layer="94"/>
<rectangle x1="-1.9685" y1="0.0127" x2="-0.7747" y2="0.0381" layer="94"/>
<rectangle x1="-0.3429" y1="0.0127" x2="5.2451" y2="0.0381" layer="94"/>
<rectangle x1="-5.2451" y1="0.0381" x2="-2.4257" y2="0.0635" layer="94"/>
<rectangle x1="-1.9431" y1="0.0381" x2="-0.7747" y2="0.0635" layer="94"/>
<rectangle x1="-0.3429" y1="0.0381" x2="5.2451" y2="0.0635" layer="94"/>
<rectangle x1="-5.2451" y1="0.0635" x2="-2.4257" y2="0.0889" layer="94"/>
<rectangle x1="-1.9431" y1="0.0635" x2="-0.8001" y2="0.0889" layer="94"/>
<rectangle x1="-0.3429" y1="0.0635" x2="5.2451" y2="0.0889" layer="94"/>
<rectangle x1="-5.2451" y1="0.0889" x2="-2.4003" y2="0.1143" layer="94"/>
<rectangle x1="-1.9177" y1="0.0889" x2="-0.8001" y2="0.1143" layer="94"/>
<rectangle x1="-0.3429" y1="0.0889" x2="5.2451" y2="0.1143" layer="94"/>
<rectangle x1="-5.2451" y1="0.1143" x2="-2.4003" y2="0.1397" layer="94"/>
<rectangle x1="-1.9177" y1="0.1143" x2="-0.8255" y2="0.1397" layer="94"/>
<rectangle x1="-0.3429" y1="0.1143" x2="5.2451" y2="0.1397" layer="94"/>
<rectangle x1="-5.2451" y1="0.1397" x2="-2.3749" y2="0.1651" layer="94"/>
<rectangle x1="-1.8923" y1="0.1397" x2="-0.8763" y2="0.1651" layer="94"/>
<rectangle x1="-0.3429" y1="0.1397" x2="5.2451" y2="0.1651" layer="94"/>
<rectangle x1="-5.2451" y1="0.1651" x2="-2.3749" y2="0.1905" layer="94"/>
<rectangle x1="-1.8923" y1="0.1651" x2="-0.9271" y2="0.1905" layer="94"/>
<rectangle x1="-0.3683" y1="0.1651" x2="5.2451" y2="0.1905" layer="94"/>
<rectangle x1="-5.2451" y1="0.1905" x2="-2.3495" y2="0.2159" layer="94"/>
<rectangle x1="-1.8923" y1="0.1905" x2="-0.9779" y2="0.2159" layer="94"/>
<rectangle x1="-0.3683" y1="0.1905" x2="5.2451" y2="0.2159" layer="94"/>
<rectangle x1="-5.2451" y1="0.2159" x2="-2.3495" y2="0.2413" layer="94"/>
<rectangle x1="-1.8669" y1="0.2159" x2="-1.0541" y2="0.2413" layer="94"/>
<rectangle x1="-0.3683" y1="0.2159" x2="5.2451" y2="0.2413" layer="94"/>
<rectangle x1="-5.2451" y1="0.2413" x2="-2.3495" y2="0.2667" layer="94"/>
<rectangle x1="-1.8669" y1="0.2413" x2="-1.1049" y2="0.2667" layer="94"/>
<rectangle x1="-0.3937" y1="0.2413" x2="5.2451" y2="0.2667" layer="94"/>
<rectangle x1="-5.2451" y1="0.2667" x2="-2.3241" y2="0.2921" layer="94"/>
<rectangle x1="-1.8415" y1="0.2667" x2="-1.1557" y2="0.2921" layer="94"/>
<rectangle x1="-0.3937" y1="0.2667" x2="5.2451" y2="0.2921" layer="94"/>
<rectangle x1="-5.2451" y1="0.2921" x2="-2.3241" y2="0.3175" layer="94"/>
<rectangle x1="-1.8415" y1="0.2921" x2="-1.2319" y2="0.3175" layer="94"/>
<rectangle x1="-0.4191" y1="0.2921" x2="5.2451" y2="0.3175" layer="94"/>
<rectangle x1="-5.2451" y1="0.3175" x2="-2.2987" y2="0.3429" layer="94"/>
<rectangle x1="-1.8161" y1="0.3175" x2="-1.2827" y2="0.3429" layer="94"/>
<rectangle x1="-0.4191" y1="0.3175" x2="5.2451" y2="0.3429" layer="94"/>
<rectangle x1="-5.2451" y1="0.3429" x2="-2.2987" y2="0.3683" layer="94"/>
<rectangle x1="-1.8161" y1="0.3429" x2="-1.3335" y2="0.3683" layer="94"/>
<rectangle x1="-0.4445" y1="0.3429" x2="5.2451" y2="0.3683" layer="94"/>
<rectangle x1="-5.2451" y1="0.3683" x2="-2.2733" y2="0.3937" layer="94"/>
<rectangle x1="-1.8161" y1="0.3683" x2="-1.4097" y2="0.3937" layer="94"/>
<rectangle x1="-0.4699" y1="0.3683" x2="5.2451" y2="0.3937" layer="94"/>
<rectangle x1="-5.2451" y1="0.3937" x2="-2.2733" y2="0.4191" layer="94"/>
<rectangle x1="-1.7907" y1="0.3937" x2="-1.4605" y2="0.4191" layer="94"/>
<rectangle x1="-0.4699" y1="0.3937" x2="5.2451" y2="0.4191" layer="94"/>
<rectangle x1="-5.2451" y1="0.4191" x2="-2.2733" y2="0.4445" layer="94"/>
<rectangle x1="-1.7907" y1="0.4191" x2="-1.5367" y2="0.4445" layer="94"/>
<rectangle x1="-0.4953" y1="0.4191" x2="5.2451" y2="0.4445" layer="94"/>
<rectangle x1="-5.2451" y1="0.4445" x2="-2.2479" y2="0.4699" layer="94"/>
<rectangle x1="-1.7653" y1="0.4445" x2="-1.5875" y2="0.4699" layer="94"/>
<rectangle x1="-0.5207" y1="0.4445" x2="5.2451" y2="0.4699" layer="94"/>
<rectangle x1="-5.2451" y1="0.4699" x2="-2.2479" y2="0.4953" layer="94"/>
<rectangle x1="-1.7145" y1="0.4699" x2="-1.6637" y2="0.4953" layer="94"/>
<rectangle x1="-0.5715" y1="0.4699" x2="5.2451" y2="0.4953" layer="94"/>
<rectangle x1="-5.2451" y1="0.4953" x2="-2.2225" y2="0.5207" layer="94"/>
<rectangle x1="-0.5969" y1="0.4953" x2="5.2451" y2="0.5207" layer="94"/>
<rectangle x1="-5.2451" y1="0.5207" x2="-2.2225" y2="0.5461" layer="94"/>
<rectangle x1="-0.6477" y1="0.5207" x2="5.2451" y2="0.5461" layer="94"/>
<rectangle x1="-5.2451" y1="0.5461" x2="-2.1971" y2="0.5715" layer="94"/>
<rectangle x1="-0.6985" y1="0.5461" x2="5.2451" y2="0.5715" layer="94"/>
<rectangle x1="-5.2451" y1="0.5715" x2="-2.1971" y2="0.5969" layer="94"/>
<rectangle x1="-0.7493" y1="0.5715" x2="5.2451" y2="0.5969" layer="94"/>
<rectangle x1="-5.2451" y1="0.5969" x2="-2.1717" y2="0.6223" layer="94"/>
<rectangle x1="-0.8255" y1="0.5969" x2="5.2451" y2="0.6223" layer="94"/>
<rectangle x1="-5.2451" y1="0.6223" x2="-2.1717" y2="0.6477" layer="94"/>
<rectangle x1="-0.8763" y1="0.6223" x2="5.2451" y2="0.6477" layer="94"/>
<rectangle x1="-5.2451" y1="0.6477" x2="-2.1463" y2="0.6731" layer="94"/>
<rectangle x1="-0.9525" y1="0.6477" x2="5.2451" y2="0.6731" layer="94"/>
<rectangle x1="-5.2451" y1="0.6731" x2="-2.1463" y2="0.6985" layer="94"/>
<rectangle x1="-1.0033" y1="0.6731" x2="5.2451" y2="0.6985" layer="94"/>
<rectangle x1="-5.2451" y1="0.6985" x2="-2.1209" y2="0.7239" layer="94"/>
<rectangle x1="-1.0541" y1="0.6985" x2="5.2451" y2="0.7239" layer="94"/>
<rectangle x1="-5.2451" y1="0.7239" x2="-2.0955" y2="0.7493" layer="94"/>
<rectangle x1="-1.1049" y1="0.7239" x2="5.2451" y2="0.7493" layer="94"/>
<rectangle x1="-5.2197" y1="0.7493" x2="-2.0701" y2="0.7747" layer="94"/>
<rectangle x1="-1.1811" y1="0.7493" x2="5.2451" y2="0.7747" layer="94"/>
<rectangle x1="-5.2197" y1="0.7747" x2="-2.0447" y2="0.8001" layer="94"/>
<rectangle x1="-1.2319" y1="0.7747" x2="5.2451" y2="0.8001" layer="94"/>
<rectangle x1="-5.2197" y1="0.8001" x2="-2.0193" y2="0.8255" layer="94"/>
<rectangle x1="-1.3081" y1="0.8001" x2="5.2197" y2="0.8255" layer="94"/>
<rectangle x1="-5.2197" y1="0.8255" x2="-1.9939" y2="0.8509" layer="94"/>
<rectangle x1="-1.3589" y1="0.8255" x2="5.2197" y2="0.8509" layer="94"/>
<rectangle x1="-5.1943" y1="0.8509" x2="-1.9431" y2="0.8763" layer="94"/>
<rectangle x1="-1.4097" y1="0.8509" x2="5.2197" y2="0.8763" layer="94"/>
<rectangle x1="-5.1943" y1="0.8763" x2="-1.8923" y2="0.9017" layer="94"/>
<rectangle x1="-1.4859" y1="0.8763" x2="5.1943" y2="0.9017" layer="94"/>
<rectangle x1="-5.1943" y1="0.9017" x2="-1.8161" y2="0.9271" layer="94"/>
<rectangle x1="-1.5621" y1="0.9017" x2="5.1943" y2="0.9271" layer="94"/>
<rectangle x1="-5.1689" y1="0.9271" x2="5.1943" y2="0.9525" layer="94"/>
<rectangle x1="-5.1689" y1="0.9525" x2="5.1689" y2="0.9779" layer="94"/>
<rectangle x1="-5.1435" y1="0.9779" x2="5.1689" y2="1.0033" layer="94"/>
<rectangle x1="-5.1435" y1="1.0033" x2="5.1435" y2="1.0287" layer="94"/>
<rectangle x1="-5.1181" y1="1.0287" x2="5.1435" y2="1.0541" layer="94"/>
<rectangle x1="-5.0927" y1="1.0541" x2="5.1181" y2="1.0795" layer="94"/>
<rectangle x1="-5.0927" y1="1.0795" x2="5.0927" y2="1.1049" layer="94"/>
<rectangle x1="-5.0673" y1="1.1049" x2="5.0673" y2="1.1303" layer="94"/>
<rectangle x1="-5.0419" y1="1.1303" x2="5.0673" y2="1.1557" layer="94"/>
<rectangle x1="-5.0165" y1="1.1557" x2="5.0419" y2="1.1811" layer="94"/>
<rectangle x1="-4.9911" y1="1.1811" x2="5.0165" y2="1.2065" layer="94"/>
<rectangle x1="-4.9657" y1="1.2065" x2="4.9911" y2="1.2319" layer="94"/>
<rectangle x1="-4.9403" y1="1.2319" x2="4.9657" y2="1.2573" layer="94"/>
<rectangle x1="-4.9149" y1="1.2573" x2="4.9403" y2="1.2827" layer="94"/>
<rectangle x1="-4.8895" y1="1.2827" x2="4.8895" y2="1.3081" layer="94"/>
<rectangle x1="-4.8387" y1="1.3081" x2="4.8641" y2="1.3335" layer="94"/>
<rectangle x1="-4.8133" y1="1.3335" x2="4.8133" y2="1.3589" layer="94"/>
<rectangle x1="-4.7625" y1="1.3589" x2="4.7625" y2="1.3843" layer="94"/>
<rectangle x1="-4.6863" y1="1.3843" x2="4.7117" y2="1.4097" layer="94"/>
<rectangle x1="-4.6355" y1="1.4097" x2="4.6355" y2="1.4351" layer="94"/>
<rectangle x1="-4.5339" y1="1.4351" x2="4.5339" y2="1.4605" layer="94"/>
</symbol>
<symbol name="L">
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="-2.54" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="IRM">
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<pin name="AC/L" x="-12.7" y="7.62" length="short" direction="in"/>
<pin name="AC/N" x="-12.7" y="-7.62" length="short" direction="in"/>
<pin name="V+" x="12.7" y="7.62" length="short" direction="out" rot="R180"/>
<pin name="V-" x="12.7" y="-7.62" length="short" direction="out" rot="R180"/>
<text x="-7.62" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="5V_REG">
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<pin name="VI" x="-7.62" y="0" length="short"/>
<pin name="GND" x="2.54" y="-10.16" length="short" rot="R90"/>
<pin name="VO" x="12.7" y="0" length="short" rot="R180"/>
<text x="-4.318" y="6.096" size="1.778" layer="95">&gt;NAME</text>
<text x="4.572" y="-9.906" size="1.778" layer="96">&gt;MPN</text>
</symbol>
<symbol name="NCP1117ST33T3G">
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<text x="-7.62" y="5.715" size="1.778" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="2.54" size="1.778" layer="96" ratio="10">&gt;VALUE</text>
<pin name="VIN" x="-10.16" y="0" length="short" direction="pwr"/>
<pin name="VOUT" x="10.16" y="0" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="0" y="-10.16" length="short" direction="pwr" rot="R90"/>
</symbol>
<symbol name="HC-SR501">
<description>&lt;b&gt;PIR Motion Sensor Module DYP-ME003&lt;/b&gt; based on &lt;b&gt;BISS0001&lt;/b&gt; pyroelectic detector</description>
<pin name="GND" x="-2.54" y="-20.32" length="middle" direction="pwr" rot="R90"/>
<pin name="OUT" x="0" y="-20.32" length="middle" direction="out" rot="R90"/>
<pin name="VCC" x="2.54" y="-20.32" length="middle" direction="pwr" rot="R90"/>
<wire x1="-12.7" y1="15.24" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="-12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="-12.7" y2="15.24" width="0.254" layer="94"/>
<circle x="0" y="2.54" radius="10.16" width="0.254" layer="94"/>
<text x="0" y="2.54" size="2.54" layer="94" align="center">PIR</text>
<text x="-12.7" y="20.32" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="17.78" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LED_1210">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="ADS1015">
<wire x1="-17.78" y1="12.7" x2="-17.78" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-12.7" x2="20.32" y2="-12.7" width="0.254" layer="94"/>
<wire x1="20.32" y1="-12.7" x2="20.32" y2="12.7" width="0.254" layer="94"/>
<wire x1="20.32" y1="12.7" x2="-17.78" y2="12.7" width="0.254" layer="94"/>
<pin name="VDD" x="-22.86" y="10.16" length="middle" direction="pwr"/>
<pin name="ADDR" x="-22.86" y="-5.08" length="middle" direction="in"/>
<pin name="GND" x="-22.86" y="-10.16" length="middle" direction="pwr"/>
<pin name="SDA" x="-22.86" y="5.08" length="middle" direction="oc"/>
<pin name="ALERT" x="25.4" y="10.16" length="middle" direction="out" rot="R180"/>
<pin name="SCL" x="-22.86" y="2.54" length="middle" direction="oc"/>
<text x="-17.68" y="13.6" size="1.778" layer="95">&gt;NAME</text>
<text x="-17.78" y="-17.78" size="1.778" layer="96">&gt;MPN</text>
<pin name="AIN0" x="25.4" y="2.54" length="middle" direction="in" rot="R180"/>
<pin name="AIN1" x="25.4" y="0" length="middle" direction="in" rot="R180"/>
<pin name="AIN2" x="25.4" y="-2.54" length="middle" direction="in" rot="R180"/>
<pin name="AIN3" x="25.4" y="-5.08" length="middle" direction="in" rot="R180"/>
<text x="-5.08" y="-2.54" size="5.08" layer="97">ADC</text>
</symbol>
<symbol name="RTRIM">
<wire x1="0.762" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0" x2="-1.8796" y2="1.7526" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.286" y1="1.27" x2="-1.651" y2="2.413" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-3.048" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-2.032" y2="-1.524" width="0.1524" layer="94"/>
<text x="-5.969" y="-3.81" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="A" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="E" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
</symbol>
<symbol name="RM51-3021-85-1012">
<wire x1="5.1" y1="-2.12" x2="5.1" y2="4.99" width="0.254" layer="94"/>
<wire x1="-9.39" y1="6.26" x2="8.39" y2="6.26" width="0.254" layer="94"/>
<wire x1="8.39" y1="6.26" x2="8.39" y2="-8.98" width="0.254" layer="94"/>
<wire x1="8.39" y1="-8.98" x2="-9.39" y2="-8.98" width="0.254" layer="94"/>
<wire x1="-9.39" y1="-8.98" x2="-9.39" y2="6.26" width="0.254" layer="94"/>
<text x="-9.39" y="6.768" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-9.39" y="-10.758" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<wire x1="-7.8" y1="5.1" x2="-2.9" y2="5.1" width="0.2" layer="94"/>
<wire x1="-2.9" y1="5.1" x2="-2.9" y2="2.632" width="0.2" layer="94"/>
<wire x1="-2.9" y1="2.632" x2="-1.3" y2="2.632" width="0.2" layer="94"/>
<wire x1="-1.3" y1="2.632" x2="0.2" y2="2.632" width="0.2" layer="94"/>
<wire x1="0.2" y1="2.632" x2="0.2" y2="-0.068" width="0.2" layer="94"/>
<wire x1="0.2" y1="-0.068" x2="-2.9" y2="-0.068" width="0.2" layer="94"/>
<wire x1="-2.9" y1="-0.068" x2="-6.3" y2="-0.068" width="0.2" layer="94"/>
<wire x1="-6.3" y1="-0.068" x2="-6.3" y2="2.632" width="0.2" layer="94"/>
<wire x1="-6.3" y1="2.632" x2="-2.9" y2="2.632" width="0.2" layer="94"/>
<wire x1="-2.9" y1="-0.068" x2="-2.9" y2="-7.6" width="0.2" layer="94"/>
<wire x1="-2.9" y1="-7.6" x2="-7.6" y2="-7.6" width="0.2" layer="94"/>
<wire x1="-4.7" y1="0.032" x2="-1.3" y2="2.632" width="0.2" layer="94"/>
<wire x1="-7.628" y1="-2.57" x2="-5.088" y2="-2.57" width="0.2" layer="94"/>
<wire x1="-5.088" y1="-2.57" x2="-2.348" y2="-5.21" width="0.2" layer="94" curve="90"/>
<wire x1="-2.348" y1="-5.21" x2="0.192" y2="-2.67" width="0.2" layer="94" curve="90"/>
<wire x1="0.192" y1="-2.67" x2="2.732" y2="-2.67" width="0.2" layer="94"/>
<wire x1="2.732" y1="-2.67" x2="7.472" y2="-7.01" width="0.2" layer="94"/>
<pin name="A2" x="-12.7" y="-7.62" visible="pad" length="middle"/>
<pin name="COM" x="-12.7" y="-2.54" visible="pad" length="middle"/>
<pin name="A1" x="-12.7" y="5.08" visible="pad" length="middle"/>
<pin name="NO" x="5.08" y="10.16" visible="pad" length="middle" rot="R270"/>
</symbol>
<symbol name="MOSFET-N">
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="1.5875" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.5875" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="-1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-2.54" x2="-1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.5875" y1="-1.905" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="1.5875" y2="1.905" width="0.254" layer="94"/>
<wire x1="2.2225" y1="-0.4445" x2="1.905" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.4445" x2="1.27" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.4445" x2="0.9525" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="0.9525" y1="-0.4445" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="2.2225" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="2.2225" y1="0.4445" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="0.9525" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.4445" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.27" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.3175" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.27" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-0.9525" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-0.9525" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="-0.3175" x2="-0.9525" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="0.3175" x2="-1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="-1.5875" y1="0" x2="-0.9525" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="-0.3175" x2="-1.397" y2="0.127" width="0.254" layer="94"/>
<circle x="0" y="1.905" radius="0.254" width="0.254" layer="94"/>
<circle x="0" y="-1.905" radius="0.254" width="0.254" layer="94"/>
<text x="-3.81" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="0" y="2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<text x="-0.635" y="2.2225" size="0.8128" layer="93">D</text>
<text x="-0.635" y="-3.175" size="0.8128" layer="93">S</text>
<text x="-3.4925" y="0" size="0.8128" layer="93">G</text>
<pin name="G" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="D-SCHOTTKY-3P">
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="3" x="0" y="2.54" visible="off" length="short" rot="R270"/>
<wire x1="-2.35" y1="0.67" x2="-2.35" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.35" y1="0" x2="-2.35" y2="-0.57" width="0.1524" layer="94"/>
<wire x1="-2.35" y1="-0.57" x2="-0.91" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.91" y1="0" x2="-2.35" y2="0.67" width="0.1524" layer="94"/>
<wire x1="-0.54" y1="0.74" x2="-0.91" y2="0.74" width="0.1524" layer="94"/>
<wire x1="-0.91" y1="0.74" x2="-0.91" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.91" y1="0" x2="-0.91" y2="-0.64" width="0.1524" layer="94"/>
<wire x1="-0.91" y1="-0.64" x2="-1.28" y2="-0.64" width="0.1524" layer="94"/>
<wire x1="-2.99" y1="1.01" x2="-2.99" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.99" y1="0" x2="-2.99" y2="-1.01" width="0.1524" layer="94"/>
<wire x1="-2.99" y1="-1.01" x2="3.81" y2="-1.01" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-1.01" x2="3.81" y2="1.01" width="0.1524" layer="94"/>
<wire x1="3.81" y1="1.01" x2="-2.99" y2="1.01" width="0.1524" layer="94"/>
<text x="-3.29" y="-2.59" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<wire x1="-2.99" y1="0" x2="-2.35" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.508" width="0" layer="94"/>
<wire x1="2.45" y1="-0.67" x2="2.45" y2="0.57" width="0.1524" layer="94"/>
<wire x1="2.45" y1="0.57" x2="1.01" y2="0" width="0.1524" layer="94"/>
<wire x1="1.01" y1="0" x2="2.45" y2="-0.67" width="0.1524" layer="94"/>
<wire x1="0.64" y1="-0.74" x2="1.01" y2="-0.74" width="0.1524" layer="94"/>
<wire x1="1.01" y1="-0.74" x2="1.01" y2="0" width="0.1524" layer="94"/>
<wire x1="1.01" y1="0" x2="1.01" y2="0.64" width="0.1524" layer="94"/>
<wire x1="1.01" y1="0.64" x2="1.38" y2="0.64" width="0.1524" layer="94"/>
<wire x1="-0.91" y1="0" x2="1.01" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="RPI_ZERO">
<wire x1="-20.32" y1="33.02" x2="22.86" y2="33.02" width="0.254" layer="94"/>
<wire x1="22.86" y1="33.02" x2="22.86" y2="-20.32" width="0.254" layer="94"/>
<wire x1="22.86" y1="-20.32" x2="-20.32" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-20.32" x2="-20.32" y2="33.02" width="0.254" layer="94"/>
<pin name="5V0@1" x="25.4" y="30.48" length="short" direction="sup" rot="R180"/>
<pin name="3V3@1" x="-22.86" y="30.48" length="short" direction="sup"/>
<pin name="GPIO2/SDA1" x="-22.86" y="27.94" length="short"/>
<pin name="5V0@2" x="25.4" y="27.94" length="short" direction="sup" rot="R180"/>
<pin name="GPIO3/SCL1" x="-22.86" y="25.4" length="short"/>
<pin name="GND@8" x="25.4" y="25.4" length="short" direction="sup" rot="R180"/>
<pin name="GPIO4/GCKL" x="-22.86" y="22.86" length="short"/>
<pin name="TXD0/GPIO14" x="25.4" y="22.86" length="short" rot="R180"/>
<pin name="GND@1" x="-22.86" y="20.32" length="short" direction="sup"/>
<pin name="RXD0/GPIO15" x="25.4" y="20.32" length="short" rot="R180"/>
<pin name="GPIO17/GEN0" x="-22.86" y="17.78" length="short"/>
<pin name="GPIO18" x="25.4" y="17.78" length="short" rot="R180"/>
<pin name="GPIO27/GEN2" x="-22.86" y="15.24" length="short"/>
<pin name="GND@4" x="25.4" y="15.24" length="short" direction="sup" rot="R180"/>
<pin name="GPIO22/GEN3" x="-22.86" y="12.7" length="short"/>
<pin name="GEN4/GPIO23" x="25.4" y="12.7" length="short" rot="R180"/>
<pin name="3V3@2" x="-22.86" y="10.16" length="short" direction="sup"/>
<pin name="GEN5/GPIO24" x="25.4" y="10.16" length="short" rot="R180"/>
<pin name="GPIO10/MOSI" x="-22.86" y="7.62" length="short"/>
<pin name="GND@5" x="25.4" y="7.62" length="short" direction="sup" rot="R180"/>
<pin name="GPIO9/MISO" x="-22.86" y="5.08" length="short"/>
<pin name="GEN/6GPIO25" x="25.4" y="5.08" length="short" rot="R180"/>
<pin name="GPIO11/SCLK" x="-22.86" y="2.54" length="short"/>
<pin name="!CE0!/GPIO8" x="25.4" y="2.54" length="short" rot="R180"/>
<pin name="GND@2" x="-22.86" y="0" length="short" direction="sup"/>
<pin name="!CE!/GPIO7" x="25.4" y="0" length="short" rot="R180"/>
<pin name="ID_SD" x="-22.86" y="-2.54" length="short"/>
<pin name="ID_SC" x="25.4" y="-2.54" length="short" rot="R180"/>
<pin name="GPIO5" x="-22.86" y="-5.08" length="short"/>
<pin name="GND@6" x="25.4" y="-5.08" length="short" direction="sup" rot="R180"/>
<pin name="GPIO6" x="-22.86" y="-7.62" length="short"/>
<pin name="GPIO12" x="25.4" y="-7.62" length="short" rot="R180"/>
<pin name="GPIO13" x="-22.86" y="-10.16" length="short"/>
<pin name="GND@7" x="25.4" y="-10.16" length="short" direction="sup" rot="R180"/>
<pin name="GPIO19" x="-22.86" y="-12.7" length="short"/>
<pin name="GPIO16" x="25.4" y="-12.7" length="short" rot="R180"/>
<pin name="GPIO26" x="-22.86" y="-15.24" length="short"/>
<pin name="GPIO20" x="25.4" y="-15.24" length="short" rot="R180"/>
<pin name="GND@3" x="-22.86" y="-17.78" length="short" direction="sup"/>
<pin name="GPIO21" x="25.4" y="-17.78" length="short" rot="R180"/>
<text x="-20.32" y="35.56" size="1.27" layer="95" font="vector">&gt;NAME</text>
<text x="-20.32" y="33.655" size="1.27" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="SWITCH_DTSM6">
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="4.445" x2="-1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="0" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="4.445" x2="-1.905" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="4.445" x2="0" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="-2.54" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="3.175" y="3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="T2" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="2"/>
<pin name="T4" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="T3" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="T1" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="2"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PCA9685" prefix="U">
<description>&lt;h3&gt;16-channel, 12-bit PWM Fm+ I2C-bus LED controller&lt;/h3&gt;</description>
<gates>
<gate name="G$1" symbol="PCA9685" x="0" y="0"/>
</gates>
<devices>
<device name="TSSOP28" package="TSSOP28">
<connects>
<connect gate="G$1" pin="!OE!" pad="23"/>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="A3" pad="4"/>
<connect gate="G$1" pin="A4" pad="5"/>
<connect gate="G$1" pin="A5" pad="24"/>
<connect gate="G$1" pin="EXTCLK" pad="25"/>
<connect gate="G$1" pin="GND" pad="14"/>
<connect gate="G$1" pin="LED0" pad="6"/>
<connect gate="G$1" pin="LED1" pad="7"/>
<connect gate="G$1" pin="LED10" pad="17"/>
<connect gate="G$1" pin="LED11" pad="18"/>
<connect gate="G$1" pin="LED12" pad="19"/>
<connect gate="G$1" pin="LED13" pad="20"/>
<connect gate="G$1" pin="LED14" pad="21"/>
<connect gate="G$1" pin="LED15" pad="22"/>
<connect gate="G$1" pin="LED2" pad="8"/>
<connect gate="G$1" pin="LED3" pad="9"/>
<connect gate="G$1" pin="LED4" pad="10"/>
<connect gate="G$1" pin="LED5" pad="11"/>
<connect gate="G$1" pin="LED6" pad="12"/>
<connect gate="G$1" pin="LED7" pad="13"/>
<connect gate="G$1" pin="LED8" pad="15"/>
<connect gate="G$1" pin="LED9" pad="16"/>
<connect gate="G$1" pin="SCL" pad="26"/>
<connect gate="G$1" pin="SDA" pad="27"/>
<connect gate="G$1" pin="VCC" pad="28"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="PCA9685" constant="no"/>
<attribute name="OC_MOUSER" value="771-PCA9685PW,118" constant="no"/>
<attribute name="OC_RS" value="727-5649" constant="no"/>
<attribute name="PACKAGE" value="TSSOP28" constant="no"/>
<attribute name="SUPPLIER" value="NXP" constant="no"/>
<attribute name="VALUE" value="16-ch PWM" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PCF8574" prefix="U">
<description>&lt;h3&gt;Remote 8-bit I/O expander for I2C-bus with interrupt&lt;/h3&gt;</description>
<gates>
<gate name="G$1" symbol="PCF8574" x="0" y="-7.62"/>
</gates>
<devices>
<device name="SO-16" package="SO-16LD">
<connects>
<connect gate="G$1" pin="!INT!" pad="13"/>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="GND" pad="8"/>
<connect gate="G$1" pin="P0" pad="4"/>
<connect gate="G$1" pin="P1" pad="5"/>
<connect gate="G$1" pin="P2" pad="6"/>
<connect gate="G$1" pin="P3" pad="7"/>
<connect gate="G$1" pin="P4" pad="9"/>
<connect gate="G$1" pin="P5" pad="10"/>
<connect gate="G$1" pin="P6" pad="11"/>
<connect gate="G$1" pin="P7" pad="12"/>
<connect gate="G$1" pin="SCL" pad="14"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="PCF8574T" constant="no"/>
<attribute name="OC_MOUSER" value="771-PCF8574TD-T" constant="no"/>
<attribute name="OC_RS" value="510-780" constant="no"/>
<attribute name="OC_TME" value="PCF8574T-SMD" constant="no"/>
<attribute name="PACKAGE" value="SO-16" constant="no"/>
<attribute name="SUPPLIER" value="NXP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BH1750FVI" prefix="U">
<description>Ambient Light Sensor IC Series</description>
<gates>
<gate name="G$1" symbol="BH1750FVI" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WSOF6I">
<connects>
<connect gate="G$1" pin="ADDR" pad="2"/>
<connect gate="G$1" pin="DVI" pad="5"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="BH1750FVI" constant="no"/>
<attribute name="OC_FARNELL" value="2421284" constant="no"/>
<attribute name="OC_MOUSER" value="755-BH1750FVI-TR" constant="no"/>
<attribute name="OC_RS" value="124-6506" constant="no"/>
<attribute name="PACKAGE" value="SMD " constant="no"/>
<attribute name="SUPPLIER" value="ROHM" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BME280" prefix="U">
<description>DIGITAL HUMIDITY, PRESSURE AND TEMPERATURE SENSOR</description>
<gates>
<gate name="G$1" symbol="BME280" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="BME280">
<connects>
<connect gate="G$1" pin="CSB" pad="2"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="GND@1" pad="7"/>
<connect gate="G$1" pin="SCL" pad="4"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="SDO" pad="5"/>
<connect gate="G$1" pin="VDD" pad="8"/>
<connect gate="G$1" pin="VDDIO" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="BME280" constant="no"/>
<attribute name="OC_MOUSER" value="262-BME280" constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
<attribute name="SUPPLIER" value="BOSCH" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INA219" prefix="U">
<description>CEMBER 2015
INA219 Zerø-Drift, Bidirectional Current/Power Monitor With I
2C Interface</description>
<gates>
<gate name="G$1" symbol="INA219" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="DCN-R-PDSO-G8">
<connects>
<connect gate="G$1" pin="A0" pad="7"/>
<connect gate="G$1" pin="A1" pad="8"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="6"/>
<connect gate="G$1" pin="VIN+" pad="1"/>
<connect gate="G$1" pin="VIN-" pad="2"/>
<connect gate="G$1" pin="VS" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="INA219" constant="no"/>
<attribute name="OC_FARNELL" value="2295989" constant="no"/>
<attribute name="OC_MOUSER" value="595-INA219AIDCNR" constant="no"/>
<attribute name="OC_RS" value="709-8855" constant="no"/>
<attribute name="PACKAGE" value="SOT-8" constant="no"/>
<attribute name="SUPPLIER" value="TI" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UBLOX_NEO-6/7" prefix="U">
<description>UBLOX NEO-6 GPS MODULE

http://www.u-blox.com/images/downloads/Product_Docs/LEA-6_NEO-6_MAX_HardwareIntegrationManual_%28GPS.G6-HW-09007%29.pdf
http://www.u-blox.com/images/downloads/Product_Docs/NEO-6_DataSheet_%28GPS.G6-HW-09005%29.pdf</description>
<gates>
<gate name="G$1" symbol="UBLOX_NEO-6/7" x="0" y="38.1"/>
</gates>
<devices>
<device name="" package="UBLOX_NEO-6/7">
<connects>
<connect gate="G$1" pin="10_GND" pad="10_GND"/>
<connect gate="G$1" pin="11_RF_IN" pad="11_RF_IN"/>
<connect gate="G$1" pin="12_GND" pad="12_GND"/>
<connect gate="G$1" pin="13_GND" pad="13_GND"/>
<connect gate="G$1" pin="14_MOSI/CFG_COM0" pad="14_MOSI/CFG_COM0"/>
<connect gate="G$1" pin="15_MISO/CFG_COM1" pad="15_MISO/CFG_COM1"/>
<connect gate="G$1" pin="16_CFG_CPS0/SCK" pad="16_CFG_CPS0/SCK"/>
<connect gate="G$1" pin="17_RESERVED" pad="17_RESERVED"/>
<connect gate="G$1" pin="18_SDA2" pad="18_SDA2"/>
<connect gate="G$1" pin="19_SCL2" pad="19_SCL2"/>
<connect gate="G$1" pin="1_RESERVED" pad="1_RESERVED"/>
<connect gate="G$1" pin="20_TXD1" pad="20_TXD1"/>
<connect gate="G$1" pin="21_RXD1" pad="21_RXD1"/>
<connect gate="G$1" pin="22_V_BCKP" pad="22_V_BCKP"/>
<connect gate="G$1" pin="23_VCC" pad="23_VCC"/>
<connect gate="G$1" pin="24_GND" pad="24_GND"/>
<connect gate="G$1" pin="2_SS_N" pad="2_SS_N"/>
<connect gate="G$1" pin="3_TIMEPULSE" pad="3_TIMEPULSE"/>
<connect gate="G$1" pin="4_EXTINT0" pad="4_EXTINT0"/>
<connect gate="G$1" pin="5_USB_DM" pad="5_USB_DM"/>
<connect gate="G$1" pin="6_USB_DP" pad="6_USB_DP"/>
<connect gate="G$1" pin="7_VDDUSB" pad="7_VDDUSB"/>
<connect gate="G$1" pin="8_RESERVED" pad="8_RESERVED"/>
<connect gate="G$1" pin="9_VCC_RF" pad="9_VCC_RF"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="FB" uservalue="yes">
<description>306050008</description>
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="L0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="BLM15BD182SN1" constant="no"/>
<attribute name="OC_FARNELL" value="1515774" constant="no"/>
<attribute name="OC_MOUSER" value="81-BLM15BD182SN1D " constant="no"/>
<attribute name="OC_RS" value="871-2023" constant="no"/>
<attribute name="PACKAGE" value="L0402" constant="no"/>
<attribute name="SUPPLIER" value="MURATA" constant="no"/>
<attribute name="TEMP" value="-55 to 125" constant="no"/>
<attribute name="VALUE" value="1.8 kOhm 100mA" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="ILC0603ER2N2S" constant="no"/>
<attribute name="OC_MOUSER" value="70-ILC0603ER2N2S" constant="no"/>
<attribute name="PACKAGE" value="0603" constant="no"/>
<attribute name="SUPPLIER" value="Vishay" constant="no"/>
<attribute name="VALUE" value="22uH" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="IRM_05/10" prefix="U" uservalue="yes">
<description>&lt;b&gt;5 or 10 watt&lt;/b&gt; &lt;br/&gt;
housing &amp; pin compatible &lt;br/&gt;&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;IRM 05&lt;/b&gt;
&lt;pre&gt;Vout [V]     Imax [A]     Pmax [W]     Eff [%]
3.3          1.25         4.125        68
5            1            5            71
12           0.42         5.04         75
15           0.33         4.95         75
24           0.23         5.52         77&lt;/pre&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;IRM 10&lt;/b&gt;
&lt;pre&gt;Vout [V]     Imax [A]     Pmax [W]     Eff [%]
3.3          2.5           8.25        74
5            2            10           77
12           0.85         10.2         82
15           0.67         10.05        82
24           0.42         10.08        82&lt;/pre&gt;</description>
<gates>
<gate name="G$1" symbol="IRM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="IRM_05/10">
<connects>
<connect gate="G$1" pin="AC/L" pad="AC/L"/>
<connect gate="G$1" pin="AC/N" pad="AC/N"/>
<connect gate="G$1" pin="V+" pad="V+"/>
<connect gate="G$1" pin="V-" pad="V-"/>
</connects>
<technologies>
<technology name="-12">
<attribute name="MPN" value="IRM-10-12" constant="no"/>
<attribute name="OC_MOUSER" value="709-IRM10-12" constant="no"/>
<attribute name="OC_TME" value="IRM-10-12 " constant="no"/>
<attribute name="SUPPLIER" value="Mean Well" constant="no"/>
<attribute name="TEMP" value="-30 to 70" constant="no"/>
<attribute name="VALUE" value="10W 12V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DCDC_OKI-78SR" prefix="U">
<description>4.95W DC/DC Converter
&lt;br&gt;&lt;br&gt;
&lt;b&gt; Manufacturer:&lt;/b&gt;&lt;br&gt;
Murata
&lt;br&gt;&lt;br&gt;&lt;b&gt;Part Number:&lt;/b&gt;&lt;br&gt;
OKI-78SR-5/1.5-W36H-C&lt;br&gt;
OKI-78SR-3.3/1.5-W36H-C</description>
<gates>
<gate name="G$1" symbol="5V_REG" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="OKI-78SRH">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VI" pad="1"/>
<connect gate="G$1" pin="VO" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="OKI-78SR-5/1.5-W36H-C " constant="no"/>
<attribute name="OC_MOUSER" value="580-OKI78SR51.5W36HC " constant="no"/>
<attribute name="OC_RS" value="796-2138" constant="no"/>
<attribute name="OC_TME" value="" constant="no"/>
<attribute name="PACKAGE" value="THT" constant="no"/>
<attribute name="SUPPLIER" value="Murata" constant="no"/>
<attribute name="TEMP" value="-40 to 85" constant="no"/>
<attribute name="VALUE" value="5V 1.5A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NCP1117ST33T3G" prefix="U">
<description>&lt;b&gt;LDO (Low Dropout) Linear Voltage Regulator - ON Semiconductor&lt;/b&gt;&lt;p&gt;
The Low dropout or LDO is a linear voltage regulator. We have a wide range of linear regulators. The LDO regulator can operate with a small input–output differential voltage. LDO voltage regulators may offer fast transient response, wide input voltage range, low quiescent current and low noise with high PSRR.</description>
<gates>
<gate name="G$1" symbol="NCP1117ST33T3G" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT223">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="NCP1117ST33T3G" constant="no"/>
<attribute name="OC_RS" value="785-7207" constant="no"/>
<attribute name="OC_TME" value="NCP1117ST33T3G" constant="no"/>
<attribute name="PACKAGE" value="SMD_SOT223" constant="no"/>
<attribute name="SUPPLIER" value="ON Semiconductor" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HC-SR501" prefix="U">
<description>HC-SR501</description>
<gates>
<gate name="G$1" symbol="HC-SR501" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HC-SR501">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="OUT" pad="2"/>
<connect gate="G$1" pin="VCC" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="D">
<description>&lt;b&gt;Lite-On C930 Series Chip LED&lt;/b&gt;&lt;p&gt;
The C930 series, from Lite-On, are in the standard 1210 Chip LED package. They are available in a variety of colours with a dome lens. 
Features of the C930 series:
Viewing angle 25°
1210 package
Surface mount (SMT)
Single Colour
Variety of colour options
Dome lens</description>
<gates>
<gate name="G$1" symbol="LED_1210" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="LTST-C930KFKT">
<attribute name="MPN" value="LTST-C930KFKT" constant="no"/>
<attribute name="OC_RS" value="692-1133" constant="no"/>
<attribute name="OC_TME" value="LTST-C930KFKT" constant="no"/>
<attribute name="PACKAGE" value="SMD1210" constant="no"/>
<attribute name="SUPPLIER" value="LITEON" constant="no"/>
<attribute name="VALUE" value="ORANGE" constant="no"/>
</technology>
<technology name="LTST-C930KGKT">
<attribute name="MPN" value="LTST-C930KGKT" constant="no"/>
<attribute name="OC_RS" value="692-1136" constant="no"/>
<attribute name="OC_TME" value="LTST-C930KGKT" constant="no"/>
<attribute name="PACKAGE" value="SMD1210" constant="no"/>
<attribute name="SUPPLIER" value="LITEON" constant="no"/>
<attribute name="VALUE" value="GREEN" constant="no"/>
</technology>
<technology name="LTST-C930KRKT">
<attribute name="MPN" value="LTST-C930KRKT " constant="no"/>
<attribute name="OC_RS" value="692-1130" constant="no"/>
<attribute name="OC_TME" value="LTST-C930KRKT" constant="no"/>
<attribute name="PACKAGE" value="SMD1210" constant="no"/>
<attribute name="SUPPLIER" value="LITEON" constant="no"/>
<attribute name="VALUE" value="RED" constant="no"/>
</technology>
<technology name="LTST-C930KSKT">
<attribute name="MPN" value="LTST-C930KSKT" constant="no"/>
<attribute name="OC_RS" value="692-1149" constant="no"/>
<attribute name="OC_TME" value="LTST-C930KSKT" constant="no"/>
<attribute name="PACKAGE" value="SMD1210" constant="no"/>
<attribute name="SUPPLIER" value="LITEON" constant="no"/>
<attribute name="VALUE" value="YELLOW" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADS1X15" prefix="U">
<gates>
<gate name="G$1" symbol="ADS1015" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="MSOP10">
<connects>
<connect gate="G$1" pin="ADDR" pad="1"/>
<connect gate="G$1" pin="AIN0" pad="4"/>
<connect gate="G$1" pin="AIN1" pad="5"/>
<connect gate="G$1" pin="AIN2" pad="6"/>
<connect gate="G$1" pin="AIN3" pad="7"/>
<connect gate="G$1" pin="ALERT" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="SCL" pad="10"/>
<connect gate="G$1" pin="SDA" pad="9"/>
<connect gate="G$1" pin="VDD" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="ADS1015IDGSR" constant="no"/>
<attribute name="OC_MOUSER" value="595-ADS1015IDGSR" constant="no"/>
<attribute name="OC_RS" value="709-4550" constant="no"/>
<attribute name="PACKAGE" value="MSOP10" constant="no"/>
<attribute name="SUPPLIER" value="TI" constant="no"/>
<attribute name="TEMP" value="-40 to 125" constant="no"/>
<attribute name="VALUE" value="12bit 3.3kSps" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RTRIM" prefix="POT">
<description>&lt;b&gt;200mW 4mm Open Frame 3364W Series&lt;/b&gt;&lt;p&gt;
Bourns® 3364W Series 4mm Square Single turn Cermet SMD Trimpot® Trimming Potentiometer
Miniature (nominal 4mm square)
Excellent mounting efficiency
Suitable for reflow soldering/hand solder
Ideal for automatic assembly
Cross slot top adjuster</description>
<gates>
<gate name="G$1" symbol="RTRIM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RTRIM3364W">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="E" pad="E"/>
<connect gate="G$1" pin="S" pad="S"/>
</connects>
<technologies>
<technology name="3364W-1-203E">
<attribute name="MPN" value="3364W-1-203E" constant="no"/>
<attribute name="OC_MOUSER" value="177-239P" constant="no"/>
<attribute name="OC_TME" value="3364X-1-203E " constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
<attribute name="SUPPLIER" value="BOURNS" constant="no"/>
<attribute name="VALUE" value="20k" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RM51-3021-85-1012" prefix="K" uservalue="yes">
<description>Relay SPST-NO RELPOL RM51-3021-85-1012
Rated coil voltage	12V DC</description>
<gates>
<gate name="G$1" symbol="RM51-3021-85-1012" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RM51-3021-85-1012">
<connects>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="COM" pad="COM"/>
<connect gate="G$1" pin="NO" pad="NO"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="RM51-3021-85-1012 "/>
<attribute name="OC_TME" value="RM513021851012" constant="no"/>
<attribute name="PACKAGE" value="THT" constant="no"/>
<attribute name="SUPPLIER" value="RELPOL" constant="no"/>
<attribute name="TEMP" value="-40 to 85" constant="no"/>
<attribute name="VALUE" value="230V 10A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-N" prefix="Q">
<gates>
<gate name="Q" symbol="MOSFET-N" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="Q" pin="D" pad="3"/>
<connect gate="Q" pin="G" pad="1"/>
<connect gate="Q" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="NTR4003NT1G">
<attribute name="MPN" value="NTR4003NT1G" constant="no"/>
<attribute name="OC_MOUSER" value="863-NTR4003NT1G" constant="no"/>
<attribute name="OC_RS" value="780-4742" constant="no"/>
<attribute name="OC_TME" value="NTR4003NT1G" constant="no"/>
<attribute name="PACKAGE" value="SOT-23" constant="no"/>
<attribute name="SUPPLIER" value="ON" constant="no"/>
<attribute name="VALUE" value="30V 500mA" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SCHOTTKY" prefix="D" uservalue="yes">
<description>304020016</description>
<gates>
<gate name="G$1" symbol="D-SCHOTTKY-3P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="BAT54C.215">
<attribute name="MPN" value="BAT54C.215 " constant="no"/>
<attribute name="OC_MOUSER" value="771-BAT54C-T/R" constant="no"/>
<attribute name="OC_RS" value="436-7789" constant="no"/>
<attribute name="OC_TME" value="BAT54C" constant="no"/>
<attribute name="PACKAGE" value="SOT-23" constant="no"/>
<attribute name="SUPPLIER" value="NEXPERIA" constant="no"/>
<attribute name="VALUE" value="30V 200mA" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RPI_ZERO" prefix="U">
<gates>
<gate name="G$1" symbol="RPI_ZERO" x="0" y="-5.08" addlevel="can"/>
</gates>
<devices>
<device name="" package="RPI_ZERO">
<connects>
<connect gate="G$1" pin="!CE!/GPIO7" pad="26"/>
<connect gate="G$1" pin="!CE0!/GPIO8" pad="24"/>
<connect gate="G$1" pin="3V3@1" pad="1"/>
<connect gate="G$1" pin="3V3@2" pad="17"/>
<connect gate="G$1" pin="5V0@1" pad="2"/>
<connect gate="G$1" pin="5V0@2" pad="4"/>
<connect gate="G$1" pin="GEN/6GPIO25" pad="22"/>
<connect gate="G$1" pin="GEN4/GPIO23" pad="16"/>
<connect gate="G$1" pin="GEN5/GPIO24" pad="18"/>
<connect gate="G$1" pin="GND@1" pad="9"/>
<connect gate="G$1" pin="GND@2" pad="25"/>
<connect gate="G$1" pin="GND@3" pad="39"/>
<connect gate="G$1" pin="GND@4" pad="14"/>
<connect gate="G$1" pin="GND@5" pad="20"/>
<connect gate="G$1" pin="GND@6" pad="30"/>
<connect gate="G$1" pin="GND@7" pad="34"/>
<connect gate="G$1" pin="GND@8" pad="6"/>
<connect gate="G$1" pin="GPIO10/MOSI" pad="19"/>
<connect gate="G$1" pin="GPIO11/SCLK" pad="23"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17/GEN0" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO2/SDA1" pad="3"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22/GEN3" pad="15"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27/GEN2" pad="13"/>
<connect gate="G$1" pin="GPIO3/SCL1" pad="5"/>
<connect gate="G$1" pin="GPIO4/GCKL" pad="7"/>
<connect gate="G$1" pin="GPIO5" pad="29"/>
<connect gate="G$1" pin="GPIO6" pad="31"/>
<connect gate="G$1" pin="GPIO9/MISO" pad="21"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="RXD0/GPIO15" pad="10"/>
<connect gate="G$1" pin="TXD0/GPIO14" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWITCH" prefix="SW">
<description>&lt;b&gt;Apem DTSM-6 Series Tact Switches&lt;/b&gt;&lt;p&gt;

Apem DTSM-6 series tactile or tact SPST switches are designed to be used with small currents and feature a sharp click feel and positive tactile feedback. A small movement distance on these tact switches mean that the user experiences a distinct sensation when the switch ’clicks’ into place. This series of Apem tact switches have SMT terminals, low contact resistance and high reliability. The ultra-miniature and lightweight structure of this series of tact switches ensures suitability of high density mounting.
Vertical operating direction
Low contact resistance: 100mΩ
Insulation resistance &gt;100MΩ at 500Vdc
Positive tactile feedback
Long life</description>
<gates>
<gate name="G$1" symbol="SWITCH_DTSM6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SWITCH_DTSM6">
<connects>
<connect gate="G$1" pin="T1" pad="T1"/>
<connect gate="G$1" pin="T2" pad="T2"/>
<connect gate="G$1" pin="T3" pad="T3"/>
<connect gate="G$1" pin="T4" pad="T4"/>
</connects>
<technologies>
<technology name="DTSM6">
<attribute name="MPN" value="DTSM63NV" constant="no"/>
<attribute name="OC_RS" value="765-1071" constant="no"/>
<attribute name="OC_TME" value="DTSM-61N-V-B" constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
<attribute name="SUPPLIER" value="DIPTRONICS" constant="no"/>
<attribute name="VALUE" value="SPST_NO" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ROMAX_RLC">
<packages>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.2" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.2" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="0.8128" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2512">
<description>2512</description>
<wire x1="-4.445" y1="1.905" x2="4.445" y2="1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.905" x2="4.445" y2="-1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="-1.905" x2="-4.445" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-1.905" x2="-4.445" y2="1.905" width="0.127" layer="21"/>
<smd name="1" x="-3.175" y="0" dx="3.302" dy="2.032" layer="1" rot="R90"/>
<smd name="2" x="3.175" y="0" dx="3.302" dy="2.032" layer="1" rot="R90"/>
<text x="-2.54" y="2.159" size="0.889" layer="25" font="vector" ratio="11">&gt;NAME</text>
<text x="-3.81" y="-3.048" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<text x="-1.778" y="-0.254" size="0.8128" layer="33" font="vector" ratio="10">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="R">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="100K">
<attribute name="MPN" value="CRCW0805100KFKEA" constant="no"/>
<attribute name="OC_RS" value="679-0803" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="Vishay" constant="no"/>
<attribute name="VALUE" value="100k 1%" constant="no"/>
</technology>
<technology name="10K">
<attribute name="MPN" value="CRG0805F10K" constant="no"/>
<attribute name="OC_RS" value="223-0562" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
<technology name="10R">
<attribute name="MPN" value="CRG0805F47R" constant="no"/>
<attribute name="OC_RS" value="223-0253" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="10R" constant="no"/>
</technology>
<technology name="120R">
<attribute name="MPN" value="CRG0805F120R" constant="no"/>
<attribute name="OC_RS" value="223-0304" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="120R 1%" constant="no"/>
</technology>
<technology name="150K">
<attribute name="MPN" value="CR1206-FX-1003ELF" constant="no"/>
<attribute name="OC_RS" value="788-3988" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="Bourns" constant="no"/>
<attribute name="VALUE" value="150k" constant="no"/>
</technology>
<technology name="15K">
<attribute name="MPN" value="CPF0805B15KE" constant="no"/>
<attribute name="OC_RS" value="666-2610" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="15k" constant="no"/>
</technology>
<technology name="1K">
<attribute name="MPN" value="CRG0805F1K0" constant="no"/>
<attribute name="OC_RS" value="223-0427" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="1k" constant="no"/>
</technology>
<technology name="22R">
<attribute name="MPN" value="CPF0805B22R1E1" constant="no"/>
<attribute name="OC_RS" value="123-1297" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="22R" constant="no"/>
</technology>
<technology name="270R">
<attribute name="MPN" value="CRG0805F270R" constant="no"/>
<attribute name="OC_RS" value="223-0348" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="270R" constant="no"/>
</technology>
<technology name="27K">
<attribute name="MPN" value="CRG0805F27K" constant="no"/>
<attribute name="OC_RS" value="223-0613" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="27k" constant="no"/>
</technology>
<technology name="2K2">
<attribute name="MPN" value="CRG0805F2K2" constant="no"/>
<attribute name="OC_RS" value="223-0477" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="2k2" constant="no"/>
</technology>
<technology name="340R">
<attribute name="MPN" value="ERA6AEB3400V" constant="no"/>
<attribute name="OC_RS" value="708-5988" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="340Ω 0.1%" constant="no"/>
</technology>
<technology name="470R">
<attribute name="MPN" value="CRG0805F270R" constant="no"/>
<attribute name="OC_RS" value="223-0348" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="470R" constant="no"/>
</technology>
<technology name="47R">
<attribute name="MPN" value="CRG0805F47R" constant="no"/>
<attribute name="OC_RS" value="223-0253" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="47R" constant="no"/>
</technology>
<technology name="4R7">
<attribute name="MPN" value="CPF0805B4R7E" constant="no"/>
<attribute name="OC_RS" value="666-2339" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="4R7 0.1%" constant="no"/>
</technology>
<technology name="51K">
<attribute name="MPN" value="CPF0805B51KE" constant="no"/>
<attribute name="OC_RS" value="666-2657" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="51k" constant="no"/>
</technology>
<technology name="680R">
<attribute name="MPN" value="CRG0805F680R" constant="no"/>
<attribute name="OC_RS" value="223-0405" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="680R" constant="no"/>
</technology>
<technology name="820R">
<attribute name="MPN" value="CRCW0805820RFKEA " constant="no"/>
<attribute name="OC_RS" value="679-1698" constant="no"/>
<attribute name="PACKAGE" value="SMD 0805"/>
<attribute name="SUPPLIER" value="TE Connectivity"/>
<attribute name="VALUE" value="820R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="0R">
<attribute name="MPN" value="CRCW04020000Z0ED" constant="no"/>
<attribute name="OC_FARNELL" value="1469661" constant="no"/>
<attribute name="OC_MOUSER" value="71-CRCW0402-0-E3" constant="no"/>
<attribute name="OC_RS" value="393-2326" constant="no"/>
<attribute name="PACKAGE" value="0402" constant="no"/>
<attribute name="SUPPLIER" value="VISHAY" constant="no"/>
<attribute name="TEMP" value="-55 to 125" constant="no"/>
<attribute name="VALUE" value="0R" constant="no"/>
</technology>
<technology name="120R">
<attribute name="MPN" value="ERJ-XGNJ121Y " constant="no"/>
<attribute name="OC_FARNELL" value="" constant="no"/>
<attribute name="OC_MOUSER" value="667-ERJ-XGNJ121Y" constant="no"/>
<attribute name="OC_RS" value="" constant="no"/>
<attribute name="PACKAGE" value="0402" constant="no"/>
<attribute name="SUPPLIER" value="Panasonic" constant="no"/>
<attribute name="TEMP" value="-55 to 125" constant="no"/>
<attribute name="VALUE" value="120R" constant="no"/>
</technology>
<technology name="12K">
<attribute name="MPN" value="CRCW040212K0FKED " constant="no"/>
<attribute name="OC_FARNELL" value="2140866" constant="no"/>
<attribute name="OC_MOUSER" value="71-CRCW0402-12K-E3" constant="no"/>
<attribute name="OC_RS" value="678-8756" constant="no"/>
<attribute name="PACKAGE" value="0402" constant="no"/>
<attribute name="SUPPLIER" value="VISHAY" constant="no"/>
<attribute name="TEMP" value="-55 to 125" constant="no"/>
<attribute name="VALUE" value="12k" constant="no"/>
</technology>
<technology name="220R">
<attribute name="MPN" value="RC0402FR-07220RL" constant="no"/>
<attribute name="OC_FARNELL" value="9239154" constant="no"/>
<attribute name="OC_MOUSER" value="603-RC0402FR-07220RL " constant="no"/>
<attribute name="OC_RS" value="9239154" constant="no"/>
<attribute name="PACKAGE" value="0402" constant="no"/>
<attribute name="SUPPLIER" value="Yageo" constant="no"/>
<attribute name="TEMP" value="-55 to 125" constant="no"/>
<attribute name="VALUE" value="220R" constant="no"/>
</technology>
<technology name="47K">
<attribute name="MPN" value="CRCW040247K0FKED" constant="no"/>
<attribute name="OC_FARNELL" value="1469719" constant="no"/>
<attribute name="OC_MOUSER" value="71-CRCW0402-47K-E3 " constant="no"/>
<attribute name="OC_RS" value="678-9361" constant="no"/>
<attribute name="PACKAGE" value="0402" constant="no"/>
<attribute name="SUPPLIER" value="VISHAY" constant="no"/>
<attribute name="TEMP" value="-55 to 125" constant="no"/>
<attribute name="VALUE" value="47k" constant="no"/>
</technology>
<technology name="820R">
<attribute name="MPN" value="CRCW0402820RFKED" constant="no"/>
<attribute name="OC_FARNELL" value="2140765" constant="no"/>
<attribute name="OC_MOUSER" value="71-CRCW0402-820-E3" constant="no"/>
<attribute name="OC_RS" value="CRCW0402820RFKED" constant="no"/>
<attribute name="PACKAGE" value="0402" constant="no"/>
<attribute name="SUPPLIER" value="Vishay" constant="no"/>
<attribute name="TEMP" value="-55 to 125" constant="no"/>
<attribute name="VALUE" value="820R" constant="no"/>
</technology>
<technology name="R100">
<attribute name="MPN" value="RL0402JR-070R1L" constant="no"/>
<attribute name="OC_FARNELL" value="" constant="no"/>
<attribute name="OC_MOUSER" value="" constant="no"/>
<attribute name="OC_RS" value="" constant="no"/>
<attribute name="PACKAGE" value="0402" constant="no"/>
<attribute name="SUPPLIER" value="Yageo" constant="no"/>
<attribute name="TEMP" value="-55 to 125" constant="no"/>
<attribute name="VALUE" value="0.1R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="R100">
<attribute name="MPN" value="WSL2512R1000FEA" constant="no"/>
<attribute name="OC_MOUSER" value="71-WSL2512R1000FEA " constant="no"/>
<attribute name="OC_RS" value="683-6599" constant="no"/>
<attribute name="PACKAGE" value="2512" constant="no"/>
<attribute name="SUPPLIER" value="VISHAY" constant="no"/>
<attribute name="TEMP" value="-60 to 170" constant="no"/>
<attribute name="VALUE" value="0.1R 1%" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Marcin_R">
<packages>
<package name="R2512">
<description>2512</description>
<wire x1="-4.445" y1="1.905" x2="4.445" y2="1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.905" x2="4.445" y2="-1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="-1.905" x2="-4.445" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-1.905" x2="-4.445" y2="1.905" width="0.127" layer="21"/>
<smd name="1" x="-3.175" y="0" dx="3.302" dy="2.032" layer="1" rot="R90"/>
<smd name="2" x="3.175" y="0" dx="3.302" dy="2.032" layer="1" rot="R90"/>
<text x="-2.54" y="2.159" size="0.889" layer="25" font="vector" ratio="11">&gt;NAME</text>
<text x="-3.81" y="-3.048" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<text x="-1.778" y="-0.254" size="0.8128" layer="33" font="vector" ratio="10">&gt;NAME</text>
</package>
<package name="R1206">
<wire x1="-2.159" y1="1.016" x2="2.159" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.159" y1="1.016" x2="2.159" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.159" y1="-1.016" x2="-2.159" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.159" y1="-1.016" x2="-2.159" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.524" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.524" layer="1"/>
<text x="-1.905" y="1.27" size="0.889" layer="25" font="vector" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-2.159" size="0.889" layer="27" font="vector" ratio="11">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.889" x2="2.032" y2="0.889" layer="39"/>
</package>
<package name="R0402">
<wire x1="-0.4445" y1="0.762" x2="-0.3175" y2="0.889" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.3175" y1="0.889" x2="0.3175" y2="0.889" width="0.0762" layer="21"/>
<wire x1="0.3175" y1="0.889" x2="0.4445" y2="0.762" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.4445" y1="0.762" x2="0.4445" y2="-0.762" width="0.0762" layer="21"/>
<wire x1="0.4445" y1="-0.762" x2="0.3175" y2="-0.889" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.3175" y1="-0.889" x2="-0.3175" y2="-0.889" width="0.0762" layer="21"/>
<wire x1="-0.3175" y1="-0.889" x2="-0.4445" y2="-0.762" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.4445" y1="-0.762" x2="-0.4445" y2="0.762" width="0.0762" layer="21"/>
<smd name="1" x="0" y="0.4445" dx="0.635" dy="0.635" layer="1" roundness="50" rot="R270"/>
<smd name="2" x="0" y="-0.4445" dx="0.635" dy="0.635" layer="1" roundness="50" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.27" size="0.889" layer="27" font="vector" ratio="11" rot="R270">&gt;VALUE</text>
</package>
<package name="R0805">
<wire x1="0.889" y1="-1.651" x2="-0.889" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-1.651" x2="-0.889" y2="1.651" width="0.127" layer="21"/>
<wire x1="-0.889" y1="1.651" x2="0.889" y2="1.651" width="0.127" layer="21"/>
<wire x1="0.889" y1="1.651" x2="0.889" y2="-1.651" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.889" dx="1.016" dy="1.397" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.889" dx="1.016" dy="1.397" layer="1" roundness="25" rot="R270"/>
<text x="1.016" y="1.905" size="0.889" layer="25" font="vector" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.905" y="1.905" size="0.889" layer="27" ratio="11" rot="R270">&gt;VALUE</text>
</package>
<package name="R0603">
<wire x1="0.635" y1="1.397" x2="0.635" y2="-1.397" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.397" x2="-0.635" y2="-1.397" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.397" x2="-0.635" y2="1.397" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.397" x2="0.635" y2="1.397" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.762" dx="0.889" dy="0.889" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.762" dx="0.889" dy="0.889" layer="1" roundness="25" rot="R270"/>
<text x="-1.016" y="-1.905" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="RES">
<wire x1="-2.54" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0" y1="-0.508" x2="-2.54" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="0.508" width="0.254" layer="94"/>
<text x="-5.08" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RES" prefix="R" uservalue="yes">
<description>SMD resistor</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="_R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_R100">
<attribute name="MPN" value="WSL2512R1000FEA" constant="no"/>
<attribute name="OC_MOUSER" value="71-WSL2512R1000FEA " constant="no"/>
<attribute name="OC_RS" value="683-6599" constant="no"/>
<attribute name="OC_TME" value="WSL2512R1000FEA" constant="no"/>
<attribute name="SUPPLIER" value="VISHAY" constant="no"/>
<attribute name="TEMP" value="-60 to 170" constant="no"/>
<attribute name="VALUE" value="0.1R 1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Marcin_Supply">
<packages>
</packages>
<symbols>
<symbol name="+12V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+12V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="0V">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="0V" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="PE">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.0922" y1="-0.508" x2="1.0922" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-0.6223" y1="-1.016" x2="0.6223" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-0.3048" y1="-1.524" x2="0.3302" y2="-1.524" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="PE" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+12V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+12V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0V" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="0V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PE" prefix="PE">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="M" symbol="PE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Marcin_C">
<packages>
<package name="EL0810">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
High Temperature solid electrolytic SMD 175 TMP&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="3.2" y1="-4.1" x2="-4.1" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-1.55" x2="-4.1" y2="1.55" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="1.55" x2="-4.1" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="4.1" x2="3.2" y2="4.1" width="0.2032" layer="21"/>
<wire x1="4.1" y1="3.2" x2="4.1" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.1" y1="1.5" x2="4.1" y2="-1.55" width="0.2032" layer="51"/>
<wire x1="4.1" y1="-1.55" x2="4.1" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-3.2" x2="3.2" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="4.1" x2="4.1" y2="3.2" width="0.2032" layer="21"/>
<wire x1="-3.6" y1="-1.45" x2="3.6" y2="-1.45" width="0.2032" layer="21" curve="136.123039"/>
<wire x1="-3.6" y1="-1.45" x2="-3.6" y2="1.45" width="0.2032" layer="51" curve="-43.876961"/>
<wire x1="-3.6" y1="1.45" x2="3.6" y2="1.45" width="0.2032" layer="21" curve="-136.123039"/>
<wire x1="3.6" y1="-1.45" x2="3.6" y2="1.45" width="0.2032" layer="51" curve="43.876961"/>
<smd name="+" x="3.5" y="0" dx="3" dy="2.5" layer="1"/>
<smd name="-" x="-3.5" y="0" dx="3" dy="2.5" layer="1"/>
<text x="-4.29" y="4.53" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.325" y="-5.785" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="EL">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="+" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="-2.54" y="2.54" size="1.27" layer="94">+</text>
<text x="-2.54" y="-0.508" size="1.27" layer="94">-</text>
</symbol>
<symbol name="C0805">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EL0810" prefix="C">
<description>ALUMINUM ELECTROLYTIC CAPACITORS</description>
<gates>
<gate name="G$1" symbol="EL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EL0810">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="_330UF">
<attribute name="MPN" value="UCW1E331MNL1GS" constant="no"/>
<attribute name="OC_MOUSER" value="647-UCW1E331MNL1GS" constant="no"/>
<attribute name="OC_RS" value="715-1701" constant="no"/>
<attribute name="OC_TME" value="UWT1E331MNL1GS" constant="no"/>
<attribute name="PACKAGE" value="SMD 0810" constant="no"/>
<attribute name="SUPPLIER" value="Nichicon" constant="no"/>
<attribute name="TEMP" value="-25 to 105" constant="no"/>
<attribute name="VALUE" value="330uF 25V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C0805" prefix="C">
<description>&lt;b&gt;Samsung 0805 X7R/Y5V MLCC series reels&lt;p&gt;
0805 Range&lt;p&gt;&lt;/b&gt;
Nickel barrier terminations covered with a layer of plated Tin (NiSn),Applications include mobile phones, video and tuner designs,COG/NPO is the most popular formulation of the "temperature compensating", EIA Class I ceramic materials,X7R, X5R formulations are called "temperature stable" ceramics and fall into the EIA Class II materials,Y5V, Z5U formulations are for general purpose use in a limited temperature range, EIA Class II materials; these characteristics are ideal for decoupling applications.</description>
<gates>
<gate name="G$1" symbol="C0805" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_100NF">
<attribute name="MPN" value="CL21B104KBCNNNC" constant="no"/>
<attribute name="OC_RS" value="766-6177" constant="no"/>
<attribute name="OC_TME" value="CL21B104KBCNNNC" constant="no"/>
<attribute name="PACKAGE" value="SMD_0805" constant="no"/>
<attribute name="SUPPLIER" value="SAMSUNG" constant="no"/>
<attribute name="VALUE" value="100nF" constant="no"/>
</technology>
<technology name="_10UF">
<attribute name="MPN" value="CL21A106KQFNNNG" constant="no"/>
<attribute name="OC_RS" value="GRM21BR60J106KE19L" constant="no"/>
<attribute name="OC_TME" value="CL21A106KQFNNNG" constant="no"/>
<attribute name="PACKAGE" value="SMD_0805" constant="no"/>
<attribute name="SUPPLIER" value="SAMSUNG" constant="no"/>
<attribute name="VALUE" value="10uF" constant="no"/>
</technology>
<technology name="_20PF">
<attribute name="MPN" value="CL10C200JB8NNNC" constant="no"/>
<attribute name="OC_RS" value="766-5815" constant="no"/>
<attribute name="OC_TME" value="CL10C200JB8NNNC" constant="no"/>
<attribute name="PACKAGE" value="SMD_0805" constant="no"/>
<attribute name="SUPPLIER" value="SAMSUNG" constant="no"/>
<attribute name="VALUE" value="20pF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Marcin_conn">
<packages>
<package name="284512-4">
<pad name="1" x="-5.25" y="-0.1" drill="1" diameter="2" rot="R90"/>
<pad name="2" x="-1.75" y="-0.1" drill="1" diameter="2" rot="R90"/>
<pad name="3" x="1.75" y="-0.1" drill="1" diameter="2" rot="R90"/>
<pad name="4" x="5.25" y="-0.1" drill="1" diameter="2" rot="R90"/>
<text x="-7.803690625" y="-5.3571" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="9.49081875" y="-6.205875" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-7.94754375" y1="-8.0393" x2="7.92135625" y2="-8.0262" layer="51"/>
<rectangle x1="-7.94754375" y1="-8.0262" x2="7.92135625" y2="-8.01309375" layer="51"/>
<rectangle x1="-7.94754375" y1="-8.013090625" x2="7.92135625" y2="-7.999990625" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.999990625" x2="7.97375625" y2="-7.986890625" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.986890625" x2="7.97375625" y2="-7.973784375" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.97378125" x2="7.97375625" y2="-7.96068125" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.96068125" x2="7.97375625" y2="-7.94756875" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.94756875" x2="7.97375625" y2="-7.93446875" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.93446875" x2="-7.89514375" y2="-7.92136875" layer="51"/>
<rectangle x1="7.86895625" y1="-7.93446875" x2="7.97375625" y2="-7.92136875" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.92136875" x2="-7.89514375" y2="-7.9082625" layer="51"/>
<rectangle x1="7.86895625" y1="-7.92136875" x2="7.97375625" y2="-7.9082625" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.908259375" x2="-7.89514375" y2="-7.895159375" layer="51"/>
<rectangle x1="7.86895625" y1="-7.908259375" x2="7.97375625" y2="-7.895159375" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.895159375" x2="-7.89514375" y2="-7.882053125" layer="51"/>
<rectangle x1="7.86895625" y1="-7.895159375" x2="7.97375625" y2="-7.882053125" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.88205" x2="-7.89514375" y2="-7.86895" layer="51"/>
<rectangle x1="7.86895625" y1="-7.88205" x2="7.97375625" y2="-7.86895" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.86895" x2="-7.89514375" y2="-7.85585" layer="51"/>
<rectangle x1="7.86895625" y1="-7.86895" x2="7.97375625" y2="-7.85585" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.85585" x2="-7.89514375" y2="-7.84274375" layer="51"/>
<rectangle x1="7.86895625" y1="-7.85585" x2="7.97375625" y2="-7.84274375" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.842740625" x2="-7.89514375" y2="-7.829640625" layer="51"/>
<rectangle x1="7.86895625" y1="-7.842740625" x2="7.97375625" y2="-7.829640625" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.829640625" x2="-7.89514375" y2="-7.816534375" layer="51"/>
<rectangle x1="7.86895625" y1="-7.829640625" x2="7.97375625" y2="-7.816534375" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.81653125" x2="-7.89514375" y2="-7.80343125" layer="51"/>
<rectangle x1="7.86895625" y1="-7.81653125" x2="7.97375625" y2="-7.80343125" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.80343125" x2="-7.89514375" y2="-7.79033125" layer="51"/>
<rectangle x1="7.86895625" y1="-7.80343125" x2="7.97375625" y2="-7.79033125" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.79033125" x2="-7.89514375" y2="-7.77721875" layer="51"/>
<rectangle x1="7.86895625" y1="-7.79033125" x2="7.97375625" y2="-7.77721875" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.77721875" x2="-7.89514375" y2="-7.76411875" layer="51"/>
<rectangle x1="7.86895625" y1="-7.77721875" x2="7.97375625" y2="-7.76411875" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.76411875" x2="-7.89514375" y2="-7.7510125" layer="51"/>
<rectangle x1="7.86895625" y1="-7.76411875" x2="7.97375625" y2="-7.7510125" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.751009375" x2="-7.89514375" y2="-7.737909375" layer="51"/>
<rectangle x1="7.86895625" y1="-7.751009375" x2="7.97375625" y2="-7.737909375" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.737909375" x2="-7.89514375" y2="-7.724809375" layer="51"/>
<rectangle x1="7.86895625" y1="-7.737909375" x2="7.97375625" y2="-7.724809375" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.724809375" x2="-7.89514375" y2="-7.711703125" layer="51"/>
<rectangle x1="7.86895625" y1="-7.724809375" x2="7.97375625" y2="-7.711703125" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.7117" x2="-7.89514375" y2="-7.6986" layer="51"/>
<rectangle x1="7.86895625" y1="-7.7117" x2="7.97375625" y2="-7.6986" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.6986" x2="-7.89514375" y2="-7.68549375" layer="51"/>
<rectangle x1="7.86895625" y1="-7.6986" x2="7.97375625" y2="-7.68549375" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.685490625" x2="-7.89514375" y2="-7.672390625" layer="51"/>
<rectangle x1="7.86895625" y1="-7.685490625" x2="7.97375625" y2="-7.672390625" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.672390625" x2="-7.89514375" y2="-7.659290625" layer="51"/>
<rectangle x1="7.86895625" y1="-7.672390625" x2="7.97375625" y2="-7.659290625" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.659290625" x2="-7.89514375" y2="-7.646184375" layer="51"/>
<rectangle x1="7.86895625" y1="-7.659290625" x2="7.97375625" y2="-7.646184375" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.64618125" x2="-7.89514375" y2="-7.63308125" layer="51"/>
<rectangle x1="7.86895625" y1="-7.64618125" x2="7.97375625" y2="-7.63308125" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.63308125" x2="-7.89514375" y2="-7.61996875" layer="51"/>
<rectangle x1="7.86895625" y1="-7.63308125" x2="7.97375625" y2="-7.61996875" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.61996875" x2="-7.89514375" y2="-7.60686875" layer="51"/>
<rectangle x1="7.86895625" y1="-7.61996875" x2="7.97375625" y2="-7.60686875" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.60686875" x2="-7.89514375" y2="-7.59376875" layer="51"/>
<rectangle x1="7.86895625" y1="-7.60686875" x2="7.97375625" y2="-7.59376875" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.59376875" x2="-7.89514375" y2="-7.5806625" layer="51"/>
<rectangle x1="7.86895625" y1="-7.59376875" x2="7.97375625" y2="-7.5806625" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.580659375" x2="-7.89514375" y2="-7.567559375" layer="51"/>
<rectangle x1="7.86895625" y1="-7.580659375" x2="7.97375625" y2="-7.567559375" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.567559375" x2="-7.89514375" y2="-7.554453125" layer="51"/>
<rectangle x1="7.86895625" y1="-7.567559375" x2="7.97375625" y2="-7.554453125" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.55445" x2="-7.89514375" y2="-7.54135" layer="51"/>
<rectangle x1="7.86895625" y1="-7.55445" x2="7.97375625" y2="-7.54135" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.54135" x2="-5.45784375" y2="-7.52825" layer="51"/>
<rectangle x1="-4.98604375" y1="-7.54135" x2="7.97375625" y2="-7.52825" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.52825" x2="-5.45784375" y2="-7.51514375" layer="51"/>
<rectangle x1="-4.98604375" y1="-7.52825" x2="7.97375625" y2="-7.51514375" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.515140625" x2="-5.45784375" y2="-7.502040625" layer="51"/>
<rectangle x1="-4.98604375" y1="-7.515140625" x2="7.97375625" y2="-7.502040625" layer="51"/>
<rectangle x1="-7.99994375" y1="-7.502040625" x2="-5.41854375" y2="-7.488934375" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.502040625" x2="7.97375625" y2="-7.488934375" layer="51"/>
<rectangle x1="-7.98684375" y1="-7.48893125" x2="-5.41854375" y2="-7.47583125" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.48893125" x2="7.97375625" y2="-7.47583125" layer="51"/>
<rectangle x1="-7.98684375" y1="-7.47583125" x2="-5.41854375" y2="-7.46273125" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.47583125" x2="7.97375625" y2="-7.46273125" layer="51"/>
<rectangle x1="-7.98684375" y1="-7.46273125" x2="-5.41854375" y2="-7.44961875" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.46273125" x2="7.97375625" y2="-7.44961875" layer="51"/>
<rectangle x1="-7.97374375" y1="-7.44961875" x2="-5.41854375" y2="-7.43651875" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.44961875" x2="7.96065625" y2="-7.43651875" layer="51"/>
<rectangle x1="-7.96064375" y1="-7.43651875" x2="-7.84274375" y2="-7.4234125" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.43651875" x2="-5.41854375" y2="-7.4234125" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.43651875" x2="-4.92054375" y2="-7.4234125" layer="51"/>
<rectangle x1="7.81655625" y1="-7.43651875" x2="7.94755625" y2="-7.4234125" layer="51"/>
<rectangle x1="-7.96064375" y1="-7.423409375" x2="-7.82964375" y2="-7.410309375" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.423409375" x2="-5.41854375" y2="-7.410309375" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.423409375" x2="-4.92054375" y2="-7.410309375" layer="51"/>
<rectangle x1="7.80345625" y1="-7.423409375" x2="7.93445625" y2="-7.410309375" layer="51"/>
<rectangle x1="-7.94754375" y1="-7.410309375" x2="-7.82964375" y2="-7.397209375" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.410309375" x2="-5.41854375" y2="-7.397209375" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.410309375" x2="-4.92054375" y2="-7.397209375" layer="51"/>
<rectangle x1="7.80345625" y1="-7.410309375" x2="7.92135625" y2="-7.397209375" layer="51"/>
<rectangle x1="-7.94754375" y1="-7.397209375" x2="-7.81654375" y2="-7.384103125" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.397209375" x2="-5.41854375" y2="-7.384103125" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.397209375" x2="-4.92054375" y2="-7.384103125" layer="51"/>
<rectangle x1="7.79035625" y1="-7.397209375" x2="7.92135625" y2="-7.384103125" layer="51"/>
<rectangle x1="-7.93444375" y1="-7.3841" x2="-7.80344375" y2="-7.371" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.3841" x2="-5.41854375" y2="-7.371" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.3841" x2="-4.92054375" y2="-7.371" layer="51"/>
<rectangle x1="7.79035625" y1="-7.3841" x2="7.90825625" y2="-7.371" layer="51"/>
<rectangle x1="-7.92134375" y1="-7.371" x2="-7.80344375" y2="-7.35789375" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.371" x2="-5.41854375" y2="-7.35789375" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.371" x2="-4.92054375" y2="-7.35789375" layer="51"/>
<rectangle x1="7.77725625" y1="-7.371" x2="7.89515625" y2="-7.35789375" layer="51"/>
<rectangle x1="-7.92134375" y1="-7.357890625" x2="-7.79034375" y2="-7.344790625" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.357890625" x2="-5.41854375" y2="-7.344790625" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.357890625" x2="-4.92054375" y2="-7.344790625" layer="51"/>
<rectangle x1="7.76415625" y1="-7.357890625" x2="7.89515625" y2="-7.344790625" layer="51"/>
<rectangle x1="-7.90824375" y1="-7.344790625" x2="-7.79034375" y2="-7.331690625" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.344790625" x2="-5.41854375" y2="-7.331690625" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.344790625" x2="-4.92054375" y2="-7.331690625" layer="51"/>
<rectangle x1="7.76415625" y1="-7.344790625" x2="7.88205625" y2="-7.331690625" layer="51"/>
<rectangle x1="-7.89514375" y1="-7.331690625" x2="-7.77724375" y2="-7.318584375" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.331690625" x2="-5.41854375" y2="-7.318584375" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.331690625" x2="-4.92054375" y2="-7.318584375" layer="51"/>
<rectangle x1="7.75105625" y1="-7.331690625" x2="7.88205625" y2="-7.318584375" layer="51"/>
<rectangle x1="-7.89514375" y1="-7.31858125" x2="-7.76414375" y2="-7.30548125" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.31858125" x2="-5.41854375" y2="-7.30548125" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.31858125" x2="-4.92054375" y2="-7.30548125" layer="51"/>
<rectangle x1="7.73795625" y1="-7.31858125" x2="7.86895625" y2="-7.30548125" layer="51"/>
<rectangle x1="-7.88204375" y1="-7.30548125" x2="-7.76414375" y2="-7.29236875" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.30548125" x2="-5.41854375" y2="-7.29236875" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.30548125" x2="-4.92054375" y2="-7.29236875" layer="51"/>
<rectangle x1="7.73795625" y1="-7.30548125" x2="7.85585625" y2="-7.29236875" layer="51"/>
<rectangle x1="-7.88204375" y1="-7.29236875" x2="-7.75104375" y2="-7.27926875" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.29236875" x2="-5.41854375" y2="-7.27926875" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.29236875" x2="-4.92054375" y2="-7.27926875" layer="51"/>
<rectangle x1="7.72485625" y1="-7.29236875" x2="7.85585625" y2="-7.27926875" layer="51"/>
<rectangle x1="-7.86894375" y1="-7.27926875" x2="-7.73794375" y2="-7.26616875" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.27926875" x2="-5.41854375" y2="-7.26616875" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.27926875" x2="-4.92054375" y2="-7.26616875" layer="51"/>
<rectangle x1="7.72485625" y1="-7.27926875" x2="7.84275625" y2="-7.26616875" layer="51"/>
<rectangle x1="-7.85584375" y1="-7.26616875" x2="-7.73794375" y2="-7.2530625" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.26616875" x2="-5.41854375" y2="-7.2530625" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.26616875" x2="-4.92054375" y2="-7.2530625" layer="51"/>
<rectangle x1="7.71175625" y1="-7.26616875" x2="7.82965625" y2="-7.2530625" layer="51"/>
<rectangle x1="-7.85584375" y1="-7.253059375" x2="-7.72484375" y2="-7.239959375" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.253059375" x2="-5.41854375" y2="-7.239959375" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.253059375" x2="-4.92054375" y2="-7.239959375" layer="51"/>
<rectangle x1="7.69865625" y1="-7.253059375" x2="7.82965625" y2="-7.239959375" layer="51"/>
<rectangle x1="-7.84274375" y1="-7.239959375" x2="-7.72484375" y2="-7.226853125" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.239959375" x2="-5.41854375" y2="-7.226853125" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.239959375" x2="-4.92054375" y2="-7.226853125" layer="51"/>
<rectangle x1="7.69865625" y1="-7.239959375" x2="7.81655625" y2="-7.226853125" layer="51"/>
<rectangle x1="-7.82964375" y1="-7.22685" x2="-7.71174375" y2="-7.21375" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.22685" x2="-5.41854375" y2="-7.21375" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.22685" x2="-4.92054375" y2="-7.21375" layer="51"/>
<rectangle x1="7.68555625" y1="-7.22685" x2="7.81655625" y2="-7.21375" layer="51"/>
<rectangle x1="-7.82964375" y1="-7.21375" x2="-7.69864375" y2="-7.20065" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.21375" x2="-5.41854375" y2="-7.20065" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.21375" x2="-4.92054375" y2="-7.20065" layer="51"/>
<rectangle x1="7.67245625" y1="-7.21375" x2="7.80345625" y2="-7.20065" layer="51"/>
<rectangle x1="-7.81654375" y1="-7.20065" x2="-7.69864375" y2="-7.18754375" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.20065" x2="-5.41854375" y2="-7.18754375" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.20065" x2="-4.92054375" y2="-7.18754375" layer="51"/>
<rectangle x1="7.67245625" y1="-7.20065" x2="7.79035625" y2="-7.18754375" layer="51"/>
<rectangle x1="-7.81654375" y1="-7.187540625" x2="-7.68554375" y2="-7.174440625" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.187540625" x2="-5.41854375" y2="-7.174440625" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.187540625" x2="-4.92054375" y2="-7.174440625" layer="51"/>
<rectangle x1="7.65925625" y1="-7.187540625" x2="7.79035625" y2="-7.174440625" layer="51"/>
<rectangle x1="-7.80344375" y1="-7.174440625" x2="-7.67234375" y2="-7.161334375" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.174440625" x2="-5.41854375" y2="-7.161334375" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.174440625" x2="-4.92054375" y2="-7.161334375" layer="51"/>
<rectangle x1="7.65925625" y1="-7.174440625" x2="7.77725625" y2="-7.161334375" layer="51"/>
<rectangle x1="-7.79034375" y1="-7.16133125" x2="-7.67234375" y2="-7.14823125" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.16133125" x2="-5.41854375" y2="-7.14823125" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.16133125" x2="-4.92054375" y2="-7.14823125" layer="51"/>
<rectangle x1="7.64615625" y1="-7.16133125" x2="7.76415625" y2="-7.14823125" layer="51"/>
<rectangle x1="-7.79034375" y1="-7.14823125" x2="-7.65924375" y2="-7.13513125" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.14823125" x2="-5.41854375" y2="-7.13513125" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.14823125" x2="-4.92054375" y2="-7.13513125" layer="51"/>
<rectangle x1="7.63305625" y1="-7.14823125" x2="7.76415625" y2="-7.13513125" layer="51"/>
<rectangle x1="-7.77724375" y1="-7.13513125" x2="-7.65924375" y2="-7.12201875" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.13513125" x2="-5.41854375" y2="-7.12201875" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.13513125" x2="-4.92054375" y2="-7.12201875" layer="51"/>
<rectangle x1="7.63305625" y1="-7.13513125" x2="7.75105625" y2="-7.12201875" layer="51"/>
<rectangle x1="-7.76414375" y1="-7.12201875" x2="-7.64614375" y2="-7.10891875" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.12201875" x2="-5.41854375" y2="-7.10891875" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.12201875" x2="-4.92054375" y2="-7.10891875" layer="51"/>
<rectangle x1="7.61995625" y1="-7.12201875" x2="7.75105625" y2="-7.10891875" layer="51"/>
<rectangle x1="-7.76414375" y1="-7.10891875" x2="-7.63304375" y2="-7.0958125" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.10891875" x2="-5.41854375" y2="-7.0958125" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.10891875" x2="-4.92054375" y2="-7.0958125" layer="51"/>
<rectangle x1="7.60685625" y1="-7.10891875" x2="7.73795625" y2="-7.0958125" layer="51"/>
<rectangle x1="-7.75104375" y1="-7.095809375" x2="-7.63304375" y2="-7.082709375" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.095809375" x2="-5.41854375" y2="-7.082709375" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.095809375" x2="-4.92054375" y2="-7.082709375" layer="51"/>
<rectangle x1="7.60685625" y1="-7.095809375" x2="7.72485625" y2="-7.082709375" layer="51"/>
<rectangle x1="-7.75104375" y1="-7.082709375" x2="-7.61994375" y2="-7.069609375" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.082709375" x2="-5.41854375" y2="-7.069609375" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.082709375" x2="-4.92054375" y2="-7.069609375" layer="51"/>
<rectangle x1="7.59375625" y1="-7.082709375" x2="7.72485625" y2="-7.069609375" layer="51"/>
<rectangle x1="-7.73794375" y1="-7.069609375" x2="-7.60684375" y2="-7.056503125" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.069609375" x2="-5.41854375" y2="-7.056503125" layer="51"/>
<rectangle x1="-5.02534375" y1="-7.069609375" x2="-4.92054375" y2="-7.056503125" layer="51"/>
<rectangle x1="7.59375625" y1="-7.069609375" x2="7.71175625" y2="-7.056503125" layer="51"/>
<rectangle x1="-7.72484375" y1="-7.0565" x2="-7.60684375" y2="-7.0434" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.0565" x2="-4.92054375" y2="-7.0434" layer="51"/>
<rectangle x1="7.58065625" y1="-7.0565" x2="7.69865625" y2="-7.0434" layer="51"/>
<rectangle x1="-7.72484375" y1="-7.0434" x2="-7.59374375" y2="-7.03029375" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.0434" x2="-4.92054375" y2="-7.03029375" layer="51"/>
<rectangle x1="7.56755625" y1="-7.0434" x2="7.69865625" y2="-7.03029375" layer="51"/>
<rectangle x1="-7.71174375" y1="-7.030290625" x2="-7.59374375" y2="-7.017190625" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.030290625" x2="-4.92054375" y2="-7.017190625" layer="51"/>
<rectangle x1="7.56755625" y1="-7.030290625" x2="7.68555625" y2="-7.017190625" layer="51"/>
<rectangle x1="-7.69864375" y1="-7.017190625" x2="-7.58064375" y2="-7.004084375" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.017190625" x2="-4.92054375" y2="-7.004084375" layer="51"/>
<rectangle x1="7.55445625" y1="-7.017190625" x2="7.68555625" y2="-7.004084375" layer="51"/>
<rectangle x1="-7.69864375" y1="-7.00408125" x2="-7.56754375" y2="-6.99098125" layer="51"/>
<rectangle x1="-5.52334375" y1="-7.00408125" x2="-4.92054375" y2="-6.99098125" layer="51"/>
<rectangle x1="7.54135625" y1="-7.00408125" x2="7.67245625" y2="-6.99098125" layer="51"/>
<rectangle x1="-7.68554375" y1="-6.99098125" x2="-7.56754375" y2="-6.97788125" layer="51"/>
<rectangle x1="-5.47094375" y1="-6.99098125" x2="-4.97294375" y2="-6.97788125" layer="51"/>
<rectangle x1="7.54135625" y1="-6.99098125" x2="7.65925625" y2="-6.97788125" layer="51"/>
<rectangle x1="-7.68554375" y1="-6.97788125" x2="-7.55444375" y2="-6.96476875" layer="51"/>
<rectangle x1="-5.47094375" y1="-6.97788125" x2="-4.97294375" y2="-6.96476875" layer="51"/>
<rectangle x1="7.52825625" y1="-6.97788125" x2="7.65925625" y2="-6.96476875" layer="51"/>
<rectangle x1="-7.67234375" y1="-6.96476875" x2="-7.54134375" y2="-6.95166875" layer="51"/>
<rectangle x1="-5.47094375" y1="-6.96476875" x2="-4.97294375" y2="-6.95166875" layer="51"/>
<rectangle x1="7.52825625" y1="-6.96476875" x2="7.64615625" y2="-6.95166875" layer="51"/>
<rectangle x1="-7.65924375" y1="-6.95166875" x2="-7.54134375" y2="-6.9385625" layer="51"/>
<rectangle x1="7.51515625" y1="-6.95166875" x2="7.63305625" y2="-6.9385625" layer="51"/>
<rectangle x1="-7.65924375" y1="-6.938559375" x2="-7.52824375" y2="-6.925459375" layer="51"/>
<rectangle x1="7.50205625" y1="-6.938559375" x2="7.63305625" y2="-6.925459375" layer="51"/>
<rectangle x1="-7.64614375" y1="-6.925459375" x2="-7.52824375" y2="-6.912359375" layer="51"/>
<rectangle x1="7.50205625" y1="-6.925459375" x2="7.61995625" y2="-6.912359375" layer="51"/>
<rectangle x1="-7.63304375" y1="-6.912359375" x2="-7.51514375" y2="-6.899253125" layer="51"/>
<rectangle x1="7.48895625" y1="-6.912359375" x2="7.61995625" y2="-6.899253125" layer="51"/>
<rectangle x1="-7.63304375" y1="-6.89925" x2="-7.50204375" y2="-6.88615" layer="51"/>
<rectangle x1="7.47585625" y1="-6.89925" x2="7.60685625" y2="-6.88615" layer="51"/>
<rectangle x1="-7.61994375" y1="-6.88615" x2="-7.50204375" y2="-6.87304375" layer="51"/>
<rectangle x1="7.47585625" y1="-6.88615" x2="7.59375625" y2="-6.87304375" layer="51"/>
<rectangle x1="-7.61994375" y1="-6.873040625" x2="-7.48894375" y2="-6.859940625" layer="51"/>
<rectangle x1="7.46275625" y1="-6.873040625" x2="7.59375625" y2="-6.859940625" layer="51"/>
<rectangle x1="-7.60684375" y1="-6.859940625" x2="-7.47584375" y2="-6.846840625" layer="51"/>
<rectangle x1="7.46275625" y1="-6.859940625" x2="7.58065625" y2="-6.846840625" layer="51"/>
<rectangle x1="-7.59374375" y1="-6.846840625" x2="-7.47584375" y2="-6.833734375" layer="51"/>
<rectangle x1="7.44965625" y1="-6.846840625" x2="7.56755625" y2="-6.833734375" layer="51"/>
<rectangle x1="-7.59374375" y1="-6.83373125" x2="-7.46274375" y2="-6.82063125" layer="51"/>
<rectangle x1="7.43655625" y1="-6.83373125" x2="7.56755625" y2="-6.82063125" layer="51"/>
<rectangle x1="-7.58064375" y1="-6.82063125" x2="-7.46274375" y2="-6.80751875" layer="51"/>
<rectangle x1="7.43655625" y1="-6.82063125" x2="7.55445625" y2="-6.80751875" layer="51"/>
<rectangle x1="-7.56754375" y1="-6.80751875" x2="-7.44964375" y2="-6.79441875" layer="51"/>
<rectangle x1="7.42345625" y1="-6.80751875" x2="7.55445625" y2="-6.79441875" layer="51"/>
<rectangle x1="-7.56754375" y1="-6.79441875" x2="-7.43654375" y2="-6.78131875" layer="51"/>
<rectangle x1="7.41035625" y1="-6.79441875" x2="7.54135625" y2="-6.78131875" layer="51"/>
<rectangle x1="-7.55444375" y1="-6.78131875" x2="-7.43654375" y2="-6.7682125" layer="51"/>
<rectangle x1="7.41035625" y1="-6.78131875" x2="7.52825625" y2="-6.7682125" layer="51"/>
<rectangle x1="-7.55444375" y1="-6.768209375" x2="-7.42344375" y2="-6.755109375" layer="51"/>
<rectangle x1="7.39725625" y1="-6.768209375" x2="7.52825625" y2="-6.755109375" layer="51"/>
<rectangle x1="-7.54134375" y1="-6.755109375" x2="-7.41034375" y2="-6.742003125" layer="51"/>
<rectangle x1="7.39725625" y1="-6.755109375" x2="7.51515625" y2="-6.742003125" layer="51"/>
<rectangle x1="-7.52824375" y1="-6.742" x2="-7.41034375" y2="-6.7289" layer="51"/>
<rectangle x1="7.38415625" y1="-6.742" x2="7.50205625" y2="-6.7289" layer="51"/>
<rectangle x1="-7.52824375" y1="-6.7289" x2="-7.39724375" y2="-6.7158" layer="51"/>
<rectangle x1="7.38415625" y1="-6.7289" x2="7.50205625" y2="-6.7158" layer="51"/>
<rectangle x1="-7.51514375" y1="-6.7158" x2="-7.39724375" y2="-6.70269375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.7158" x2="7.48895625" y2="-6.70269375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.702690625" x2="-7.39724375" y2="-6.689590625" layer="51"/>
<rectangle x1="7.37105625" y1="-6.702690625" x2="7.48895625" y2="-6.689590625" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.689590625" x2="-7.39724375" y2="-6.676484375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.689590625" x2="7.47585625" y2="-6.676484375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.67648125" x2="-7.39724375" y2="-6.66338125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.67648125" x2="7.47585625" y2="-6.66338125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.66338125" x2="-7.39724375" y2="-6.65028125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.66338125" x2="7.47585625" y2="-6.65028125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.65028125" x2="-7.39724375" y2="-6.63716875" layer="51"/>
<rectangle x1="7.37105625" y1="-6.65028125" x2="7.47585625" y2="-6.63716875" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.63716875" x2="-7.39724375" y2="-6.62406875" layer="51"/>
<rectangle x1="7.37105625" y1="-6.63716875" x2="7.47585625" y2="-6.62406875" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.62406875" x2="-7.39724375" y2="-6.6109625" layer="51"/>
<rectangle x1="7.37105625" y1="-6.62406875" x2="7.47585625" y2="-6.6109625" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.610959375" x2="-7.39724375" y2="-6.597859375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.610959375" x2="7.47585625" y2="-6.597859375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.597859375" x2="-7.39724375" y2="-6.584759375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.597859375" x2="7.47585625" y2="-6.584759375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.584759375" x2="-7.39724375" y2="-6.571653125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.584759375" x2="7.47585625" y2="-6.571653125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.57165" x2="-7.39724375" y2="-6.55855" layer="51"/>
<rectangle x1="7.37105625" y1="-6.57165" x2="7.47585625" y2="-6.55855" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.55855" x2="-7.39724375" y2="-6.54544375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.55855" x2="7.47585625" y2="-6.54544375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.545440625" x2="-7.39724375" y2="-6.532340625" layer="51"/>
<rectangle x1="7.37105625" y1="-6.545440625" x2="7.47585625" y2="-6.532340625" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.532340625" x2="-7.39724375" y2="-6.519240625" layer="51"/>
<rectangle x1="7.37105625" y1="-6.532340625" x2="7.47585625" y2="-6.519240625" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.519240625" x2="-7.39724375" y2="-6.506134375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.519240625" x2="7.47585625" y2="-6.506134375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.50613125" x2="-7.39724375" y2="-6.49303125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.50613125" x2="7.47585625" y2="-6.49303125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.49303125" x2="-7.39724375" y2="-6.47991875" layer="51"/>
<rectangle x1="7.37105625" y1="-6.49303125" x2="7.47585625" y2="-6.47991875" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.47991875" x2="-7.39724375" y2="-6.46681875" layer="51"/>
<rectangle x1="7.37105625" y1="-6.47991875" x2="7.47585625" y2="-6.46681875" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.46681875" x2="-7.39724375" y2="-6.45371875" layer="51"/>
<rectangle x1="7.37105625" y1="-6.46681875" x2="7.47585625" y2="-6.45371875" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.45371875" x2="-7.39724375" y2="-6.4406125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.45371875" x2="7.47585625" y2="-6.4406125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.440609375" x2="-7.39724375" y2="-6.427509375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.440609375" x2="7.47585625" y2="-6.427509375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.427509375" x2="-7.39724375" y2="-6.414403125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.427509375" x2="7.47585625" y2="-6.414403125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.4144" x2="-7.39724375" y2="-6.4013" layer="51"/>
<rectangle x1="7.37105625" y1="-6.4144" x2="7.47585625" y2="-6.4013" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.4013" x2="-7.39724375" y2="-6.3882" layer="51"/>
<rectangle x1="7.37105625" y1="-6.4013" x2="7.47585625" y2="-6.3882" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.3882" x2="-7.39724375" y2="-6.37509375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.3882" x2="7.47585625" y2="-6.37509375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.375090625" x2="-7.39724375" y2="-6.361990625" layer="51"/>
<rectangle x1="7.37105625" y1="-6.375090625" x2="7.47585625" y2="-6.361990625" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.361990625" x2="-7.39724375" y2="-6.348884375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.361990625" x2="7.47585625" y2="-6.348884375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.34888125" x2="-7.39724375" y2="-6.33578125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.34888125" x2="7.47585625" y2="-6.33578125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.33578125" x2="-7.39724375" y2="-6.32268125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.33578125" x2="7.47585625" y2="-6.32268125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.32268125" x2="-7.39724375" y2="-6.30956875" layer="51"/>
<rectangle x1="7.37105625" y1="-6.32268125" x2="7.47585625" y2="-6.30956875" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.30956875" x2="-7.39724375" y2="-6.29646875" layer="51"/>
<rectangle x1="7.37105625" y1="-6.30956875" x2="7.47585625" y2="-6.29646875" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.29646875" x2="-7.39724375" y2="-6.2833625" layer="51"/>
<rectangle x1="7.37105625" y1="-6.29646875" x2="7.47585625" y2="-6.2833625" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.283359375" x2="-7.39724375" y2="-6.270259375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.283359375" x2="7.47585625" y2="-6.270259375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.270259375" x2="-7.39724375" y2="-6.257159375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.270259375" x2="7.47585625" y2="-6.257159375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.257159375" x2="-7.39724375" y2="-6.244053125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.257159375" x2="7.47585625" y2="-6.244053125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.24405" x2="-7.39724375" y2="-6.23095" layer="51"/>
<rectangle x1="7.37105625" y1="-6.24405" x2="7.47585625" y2="-6.23095" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.23095" x2="-7.39724375" y2="-6.21784375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.23095" x2="7.47585625" y2="-6.21784375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.217840625" x2="-7.39724375" y2="-6.204740625" layer="51"/>
<rectangle x1="7.37105625" y1="-6.217840625" x2="7.47585625" y2="-6.204740625" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.204740625" x2="-7.39724375" y2="-6.191640625" layer="51"/>
<rectangle x1="7.37105625" y1="-6.204740625" x2="7.47585625" y2="-6.191640625" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.191640625" x2="-7.39724375" y2="-6.178534375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.191640625" x2="7.47585625" y2="-6.178534375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.17853125" x2="-7.39724375" y2="-6.16543125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.17853125" x2="7.47585625" y2="-6.16543125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.16543125" x2="-7.39724375" y2="-6.15231875" layer="51"/>
<rectangle x1="7.37105625" y1="-6.16543125" x2="7.47585625" y2="-6.15231875" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.15231875" x2="-7.39724375" y2="-6.13921875" layer="51"/>
<rectangle x1="7.37105625" y1="-6.15231875" x2="7.47585625" y2="-6.13921875" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.13921875" x2="-7.39724375" y2="-6.12611875" layer="51"/>
<rectangle x1="7.37105625" y1="-6.13921875" x2="7.47585625" y2="-6.12611875" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.12611875" x2="-7.39724375" y2="-6.1130125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.12611875" x2="7.47585625" y2="-6.1130125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.113009375" x2="-7.39724375" y2="-6.099909375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.113009375" x2="7.47585625" y2="-6.099909375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.099909375" x2="-7.39724375" y2="-6.086803125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.099909375" x2="7.47585625" y2="-6.086803125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.0868" x2="-7.39724375" y2="-6.0737" layer="51"/>
<rectangle x1="7.37105625" y1="-6.0868" x2="7.47585625" y2="-6.0737" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.0737" x2="-7.39724375" y2="-6.0606" layer="51"/>
<rectangle x1="7.37105625" y1="-6.0737" x2="7.47585625" y2="-6.0606" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.0606" x2="-7.39724375" y2="-6.04749375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.0606" x2="7.47585625" y2="-6.04749375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.047490625" x2="-7.39724375" y2="-6.034390625" layer="51"/>
<rectangle x1="7.37105625" y1="-6.047490625" x2="7.47585625" y2="-6.034390625" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.034390625" x2="-7.39724375" y2="-6.021284375" layer="51"/>
<rectangle x1="7.37105625" y1="-6.034390625" x2="7.47585625" y2="-6.021284375" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.02128125" x2="-7.39724375" y2="-6.00818125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.02128125" x2="7.47585625" y2="-6.00818125" layer="51"/>
<rectangle x1="-7.50204375" y1="-6.00818125" x2="-7.39724375" y2="-5.99508125" layer="51"/>
<rectangle x1="7.37105625" y1="-6.00818125" x2="7.47585625" y2="-5.99508125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.99508125" x2="-7.39724375" y2="-5.98196875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.99508125" x2="7.47585625" y2="-5.98196875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.98196875" x2="-7.39724375" y2="-5.96886875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.98196875" x2="7.47585625" y2="-5.96886875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.96886875" x2="-7.39724375" y2="-5.9557625" layer="51"/>
<rectangle x1="7.37105625" y1="-5.96886875" x2="7.47585625" y2="-5.9557625" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.955759375" x2="-7.39724375" y2="-5.942659375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.955759375" x2="7.47585625" y2="-5.942659375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.942659375" x2="-7.39724375" y2="-5.929559375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.942659375" x2="7.47585625" y2="-5.929559375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.929559375" x2="-7.39724375" y2="-5.916453125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.929559375" x2="7.47585625" y2="-5.916453125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.91645" x2="-7.39724375" y2="-5.90335" layer="51"/>
<rectangle x1="7.37105625" y1="-5.91645" x2="7.47585625" y2="-5.90335" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.90335" x2="-7.39724375" y2="-5.89024375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.90335" x2="7.47585625" y2="-5.89024375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.890240625" x2="-7.39724375" y2="-5.877140625" layer="51"/>
<rectangle x1="7.37105625" y1="-5.890240625" x2="7.47585625" y2="-5.877140625" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.877140625" x2="-7.39724375" y2="-5.864040625" layer="51"/>
<rectangle x1="7.37105625" y1="-5.877140625" x2="7.47585625" y2="-5.864040625" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.864040625" x2="-7.39724375" y2="-5.850934375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.864040625" x2="7.47585625" y2="-5.850934375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.85093125" x2="-7.39724375" y2="-5.83783125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.85093125" x2="7.47585625" y2="-5.83783125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.83783125" x2="-7.39724375" y2="-5.82471875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.83783125" x2="7.47585625" y2="-5.82471875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.82471875" x2="-7.39724375" y2="-5.81161875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.82471875" x2="7.47585625" y2="-5.81161875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.81161875" x2="-7.39724375" y2="-5.79851875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.81161875" x2="7.47585625" y2="-5.79851875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.79851875" x2="-7.39724375" y2="-5.7854125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.79851875" x2="7.47585625" y2="-5.7854125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.785409375" x2="-7.39724375" y2="-5.772309375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.785409375" x2="7.47585625" y2="-5.772309375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.772309375" x2="-7.39724375" y2="-5.759203125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.772309375" x2="7.47585625" y2="-5.759203125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.7592" x2="-7.39724375" y2="-5.7461" layer="51"/>
<rectangle x1="7.37105625" y1="-5.7592" x2="7.47585625" y2="-5.7461" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.7461" x2="-7.39724375" y2="-5.733" layer="51"/>
<rectangle x1="7.37105625" y1="-5.7461" x2="7.47585625" y2="-5.733" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.733" x2="-7.39724375" y2="-5.71989375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.733" x2="7.47585625" y2="-5.71989375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.719890625" x2="-7.39724375" y2="-5.706790625" layer="51"/>
<rectangle x1="7.37105625" y1="-5.719890625" x2="7.47585625" y2="-5.706790625" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.706790625" x2="-7.39724375" y2="-5.693684375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.706790625" x2="7.47585625" y2="-5.693684375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.69368125" x2="-7.39724375" y2="-5.68058125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.69368125" x2="7.47585625" y2="-5.68058125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.68058125" x2="-7.39724375" y2="-5.66748125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.68058125" x2="7.47585625" y2="-5.66748125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.66748125" x2="-7.39724375" y2="-5.65436875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.66748125" x2="7.47585625" y2="-5.65436875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.65436875" x2="-7.39724375" y2="-5.64126875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.65436875" x2="7.47585625" y2="-5.64126875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.64126875" x2="-7.39724375" y2="-5.6281625" layer="51"/>
<rectangle x1="7.37105625" y1="-5.64126875" x2="7.47585625" y2="-5.6281625" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.628159375" x2="-7.39724375" y2="-5.615059375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.628159375" x2="7.47585625" y2="-5.615059375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.615059375" x2="-7.39724375" y2="-5.601959375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.615059375" x2="7.47585625" y2="-5.601959375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.601959375" x2="-7.39724375" y2="-5.588853125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.601959375" x2="7.47585625" y2="-5.588853125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.58885" x2="-7.39724375" y2="-5.57575" layer="51"/>
<rectangle x1="7.37105625" y1="-5.58885" x2="7.47585625" y2="-5.57575" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.57575" x2="-7.39724375" y2="-5.56264375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.57575" x2="7.47585625" y2="-5.56264375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.562640625" x2="-7.39724375" y2="-5.549540625" layer="51"/>
<rectangle x1="7.37105625" y1="-5.562640625" x2="7.47585625" y2="-5.549540625" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.549540625" x2="-7.39724375" y2="-5.536440625" layer="51"/>
<rectangle x1="7.37105625" y1="-5.549540625" x2="7.47585625" y2="-5.536440625" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.536440625" x2="-7.39724375" y2="-5.523334375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.536440625" x2="7.47585625" y2="-5.523334375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.52333125" x2="-7.39724375" y2="-5.51023125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.52333125" x2="7.47585625" y2="-5.51023125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.51023125" x2="-7.39724375" y2="-5.49711875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.51023125" x2="7.47585625" y2="-5.49711875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.49711875" x2="-7.39724375" y2="-5.48401875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.49711875" x2="7.47585625" y2="-5.48401875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.48401875" x2="-7.39724375" y2="-5.47091875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.48401875" x2="7.47585625" y2="-5.47091875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.47091875" x2="-7.39724375" y2="-5.4578125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.47091875" x2="7.47585625" y2="-5.4578125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.457809375" x2="-7.39724375" y2="-5.444709375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.457809375" x2="7.47585625" y2="-5.444709375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.444709375" x2="-7.39724375" y2="-5.431603125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.444709375" x2="7.47585625" y2="-5.431603125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.4316" x2="-7.39724375" y2="-5.4185" layer="51"/>
<rectangle x1="7.37105625" y1="-5.4316" x2="7.47585625" y2="-5.4185" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.4185" x2="-7.39724375" y2="-5.4054" layer="51"/>
<rectangle x1="7.37105625" y1="-5.4185" x2="7.47585625" y2="-5.4054" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.4054" x2="-7.39724375" y2="-5.39229375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.4054" x2="7.47585625" y2="-5.39229375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.392290625" x2="-7.39724375" y2="-5.379190625" layer="51"/>
<rectangle x1="7.37105625" y1="-5.392290625" x2="7.47585625" y2="-5.379190625" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.379190625" x2="-7.39724375" y2="-5.366084375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.379190625" x2="7.47585625" y2="-5.366084375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.36608125" x2="-7.39724375" y2="-5.35298125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.36608125" x2="7.47585625" y2="-5.35298125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.35298125" x2="-7.39724375" y2="-5.33988125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.35298125" x2="7.47585625" y2="-5.33988125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.33988125" x2="-7.39724375" y2="-5.32676875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.33988125" x2="7.47585625" y2="-5.32676875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.32676875" x2="-7.39724375" y2="-5.31366875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.32676875" x2="7.47585625" y2="-5.31366875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.31366875" x2="-7.39724375" y2="-5.3005625" layer="51"/>
<rectangle x1="7.37105625" y1="-5.31366875" x2="7.47585625" y2="-5.3005625" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.300559375" x2="-7.39724375" y2="-5.287459375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.300559375" x2="7.47585625" y2="-5.287459375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.287459375" x2="-7.39724375" y2="-5.274359375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.287459375" x2="7.47585625" y2="-5.274359375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.274359375" x2="-7.39724375" y2="-5.261253125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.274359375" x2="7.47585625" y2="-5.261253125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.26125" x2="-7.39724375" y2="-5.24815" layer="51"/>
<rectangle x1="7.37105625" y1="-5.26125" x2="7.47585625" y2="-5.24815" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.24815" x2="-7.39724375" y2="-5.23504375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.24815" x2="7.47585625" y2="-5.23504375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.235040625" x2="-7.39724375" y2="-5.221940625" layer="51"/>
<rectangle x1="7.37105625" y1="-5.235040625" x2="7.47585625" y2="-5.221940625" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.221940625" x2="-7.39724375" y2="-5.208840625" layer="51"/>
<rectangle x1="7.37105625" y1="-5.221940625" x2="7.47585625" y2="-5.208840625" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.208840625" x2="-7.39724375" y2="-5.195734375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.208840625" x2="7.47585625" y2="-5.195734375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.19573125" x2="-7.39724375" y2="-5.18263125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.19573125" x2="7.47585625" y2="-5.18263125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.18263125" x2="-7.39724375" y2="-5.16951875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.18263125" x2="7.47585625" y2="-5.16951875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.16951875" x2="-7.39724375" y2="-5.15641875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.16951875" x2="7.47585625" y2="-5.15641875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.15641875" x2="-7.39724375" y2="-5.14331875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.15641875" x2="7.47585625" y2="-5.14331875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.14331875" x2="-7.39724375" y2="-5.1302125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.14331875" x2="7.47585625" y2="-5.1302125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.130209375" x2="-7.39724375" y2="-5.117109375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.130209375" x2="7.47585625" y2="-5.117109375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.117109375" x2="-7.39724375" y2="-5.104003125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.117109375" x2="7.47585625" y2="-5.104003125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.104" x2="-7.39724375" y2="-5.0909" layer="51"/>
<rectangle x1="7.37105625" y1="-5.104" x2="7.47585625" y2="-5.0909" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.0909" x2="-7.39724375" y2="-5.0778" layer="51"/>
<rectangle x1="7.37105625" y1="-5.0909" x2="7.47585625" y2="-5.0778" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.0778" x2="-7.39724375" y2="-5.06469375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.0778" x2="7.47585625" y2="-5.06469375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.064690625" x2="-7.39724375" y2="-5.051590625" layer="51"/>
<rectangle x1="7.37105625" y1="-5.064690625" x2="7.47585625" y2="-5.051590625" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.051590625" x2="-7.39724375" y2="-5.038484375" layer="51"/>
<rectangle x1="7.37105625" y1="-5.051590625" x2="7.47585625" y2="-5.038484375" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.03848125" x2="-7.39724375" y2="-5.02538125" layer="51"/>
<rectangle x1="7.37105625" y1="-5.03848125" x2="7.47585625" y2="-5.02538125" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.02538125" x2="-7.39724375" y2="-5.01226875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.02538125" x2="7.47585625" y2="-5.01226875" layer="51"/>
<rectangle x1="-7.50204375" y1="-5.01226875" x2="-7.39724375" y2="-4.99916875" layer="51"/>
<rectangle x1="7.37105625" y1="-5.01226875" x2="7.47585625" y2="-4.99916875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.99916875" x2="-7.39724375" y2="-4.98606875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.99916875" x2="7.47585625" y2="-4.98606875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.98606875" x2="-7.39724375" y2="-4.9729625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.98606875" x2="7.47585625" y2="-4.9729625" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.972959375" x2="-7.39724375" y2="-4.959859375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.972959375" x2="7.47585625" y2="-4.959859375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.959859375" x2="-7.39724375" y2="-4.946753125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.959859375" x2="7.47585625" y2="-4.946753125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.94675" x2="-7.39724375" y2="-4.93365" layer="51"/>
<rectangle x1="7.37105625" y1="-4.94675" x2="7.47585625" y2="-4.93365" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.93365" x2="-7.39724375" y2="-4.92055" layer="51"/>
<rectangle x1="7.37105625" y1="-4.93365" x2="7.47585625" y2="-4.92055" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.92055" x2="-7.39724375" y2="-4.90744375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.92055" x2="7.47585625" y2="-4.90744375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.907440625" x2="-7.39724375" y2="-4.894340625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.907440625" x2="7.47585625" y2="-4.894340625" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.894340625" x2="-7.39724375" y2="-4.881234375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.894340625" x2="7.47585625" y2="-4.881234375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.88123125" x2="-7.39724375" y2="-4.86813125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.88123125" x2="7.47585625" y2="-4.86813125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.86813125" x2="-7.39724375" y2="-4.85503125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.86813125" x2="7.47585625" y2="-4.85503125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.85503125" x2="-7.39724375" y2="-4.84191875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.85503125" x2="7.47585625" y2="-4.84191875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.84191875" x2="-7.39724375" y2="-4.82881875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.84191875" x2="7.47585625" y2="-4.82881875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.82881875" x2="-7.39724375" y2="-4.8157125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.82881875" x2="7.47585625" y2="-4.8157125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.815709375" x2="-7.39724375" y2="-4.802609375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.815709375" x2="7.47585625" y2="-4.802609375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.802609375" x2="-7.39724375" y2="-4.789509375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.802609375" x2="7.47585625" y2="-4.789509375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.789509375" x2="-7.39724375" y2="-4.776403125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.789509375" x2="7.47585625" y2="-4.776403125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.7764" x2="-7.39724375" y2="-4.7633" layer="51"/>
<rectangle x1="7.37105625" y1="-4.7764" x2="7.47585625" y2="-4.7633" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.7633" x2="-7.39724375" y2="-4.75019375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.7633" x2="7.47585625" y2="-4.75019375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.750190625" x2="-7.39724375" y2="-4.737090625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.750190625" x2="7.47585625" y2="-4.737090625" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.737090625" x2="-7.39724375" y2="-4.723990625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.737090625" x2="7.47585625" y2="-4.723990625" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.723990625" x2="-7.39724375" y2="-4.710884375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.723990625" x2="7.47585625" y2="-4.710884375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.71088125" x2="-7.39724375" y2="-4.69778125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.71088125" x2="7.47585625" y2="-4.69778125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.69778125" x2="-7.39724375" y2="-4.68466875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.69778125" x2="7.47585625" y2="-4.68466875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.68466875" x2="-7.39724375" y2="-4.67156875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.68466875" x2="7.47585625" y2="-4.67156875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.67156875" x2="-7.39724375" y2="-4.65846875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.67156875" x2="7.47585625" y2="-4.65846875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.65846875" x2="-7.39724375" y2="-4.6453625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.65846875" x2="7.47585625" y2="-4.6453625" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.645359375" x2="-7.39724375" y2="-4.632259375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.645359375" x2="7.47585625" y2="-4.632259375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.632259375" x2="-7.39724375" y2="-4.619153125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.632259375" x2="7.47585625" y2="-4.619153125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.61915" x2="-7.39724375" y2="-4.60605" layer="51"/>
<rectangle x1="7.37105625" y1="-4.61915" x2="7.47585625" y2="-4.60605" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.60605" x2="-7.39724375" y2="-4.59295" layer="51"/>
<rectangle x1="7.37105625" y1="-4.60605" x2="7.47585625" y2="-4.59295" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.59295" x2="-7.39724375" y2="-4.57984375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.59295" x2="7.47585625" y2="-4.57984375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.579840625" x2="-7.39724375" y2="-4.566740625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.579840625" x2="7.47585625" y2="-4.566740625" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.566740625" x2="-7.39724375" y2="-4.553634375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.566740625" x2="7.47585625" y2="-4.553634375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.55363125" x2="-7.39724375" y2="-4.54053125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.55363125" x2="7.47585625" y2="-4.54053125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.54053125" x2="-7.39724375" y2="-4.52743125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.54053125" x2="7.47585625" y2="-4.52743125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.52743125" x2="-7.39724375" y2="-4.51431875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.52743125" x2="7.47585625" y2="-4.51431875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.51431875" x2="-7.39724375" y2="-4.50121875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.51431875" x2="7.47585625" y2="-4.50121875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.50121875" x2="-7.39724375" y2="-4.4881125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.50121875" x2="7.47585625" y2="-4.4881125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.488109375" x2="-7.39724375" y2="-4.475009375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.488109375" x2="7.47585625" y2="-4.475009375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.475009375" x2="-7.39724375" y2="-4.461909375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.475009375" x2="7.47585625" y2="-4.461909375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.461909375" x2="-7.39724375" y2="-4.448803125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.461909375" x2="7.47585625" y2="-4.448803125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.4488" x2="-7.39724375" y2="-4.4357" layer="51"/>
<rectangle x1="7.37105625" y1="-4.4488" x2="7.47585625" y2="-4.4357" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.4357" x2="-7.39724375" y2="-4.42259375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.4357" x2="7.47585625" y2="-4.42259375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.422590625" x2="-7.39724375" y2="-4.409490625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.422590625" x2="7.47585625" y2="-4.409490625" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.409490625" x2="-7.39724375" y2="-4.396390625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.409490625" x2="7.47585625" y2="-4.396390625" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.396390625" x2="-7.39724375" y2="-4.383284375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.396390625" x2="7.47585625" y2="-4.383284375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.38328125" x2="-7.39724375" y2="-4.37018125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.38328125" x2="7.47585625" y2="-4.37018125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.37018125" x2="-7.39724375" y2="-4.35706875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.37018125" x2="7.47585625" y2="-4.35706875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.35706875" x2="-7.39724375" y2="-4.34396875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.35706875" x2="7.47585625" y2="-4.34396875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.34396875" x2="-7.39724375" y2="-4.33086875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.34396875" x2="7.47585625" y2="-4.33086875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.33086875" x2="-7.39724375" y2="-4.3177625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.33086875" x2="7.47585625" y2="-4.3177625" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.317759375" x2="-7.39724375" y2="-4.304659375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.317759375" x2="7.47585625" y2="-4.304659375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.304659375" x2="-7.39724375" y2="-4.291553125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.304659375" x2="7.47585625" y2="-4.291553125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.29155" x2="-7.39724375" y2="-4.27845" layer="51"/>
<rectangle x1="7.37105625" y1="-4.29155" x2="7.47585625" y2="-4.27845" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.27845" x2="-7.39724375" y2="-4.26535" layer="51"/>
<rectangle x1="7.37105625" y1="-4.27845" x2="7.47585625" y2="-4.26535" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.26535" x2="-7.39724375" y2="-4.25224375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.26535" x2="7.47585625" y2="-4.25224375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.252240625" x2="-7.39724375" y2="-4.239140625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.252240625" x2="7.47585625" y2="-4.239140625" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.239140625" x2="-7.39724375" y2="-4.226034375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.239140625" x2="7.47585625" y2="-4.226034375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.22603125" x2="-7.39724375" y2="-4.21293125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.22603125" x2="7.47585625" y2="-4.21293125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.21293125" x2="-7.39724375" y2="-4.19983125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.21293125" x2="7.47585625" y2="-4.19983125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.19983125" x2="-7.39724375" y2="-4.18671875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.19983125" x2="7.47585625" y2="-4.18671875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.18671875" x2="-7.39724375" y2="-4.17361875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.18671875" x2="7.47585625" y2="-4.17361875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.17361875" x2="-7.39724375" y2="-4.1605125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.17361875" x2="7.47585625" y2="-4.1605125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.160509375" x2="-7.39724375" y2="-4.147409375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.160509375" x2="7.47585625" y2="-4.147409375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.147409375" x2="-7.39724375" y2="-4.134309375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.147409375" x2="7.47585625" y2="-4.134309375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.134309375" x2="-7.39724375" y2="-4.121203125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.134309375" x2="7.47585625" y2="-4.121203125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.1212" x2="-7.39724375" y2="-4.1081" layer="51"/>
<rectangle x1="7.37105625" y1="-4.1212" x2="7.47585625" y2="-4.1081" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.1081" x2="-7.39724375" y2="-4.09499375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.1081" x2="7.47585625" y2="-4.09499375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.094990625" x2="-7.39724375" y2="-4.081890625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.094990625" x2="7.47585625" y2="-4.081890625" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.081890625" x2="-7.39724375" y2="-4.068790625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.081890625" x2="7.47585625" y2="-4.068790625" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.068790625" x2="-7.39724375" y2="-4.055684375" layer="51"/>
<rectangle x1="7.37105625" y1="-4.068790625" x2="7.47585625" y2="-4.055684375" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.05568125" x2="-7.39724375" y2="-4.04258125" layer="51"/>
<rectangle x1="7.37105625" y1="-4.05568125" x2="7.47585625" y2="-4.04258125" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.04258125" x2="-7.39724375" y2="-4.02946875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.04258125" x2="7.47585625" y2="-4.02946875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.02946875" x2="-7.39724375" y2="-4.01636875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.02946875" x2="7.47585625" y2="-4.01636875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.01636875" x2="-7.39724375" y2="-4.00326875" layer="51"/>
<rectangle x1="7.37105625" y1="-4.01636875" x2="7.47585625" y2="-4.00326875" layer="51"/>
<rectangle x1="-7.50204375" y1="-4.00326875" x2="-7.39724375" y2="-3.9901625" layer="51"/>
<rectangle x1="7.37105625" y1="-4.00326875" x2="7.47585625" y2="-3.9901625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.990159375" x2="-7.39724375" y2="-3.977059375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.990159375" x2="7.47585625" y2="-3.977059375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.977059375" x2="-7.39724375" y2="-3.963953125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.977059375" x2="7.47585625" y2="-3.963953125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.96395" x2="-7.39724375" y2="-3.95085" layer="51"/>
<rectangle x1="7.37105625" y1="-3.96395" x2="7.47585625" y2="-3.95085" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.95085" x2="-7.39724375" y2="-3.93775" layer="51"/>
<rectangle x1="7.37105625" y1="-3.95085" x2="7.47585625" y2="-3.93775" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.93775" x2="-7.39724375" y2="-3.92464375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.93775" x2="7.47585625" y2="-3.92464375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.924640625" x2="-7.39724375" y2="-3.911540625" layer="51"/>
<rectangle x1="7.37105625" y1="-3.924640625" x2="7.47585625" y2="-3.911540625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.911540625" x2="-7.39724375" y2="-3.898434375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.911540625" x2="7.47585625" y2="-3.898434375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.89843125" x2="-7.39724375" y2="-3.88533125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.89843125" x2="7.47585625" y2="-3.88533125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.88533125" x2="-7.39724375" y2="-3.87223125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.88533125" x2="7.47585625" y2="-3.87223125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.87223125" x2="-7.39724375" y2="-3.85911875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.87223125" x2="7.47585625" y2="-3.85911875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.85911875" x2="-7.39724375" y2="-3.84601875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.85911875" x2="7.47585625" y2="-3.84601875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.84601875" x2="-7.39724375" y2="-3.8329125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.84601875" x2="7.47585625" y2="-3.8329125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.832909375" x2="-7.39724375" y2="-3.819809375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.832909375" x2="7.47585625" y2="-3.819809375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.819809375" x2="-7.39724375" y2="-3.806709375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.819809375" x2="7.47585625" y2="-3.806709375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.806709375" x2="-7.39724375" y2="-3.793603125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.806709375" x2="7.47585625" y2="-3.793603125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.7936" x2="-7.39724375" y2="-3.7805" layer="51"/>
<rectangle x1="7.37105625" y1="-3.7936" x2="7.47585625" y2="-3.7805" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.7805" x2="-7.39724375" y2="-3.76739375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.7805" x2="7.47585625" y2="-3.76739375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.767390625" x2="-7.39724375" y2="-3.754290625" layer="51"/>
<rectangle x1="7.37105625" y1="-3.767390625" x2="7.47585625" y2="-3.754290625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.754290625" x2="-7.39724375" y2="-3.741190625" layer="51"/>
<rectangle x1="7.37105625" y1="-3.754290625" x2="7.47585625" y2="-3.741190625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.741190625" x2="-7.39724375" y2="-3.728084375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.741190625" x2="7.47585625" y2="-3.728084375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.72808125" x2="-7.39724375" y2="-3.71498125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.72808125" x2="7.47585625" y2="-3.71498125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.71498125" x2="-7.39724375" y2="-3.70186875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.71498125" x2="7.47585625" y2="-3.70186875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.70186875" x2="-7.39724375" y2="-3.68876875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.70186875" x2="7.47585625" y2="-3.68876875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.68876875" x2="-7.39724375" y2="-3.67566875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.68876875" x2="7.47585625" y2="-3.67566875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.67566875" x2="-7.39724375" y2="-3.6625625" layer="51"/>
<rectangle x1="7.37105625" y1="-3.67566875" x2="7.47585625" y2="-3.6625625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.662559375" x2="-7.39724375" y2="-3.649459375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.662559375" x2="7.47585625" y2="-3.649459375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.649459375" x2="-7.39724375" y2="-3.636353125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.649459375" x2="7.47585625" y2="-3.636353125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.63635" x2="-7.39724375" y2="-3.62325" layer="51"/>
<rectangle x1="7.37105625" y1="-3.63635" x2="7.47585625" y2="-3.62325" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.62325" x2="-7.39724375" y2="-3.61015" layer="51"/>
<rectangle x1="7.37105625" y1="-3.62325" x2="7.47585625" y2="-3.61015" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.61015" x2="-7.39724375" y2="-3.59704375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.61015" x2="7.47585625" y2="-3.59704375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.597040625" x2="-7.39724375" y2="-3.583940625" layer="51"/>
<rectangle x1="7.37105625" y1="-3.597040625" x2="7.47585625" y2="-3.583940625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.583940625" x2="-7.39724375" y2="-3.570834375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.583940625" x2="7.47585625" y2="-3.570834375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.57083125" x2="-7.39724375" y2="-3.55773125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.57083125" x2="7.47585625" y2="-3.55773125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.55773125" x2="-7.39724375" y2="-3.54463125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.55773125" x2="7.47585625" y2="-3.54463125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.54463125" x2="-7.39724375" y2="-3.53151875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.54463125" x2="7.47585625" y2="-3.53151875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.53151875" x2="-7.39724375" y2="-3.51841875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.53151875" x2="7.47585625" y2="-3.51841875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.51841875" x2="-7.39724375" y2="-3.5053125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.51841875" x2="7.47585625" y2="-3.5053125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.505309375" x2="-7.39724375" y2="-3.492209375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.505309375" x2="7.47585625" y2="-3.492209375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.492209375" x2="-7.39724375" y2="-3.479109375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.492209375" x2="7.47585625" y2="-3.479109375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.479109375" x2="-7.39724375" y2="-3.466003125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.479109375" x2="7.47585625" y2="-3.466003125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.466" x2="-7.39724375" y2="-3.4529" layer="51"/>
<rectangle x1="7.37105625" y1="-3.466" x2="7.47585625" y2="-3.4529" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.4529" x2="-7.39724375" y2="-3.43979375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.4529" x2="7.47585625" y2="-3.43979375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.439790625" x2="-7.39724375" y2="-3.426690625" layer="51"/>
<rectangle x1="7.37105625" y1="-3.439790625" x2="7.47585625" y2="-3.426690625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.426690625" x2="-7.39724375" y2="-3.413590625" layer="51"/>
<rectangle x1="7.37105625" y1="-3.426690625" x2="7.47585625" y2="-3.413590625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.413590625" x2="-7.39724375" y2="-3.400484375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.413590625" x2="7.47585625" y2="-3.400484375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.40048125" x2="-7.39724375" y2="-3.38738125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.40048125" x2="7.47585625" y2="-3.38738125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.38738125" x2="-7.39724375" y2="-3.37426875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.38738125" x2="7.47585625" y2="-3.37426875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.37426875" x2="-7.39724375" y2="-3.36116875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.37426875" x2="7.47585625" y2="-3.36116875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.36116875" x2="-7.39724375" y2="-3.34806875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.36116875" x2="7.47585625" y2="-3.34806875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.34806875" x2="-7.39724375" y2="-3.3349625" layer="51"/>
<rectangle x1="7.37105625" y1="-3.34806875" x2="7.47585625" y2="-3.3349625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.334959375" x2="-7.39724375" y2="-3.321859375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.334959375" x2="7.47585625" y2="-3.321859375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.321859375" x2="-7.39724375" y2="-3.308753125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.321859375" x2="7.47585625" y2="-3.308753125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.30875" x2="-7.39724375" y2="-3.29565" layer="51"/>
<rectangle x1="7.37105625" y1="-3.30875" x2="7.47585625" y2="-3.29565" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.29565" x2="-7.39724375" y2="-3.28255" layer="51"/>
<rectangle x1="7.37105625" y1="-3.29565" x2="7.47585625" y2="-3.28255" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.28255" x2="-7.39724375" y2="-3.26944375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.28255" x2="7.47585625" y2="-3.26944375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.269440625" x2="-7.39724375" y2="-3.256340625" layer="51"/>
<rectangle x1="7.37105625" y1="-3.269440625" x2="7.47585625" y2="-3.256340625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.256340625" x2="-7.39724375" y2="-3.243234375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.256340625" x2="7.47585625" y2="-3.243234375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.24323125" x2="-7.39724375" y2="-3.23013125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.24323125" x2="7.47585625" y2="-3.23013125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.23013125" x2="-7.39724375" y2="-3.21703125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.23013125" x2="7.47585625" y2="-3.21703125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.21703125" x2="-7.39724375" y2="-3.20391875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.21703125" x2="7.47585625" y2="-3.20391875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.20391875" x2="-7.39724375" y2="-3.19081875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.20391875" x2="7.47585625" y2="-3.19081875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.19081875" x2="-7.39724375" y2="-3.1777125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.19081875" x2="7.47585625" y2="-3.1777125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.177709375" x2="-7.39724375" y2="-3.164609375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.177709375" x2="7.47585625" y2="-3.164609375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.164609375" x2="-7.39724375" y2="-3.151509375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.164609375" x2="7.47585625" y2="-3.151509375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.151509375" x2="-7.39724375" y2="-3.138403125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.151509375" x2="7.47585625" y2="-3.138403125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.1384" x2="-7.39724375" y2="-3.1253" layer="51"/>
<rectangle x1="7.37105625" y1="-3.1384" x2="7.47585625" y2="-3.1253" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.1253" x2="-7.39724375" y2="-3.11219375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.1253" x2="7.47585625" y2="-3.11219375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.112190625" x2="-7.39724375" y2="-3.099090625" layer="51"/>
<rectangle x1="7.37105625" y1="-3.112190625" x2="7.47585625" y2="-3.099090625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.099090625" x2="-7.39724375" y2="-3.085990625" layer="51"/>
<rectangle x1="7.37105625" y1="-3.099090625" x2="7.47585625" y2="-3.085990625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.085990625" x2="-7.39724375" y2="-3.072884375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.085990625" x2="7.47585625" y2="-3.072884375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.07288125" x2="-7.39724375" y2="-3.05978125" layer="51"/>
<rectangle x1="7.37105625" y1="-3.07288125" x2="7.47585625" y2="-3.05978125" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.05978125" x2="-7.39724375" y2="-3.04666875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.05978125" x2="7.47585625" y2="-3.04666875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.04666875" x2="-7.39724375" y2="-3.03356875" layer="51"/>
<rectangle x1="7.37105625" y1="-3.04666875" x2="7.47585625" y2="-3.03356875" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.03356875" x2="-7.39724375" y2="-3.0204625" layer="51"/>
<rectangle x1="7.37105625" y1="-3.03356875" x2="7.47585625" y2="-3.0204625" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.020459375" x2="-7.39724375" y2="-3.007359375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.020459375" x2="7.47585625" y2="-3.007359375" layer="51"/>
<rectangle x1="-7.50204375" y1="-3.007359375" x2="-7.39724375" y2="-2.994259375" layer="51"/>
<rectangle x1="7.37105625" y1="-3.007359375" x2="7.47585625" y2="-2.994259375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.994259375" x2="-7.39724375" y2="-2.981153125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.994259375" x2="7.47585625" y2="-2.981153125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.98115" x2="-7.39724375" y2="-2.96805" layer="51"/>
<rectangle x1="7.37105625" y1="-2.98115" x2="7.47585625" y2="-2.96805" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.96805" x2="-7.39724375" y2="-2.95494375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.96805" x2="7.47585625" y2="-2.95494375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.954940625" x2="-7.39724375" y2="-2.941840625" layer="51"/>
<rectangle x1="7.37105625" y1="-2.954940625" x2="7.47585625" y2="-2.941840625" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.941840625" x2="-7.39724375" y2="-2.928740625" layer="51"/>
<rectangle x1="7.37105625" y1="-2.941840625" x2="7.47585625" y2="-2.928740625" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.928740625" x2="-7.39724375" y2="-2.915634375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.928740625" x2="7.47585625" y2="-2.915634375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.91563125" x2="-7.39724375" y2="-2.90253125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.91563125" x2="7.47585625" y2="-2.90253125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.90253125" x2="-7.39724375" y2="-2.88941875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.90253125" x2="7.47585625" y2="-2.88941875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.88941875" x2="-7.39724375" y2="-2.87631875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.88941875" x2="7.47585625" y2="-2.87631875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.87631875" x2="-7.39724375" y2="-2.86321875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.87631875" x2="7.47585625" y2="-2.86321875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.86321875" x2="-7.39724375" y2="-2.8501125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.86321875" x2="7.47585625" y2="-2.8501125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.850109375" x2="-7.39724375" y2="-2.837009375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.850109375" x2="7.47585625" y2="-2.837009375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.837009375" x2="-7.39724375" y2="-2.823903125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.837009375" x2="7.47585625" y2="-2.823903125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.8239" x2="-7.39724375" y2="-2.8108" layer="51"/>
<rectangle x1="7.37105625" y1="-2.8239" x2="7.47585625" y2="-2.8108" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.8108" x2="-7.39724375" y2="-2.7977" layer="51"/>
<rectangle x1="7.37105625" y1="-2.8108" x2="7.47585625" y2="-2.7977" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.7977" x2="-7.39724375" y2="-2.78459375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.7977" x2="7.47585625" y2="-2.78459375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.784590625" x2="-7.39724375" y2="-2.771490625" layer="51"/>
<rectangle x1="7.37105625" y1="-2.784590625" x2="7.47585625" y2="-2.771490625" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.771490625" x2="-7.39724375" y2="-2.758384375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.771490625" x2="7.47585625" y2="-2.758384375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.75838125" x2="-7.39724375" y2="-2.74528125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.75838125" x2="7.47585625" y2="-2.74528125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.74528125" x2="-7.39724375" y2="-2.73218125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.74528125" x2="7.47585625" y2="-2.73218125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.73218125" x2="-7.39724375" y2="-2.71906875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.73218125" x2="7.47585625" y2="-2.71906875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.71906875" x2="-7.39724375" y2="-2.70596875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.71906875" x2="7.47585625" y2="-2.70596875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.70596875" x2="-7.39724375" y2="-2.6928625" layer="51"/>
<rectangle x1="7.37105625" y1="-2.70596875" x2="7.47585625" y2="-2.6928625" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.692859375" x2="-7.39724375" y2="-2.679759375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.692859375" x2="7.47585625" y2="-2.679759375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.679759375" x2="-7.39724375" y2="-2.666659375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.679759375" x2="7.47585625" y2="-2.666659375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.666659375" x2="-7.39724375" y2="-2.653553125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.666659375" x2="7.47585625" y2="-2.653553125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.65355" x2="-7.39724375" y2="-2.64045" layer="51"/>
<rectangle x1="7.37105625" y1="-2.65355" x2="7.47585625" y2="-2.64045" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.64045" x2="-7.39724375" y2="-2.62734375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.64045" x2="7.47585625" y2="-2.62734375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.627340625" x2="-7.39724375" y2="-2.614240625" layer="51"/>
<rectangle x1="7.37105625" y1="-2.627340625" x2="7.47585625" y2="-2.614240625" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.614240625" x2="-7.39724375" y2="-2.601140625" layer="51"/>
<rectangle x1="7.37105625" y1="-2.614240625" x2="7.47585625" y2="-2.601140625" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.601140625" x2="-7.39724375" y2="-2.588034375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.601140625" x2="7.47585625" y2="-2.588034375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.58803125" x2="-7.39724375" y2="-2.57493125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.58803125" x2="7.47585625" y2="-2.57493125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.57493125" x2="-7.39724375" y2="-2.56181875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.57493125" x2="7.47585625" y2="-2.56181875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.56181875" x2="-7.39724375" y2="-2.54871875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.56181875" x2="7.47585625" y2="-2.54871875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.54871875" x2="-7.39724375" y2="-2.53561875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.54871875" x2="7.47585625" y2="-2.53561875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.53561875" x2="-7.39724375" y2="-2.5225125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.53561875" x2="7.47585625" y2="-2.5225125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.522509375" x2="-7.39724375" y2="-2.509409375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.522509375" x2="7.47585625" y2="-2.509409375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.509409375" x2="-7.39724375" y2="-2.496303125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.509409375" x2="7.47585625" y2="-2.496303125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.4963" x2="-7.39724375" y2="-2.4832" layer="51"/>
<rectangle x1="7.37105625" y1="-2.4963" x2="7.47585625" y2="-2.4832" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.4832" x2="-7.39724375" y2="-2.4701" layer="51"/>
<rectangle x1="7.37105625" y1="-2.4832" x2="7.47585625" y2="-2.4701" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.4701" x2="-7.39724375" y2="-2.45699375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.4701" x2="7.47585625" y2="-2.45699375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.456990625" x2="-7.39724375" y2="-2.443890625" layer="51"/>
<rectangle x1="7.37105625" y1="-2.456990625" x2="7.47585625" y2="-2.443890625" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.443890625" x2="-7.39724375" y2="-2.430784375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.443890625" x2="7.47585625" y2="-2.430784375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.43078125" x2="-7.39724375" y2="-2.41768125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.43078125" x2="7.47585625" y2="-2.41768125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.41768125" x2="-7.39724375" y2="-2.40458125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.41768125" x2="7.47585625" y2="-2.40458125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.40458125" x2="-7.39724375" y2="-2.39146875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.40458125" x2="7.47585625" y2="-2.39146875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.39146875" x2="-7.39724375" y2="-2.37836875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.39146875" x2="7.47585625" y2="-2.37836875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.37836875" x2="-7.39724375" y2="-2.3652625" layer="51"/>
<rectangle x1="7.37105625" y1="-2.37836875" x2="7.47585625" y2="-2.3652625" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.365259375" x2="-7.39724375" y2="-2.352159375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.365259375" x2="7.47585625" y2="-2.352159375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.352159375" x2="-7.39724375" y2="-2.339059375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.352159375" x2="7.47585625" y2="-2.339059375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.339059375" x2="-7.39724375" y2="-2.325953125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.339059375" x2="7.47585625" y2="-2.325953125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.32595" x2="-7.39724375" y2="-2.31285" layer="51"/>
<rectangle x1="7.37105625" y1="-2.32595" x2="7.47585625" y2="-2.31285" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.31285" x2="-7.39724375" y2="-2.29974375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.31285" x2="7.47585625" y2="-2.29974375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.299740625" x2="-7.39724375" y2="-2.286640625" layer="51"/>
<rectangle x1="7.37105625" y1="-2.299740625" x2="7.47585625" y2="-2.286640625" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.286640625" x2="-7.39724375" y2="-2.273540625" layer="51"/>
<rectangle x1="7.37105625" y1="-2.286640625" x2="7.47585625" y2="-2.273540625" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.273540625" x2="-7.39724375" y2="-2.260434375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.273540625" x2="7.47585625" y2="-2.260434375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.26043125" x2="-7.39724375" y2="-2.24733125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.26043125" x2="7.47585625" y2="-2.24733125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.24733125" x2="-7.39724375" y2="-2.23421875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.24733125" x2="7.47585625" y2="-2.23421875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.23421875" x2="-7.39724375" y2="-2.22111875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.23421875" x2="7.47585625" y2="-2.22111875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.22111875" x2="-7.39724375" y2="-2.20801875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.22111875" x2="7.47585625" y2="-2.20801875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.20801875" x2="-7.39724375" y2="-2.1949125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.20801875" x2="7.47585625" y2="-2.1949125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.194909375" x2="-7.39724375" y2="-2.181809375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.194909375" x2="7.47585625" y2="-2.181809375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.181809375" x2="-7.39724375" y2="-2.168703125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.181809375" x2="7.47585625" y2="-2.168703125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.1687" x2="-7.39724375" y2="-2.1556" layer="51"/>
<rectangle x1="7.37105625" y1="-2.1687" x2="7.47585625" y2="-2.1556" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.1556" x2="-7.39724375" y2="-2.1425" layer="51"/>
<rectangle x1="7.37105625" y1="-2.1556" x2="7.47585625" y2="-2.1425" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.1425" x2="-7.39724375" y2="-2.12939375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.1425" x2="7.47585625" y2="-2.12939375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.129390625" x2="-7.39724375" y2="-2.116290625" layer="51"/>
<rectangle x1="7.37105625" y1="-2.129390625" x2="7.47585625" y2="-2.116290625" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.116290625" x2="-7.39724375" y2="-2.103184375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.116290625" x2="7.47585625" y2="-2.103184375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.10318125" x2="-7.39724375" y2="-2.09008125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.10318125" x2="7.47585625" y2="-2.09008125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.09008125" x2="-7.39724375" y2="-2.07698125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.09008125" x2="7.47585625" y2="-2.07698125" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.07698125" x2="-7.39724375" y2="-2.06386875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.07698125" x2="7.47585625" y2="-2.06386875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.06386875" x2="-7.39724375" y2="-2.05076875" layer="51"/>
<rectangle x1="7.37105625" y1="-2.06386875" x2="7.47585625" y2="-2.05076875" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.05076875" x2="-7.39724375" y2="-2.0376625" layer="51"/>
<rectangle x1="7.37105625" y1="-2.05076875" x2="7.47585625" y2="-2.0376625" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.037659375" x2="-7.39724375" y2="-2.024559375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.037659375" x2="7.47585625" y2="-2.024559375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.024559375" x2="-7.39724375" y2="-2.011459375" layer="51"/>
<rectangle x1="7.37105625" y1="-2.024559375" x2="7.47585625" y2="-2.011459375" layer="51"/>
<rectangle x1="-7.50204375" y1="-2.011459375" x2="-7.39724375" y2="-1.998353125" layer="51"/>
<rectangle x1="7.37105625" y1="-2.011459375" x2="7.47585625" y2="-1.998353125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.99835" x2="-7.39724375" y2="-1.98525" layer="51"/>
<rectangle x1="7.37105625" y1="-1.99835" x2="7.47585625" y2="-1.98525" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.98525" x2="-7.39724375" y2="-1.97214375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.98525" x2="7.47585625" y2="-1.97214375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.972140625" x2="-7.39724375" y2="-1.959040625" layer="51"/>
<rectangle x1="7.37105625" y1="-1.972140625" x2="7.47585625" y2="-1.959040625" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.959040625" x2="-7.39724375" y2="-1.945940625" layer="51"/>
<rectangle x1="7.37105625" y1="-1.959040625" x2="7.47585625" y2="-1.945940625" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.945940625" x2="-7.39724375" y2="-1.932834375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.945940625" x2="7.47585625" y2="-1.932834375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.93283125" x2="-7.39724375" y2="-1.91973125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.93283125" x2="7.47585625" y2="-1.91973125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.91973125" x2="-7.39724375" y2="-1.90661875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.91973125" x2="7.47585625" y2="-1.90661875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.90661875" x2="-7.39724375" y2="-1.89351875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.90661875" x2="7.47585625" y2="-1.89351875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.89351875" x2="-7.39724375" y2="-1.88041875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.89351875" x2="7.47585625" y2="-1.88041875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.88041875" x2="-7.39724375" y2="-1.8673125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.88041875" x2="7.47585625" y2="-1.8673125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.867309375" x2="-7.39724375" y2="-1.854209375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.867309375" x2="7.47585625" y2="-1.854209375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.854209375" x2="-7.39724375" y2="-1.841103125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.854209375" x2="7.47585625" y2="-1.841103125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.8411" x2="-7.39724375" y2="-1.828" layer="51"/>
<rectangle x1="7.37105625" y1="-1.8411" x2="7.47585625" y2="-1.828" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.828" x2="-7.39724375" y2="-1.8149" layer="51"/>
<rectangle x1="7.37105625" y1="-1.828" x2="7.47585625" y2="-1.8149" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.8149" x2="-7.39724375" y2="-1.80179375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.8149" x2="7.47585625" y2="-1.80179375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.801790625" x2="-7.39724375" y2="-1.788690625" layer="51"/>
<rectangle x1="7.37105625" y1="-1.801790625" x2="7.47585625" y2="-1.788690625" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.788690625" x2="-7.39724375" y2="-1.775584375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.788690625" x2="7.47585625" y2="-1.775584375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.77558125" x2="-7.39724375" y2="-1.76248125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.77558125" x2="7.47585625" y2="-1.76248125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.76248125" x2="-7.39724375" y2="-1.74938125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.76248125" x2="7.47585625" y2="-1.74938125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.74938125" x2="-7.39724375" y2="-1.73626875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.74938125" x2="7.47585625" y2="-1.73626875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.73626875" x2="-7.39724375" y2="-1.72316875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.73626875" x2="7.47585625" y2="-1.72316875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.72316875" x2="-7.39724375" y2="-1.7100625" layer="51"/>
<rectangle x1="7.37105625" y1="-1.72316875" x2="7.47585625" y2="-1.7100625" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.710059375" x2="-7.39724375" y2="-1.696959375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.710059375" x2="7.47585625" y2="-1.696959375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.696959375" x2="-7.39724375" y2="-1.683859375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.696959375" x2="7.47585625" y2="-1.683859375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.683859375" x2="-7.39724375" y2="-1.670753125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.683859375" x2="7.47585625" y2="-1.670753125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.67075" x2="-7.39724375" y2="-1.65765" layer="51"/>
<rectangle x1="7.37105625" y1="-1.67075" x2="7.47585625" y2="-1.65765" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.65765" x2="-7.39724375" y2="-1.64454375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.65765" x2="7.47585625" y2="-1.64454375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.644540625" x2="-7.39724375" y2="-1.631440625" layer="51"/>
<rectangle x1="7.37105625" y1="-1.644540625" x2="7.47585625" y2="-1.631440625" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.631440625" x2="-7.39724375" y2="-1.618340625" layer="51"/>
<rectangle x1="7.37105625" y1="-1.631440625" x2="7.47585625" y2="-1.618340625" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.618340625" x2="-7.39724375" y2="-1.605234375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.618340625" x2="7.47585625" y2="-1.605234375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.60523125" x2="-7.39724375" y2="-1.59213125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.60523125" x2="7.47585625" y2="-1.59213125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.59213125" x2="-7.39724375" y2="-1.57901875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.59213125" x2="7.47585625" y2="-1.57901875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.57901875" x2="-7.39724375" y2="-1.56591875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.57901875" x2="7.47585625" y2="-1.56591875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.56591875" x2="-7.39724375" y2="-1.55281875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.56591875" x2="7.47585625" y2="-1.55281875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.55281875" x2="-7.39724375" y2="-1.5397125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.55281875" x2="7.47585625" y2="-1.5397125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.539709375" x2="-7.39724375" y2="-1.526609375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.539709375" x2="7.47585625" y2="-1.526609375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.526609375" x2="-7.39724375" y2="-1.513503125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.526609375" x2="7.47585625" y2="-1.513503125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.5135" x2="-7.39724375" y2="-1.5004" layer="51"/>
<rectangle x1="7.37105625" y1="-1.5135" x2="7.47585625" y2="-1.5004" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.5004" x2="-7.39724375" y2="-1.4873" layer="51"/>
<rectangle x1="7.37105625" y1="-1.5004" x2="7.47585625" y2="-1.4873" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.4873" x2="-7.39724375" y2="-1.47419375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.4873" x2="7.47585625" y2="-1.47419375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.474190625" x2="-7.39724375" y2="-1.461090625" layer="51"/>
<rectangle x1="7.37105625" y1="-1.474190625" x2="7.47585625" y2="-1.461090625" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.461090625" x2="-7.39724375" y2="-1.447984375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.461090625" x2="7.47585625" y2="-1.447984375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.44798125" x2="-7.39724375" y2="-1.43488125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.44798125" x2="7.47585625" y2="-1.43488125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.43488125" x2="-7.39724375" y2="-1.42178125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.43488125" x2="7.47585625" y2="-1.42178125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.42178125" x2="-7.39724375" y2="-1.40866875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.42178125" x2="7.47585625" y2="-1.40866875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.40866875" x2="-7.39724375" y2="-1.39556875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.40866875" x2="7.47585625" y2="-1.39556875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.39556875" x2="-7.39724375" y2="-1.3824625" layer="51"/>
<rectangle x1="7.37105625" y1="-1.39556875" x2="7.47585625" y2="-1.3824625" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.382459375" x2="-7.39724375" y2="-1.369359375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.382459375" x2="7.47585625" y2="-1.369359375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.369359375" x2="-7.39724375" y2="-1.356259375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.369359375" x2="7.47585625" y2="-1.356259375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.356259375" x2="-7.39724375" y2="-1.343153125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.356259375" x2="7.47585625" y2="-1.343153125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.34315" x2="-7.39724375" y2="-1.33005" layer="51"/>
<rectangle x1="7.37105625" y1="-1.34315" x2="7.47585625" y2="-1.33005" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.33005" x2="-7.39724375" y2="-1.31694375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.33005" x2="7.47585625" y2="-1.31694375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.316940625" x2="-7.39724375" y2="-1.303840625" layer="51"/>
<rectangle x1="7.37105625" y1="-1.316940625" x2="7.47585625" y2="-1.303840625" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.303840625" x2="-7.39724375" y2="-1.290740625" layer="51"/>
<rectangle x1="7.37105625" y1="-1.303840625" x2="7.47585625" y2="-1.290740625" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.290740625" x2="-7.39724375" y2="-1.277634375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.290740625" x2="7.47585625" y2="-1.277634375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.27763125" x2="-7.39724375" y2="-1.26453125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.27763125" x2="7.47585625" y2="-1.26453125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.26453125" x2="-7.39724375" y2="-1.25141875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.26453125" x2="7.47585625" y2="-1.25141875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.25141875" x2="-7.39724375" y2="-1.23831875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.25141875" x2="7.47585625" y2="-1.23831875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.23831875" x2="-7.39724375" y2="-1.22521875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.23831875" x2="7.47585625" y2="-1.22521875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.22521875" x2="-7.39724375" y2="-1.2121125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.22521875" x2="7.47585625" y2="-1.2121125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.212109375" x2="-7.39724375" y2="-1.199009375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.212109375" x2="7.47585625" y2="-1.199009375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.199009375" x2="-7.39724375" y2="-1.185903125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.199009375" x2="7.47585625" y2="-1.185903125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.1859" x2="-7.39724375" y2="-1.1728" layer="51"/>
<rectangle x1="7.37105625" y1="-1.1859" x2="7.47585625" y2="-1.1728" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.1728" x2="-7.39724375" y2="-1.1597" layer="51"/>
<rectangle x1="7.37105625" y1="-1.1728" x2="7.47585625" y2="-1.1597" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.1597" x2="-7.39724375" y2="-1.14659375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.1597" x2="7.47585625" y2="-1.14659375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.146590625" x2="-7.39724375" y2="-1.133490625" layer="51"/>
<rectangle x1="7.37105625" y1="-1.146590625" x2="7.47585625" y2="-1.133490625" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.133490625" x2="-7.39724375" y2="-1.120384375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.133490625" x2="7.47585625" y2="-1.120384375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.12038125" x2="-7.39724375" y2="-1.10728125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.12038125" x2="7.47585625" y2="-1.10728125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.10728125" x2="-7.39724375" y2="-1.09418125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.10728125" x2="7.47585625" y2="-1.09418125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.09418125" x2="-7.39724375" y2="-1.08106875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.09418125" x2="7.47585625" y2="-1.08106875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.08106875" x2="-7.39724375" y2="-1.06796875" layer="51"/>
<rectangle x1="7.37105625" y1="-1.08106875" x2="7.47585625" y2="-1.06796875" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.06796875" x2="-7.39724375" y2="-1.0548625" layer="51"/>
<rectangle x1="7.37105625" y1="-1.06796875" x2="7.47585625" y2="-1.0548625" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.054859375" x2="-7.39724375" y2="-1.041759375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.054859375" x2="7.47585625" y2="-1.041759375" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.041759375" x2="-7.39724375" y2="-1.028653125" layer="51"/>
<rectangle x1="7.37105625" y1="-1.041759375" x2="7.47585625" y2="-1.028653125" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.02865" x2="-7.39724375" y2="-1.01555" layer="51"/>
<rectangle x1="7.37105625" y1="-1.02865" x2="7.47585625" y2="-1.01555" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.01555" x2="-7.39724375" y2="-1.00245" layer="51"/>
<rectangle x1="7.37105625" y1="-1.01555" x2="7.47585625" y2="-1.00245" layer="51"/>
<rectangle x1="-7.50204375" y1="-1.00245" x2="-7.39724375" y2="-0.98934375" layer="51"/>
<rectangle x1="7.37105625" y1="-1.00245" x2="7.47585625" y2="-0.98934375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.989340625" x2="-7.39724375" y2="-0.976240625" layer="51"/>
<rectangle x1="7.37105625" y1="-0.989340625" x2="7.47585625" y2="-0.976240625" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.976240625" x2="-7.39724375" y2="-0.963134375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.976240625" x2="7.47585625" y2="-0.963134375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.96313125" x2="-7.39724375" y2="-0.95003125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.96313125" x2="7.47585625" y2="-0.95003125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.95003125" x2="-7.39724375" y2="-0.93693125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.95003125" x2="7.47585625" y2="-0.93693125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.93693125" x2="-7.39724375" y2="-0.92381875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.93693125" x2="7.47585625" y2="-0.92381875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.92381875" x2="-7.39724375" y2="-0.91071875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.92381875" x2="7.47585625" y2="-0.91071875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.91071875" x2="-7.39724375" y2="-0.8976125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.91071875" x2="7.47585625" y2="-0.8976125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.897609375" x2="-7.39724375" y2="-0.884509375" layer="51"/>
<rectangle x1="-6.67644375" y1="-0.897609375" x2="-6.66334375" y2="-0.884509375" layer="51"/>
<rectangle x1="-3.78054375" y1="-0.897609375" x2="-3.76744375" y2="-0.884509375" layer="51"/>
<rectangle x1="-3.21704375" y1="-0.897609375" x2="-3.20394375" y2="-0.884509375" layer="51"/>
<rectangle x1="-0.30794375" y1="-0.897609375" x2="-0.29484375" y2="-0.884509375" layer="51"/>
<rectangle x1="0.26865625" y1="-0.897609375" x2="0.28175625" y2="-0.884509375" layer="51"/>
<rectangle x1="3.17775625" y1="-0.897609375" x2="3.19085625" y2="-0.884509375" layer="51"/>
<rectangle x1="3.74115625" y1="-0.897609375" x2="3.75435625" y2="-0.884509375" layer="51"/>
<rectangle x1="6.63715625" y1="-0.897609375" x2="6.65025625" y2="-0.884509375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.897609375" x2="7.47585625" y2="-0.884509375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.884509375" x2="-7.39724375" y2="-0.871409375" layer="51"/>
<rectangle x1="-6.68954375" y1="-0.884509375" x2="-6.65024375" y2="-0.871409375" layer="51"/>
<rectangle x1="-3.79364375" y1="-0.884509375" x2="-3.75424375" y2="-0.871409375" layer="51"/>
<rectangle x1="-3.23014375" y1="-0.884509375" x2="-3.19084375" y2="-0.871409375" layer="51"/>
<rectangle x1="-0.32104375" y1="-0.884509375" x2="-0.28174375" y2="-0.871409375" layer="51"/>
<rectangle x1="0.25555625" y1="-0.884509375" x2="0.29485625" y2="-0.871409375" layer="51"/>
<rectangle x1="3.16465625" y1="-0.884509375" x2="3.20395625" y2="-0.871409375" layer="51"/>
<rectangle x1="3.72805625" y1="-0.884509375" x2="3.76745625" y2="-0.871409375" layer="51"/>
<rectangle x1="6.62405625" y1="-0.884509375" x2="6.67645625" y2="-0.871409375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.884509375" x2="7.47585625" y2="-0.871409375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.871409375" x2="-7.39724375" y2="-0.858303125" layer="51"/>
<rectangle x1="-6.70274375" y1="-0.871409375" x2="-6.63714375" y2="-0.858303125" layer="51"/>
<rectangle x1="-3.80674375" y1="-0.871409375" x2="-3.74114375" y2="-0.858303125" layer="51"/>
<rectangle x1="-3.24324375" y1="-0.871409375" x2="-3.17774375" y2="-0.858303125" layer="51"/>
<rectangle x1="-0.33414375" y1="-0.871409375" x2="-0.26864375" y2="-0.858303125" layer="51"/>
<rectangle x1="0.24245625" y1="-0.871409375" x2="0.30795625" y2="-0.858303125" layer="51"/>
<rectangle x1="3.15155625" y1="-0.871409375" x2="3.21705625" y2="-0.858303125" layer="51"/>
<rectangle x1="3.71495625" y1="-0.871409375" x2="3.78055625" y2="-0.858303125" layer="51"/>
<rectangle x1="6.61095625" y1="-0.871409375" x2="6.68965625" y2="-0.858303125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.871409375" x2="7.47585625" y2="-0.858303125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.8583" x2="-7.39724375" y2="-0.8452" layer="51"/>
<rectangle x1="-6.71584375" y1="-0.8583" x2="-6.62404375" y2="-0.8452" layer="51"/>
<rectangle x1="-3.81984375" y1="-0.8583" x2="-3.71494375" y2="-0.8452" layer="51"/>
<rectangle x1="-3.25634375" y1="-0.8583" x2="-3.16464375" y2="-0.8452" layer="51"/>
<rectangle x1="-0.34724375" y1="-0.8583" x2="-0.25554375" y2="-0.8452" layer="51"/>
<rectangle x1="0.22935625" y1="-0.8583" x2="0.32105625" y2="-0.8452" layer="51"/>
<rectangle x1="3.13845625" y1="-0.8583" x2="3.23015625" y2="-0.8452" layer="51"/>
<rectangle x1="3.70185625" y1="-0.8583" x2="3.79365625" y2="-0.8452" layer="51"/>
<rectangle x1="6.59785625" y1="-0.8583" x2="6.70275625" y2="-0.8452" layer="51"/>
<rectangle x1="7.37105625" y1="-0.8583" x2="7.47585625" y2="-0.8452" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.8452" x2="-7.39724375" y2="-0.83209375" layer="51"/>
<rectangle x1="-6.72894375" y1="-0.8452" x2="-6.61094375" y2="-0.83209375" layer="51"/>
<rectangle x1="-3.83294375" y1="-0.8452" x2="-3.70184375" y2="-0.83209375" layer="51"/>
<rectangle x1="-3.26944375" y1="-0.8452" x2="-3.15154375" y2="-0.83209375" layer="51"/>
<rectangle x1="-0.36034375" y1="-0.8452" x2="-0.24244375" y2="-0.83209375" layer="51"/>
<rectangle x1="0.21625625" y1="-0.8452" x2="0.33415625" y2="-0.83209375" layer="51"/>
<rectangle x1="3.12535625" y1="-0.8452" x2="3.24325625" y2="-0.83209375" layer="51"/>
<rectangle x1="3.68875625" y1="-0.8452" x2="3.80675625" y2="-0.83209375" layer="51"/>
<rectangle x1="6.58475625" y1="-0.8452" x2="6.71585625" y2="-0.83209375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.8452" x2="7.47585625" y2="-0.83209375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.832090625" x2="-7.39724375" y2="-0.818990625" layer="51"/>
<rectangle x1="-6.74204375" y1="-0.832090625" x2="-6.59784375" y2="-0.818990625" layer="51"/>
<rectangle x1="-3.84604375" y1="-0.832090625" x2="-3.70184375" y2="-0.818990625" layer="51"/>
<rectangle x1="-3.28254375" y1="-0.832090625" x2="-3.13844375" y2="-0.818990625" layer="51"/>
<rectangle x1="-0.37344375" y1="-0.832090625" x2="-0.22934375" y2="-0.818990625" layer="51"/>
<rectangle x1="0.20315625" y1="-0.832090625" x2="0.34725625" y2="-0.818990625" layer="51"/>
<rectangle x1="3.11225625" y1="-0.832090625" x2="3.25635625" y2="-0.818990625" layer="51"/>
<rectangle x1="3.67565625" y1="-0.832090625" x2="3.81985625" y2="-0.818990625" layer="51"/>
<rectangle x1="6.57165625" y1="-0.832090625" x2="6.71585625" y2="-0.818990625" layer="51"/>
<rectangle x1="7.37105625" y1="-0.832090625" x2="7.47585625" y2="-0.818990625" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.818990625" x2="-7.39724375" y2="-0.805890625" layer="51"/>
<rectangle x1="-6.74204375" y1="-0.818990625" x2="-6.58474375" y2="-0.805890625" layer="51"/>
<rectangle x1="-3.85914375" y1="-0.818990625" x2="-3.70184375" y2="-0.805890625" layer="51"/>
<rectangle x1="-3.26944375" y1="-0.818990625" x2="-3.12534375" y2="-0.805890625" layer="51"/>
<rectangle x1="-0.38654375" y1="-0.818990625" x2="-0.24244375" y2="-0.805890625" layer="51"/>
<rectangle x1="0.21625625" y1="-0.818990625" x2="0.36035625" y2="-0.805890625" layer="51"/>
<rectangle x1="3.09915625" y1="-0.818990625" x2="3.24325625" y2="-0.805890625" layer="51"/>
<rectangle x1="3.67565625" y1="-0.818990625" x2="3.83295625" y2="-0.805890625" layer="51"/>
<rectangle x1="6.55855625" y1="-0.818990625" x2="6.71585625" y2="-0.805890625" layer="51"/>
<rectangle x1="7.37105625" y1="-0.818990625" x2="7.47585625" y2="-0.805890625" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.805890625" x2="-7.39724375" y2="-0.792784375" layer="51"/>
<rectangle x1="-6.72894375" y1="-0.805890625" x2="-6.57164375" y2="-0.792784375" layer="51"/>
<rectangle x1="-3.87224375" y1="-0.805890625" x2="-3.71494375" y2="-0.792784375" layer="51"/>
<rectangle x1="-3.25634375" y1="-0.805890625" x2="-3.11224375" y2="-0.792784375" layer="51"/>
<rectangle x1="-0.39964375" y1="-0.805890625" x2="-0.25554375" y2="-0.792784375" layer="51"/>
<rectangle x1="0.22935625" y1="-0.805890625" x2="0.37345625" y2="-0.792784375" layer="51"/>
<rectangle x1="3.08595625" y1="-0.805890625" x2="3.23015625" y2="-0.792784375" layer="51"/>
<rectangle x1="3.68875625" y1="-0.805890625" x2="3.84605625" y2="-0.792784375" layer="51"/>
<rectangle x1="6.54545625" y1="-0.805890625" x2="6.70275625" y2="-0.792784375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.805890625" x2="7.47585625" y2="-0.792784375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.79278125" x2="-7.39724375" y2="-0.77968125" layer="51"/>
<rectangle x1="-6.71584375" y1="-0.79278125" x2="-6.55854375" y2="-0.77968125" layer="51"/>
<rectangle x1="-3.88534375" y1="-0.79278125" x2="-3.72804375" y2="-0.77968125" layer="51"/>
<rectangle x1="-3.24324375" y1="-0.79278125" x2="-3.09904375" y2="-0.77968125" layer="51"/>
<rectangle x1="-0.41274375" y1="-0.79278125" x2="-0.26864375" y2="-0.77968125" layer="51"/>
<rectangle x1="0.24245625" y1="-0.79278125" x2="0.38655625" y2="-0.77968125" layer="51"/>
<rectangle x1="3.07285625" y1="-0.79278125" x2="3.21705625" y2="-0.77968125" layer="51"/>
<rectangle x1="3.70185625" y1="-0.79278125" x2="3.85915625" y2="-0.77968125" layer="51"/>
<rectangle x1="6.53235625" y1="-0.79278125" x2="6.68965625" y2="-0.77968125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.79278125" x2="7.47585625" y2="-0.77968125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.77968125" x2="-7.39724375" y2="-0.76656875" layer="51"/>
<rectangle x1="-6.70274375" y1="-0.77968125" x2="-6.54544375" y2="-0.76656875" layer="51"/>
<rectangle x1="-3.89844375" y1="-0.77968125" x2="-3.74114375" y2="-0.76656875" layer="51"/>
<rectangle x1="-3.23014375" y1="-0.77968125" x2="-3.08594375" y2="-0.76656875" layer="51"/>
<rectangle x1="-0.42584375" y1="-0.77968125" x2="-0.28174375" y2="-0.76656875" layer="51"/>
<rectangle x1="0.25555625" y1="-0.77968125" x2="0.39965625" y2="-0.76656875" layer="51"/>
<rectangle x1="3.05975625" y1="-0.77968125" x2="3.20395625" y2="-0.76656875" layer="51"/>
<rectangle x1="3.71495625" y1="-0.77968125" x2="3.87225625" y2="-0.76656875" layer="51"/>
<rectangle x1="6.51925625" y1="-0.77968125" x2="6.67645625" y2="-0.76656875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.77968125" x2="7.47585625" y2="-0.76656875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.76656875" x2="-7.39724375" y2="-0.75346875" layer="51"/>
<rectangle x1="-6.68954375" y1="-0.76656875" x2="-6.53234375" y2="-0.75346875" layer="51"/>
<rectangle x1="-3.91154375" y1="-0.76656875" x2="-3.75424375" y2="-0.75346875" layer="51"/>
<rectangle x1="-3.21704375" y1="-0.76656875" x2="-3.07284375" y2="-0.75346875" layer="51"/>
<rectangle x1="-0.43894375" y1="-0.76656875" x2="-0.29484375" y2="-0.75346875" layer="51"/>
<rectangle x1="0.26865625" y1="-0.76656875" x2="0.41275625" y2="-0.75346875" layer="51"/>
<rectangle x1="3.04665625" y1="-0.76656875" x2="3.19085625" y2="-0.75346875" layer="51"/>
<rectangle x1="3.72805625" y1="-0.76656875" x2="3.88535625" y2="-0.75346875" layer="51"/>
<rectangle x1="6.50615625" y1="-0.76656875" x2="6.66335625" y2="-0.75346875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.76656875" x2="7.47585625" y2="-0.75346875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.75346875" x2="-7.39724375" y2="-0.74036875" layer="51"/>
<rectangle x1="-6.67644375" y1="-0.75346875" x2="-6.51924375" y2="-0.74036875" layer="51"/>
<rectangle x1="-3.92464375" y1="-0.75346875" x2="-3.76744375" y2="-0.74036875" layer="51"/>
<rectangle x1="-3.20394375" y1="-0.75346875" x2="-3.05974375" y2="-0.74036875" layer="51"/>
<rectangle x1="-0.45204375" y1="-0.75346875" x2="-0.30794375" y2="-0.74036875" layer="51"/>
<rectangle x1="0.28175625" y1="-0.75346875" x2="0.42585625" y2="-0.74036875" layer="51"/>
<rectangle x1="3.03355625" y1="-0.75346875" x2="3.17775625" y2="-0.74036875" layer="51"/>
<rectangle x1="3.74115625" y1="-0.75346875" x2="3.89845625" y2="-0.74036875" layer="51"/>
<rectangle x1="6.49305625" y1="-0.75346875" x2="6.65025625" y2="-0.74036875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.75346875" x2="7.47585625" y2="-0.74036875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.74036875" x2="-7.39724375" y2="-0.7272625" layer="51"/>
<rectangle x1="-6.66334375" y1="-0.74036875" x2="-6.50614375" y2="-0.7272625" layer="51"/>
<rectangle x1="-3.93774375" y1="-0.74036875" x2="-3.78054375" y2="-0.7272625" layer="51"/>
<rectangle x1="-3.19084375" y1="-0.74036875" x2="-3.04664375" y2="-0.7272625" layer="51"/>
<rectangle x1="-0.46514375" y1="-0.74036875" x2="-0.32104375" y2="-0.7272625" layer="51"/>
<rectangle x1="0.29485625" y1="-0.74036875" x2="0.43895625" y2="-0.7272625" layer="51"/>
<rectangle x1="3.02045625" y1="-0.74036875" x2="3.16465625" y2="-0.7272625" layer="51"/>
<rectangle x1="3.75435625" y1="-0.74036875" x2="3.91155625" y2="-0.7272625" layer="51"/>
<rectangle x1="6.47995625" y1="-0.74036875" x2="6.63715625" y2="-0.7272625" layer="51"/>
<rectangle x1="7.37105625" y1="-0.74036875" x2="7.47585625" y2="-0.7272625" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.727259375" x2="-7.39724375" y2="-0.714159375" layer="51"/>
<rectangle x1="-6.65024375" y1="-0.727259375" x2="-6.49304375" y2="-0.714159375" layer="51"/>
<rectangle x1="-3.95084375" y1="-0.727259375" x2="-3.79364375" y2="-0.714159375" layer="51"/>
<rectangle x1="-3.17774375" y1="-0.727259375" x2="-3.03354375" y2="-0.714159375" layer="51"/>
<rectangle x1="-0.47824375" y1="-0.727259375" x2="-0.33414375" y2="-0.714159375" layer="51"/>
<rectangle x1="0.30795625" y1="-0.727259375" x2="0.45205625" y2="-0.714159375" layer="51"/>
<rectangle x1="3.00735625" y1="-0.727259375" x2="3.15155625" y2="-0.714159375" layer="51"/>
<rectangle x1="3.76745625" y1="-0.727259375" x2="3.92465625" y2="-0.714159375" layer="51"/>
<rectangle x1="6.46685625" y1="-0.727259375" x2="6.62405625" y2="-0.714159375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.727259375" x2="7.47585625" y2="-0.714159375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.714159375" x2="-7.39724375" y2="-0.701053125" layer="51"/>
<rectangle x1="-6.63714375" y1="-0.714159375" x2="-3.80674375" y2="-0.701053125" layer="51"/>
<rectangle x1="-3.16464375" y1="-0.714159375" x2="-0.34724375" y2="-0.701053125" layer="51"/>
<rectangle x1="0.32105625" y1="-0.714159375" x2="3.13845625" y2="-0.701053125" layer="51"/>
<rectangle x1="3.78055625" y1="-0.714159375" x2="6.61095625" y2="-0.701053125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.714159375" x2="7.47585625" y2="-0.701053125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.70105" x2="-7.39724375" y2="-0.68795" layer="51"/>
<rectangle x1="-6.62404375" y1="-0.70105" x2="-3.81984375" y2="-0.68795" layer="51"/>
<rectangle x1="-3.15154375" y1="-0.70105" x2="-0.36034375" y2="-0.68795" layer="51"/>
<rectangle x1="0.33415625" y1="-0.70105" x2="3.12535625" y2="-0.68795" layer="51"/>
<rectangle x1="3.79365625" y1="-0.70105" x2="6.59785625" y2="-0.68795" layer="51"/>
<rectangle x1="7.37105625" y1="-0.70105" x2="7.47585625" y2="-0.68795" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.68795" x2="-7.39724375" y2="-0.67485" layer="51"/>
<rectangle x1="-6.61094375" y1="-0.68795" x2="-3.83294375" y2="-0.67485" layer="51"/>
<rectangle x1="-3.13844375" y1="-0.68795" x2="-0.37344375" y2="-0.67485" layer="51"/>
<rectangle x1="0.34725625" y1="-0.68795" x2="3.11225625" y2="-0.67485" layer="51"/>
<rectangle x1="3.80675625" y1="-0.68795" x2="6.58475625" y2="-0.67485" layer="51"/>
<rectangle x1="7.37105625" y1="-0.68795" x2="7.47585625" y2="-0.67485" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.67485" x2="-7.39724375" y2="-0.66174375" layer="51"/>
<rectangle x1="-6.59784375" y1="-0.67485" x2="-3.84604375" y2="-0.66174375" layer="51"/>
<rectangle x1="-3.12534375" y1="-0.67485" x2="-0.38654375" y2="-0.66174375" layer="51"/>
<rectangle x1="0.36035625" y1="-0.67485" x2="3.09915625" y2="-0.66174375" layer="51"/>
<rectangle x1="3.81985625" y1="-0.67485" x2="6.57165625" y2="-0.66174375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.67485" x2="7.47585625" y2="-0.66174375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.661740625" x2="-7.39724375" y2="-0.648640625" layer="51"/>
<rectangle x1="-6.58474375" y1="-0.661740625" x2="-3.85914375" y2="-0.648640625" layer="51"/>
<rectangle x1="-3.11224375" y1="-0.661740625" x2="-0.39964375" y2="-0.648640625" layer="51"/>
<rectangle x1="0.37345625" y1="-0.661740625" x2="3.08595625" y2="-0.648640625" layer="51"/>
<rectangle x1="3.83295625" y1="-0.661740625" x2="6.55855625" y2="-0.648640625" layer="51"/>
<rectangle x1="7.37105625" y1="-0.661740625" x2="7.47585625" y2="-0.648640625" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.648640625" x2="-7.39724375" y2="-0.635534375" layer="51"/>
<rectangle x1="-6.57164375" y1="-0.648640625" x2="-3.87224375" y2="-0.635534375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.648640625" x2="-0.41274375" y2="-0.635534375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.648640625" x2="3.07285625" y2="-0.635534375" layer="51"/>
<rectangle x1="3.84605625" y1="-0.648640625" x2="6.54545625" y2="-0.635534375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.648640625" x2="7.47585625" y2="-0.635534375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.63553125" x2="-7.39724375" y2="-0.62243125" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.63553125" x2="-3.88534375" y2="-0.62243125" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.63553125" x2="-0.41274375" y2="-0.62243125" layer="51"/>
<rectangle x1="0.38655625" y1="-0.63553125" x2="3.07285625" y2="-0.62243125" layer="51"/>
<rectangle x1="3.85915625" y1="-0.63553125" x2="6.53235625" y2="-0.62243125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.63553125" x2="7.47585625" y2="-0.62243125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.62243125" x2="-7.39724375" y2="-0.60933125" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.62243125" x2="-3.88534375" y2="-0.60933125" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.62243125" x2="-0.41274375" y2="-0.60933125" layer="51"/>
<rectangle x1="0.38655625" y1="-0.62243125" x2="3.07285625" y2="-0.60933125" layer="51"/>
<rectangle x1="3.85915625" y1="-0.62243125" x2="6.53235625" y2="-0.60933125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.62243125" x2="7.47585625" y2="-0.60933125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.60933125" x2="-7.39724375" y2="-0.59621875" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.60933125" x2="-6.45374375" y2="-0.59621875" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.60933125" x2="-3.88534375" y2="-0.59621875" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.60933125" x2="-2.99424375" y2="-0.59621875" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.60933125" x2="-0.41274375" y2="-0.59621875" layer="51"/>
<rectangle x1="0.38655625" y1="-0.60933125" x2="0.49145625" y2="-0.59621875" layer="51"/>
<rectangle x1="2.96805625" y1="-0.60933125" x2="3.07285625" y2="-0.59621875" layer="51"/>
<rectangle x1="3.85915625" y1="-0.60933125" x2="3.96395625" y2="-0.59621875" layer="51"/>
<rectangle x1="6.42755625" y1="-0.60933125" x2="6.53235625" y2="-0.59621875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.60933125" x2="7.47585625" y2="-0.59621875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.59621875" x2="-7.39724375" y2="-0.58311875" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.59621875" x2="-6.45374375" y2="-0.58311875" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.59621875" x2="-3.88534375" y2="-0.58311875" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.59621875" x2="-2.99424375" y2="-0.58311875" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.59621875" x2="-0.41274375" y2="-0.58311875" layer="51"/>
<rectangle x1="0.38655625" y1="-0.59621875" x2="0.49145625" y2="-0.58311875" layer="51"/>
<rectangle x1="2.96805625" y1="-0.59621875" x2="3.07285625" y2="-0.58311875" layer="51"/>
<rectangle x1="3.85915625" y1="-0.59621875" x2="3.96395625" y2="-0.58311875" layer="51"/>
<rectangle x1="6.42755625" y1="-0.59621875" x2="6.53235625" y2="-0.58311875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.59621875" x2="7.47585625" y2="-0.58311875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.58311875" x2="-7.39724375" y2="-0.5700125" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.58311875" x2="-6.45374375" y2="-0.5700125" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.58311875" x2="-3.88534375" y2="-0.5700125" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.58311875" x2="-2.99424375" y2="-0.5700125" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.58311875" x2="-0.41274375" y2="-0.5700125" layer="51"/>
<rectangle x1="0.38655625" y1="-0.58311875" x2="0.49145625" y2="-0.5700125" layer="51"/>
<rectangle x1="2.96805625" y1="-0.58311875" x2="3.07285625" y2="-0.5700125" layer="51"/>
<rectangle x1="3.85915625" y1="-0.58311875" x2="3.96395625" y2="-0.5700125" layer="51"/>
<rectangle x1="6.42755625" y1="-0.58311875" x2="6.53235625" y2="-0.5700125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.58311875" x2="7.47585625" y2="-0.5700125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.570009375" x2="-7.39724375" y2="-0.556909375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.570009375" x2="-6.45374375" y2="-0.556909375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.570009375" x2="-3.88534375" y2="-0.556909375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.570009375" x2="-2.99424375" y2="-0.556909375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.570009375" x2="-0.41274375" y2="-0.556909375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.570009375" x2="0.49145625" y2="-0.556909375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.570009375" x2="3.07285625" y2="-0.556909375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.570009375" x2="3.96395625" y2="-0.556909375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.570009375" x2="6.53235625" y2="-0.556909375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.570009375" x2="7.47585625" y2="-0.556909375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.556909375" x2="-7.39724375" y2="-0.543809375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.556909375" x2="-6.45374375" y2="-0.543809375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.556909375" x2="-3.88534375" y2="-0.543809375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.556909375" x2="-2.99424375" y2="-0.543809375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.556909375" x2="-0.41274375" y2="-0.543809375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.556909375" x2="0.49145625" y2="-0.543809375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.556909375" x2="3.07285625" y2="-0.543809375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.556909375" x2="3.96395625" y2="-0.543809375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.556909375" x2="6.53235625" y2="-0.543809375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.556909375" x2="7.47585625" y2="-0.543809375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.543809375" x2="-7.39724375" y2="-0.530703125" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.543809375" x2="-6.45374375" y2="-0.530703125" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.543809375" x2="-3.88534375" y2="-0.530703125" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.543809375" x2="-2.99424375" y2="-0.530703125" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.543809375" x2="-0.41274375" y2="-0.530703125" layer="51"/>
<rectangle x1="0.38655625" y1="-0.543809375" x2="0.49145625" y2="-0.530703125" layer="51"/>
<rectangle x1="2.96805625" y1="-0.543809375" x2="3.07285625" y2="-0.530703125" layer="51"/>
<rectangle x1="3.85915625" y1="-0.543809375" x2="3.96395625" y2="-0.530703125" layer="51"/>
<rectangle x1="6.42755625" y1="-0.543809375" x2="6.53235625" y2="-0.530703125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.543809375" x2="7.47585625" y2="-0.530703125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.5307" x2="-7.39724375" y2="-0.5176" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.5307" x2="-6.45374375" y2="-0.5176" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.5307" x2="-3.88534375" y2="-0.5176" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.5307" x2="-2.99424375" y2="-0.5176" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.5307" x2="-0.41274375" y2="-0.5176" layer="51"/>
<rectangle x1="0.38655625" y1="-0.5307" x2="0.49145625" y2="-0.5176" layer="51"/>
<rectangle x1="2.96805625" y1="-0.5307" x2="3.07285625" y2="-0.5176" layer="51"/>
<rectangle x1="3.85915625" y1="-0.5307" x2="3.96395625" y2="-0.5176" layer="51"/>
<rectangle x1="6.42755625" y1="-0.5307" x2="6.53235625" y2="-0.5176" layer="51"/>
<rectangle x1="7.37105625" y1="-0.5307" x2="7.47585625" y2="-0.5176" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.5176" x2="-7.39724375" y2="-0.50449375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.5176" x2="-6.45374375" y2="-0.50449375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.5176" x2="-3.88534375" y2="-0.50449375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.5176" x2="-2.99424375" y2="-0.50449375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.5176" x2="-0.41274375" y2="-0.50449375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.5176" x2="0.49145625" y2="-0.50449375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.5176" x2="3.07285625" y2="-0.50449375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.5176" x2="3.96395625" y2="-0.50449375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.5176" x2="6.53235625" y2="-0.50449375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.5176" x2="7.47585625" y2="-0.50449375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.504490625" x2="-7.39724375" y2="-0.491390625" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.504490625" x2="-6.45374375" y2="-0.491390625" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.504490625" x2="-3.88534375" y2="-0.491390625" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.504490625" x2="-2.99424375" y2="-0.491390625" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.504490625" x2="-0.41274375" y2="-0.491390625" layer="51"/>
<rectangle x1="0.38655625" y1="-0.504490625" x2="0.49145625" y2="-0.491390625" layer="51"/>
<rectangle x1="2.96805625" y1="-0.504490625" x2="3.07285625" y2="-0.491390625" layer="51"/>
<rectangle x1="3.85915625" y1="-0.504490625" x2="3.96395625" y2="-0.491390625" layer="51"/>
<rectangle x1="6.42755625" y1="-0.504490625" x2="6.53235625" y2="-0.491390625" layer="51"/>
<rectangle x1="7.37105625" y1="-0.504490625" x2="7.47585625" y2="-0.491390625" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.491390625" x2="-7.39724375" y2="-0.478290625" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.491390625" x2="-6.45374375" y2="-0.478290625" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.491390625" x2="-3.88534375" y2="-0.478290625" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.491390625" x2="-2.99424375" y2="-0.478290625" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.491390625" x2="-0.41274375" y2="-0.478290625" layer="51"/>
<rectangle x1="0.38655625" y1="-0.491390625" x2="0.49145625" y2="-0.478290625" layer="51"/>
<rectangle x1="2.96805625" y1="-0.491390625" x2="3.07285625" y2="-0.478290625" layer="51"/>
<rectangle x1="3.85915625" y1="-0.491390625" x2="3.96395625" y2="-0.478290625" layer="51"/>
<rectangle x1="6.42755625" y1="-0.491390625" x2="6.53235625" y2="-0.478290625" layer="51"/>
<rectangle x1="7.37105625" y1="-0.491390625" x2="7.47585625" y2="-0.478290625" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.478290625" x2="-7.39724375" y2="-0.465184375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.478290625" x2="-6.45374375" y2="-0.465184375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.478290625" x2="-3.88534375" y2="-0.465184375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.478290625" x2="-2.99424375" y2="-0.465184375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.478290625" x2="-0.41274375" y2="-0.465184375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.478290625" x2="0.49145625" y2="-0.465184375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.478290625" x2="3.07285625" y2="-0.465184375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.478290625" x2="3.96395625" y2="-0.465184375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.478290625" x2="6.53235625" y2="-0.465184375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.478290625" x2="7.47585625" y2="-0.465184375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.46518125" x2="-7.39724375" y2="-0.45208125" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.46518125" x2="-6.45374375" y2="-0.45208125" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.46518125" x2="-3.88534375" y2="-0.45208125" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.46518125" x2="-2.99424375" y2="-0.45208125" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.46518125" x2="-0.41274375" y2="-0.45208125" layer="51"/>
<rectangle x1="0.38655625" y1="-0.46518125" x2="0.49145625" y2="-0.45208125" layer="51"/>
<rectangle x1="2.96805625" y1="-0.46518125" x2="3.07285625" y2="-0.45208125" layer="51"/>
<rectangle x1="3.85915625" y1="-0.46518125" x2="3.96395625" y2="-0.45208125" layer="51"/>
<rectangle x1="6.42755625" y1="-0.46518125" x2="6.53235625" y2="-0.45208125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.46518125" x2="7.47585625" y2="-0.45208125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.45208125" x2="-7.39724375" y2="-0.43896875" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.45208125" x2="-6.45374375" y2="-0.43896875" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.45208125" x2="-3.88534375" y2="-0.43896875" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.45208125" x2="-2.99424375" y2="-0.43896875" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.45208125" x2="-0.41274375" y2="-0.43896875" layer="51"/>
<rectangle x1="0.38655625" y1="-0.45208125" x2="0.49145625" y2="-0.43896875" layer="51"/>
<rectangle x1="2.96805625" y1="-0.45208125" x2="3.07285625" y2="-0.43896875" layer="51"/>
<rectangle x1="3.85915625" y1="-0.45208125" x2="3.96395625" y2="-0.43896875" layer="51"/>
<rectangle x1="6.42755625" y1="-0.45208125" x2="6.53235625" y2="-0.43896875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.45208125" x2="7.47585625" y2="-0.43896875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.43896875" x2="-7.39724375" y2="-0.42586875" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.43896875" x2="-6.45374375" y2="-0.42586875" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.43896875" x2="-3.88534375" y2="-0.42586875" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.43896875" x2="-2.99424375" y2="-0.42586875" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.43896875" x2="-0.41274375" y2="-0.42586875" layer="51"/>
<rectangle x1="0.38655625" y1="-0.43896875" x2="0.49145625" y2="-0.42586875" layer="51"/>
<rectangle x1="2.96805625" y1="-0.43896875" x2="3.07285625" y2="-0.42586875" layer="51"/>
<rectangle x1="3.85915625" y1="-0.43896875" x2="3.96395625" y2="-0.42586875" layer="51"/>
<rectangle x1="6.42755625" y1="-0.43896875" x2="6.53235625" y2="-0.42586875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.43896875" x2="7.47585625" y2="-0.42586875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.42586875" x2="-7.39724375" y2="-0.41276875" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.42586875" x2="-6.45374375" y2="-0.41276875" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.42586875" x2="-3.88534375" y2="-0.41276875" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.42586875" x2="-2.99424375" y2="-0.41276875" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.42586875" x2="-0.41274375" y2="-0.41276875" layer="51"/>
<rectangle x1="0.38655625" y1="-0.42586875" x2="0.49145625" y2="-0.41276875" layer="51"/>
<rectangle x1="2.96805625" y1="-0.42586875" x2="3.07285625" y2="-0.41276875" layer="51"/>
<rectangle x1="3.85915625" y1="-0.42586875" x2="3.96395625" y2="-0.41276875" layer="51"/>
<rectangle x1="6.42755625" y1="-0.42586875" x2="6.53235625" y2="-0.41276875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.42586875" x2="7.47585625" y2="-0.41276875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.41276875" x2="-7.39724375" y2="-0.3996625" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.41276875" x2="-6.45374375" y2="-0.3996625" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.41276875" x2="-3.88534375" y2="-0.3996625" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.41276875" x2="-2.99424375" y2="-0.3996625" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.41276875" x2="-0.41274375" y2="-0.3996625" layer="51"/>
<rectangle x1="0.38655625" y1="-0.41276875" x2="0.49145625" y2="-0.3996625" layer="51"/>
<rectangle x1="2.96805625" y1="-0.41276875" x2="3.07285625" y2="-0.3996625" layer="51"/>
<rectangle x1="3.85915625" y1="-0.41276875" x2="3.96395625" y2="-0.3996625" layer="51"/>
<rectangle x1="6.42755625" y1="-0.41276875" x2="6.53235625" y2="-0.3996625" layer="51"/>
<rectangle x1="7.37105625" y1="-0.41276875" x2="7.47585625" y2="-0.3996625" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.399659375" x2="-7.39724375" y2="-0.386559375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.399659375" x2="-6.45374375" y2="-0.386559375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.399659375" x2="-3.88534375" y2="-0.386559375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.399659375" x2="-2.99424375" y2="-0.386559375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.399659375" x2="-0.41274375" y2="-0.386559375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.399659375" x2="0.49145625" y2="-0.386559375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.399659375" x2="3.07285625" y2="-0.386559375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.399659375" x2="3.96395625" y2="-0.386559375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.399659375" x2="6.53235625" y2="-0.386559375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.399659375" x2="7.47585625" y2="-0.386559375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.386559375" x2="-7.39724375" y2="-0.373453125" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.386559375" x2="-6.45374375" y2="-0.373453125" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.386559375" x2="-3.88534375" y2="-0.373453125" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.386559375" x2="-2.99424375" y2="-0.373453125" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.386559375" x2="-0.41274375" y2="-0.373453125" layer="51"/>
<rectangle x1="0.38655625" y1="-0.386559375" x2="0.49145625" y2="-0.373453125" layer="51"/>
<rectangle x1="2.96805625" y1="-0.386559375" x2="3.07285625" y2="-0.373453125" layer="51"/>
<rectangle x1="3.85915625" y1="-0.386559375" x2="3.96395625" y2="-0.373453125" layer="51"/>
<rectangle x1="6.42755625" y1="-0.386559375" x2="6.53235625" y2="-0.373453125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.386559375" x2="7.47585625" y2="-0.373453125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.37345" x2="-7.39724375" y2="-0.36035" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.37345" x2="-6.45374375" y2="-0.36035" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.37345" x2="-3.88534375" y2="-0.36035" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.37345" x2="-2.99424375" y2="-0.36035" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.37345" x2="-0.41274375" y2="-0.36035" layer="51"/>
<rectangle x1="0.38655625" y1="-0.37345" x2="0.49145625" y2="-0.36035" layer="51"/>
<rectangle x1="2.96805625" y1="-0.37345" x2="3.07285625" y2="-0.36035" layer="51"/>
<rectangle x1="3.85915625" y1="-0.37345" x2="3.96395625" y2="-0.36035" layer="51"/>
<rectangle x1="6.42755625" y1="-0.37345" x2="6.53235625" y2="-0.36035" layer="51"/>
<rectangle x1="7.37105625" y1="-0.37345" x2="7.47585625" y2="-0.36035" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.36035" x2="-7.39724375" y2="-0.34725" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.36035" x2="-6.45374375" y2="-0.34725" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.36035" x2="-3.88534375" y2="-0.34725" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.36035" x2="-2.99424375" y2="-0.34725" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.36035" x2="-0.41274375" y2="-0.34725" layer="51"/>
<rectangle x1="0.38655625" y1="-0.36035" x2="0.49145625" y2="-0.34725" layer="51"/>
<rectangle x1="2.96805625" y1="-0.36035" x2="3.07285625" y2="-0.34725" layer="51"/>
<rectangle x1="3.85915625" y1="-0.36035" x2="3.96395625" y2="-0.34725" layer="51"/>
<rectangle x1="6.42755625" y1="-0.36035" x2="6.53235625" y2="-0.34725" layer="51"/>
<rectangle x1="7.37105625" y1="-0.36035" x2="7.47585625" y2="-0.34725" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.34725" x2="-7.39724375" y2="-0.33414375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.34725" x2="-6.45374375" y2="-0.33414375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.34725" x2="-3.88534375" y2="-0.33414375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.34725" x2="-2.99424375" y2="-0.33414375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.34725" x2="-0.41274375" y2="-0.33414375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.34725" x2="0.49145625" y2="-0.33414375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.34725" x2="3.07285625" y2="-0.33414375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.34725" x2="3.96395625" y2="-0.33414375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.34725" x2="6.53235625" y2="-0.33414375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.34725" x2="7.47585625" y2="-0.33414375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.334140625" x2="-7.39724375" y2="-0.321040625" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.334140625" x2="-6.45374375" y2="-0.321040625" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.334140625" x2="-3.88534375" y2="-0.321040625" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.334140625" x2="-2.99424375" y2="-0.321040625" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.334140625" x2="-0.41274375" y2="-0.321040625" layer="51"/>
<rectangle x1="0.38655625" y1="-0.334140625" x2="0.49145625" y2="-0.321040625" layer="51"/>
<rectangle x1="2.96805625" y1="-0.334140625" x2="3.07285625" y2="-0.321040625" layer="51"/>
<rectangle x1="3.85915625" y1="-0.334140625" x2="3.96395625" y2="-0.321040625" layer="51"/>
<rectangle x1="6.42755625" y1="-0.334140625" x2="6.53235625" y2="-0.321040625" layer="51"/>
<rectangle x1="7.37105625" y1="-0.334140625" x2="7.47585625" y2="-0.321040625" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.321040625" x2="-7.39724375" y2="-0.307934375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.321040625" x2="-6.45374375" y2="-0.307934375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.321040625" x2="-3.88534375" y2="-0.307934375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.321040625" x2="-2.99424375" y2="-0.307934375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.321040625" x2="-0.41274375" y2="-0.307934375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.321040625" x2="0.49145625" y2="-0.307934375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.321040625" x2="3.07285625" y2="-0.307934375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.321040625" x2="3.96395625" y2="-0.307934375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.321040625" x2="6.53235625" y2="-0.307934375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.321040625" x2="7.47585625" y2="-0.307934375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.30793125" x2="-7.39724375" y2="-0.29483125" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.30793125" x2="-6.45374375" y2="-0.29483125" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.30793125" x2="-3.88534375" y2="-0.29483125" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.30793125" x2="-2.99424375" y2="-0.29483125" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.30793125" x2="-0.41274375" y2="-0.29483125" layer="51"/>
<rectangle x1="0.38655625" y1="-0.30793125" x2="0.49145625" y2="-0.29483125" layer="51"/>
<rectangle x1="2.96805625" y1="-0.30793125" x2="3.07285625" y2="-0.29483125" layer="51"/>
<rectangle x1="3.85915625" y1="-0.30793125" x2="3.96395625" y2="-0.29483125" layer="51"/>
<rectangle x1="6.42755625" y1="-0.30793125" x2="6.53235625" y2="-0.29483125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.30793125" x2="7.47585625" y2="-0.29483125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.29483125" x2="-7.39724375" y2="-0.28173125" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.29483125" x2="-6.45374375" y2="-0.28173125" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.29483125" x2="-3.88534375" y2="-0.28173125" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.29483125" x2="-2.99424375" y2="-0.28173125" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.29483125" x2="-0.41274375" y2="-0.28173125" layer="51"/>
<rectangle x1="0.38655625" y1="-0.29483125" x2="0.49145625" y2="-0.28173125" layer="51"/>
<rectangle x1="2.96805625" y1="-0.29483125" x2="3.07285625" y2="-0.28173125" layer="51"/>
<rectangle x1="3.85915625" y1="-0.29483125" x2="3.96395625" y2="-0.28173125" layer="51"/>
<rectangle x1="6.42755625" y1="-0.29483125" x2="6.53235625" y2="-0.28173125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.29483125" x2="7.47585625" y2="-0.28173125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.28173125" x2="-7.39724375" y2="-0.26861875" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.28173125" x2="-6.45374375" y2="-0.26861875" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.28173125" x2="-3.88534375" y2="-0.26861875" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.28173125" x2="-2.99424375" y2="-0.26861875" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.28173125" x2="-0.41274375" y2="-0.26861875" layer="51"/>
<rectangle x1="0.38655625" y1="-0.28173125" x2="0.49145625" y2="-0.26861875" layer="51"/>
<rectangle x1="2.96805625" y1="-0.28173125" x2="3.07285625" y2="-0.26861875" layer="51"/>
<rectangle x1="3.85915625" y1="-0.28173125" x2="3.96395625" y2="-0.26861875" layer="51"/>
<rectangle x1="6.42755625" y1="-0.28173125" x2="6.53235625" y2="-0.26861875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.28173125" x2="7.47585625" y2="-0.26861875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.26861875" x2="-7.39724375" y2="-0.25551875" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.26861875" x2="-6.45374375" y2="-0.25551875" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.26861875" x2="-3.88534375" y2="-0.25551875" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.26861875" x2="-2.99424375" y2="-0.25551875" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.26861875" x2="-0.41274375" y2="-0.25551875" layer="51"/>
<rectangle x1="0.38655625" y1="-0.26861875" x2="0.49145625" y2="-0.25551875" layer="51"/>
<rectangle x1="2.96805625" y1="-0.26861875" x2="3.07285625" y2="-0.25551875" layer="51"/>
<rectangle x1="3.85915625" y1="-0.26861875" x2="3.96395625" y2="-0.25551875" layer="51"/>
<rectangle x1="6.42755625" y1="-0.26861875" x2="6.53235625" y2="-0.25551875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.26861875" x2="7.47585625" y2="-0.25551875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.25551875" x2="-7.39724375" y2="-0.2424125" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.25551875" x2="-6.45374375" y2="-0.2424125" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.25551875" x2="-3.88534375" y2="-0.2424125" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.25551875" x2="-2.99424375" y2="-0.2424125" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.25551875" x2="-0.41274375" y2="-0.2424125" layer="51"/>
<rectangle x1="0.38655625" y1="-0.25551875" x2="0.49145625" y2="-0.2424125" layer="51"/>
<rectangle x1="2.96805625" y1="-0.25551875" x2="3.07285625" y2="-0.2424125" layer="51"/>
<rectangle x1="3.85915625" y1="-0.25551875" x2="3.96395625" y2="-0.2424125" layer="51"/>
<rectangle x1="6.42755625" y1="-0.25551875" x2="6.53235625" y2="-0.2424125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.25551875" x2="7.47585625" y2="-0.2424125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.242409375" x2="-7.39724375" y2="-0.229309375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.242409375" x2="-6.45374375" y2="-0.229309375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.242409375" x2="-3.88534375" y2="-0.229309375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.242409375" x2="-2.99424375" y2="-0.229309375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.242409375" x2="-0.41274375" y2="-0.229309375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.242409375" x2="0.49145625" y2="-0.229309375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.242409375" x2="3.07285625" y2="-0.229309375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.242409375" x2="3.96395625" y2="-0.229309375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.242409375" x2="6.53235625" y2="-0.229309375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.242409375" x2="7.47585625" y2="-0.229309375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.229309375" x2="-7.39724375" y2="-0.216209375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.229309375" x2="-6.45374375" y2="-0.216209375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.229309375" x2="-3.88534375" y2="-0.216209375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.229309375" x2="-2.99424375" y2="-0.216209375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.229309375" x2="-0.41274375" y2="-0.216209375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.229309375" x2="0.49145625" y2="-0.216209375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.229309375" x2="3.07285625" y2="-0.216209375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.229309375" x2="3.96395625" y2="-0.216209375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.229309375" x2="6.53235625" y2="-0.216209375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.229309375" x2="7.47585625" y2="-0.216209375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.216209375" x2="-7.39724375" y2="-0.203103125" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.216209375" x2="-6.45374375" y2="-0.203103125" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.216209375" x2="-3.88534375" y2="-0.203103125" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.216209375" x2="-2.99424375" y2="-0.203103125" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.216209375" x2="-0.41274375" y2="-0.203103125" layer="51"/>
<rectangle x1="0.38655625" y1="-0.216209375" x2="0.49145625" y2="-0.203103125" layer="51"/>
<rectangle x1="2.96805625" y1="-0.216209375" x2="3.07285625" y2="-0.203103125" layer="51"/>
<rectangle x1="3.85915625" y1="-0.216209375" x2="3.96395625" y2="-0.203103125" layer="51"/>
<rectangle x1="6.42755625" y1="-0.216209375" x2="6.53235625" y2="-0.203103125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.216209375" x2="7.47585625" y2="-0.203103125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.2031" x2="-7.39724375" y2="-0.19" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.2031" x2="-6.45374375" y2="-0.19" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.2031" x2="-3.88534375" y2="-0.19" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.2031" x2="-2.99424375" y2="-0.19" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.2031" x2="-0.41274375" y2="-0.19" layer="51"/>
<rectangle x1="0.38655625" y1="-0.2031" x2="0.49145625" y2="-0.19" layer="51"/>
<rectangle x1="2.96805625" y1="-0.2031" x2="3.07285625" y2="-0.19" layer="51"/>
<rectangle x1="3.85915625" y1="-0.2031" x2="3.96395625" y2="-0.19" layer="51"/>
<rectangle x1="6.42755625" y1="-0.2031" x2="6.53235625" y2="-0.19" layer="51"/>
<rectangle x1="7.37105625" y1="-0.2031" x2="7.47585625" y2="-0.19" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.19" x2="-7.39724375" y2="-0.17689375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.19" x2="-6.45374375" y2="-0.17689375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.19" x2="-3.88534375" y2="-0.17689375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.19" x2="-2.99424375" y2="-0.17689375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.19" x2="-0.41274375" y2="-0.17689375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.19" x2="0.49145625" y2="-0.17689375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.19" x2="3.07285625" y2="-0.17689375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.19" x2="3.96395625" y2="-0.17689375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.19" x2="6.53235625" y2="-0.17689375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.19" x2="7.47585625" y2="-0.17689375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.176890625" x2="-7.39724375" y2="-0.163790625" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.176890625" x2="-6.45374375" y2="-0.163790625" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.176890625" x2="-3.88534375" y2="-0.163790625" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.176890625" x2="-2.99424375" y2="-0.163790625" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.176890625" x2="-0.41274375" y2="-0.163790625" layer="51"/>
<rectangle x1="0.38655625" y1="-0.176890625" x2="0.49145625" y2="-0.163790625" layer="51"/>
<rectangle x1="2.96805625" y1="-0.176890625" x2="3.07285625" y2="-0.163790625" layer="51"/>
<rectangle x1="3.85915625" y1="-0.176890625" x2="3.96395625" y2="-0.163790625" layer="51"/>
<rectangle x1="6.42755625" y1="-0.176890625" x2="6.53235625" y2="-0.163790625" layer="51"/>
<rectangle x1="7.37105625" y1="-0.176890625" x2="7.47585625" y2="-0.163790625" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.163790625" x2="-7.39724375" y2="-0.150690625" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.163790625" x2="-6.45374375" y2="-0.150690625" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.163790625" x2="-3.88534375" y2="-0.150690625" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.163790625" x2="-2.99424375" y2="-0.150690625" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.163790625" x2="-0.41274375" y2="-0.150690625" layer="51"/>
<rectangle x1="0.38655625" y1="-0.163790625" x2="0.49145625" y2="-0.150690625" layer="51"/>
<rectangle x1="2.96805625" y1="-0.163790625" x2="3.07285625" y2="-0.150690625" layer="51"/>
<rectangle x1="3.85915625" y1="-0.163790625" x2="3.96395625" y2="-0.150690625" layer="51"/>
<rectangle x1="6.42755625" y1="-0.163790625" x2="6.53235625" y2="-0.150690625" layer="51"/>
<rectangle x1="7.37105625" y1="-0.163790625" x2="7.47585625" y2="-0.150690625" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.150690625" x2="-7.39724375" y2="-0.137584375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.150690625" x2="-6.45374375" y2="-0.137584375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.150690625" x2="-3.88534375" y2="-0.137584375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.150690625" x2="-2.99424375" y2="-0.137584375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.150690625" x2="-0.41274375" y2="-0.137584375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.150690625" x2="0.49145625" y2="-0.137584375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.150690625" x2="3.07285625" y2="-0.137584375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.150690625" x2="3.96395625" y2="-0.137584375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.150690625" x2="6.53235625" y2="-0.137584375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.150690625" x2="7.47585625" y2="-0.137584375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.13758125" x2="-7.39724375" y2="-0.12448125" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.13758125" x2="-6.45374375" y2="-0.12448125" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.13758125" x2="-3.88534375" y2="-0.12448125" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.13758125" x2="-2.99424375" y2="-0.12448125" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.13758125" x2="-0.41274375" y2="-0.12448125" layer="51"/>
<rectangle x1="0.38655625" y1="-0.13758125" x2="0.49145625" y2="-0.12448125" layer="51"/>
<rectangle x1="2.96805625" y1="-0.13758125" x2="3.07285625" y2="-0.12448125" layer="51"/>
<rectangle x1="3.85915625" y1="-0.13758125" x2="3.96395625" y2="-0.12448125" layer="51"/>
<rectangle x1="6.42755625" y1="-0.13758125" x2="6.53235625" y2="-0.12448125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.13758125" x2="7.47585625" y2="-0.12448125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.12448125" x2="-7.39724375" y2="-0.11136875" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.12448125" x2="-6.45374375" y2="-0.11136875" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.12448125" x2="-3.88534375" y2="-0.11136875" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.12448125" x2="-2.99424375" y2="-0.11136875" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.12448125" x2="-0.41274375" y2="-0.11136875" layer="51"/>
<rectangle x1="0.38655625" y1="-0.12448125" x2="0.49145625" y2="-0.11136875" layer="51"/>
<rectangle x1="2.96805625" y1="-0.12448125" x2="3.07285625" y2="-0.11136875" layer="51"/>
<rectangle x1="3.85915625" y1="-0.12448125" x2="3.96395625" y2="-0.11136875" layer="51"/>
<rectangle x1="6.42755625" y1="-0.12448125" x2="6.53235625" y2="-0.11136875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.12448125" x2="7.47585625" y2="-0.11136875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.11136875" x2="-7.39724375" y2="-0.09826875" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.11136875" x2="-6.45374375" y2="-0.09826875" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.11136875" x2="-3.88534375" y2="-0.09826875" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.11136875" x2="-2.99424375" y2="-0.09826875" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.11136875" x2="-0.41274375" y2="-0.09826875" layer="51"/>
<rectangle x1="0.38655625" y1="-0.11136875" x2="0.49145625" y2="-0.09826875" layer="51"/>
<rectangle x1="2.96805625" y1="-0.11136875" x2="3.07285625" y2="-0.09826875" layer="51"/>
<rectangle x1="3.85915625" y1="-0.11136875" x2="3.96395625" y2="-0.09826875" layer="51"/>
<rectangle x1="6.42755625" y1="-0.11136875" x2="6.53235625" y2="-0.09826875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.11136875" x2="7.47585625" y2="-0.09826875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.09826875" x2="-7.39724375" y2="-0.08516875" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.09826875" x2="-6.45374375" y2="-0.08516875" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.09826875" x2="-3.88534375" y2="-0.08516875" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.09826875" x2="-2.99424375" y2="-0.08516875" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.09826875" x2="-0.41274375" y2="-0.08516875" layer="51"/>
<rectangle x1="0.38655625" y1="-0.09826875" x2="0.49145625" y2="-0.08516875" layer="51"/>
<rectangle x1="2.96805625" y1="-0.09826875" x2="3.07285625" y2="-0.08516875" layer="51"/>
<rectangle x1="3.85915625" y1="-0.09826875" x2="3.96395625" y2="-0.08516875" layer="51"/>
<rectangle x1="6.42755625" y1="-0.09826875" x2="6.53235625" y2="-0.08516875" layer="51"/>
<rectangle x1="7.37105625" y1="-0.09826875" x2="7.47585625" y2="-0.08516875" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.08516875" x2="-7.39724375" y2="-0.0720625" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.08516875" x2="-6.45374375" y2="-0.0720625" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.08516875" x2="-3.88534375" y2="-0.0720625" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.08516875" x2="-2.99424375" y2="-0.0720625" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.08516875" x2="-0.41274375" y2="-0.0720625" layer="51"/>
<rectangle x1="0.38655625" y1="-0.08516875" x2="0.49145625" y2="-0.0720625" layer="51"/>
<rectangle x1="2.96805625" y1="-0.08516875" x2="3.07285625" y2="-0.0720625" layer="51"/>
<rectangle x1="3.85915625" y1="-0.08516875" x2="3.96395625" y2="-0.0720625" layer="51"/>
<rectangle x1="6.42755625" y1="-0.08516875" x2="6.53235625" y2="-0.0720625" layer="51"/>
<rectangle x1="7.37105625" y1="-0.08516875" x2="7.47585625" y2="-0.0720625" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.072059375" x2="-7.39724375" y2="-0.058959375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.072059375" x2="-6.45374375" y2="-0.058959375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.072059375" x2="-3.88534375" y2="-0.058959375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.072059375" x2="-2.99424375" y2="-0.058959375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.072059375" x2="-0.41274375" y2="-0.058959375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.072059375" x2="0.49145625" y2="-0.058959375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.072059375" x2="3.07285625" y2="-0.058959375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.072059375" x2="3.96395625" y2="-0.058959375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.072059375" x2="6.53235625" y2="-0.058959375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.072059375" x2="7.47585625" y2="-0.058959375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.058959375" x2="-7.39724375" y2="-0.045853125" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.058959375" x2="-6.45374375" y2="-0.045853125" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.058959375" x2="-3.88534375" y2="-0.045853125" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.058959375" x2="-2.99424375" y2="-0.045853125" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.058959375" x2="-0.41274375" y2="-0.045853125" layer="51"/>
<rectangle x1="0.38655625" y1="-0.058959375" x2="0.49145625" y2="-0.045853125" layer="51"/>
<rectangle x1="2.96805625" y1="-0.058959375" x2="3.07285625" y2="-0.045853125" layer="51"/>
<rectangle x1="3.85915625" y1="-0.058959375" x2="3.96395625" y2="-0.045853125" layer="51"/>
<rectangle x1="6.42755625" y1="-0.058959375" x2="6.53235625" y2="-0.045853125" layer="51"/>
<rectangle x1="7.37105625" y1="-0.058959375" x2="7.47585625" y2="-0.045853125" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.04585" x2="-7.39724375" y2="-0.03275" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.04585" x2="-6.45374375" y2="-0.03275" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.04585" x2="-3.88534375" y2="-0.03275" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.04585" x2="-2.99424375" y2="-0.03275" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.04585" x2="-0.41274375" y2="-0.03275" layer="51"/>
<rectangle x1="0.38655625" y1="-0.04585" x2="0.49145625" y2="-0.03275" layer="51"/>
<rectangle x1="2.96805625" y1="-0.04585" x2="3.07285625" y2="-0.03275" layer="51"/>
<rectangle x1="3.85915625" y1="-0.04585" x2="3.96395625" y2="-0.03275" layer="51"/>
<rectangle x1="6.42755625" y1="-0.04585" x2="6.53235625" y2="-0.03275" layer="51"/>
<rectangle x1="7.37105625" y1="-0.04585" x2="7.47585625" y2="-0.03275" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.03275" x2="-7.39724375" y2="-0.01965" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.03275" x2="-6.45374375" y2="-0.01965" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.03275" x2="-3.88534375" y2="-0.01965" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.03275" x2="-2.99424375" y2="-0.01965" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.03275" x2="-0.41274375" y2="-0.01965" layer="51"/>
<rectangle x1="0.38655625" y1="-0.03275" x2="0.49145625" y2="-0.01965" layer="51"/>
<rectangle x1="2.96805625" y1="-0.03275" x2="3.07285625" y2="-0.01965" layer="51"/>
<rectangle x1="3.85915625" y1="-0.03275" x2="3.96395625" y2="-0.01965" layer="51"/>
<rectangle x1="6.42755625" y1="-0.03275" x2="6.53235625" y2="-0.01965" layer="51"/>
<rectangle x1="7.37105625" y1="-0.03275" x2="7.47585625" y2="-0.01965" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.01965" x2="-7.39724375" y2="-0.00654375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.01965" x2="-6.45374375" y2="-0.00654375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.01965" x2="-3.88534375" y2="-0.00654375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.01965" x2="-2.99424375" y2="-0.00654375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.01965" x2="-0.41274375" y2="-0.00654375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.01965" x2="0.49145625" y2="-0.00654375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.01965" x2="3.07285625" y2="-0.00654375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.01965" x2="3.96395625" y2="-0.00654375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.01965" x2="6.53235625" y2="-0.00654375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.01965" x2="7.47585625" y2="-0.00654375" layer="51"/>
<rectangle x1="-7.50204375" y1="-0.006540625" x2="-7.39724375" y2="0.006559375" layer="51"/>
<rectangle x1="-6.55854375" y1="-0.006540625" x2="-6.45374375" y2="0.006559375" layer="51"/>
<rectangle x1="-3.99014375" y1="-0.006540625" x2="-3.88534375" y2="0.006559375" layer="51"/>
<rectangle x1="-3.09904375" y1="-0.006540625" x2="-2.99424375" y2="0.006559375" layer="51"/>
<rectangle x1="-0.51764375" y1="-0.006540625" x2="-0.41274375" y2="0.006559375" layer="51"/>
<rectangle x1="0.38655625" y1="-0.006540625" x2="0.49145625" y2="0.006559375" layer="51"/>
<rectangle x1="2.96805625" y1="-0.006540625" x2="3.07285625" y2="0.006559375" layer="51"/>
<rectangle x1="3.85915625" y1="-0.006540625" x2="3.96395625" y2="0.006559375" layer="51"/>
<rectangle x1="6.42755625" y1="-0.006540625" x2="6.53235625" y2="0.006559375" layer="51"/>
<rectangle x1="7.37105625" y1="-0.006540625" x2="7.47585625" y2="0.006559375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.006559375" x2="-7.39724375" y2="0.019665625" layer="51"/>
<rectangle x1="-6.55854375" y1="0.006559375" x2="-6.45374375" y2="0.019665625" layer="51"/>
<rectangle x1="-3.99014375" y1="0.006559375" x2="-3.88534375" y2="0.019665625" layer="51"/>
<rectangle x1="-3.09904375" y1="0.006559375" x2="-2.99424375" y2="0.019665625" layer="51"/>
<rectangle x1="-0.51764375" y1="0.006559375" x2="-0.41274375" y2="0.019665625" layer="51"/>
<rectangle x1="0.38655625" y1="0.006559375" x2="0.49145625" y2="0.019665625" layer="51"/>
<rectangle x1="2.96805625" y1="0.006559375" x2="3.07285625" y2="0.019665625" layer="51"/>
<rectangle x1="3.85915625" y1="0.006559375" x2="3.96395625" y2="0.019665625" layer="51"/>
<rectangle x1="6.42755625" y1="0.006559375" x2="6.53235625" y2="0.019665625" layer="51"/>
<rectangle x1="7.37105625" y1="0.006559375" x2="7.47585625" y2="0.019665625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.01966875" x2="-7.39724375" y2="0.03276875" layer="51"/>
<rectangle x1="-6.55854375" y1="0.01966875" x2="-6.45374375" y2="0.03276875" layer="51"/>
<rectangle x1="-3.99014375" y1="0.01966875" x2="-3.88534375" y2="0.03276875" layer="51"/>
<rectangle x1="-3.09904375" y1="0.01966875" x2="-2.99424375" y2="0.03276875" layer="51"/>
<rectangle x1="-0.51764375" y1="0.01966875" x2="-0.41274375" y2="0.03276875" layer="51"/>
<rectangle x1="0.38655625" y1="0.01966875" x2="0.49145625" y2="0.03276875" layer="51"/>
<rectangle x1="2.96805625" y1="0.01966875" x2="3.07285625" y2="0.03276875" layer="51"/>
<rectangle x1="3.85915625" y1="0.01966875" x2="3.96395625" y2="0.03276875" layer="51"/>
<rectangle x1="6.42755625" y1="0.01966875" x2="6.53235625" y2="0.03276875" layer="51"/>
<rectangle x1="7.37105625" y1="0.01966875" x2="7.47585625" y2="0.03276875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.03276875" x2="-7.39724375" y2="0.04586875" layer="51"/>
<rectangle x1="-6.55854375" y1="0.03276875" x2="-6.45374375" y2="0.04586875" layer="51"/>
<rectangle x1="-3.99014375" y1="0.03276875" x2="-3.88534375" y2="0.04586875" layer="51"/>
<rectangle x1="-3.09904375" y1="0.03276875" x2="-2.99424375" y2="0.04586875" layer="51"/>
<rectangle x1="-0.51764375" y1="0.03276875" x2="-0.41274375" y2="0.04586875" layer="51"/>
<rectangle x1="0.38655625" y1="0.03276875" x2="0.49145625" y2="0.04586875" layer="51"/>
<rectangle x1="2.96805625" y1="0.03276875" x2="3.07285625" y2="0.04586875" layer="51"/>
<rectangle x1="3.85915625" y1="0.03276875" x2="3.96395625" y2="0.04586875" layer="51"/>
<rectangle x1="6.42755625" y1="0.03276875" x2="6.53235625" y2="0.04586875" layer="51"/>
<rectangle x1="7.37105625" y1="0.03276875" x2="7.47585625" y2="0.04586875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.04586875" x2="-7.39724375" y2="0.05898125" layer="51"/>
<rectangle x1="-6.55854375" y1="0.04586875" x2="-6.45374375" y2="0.05898125" layer="51"/>
<rectangle x1="-3.99014375" y1="0.04586875" x2="-3.88534375" y2="0.05898125" layer="51"/>
<rectangle x1="-3.09904375" y1="0.04586875" x2="-2.99424375" y2="0.05898125" layer="51"/>
<rectangle x1="-0.51764375" y1="0.04586875" x2="-0.41274375" y2="0.05898125" layer="51"/>
<rectangle x1="0.38655625" y1="0.04586875" x2="0.49145625" y2="0.05898125" layer="51"/>
<rectangle x1="2.96805625" y1="0.04586875" x2="3.07285625" y2="0.05898125" layer="51"/>
<rectangle x1="3.85915625" y1="0.04586875" x2="3.96395625" y2="0.05898125" layer="51"/>
<rectangle x1="6.42755625" y1="0.04586875" x2="6.53235625" y2="0.05898125" layer="51"/>
<rectangle x1="7.37105625" y1="0.04586875" x2="7.47585625" y2="0.05898125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.05898125" x2="-7.39724375" y2="0.07208125" layer="51"/>
<rectangle x1="-6.55854375" y1="0.05898125" x2="-6.45374375" y2="0.07208125" layer="51"/>
<rectangle x1="-3.99014375" y1="0.05898125" x2="-3.88534375" y2="0.07208125" layer="51"/>
<rectangle x1="-3.09904375" y1="0.05898125" x2="-2.99424375" y2="0.07208125" layer="51"/>
<rectangle x1="-0.51764375" y1="0.05898125" x2="-0.41274375" y2="0.07208125" layer="51"/>
<rectangle x1="0.38655625" y1="0.05898125" x2="0.49145625" y2="0.07208125" layer="51"/>
<rectangle x1="2.96805625" y1="0.05898125" x2="3.07285625" y2="0.07208125" layer="51"/>
<rectangle x1="3.85915625" y1="0.05898125" x2="3.96395625" y2="0.07208125" layer="51"/>
<rectangle x1="6.42755625" y1="0.05898125" x2="6.53235625" y2="0.07208125" layer="51"/>
<rectangle x1="7.37105625" y1="0.05898125" x2="7.47585625" y2="0.07208125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.07208125" x2="-7.39724375" y2="0.0851875" layer="51"/>
<rectangle x1="-6.55854375" y1="0.07208125" x2="-6.45374375" y2="0.0851875" layer="51"/>
<rectangle x1="-3.99014375" y1="0.07208125" x2="-3.88534375" y2="0.0851875" layer="51"/>
<rectangle x1="-3.09904375" y1="0.07208125" x2="-2.99424375" y2="0.0851875" layer="51"/>
<rectangle x1="-0.51764375" y1="0.07208125" x2="-0.41274375" y2="0.0851875" layer="51"/>
<rectangle x1="0.38655625" y1="0.07208125" x2="0.49145625" y2="0.0851875" layer="51"/>
<rectangle x1="2.96805625" y1="0.07208125" x2="3.07285625" y2="0.0851875" layer="51"/>
<rectangle x1="3.85915625" y1="0.07208125" x2="3.96395625" y2="0.0851875" layer="51"/>
<rectangle x1="6.42755625" y1="0.07208125" x2="6.53235625" y2="0.0851875" layer="51"/>
<rectangle x1="7.37105625" y1="0.07208125" x2="7.47585625" y2="0.0851875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.085190625" x2="-7.39724375" y2="0.098290625" layer="51"/>
<rectangle x1="-6.55854375" y1="0.085190625" x2="-6.08684375" y2="0.098290625" layer="51"/>
<rectangle x1="-4.35704375" y1="0.085190625" x2="-3.88534375" y2="0.098290625" layer="51"/>
<rectangle x1="-3.09904375" y1="0.085190625" x2="-2.61424375" y2="0.098290625" layer="51"/>
<rectangle x1="-0.89764375" y1="0.085190625" x2="-0.41274375" y2="0.098290625" layer="51"/>
<rectangle x1="0.38655625" y1="0.085190625" x2="0.87145625" y2="0.098290625" layer="51"/>
<rectangle x1="2.58805625" y1="0.085190625" x2="3.07285625" y2="0.098290625" layer="51"/>
<rectangle x1="3.85915625" y1="0.085190625" x2="4.33085625" y2="0.098290625" layer="51"/>
<rectangle x1="6.06065625" y1="0.085190625" x2="6.53235625" y2="0.098290625" layer="51"/>
<rectangle x1="7.37105625" y1="0.085190625" x2="7.47585625" y2="0.098290625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.098290625" x2="-7.39724375" y2="0.111390625" layer="51"/>
<rectangle x1="-6.55854375" y1="0.098290625" x2="-6.04754375" y2="0.111390625" layer="51"/>
<rectangle x1="-4.39634375" y1="0.098290625" x2="-3.88534375" y2="0.111390625" layer="51"/>
<rectangle x1="-3.09904375" y1="0.098290625" x2="-2.58804375" y2="0.111390625" layer="51"/>
<rectangle x1="-0.92384375" y1="0.098290625" x2="-0.41274375" y2="0.111390625" layer="51"/>
<rectangle x1="0.38655625" y1="0.098290625" x2="0.89765625" y2="0.111390625" layer="51"/>
<rectangle x1="2.56185625" y1="0.098290625" x2="3.07285625" y2="0.111390625" layer="51"/>
<rectangle x1="3.85915625" y1="0.098290625" x2="4.37015625" y2="0.111390625" layer="51"/>
<rectangle x1="6.03445625" y1="0.098290625" x2="6.53235625" y2="0.111390625" layer="51"/>
<rectangle x1="7.37105625" y1="0.098290625" x2="7.47585625" y2="0.111390625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.111390625" x2="-7.39724375" y2="0.124496875" layer="51"/>
<rectangle x1="-6.57164375" y1="0.111390625" x2="-6.03434375" y2="0.124496875" layer="51"/>
<rectangle x1="-4.40944375" y1="0.111390625" x2="-3.87224375" y2="0.124496875" layer="51"/>
<rectangle x1="-3.09904375" y1="0.111390625" x2="-2.56184375" y2="0.124496875" layer="51"/>
<rectangle x1="-0.95004375" y1="0.111390625" x2="-0.41274375" y2="0.124496875" layer="51"/>
<rectangle x1="0.38655625" y1="0.111390625" x2="0.92385625" y2="0.124496875" layer="51"/>
<rectangle x1="2.53565625" y1="0.111390625" x2="3.07285625" y2="0.124496875" layer="51"/>
<rectangle x1="3.84605625" y1="0.111390625" x2="4.38325625" y2="0.124496875" layer="51"/>
<rectangle x1="6.00815625" y1="0.111390625" x2="6.54545625" y2="0.124496875" layer="51"/>
<rectangle x1="7.37105625" y1="0.111390625" x2="7.47585625" y2="0.124496875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.1245" x2="-7.39724375" y2="0.1376" layer="51"/>
<rectangle x1="-6.58474375" y1="0.1245" x2="-6.00814375" y2="0.1376" layer="51"/>
<rectangle x1="-4.43574375" y1="0.1245" x2="-3.85914375" y2="0.1376" layer="51"/>
<rectangle x1="-3.11224375" y1="0.1245" x2="-2.54874375" y2="0.1376" layer="51"/>
<rectangle x1="-0.96314375" y1="0.1245" x2="-0.39964375" y2="0.1376" layer="51"/>
<rectangle x1="0.37345625" y1="0.1245" x2="0.93695625" y2="0.1376" layer="51"/>
<rectangle x1="2.52255625" y1="0.1245" x2="3.08595625" y2="0.1376" layer="51"/>
<rectangle x1="3.83295625" y1="0.1245" x2="4.40955625" y2="0.1376" layer="51"/>
<rectangle x1="5.98195625" y1="0.1245" x2="6.55855625" y2="0.1376" layer="51"/>
<rectangle x1="7.37105625" y1="0.1245" x2="7.47585625" y2="0.1376" layer="51"/>
<rectangle x1="-7.50204375" y1="0.1376" x2="-7.39724375" y2="0.15070625" layer="51"/>
<rectangle x1="-6.59784375" y1="0.1376" x2="-5.99504375" y2="0.15070625" layer="51"/>
<rectangle x1="-4.44884375" y1="0.1376" x2="-3.84604375" y2="0.15070625" layer="51"/>
<rectangle x1="-3.12534375" y1="0.1376" x2="-2.52254375" y2="0.15070625" layer="51"/>
<rectangle x1="-0.97624375" y1="0.1376" x2="-0.38654375" y2="0.15070625" layer="51"/>
<rectangle x1="0.36035625" y1="0.1376" x2="0.96315625" y2="0.15070625" layer="51"/>
<rectangle x1="2.50945625" y1="0.1376" x2="3.09915625" y2="0.15070625" layer="51"/>
<rectangle x1="3.81985625" y1="0.1376" x2="4.42265625" y2="0.15070625" layer="51"/>
<rectangle x1="5.96885625" y1="0.1376" x2="6.57165625" y2="0.15070625" layer="51"/>
<rectangle x1="7.37105625" y1="0.1376" x2="7.47585625" y2="0.15070625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.150709375" x2="-7.39724375" y2="0.163809375" layer="51"/>
<rectangle x1="-6.61094375" y1="0.150709375" x2="-5.98194375" y2="0.163809375" layer="51"/>
<rectangle x1="-4.46194375" y1="0.150709375" x2="-3.83294375" y2="0.163809375" layer="51"/>
<rectangle x1="-3.13844375" y1="0.150709375" x2="-2.50944375" y2="0.163809375" layer="51"/>
<rectangle x1="-0.98934375" y1="0.150709375" x2="-0.37344375" y2="0.163809375" layer="51"/>
<rectangle x1="0.34725625" y1="0.150709375" x2="0.97625625" y2="0.163809375" layer="51"/>
<rectangle x1="2.49635625" y1="0.150709375" x2="3.11225625" y2="0.163809375" layer="51"/>
<rectangle x1="3.80675625" y1="0.150709375" x2="4.43575625" y2="0.163809375" layer="51"/>
<rectangle x1="5.95575625" y1="0.150709375" x2="6.58475625" y2="0.163809375" layer="51"/>
<rectangle x1="7.37105625" y1="0.150709375" x2="7.47585625" y2="0.163809375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.163809375" x2="-7.39724375" y2="0.176909375" layer="51"/>
<rectangle x1="-6.62404375" y1="0.163809375" x2="-5.96884375" y2="0.176909375" layer="51"/>
<rectangle x1="-4.47504375" y1="0.163809375" x2="-3.81984375" y2="0.176909375" layer="51"/>
<rectangle x1="-3.15154375" y1="0.163809375" x2="-2.49634375" y2="0.176909375" layer="51"/>
<rectangle x1="-1.00244375" y1="0.163809375" x2="-0.36034375" y2="0.176909375" layer="51"/>
<rectangle x1="0.33415625" y1="0.163809375" x2="0.98935625" y2="0.176909375" layer="51"/>
<rectangle x1="2.48325625" y1="0.163809375" x2="3.12535625" y2="0.176909375" layer="51"/>
<rectangle x1="3.79365625" y1="0.163809375" x2="4.44885625" y2="0.176909375" layer="51"/>
<rectangle x1="5.94265625" y1="0.163809375" x2="6.59785625" y2="0.176909375" layer="51"/>
<rectangle x1="7.37105625" y1="0.163809375" x2="7.47585625" y2="0.176909375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.176909375" x2="-7.39724375" y2="0.190015625" layer="51"/>
<rectangle x1="-6.63714375" y1="0.176909375" x2="-5.95574375" y2="0.190015625" layer="51"/>
<rectangle x1="-4.48814375" y1="0.176909375" x2="-3.80674375" y2="0.190015625" layer="51"/>
<rectangle x1="-3.16464375" y1="0.176909375" x2="-2.49634375" y2="0.190015625" layer="51"/>
<rectangle x1="-1.01554375" y1="0.176909375" x2="-0.34724375" y2="0.190015625" layer="51"/>
<rectangle x1="0.32105625" y1="0.176909375" x2="0.98935625" y2="0.190015625" layer="51"/>
<rectangle x1="2.47015625" y1="0.176909375" x2="3.13845625" y2="0.190015625" layer="51"/>
<rectangle x1="3.78055625" y1="0.176909375" x2="4.46195625" y2="0.190015625" layer="51"/>
<rectangle x1="5.92955625" y1="0.176909375" x2="6.61095625" y2="0.190015625" layer="51"/>
<rectangle x1="7.37105625" y1="0.176909375" x2="7.47585625" y2="0.190015625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.19001875" x2="-7.39724375" y2="0.20311875" layer="51"/>
<rectangle x1="-6.65024375" y1="0.19001875" x2="-6.49304375" y2="0.20311875" layer="51"/>
<rectangle x1="-6.11304375" y1="0.19001875" x2="-5.95574375" y2="0.20311875" layer="51"/>
<rectangle x1="-4.48814375" y1="0.19001875" x2="-4.33084375" y2="0.20311875" layer="51"/>
<rectangle x1="-3.95084375" y1="0.19001875" x2="-3.79364375" y2="0.20311875" layer="51"/>
<rectangle x1="-3.17774375" y1="0.19001875" x2="-3.03354375" y2="0.20311875" layer="51"/>
<rectangle x1="-2.64044375" y1="0.19001875" x2="-2.48324375" y2="0.20311875" layer="51"/>
<rectangle x1="-1.02864375" y1="0.19001875" x2="-0.87144375" y2="0.20311875" layer="51"/>
<rectangle x1="-0.47824375" y1="0.19001875" x2="-0.33414375" y2="0.20311875" layer="51"/>
<rectangle x1="0.30795625" y1="0.19001875" x2="0.45205625" y2="0.20311875" layer="51"/>
<rectangle x1="0.84525625" y1="0.19001875" x2="1.00245625" y2="0.20311875" layer="51"/>
<rectangle x1="2.45705625" y1="0.19001875" x2="2.61425625" y2="0.20311875" layer="51"/>
<rectangle x1="3.00735625" y1="0.19001875" x2="3.15155625" y2="0.20311875" layer="51"/>
<rectangle x1="3.76745625" y1="0.19001875" x2="3.92465625" y2="0.20311875" layer="51"/>
<rectangle x1="4.30465625" y1="0.19001875" x2="4.46195625" y2="0.20311875" layer="51"/>
<rectangle x1="5.92955625" y1="0.19001875" x2="6.08685625" y2="0.20311875" layer="51"/>
<rectangle x1="6.46685625" y1="0.19001875" x2="6.62405625" y2="0.20311875" layer="51"/>
<rectangle x1="7.37105625" y1="0.19001875" x2="7.47585625" y2="0.20311875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.20311875" x2="-7.39724375" y2="0.21623125" layer="51"/>
<rectangle x1="-6.66334375" y1="0.20311875" x2="-6.50614375" y2="0.21623125" layer="51"/>
<rectangle x1="-6.08684375" y1="0.20311875" x2="-5.94264375" y2="0.21623125" layer="51"/>
<rectangle x1="-4.50124375" y1="0.20311875" x2="-4.35704375" y2="0.21623125" layer="51"/>
<rectangle x1="-3.93774375" y1="0.20311875" x2="-3.78054375" y2="0.21623125" layer="51"/>
<rectangle x1="-3.19084375" y1="0.20311875" x2="-3.04664375" y2="0.21623125" layer="51"/>
<rectangle x1="-2.61424375" y1="0.20311875" x2="-2.47014375" y2="0.21623125" layer="51"/>
<rectangle x1="-1.02864375" y1="0.20311875" x2="-0.89764375" y2="0.21623125" layer="51"/>
<rectangle x1="-0.46514375" y1="0.20311875" x2="-0.32104375" y2="0.21623125" layer="51"/>
<rectangle x1="0.29485625" y1="0.20311875" x2="0.43895625" y2="0.21623125" layer="51"/>
<rectangle x1="0.87145625" y1="0.20311875" x2="1.01555625" y2="0.21623125" layer="51"/>
<rectangle x1="2.45705625" y1="0.20311875" x2="2.58805625" y2="0.21623125" layer="51"/>
<rectangle x1="3.02045625" y1="0.20311875" x2="3.16465625" y2="0.21623125" layer="51"/>
<rectangle x1="3.75435625" y1="0.20311875" x2="3.91155625" y2="0.21623125" layer="51"/>
<rectangle x1="4.33085625" y1="0.20311875" x2="4.47505625" y2="0.21623125" layer="51"/>
<rectangle x1="5.91645625" y1="0.20311875" x2="6.06065625" y2="0.21623125" layer="51"/>
<rectangle x1="6.47995625" y1="0.20311875" x2="6.63715625" y2="0.21623125" layer="51"/>
<rectangle x1="7.37105625" y1="0.20311875" x2="7.47585625" y2="0.21623125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.21623125" x2="-7.39724375" y2="0.22933125" layer="51"/>
<rectangle x1="-6.67644375" y1="0.21623125" x2="-6.51924375" y2="0.22933125" layer="51"/>
<rectangle x1="-6.06064375" y1="0.21623125" x2="-5.94264375" y2="0.22933125" layer="51"/>
<rectangle x1="-4.50124375" y1="0.21623125" x2="-4.38324375" y2="0.22933125" layer="51"/>
<rectangle x1="-3.92464375" y1="0.21623125" x2="-3.76744375" y2="0.22933125" layer="51"/>
<rectangle x1="-3.20394375" y1="0.21623125" x2="-3.05974375" y2="0.22933125" layer="51"/>
<rectangle x1="-2.60114375" y1="0.21623125" x2="-2.47014375" y2="0.22933125" layer="51"/>
<rectangle x1="-1.04174375" y1="0.21623125" x2="-0.91074375" y2="0.22933125" layer="51"/>
<rectangle x1="-0.45204375" y1="0.21623125" x2="-0.30794375" y2="0.22933125" layer="51"/>
<rectangle x1="0.28175625" y1="0.21623125" x2="0.42585625" y2="0.22933125" layer="51"/>
<rectangle x1="0.88455625" y1="0.21623125" x2="1.01555625" y2="0.22933125" layer="51"/>
<rectangle x1="2.44395625" y1="0.21623125" x2="2.57495625" y2="0.22933125" layer="51"/>
<rectangle x1="3.03355625" y1="0.21623125" x2="3.17775625" y2="0.22933125" layer="51"/>
<rectangle x1="3.74115625" y1="0.21623125" x2="3.89845625" y2="0.22933125" layer="51"/>
<rectangle x1="4.35705625" y1="0.21623125" x2="4.48815625" y2="0.22933125" layer="51"/>
<rectangle x1="5.91645625" y1="0.21623125" x2="6.03445625" y2="0.22933125" layer="51"/>
<rectangle x1="6.49305625" y1="0.21623125" x2="6.65025625" y2="0.22933125" layer="51"/>
<rectangle x1="7.37105625" y1="0.21623125" x2="7.47585625" y2="0.22933125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.22933125" x2="-7.39724375" y2="0.24243125" layer="51"/>
<rectangle x1="-6.68954375" y1="0.22933125" x2="-6.53234375" y2="0.24243125" layer="51"/>
<rectangle x1="-6.04754375" y1="0.22933125" x2="-5.92954375" y2="0.24243125" layer="51"/>
<rectangle x1="-4.51434375" y1="0.22933125" x2="-4.39634375" y2="0.24243125" layer="51"/>
<rectangle x1="-3.91154375" y1="0.22933125" x2="-3.75424375" y2="0.24243125" layer="51"/>
<rectangle x1="-3.21704375" y1="0.22933125" x2="-3.07284375" y2="0.24243125" layer="51"/>
<rectangle x1="-2.58804375" y1="0.22933125" x2="-2.47014375" y2="0.24243125" layer="51"/>
<rectangle x1="-1.04174375" y1="0.22933125" x2="-0.92384375" y2="0.24243125" layer="51"/>
<rectangle x1="-0.43894375" y1="0.22933125" x2="-0.29484375" y2="0.24243125" layer="51"/>
<rectangle x1="0.26865625" y1="0.22933125" x2="0.41275625" y2="0.24243125" layer="51"/>
<rectangle x1="0.89765625" y1="0.22933125" x2="1.01555625" y2="0.24243125" layer="51"/>
<rectangle x1="2.44395625" y1="0.22933125" x2="2.56185625" y2="0.24243125" layer="51"/>
<rectangle x1="3.04665625" y1="0.22933125" x2="3.19085625" y2="0.24243125" layer="51"/>
<rectangle x1="3.72805625" y1="0.22933125" x2="3.88535625" y2="0.24243125" layer="51"/>
<rectangle x1="4.37015625" y1="0.22933125" x2="4.48815625" y2="0.24243125" layer="51"/>
<rectangle x1="5.90335625" y1="0.22933125" x2="6.02125625" y2="0.24243125" layer="51"/>
<rectangle x1="6.50615625" y1="0.22933125" x2="6.66335625" y2="0.24243125" layer="51"/>
<rectangle x1="7.37105625" y1="0.22933125" x2="7.47585625" y2="0.24243125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.24243125" x2="-7.39724375" y2="0.2555375" layer="51"/>
<rectangle x1="-6.70274375" y1="0.24243125" x2="-6.54544375" y2="0.2555375" layer="51"/>
<rectangle x1="-6.04754375" y1="0.24243125" x2="-5.92954375" y2="0.2555375" layer="51"/>
<rectangle x1="-4.51434375" y1="0.24243125" x2="-4.39634375" y2="0.2555375" layer="51"/>
<rectangle x1="-3.89844375" y1="0.24243125" x2="-3.74114375" y2="0.2555375" layer="51"/>
<rectangle x1="-3.23014375" y1="0.24243125" x2="-3.08594375" y2="0.2555375" layer="51"/>
<rectangle x1="-2.57494375" y1="0.24243125" x2="-2.45704375" y2="0.2555375" layer="51"/>
<rectangle x1="-1.05484375" y1="0.24243125" x2="-0.93694375" y2="0.2555375" layer="51"/>
<rectangle x1="-0.42584375" y1="0.24243125" x2="-0.28174375" y2="0.2555375" layer="51"/>
<rectangle x1="0.25555625" y1="0.24243125" x2="0.39965625" y2="0.2555375" layer="51"/>
<rectangle x1="0.91075625" y1="0.24243125" x2="1.02865625" y2="0.2555375" layer="51"/>
<rectangle x1="2.43075625" y1="0.24243125" x2="2.54875625" y2="0.2555375" layer="51"/>
<rectangle x1="3.05975625" y1="0.24243125" x2="3.20395625" y2="0.2555375" layer="51"/>
<rectangle x1="3.71495625" y1="0.24243125" x2="3.87225625" y2="0.2555375" layer="51"/>
<rectangle x1="4.37015625" y1="0.24243125" x2="4.48815625" y2="0.2555375" layer="51"/>
<rectangle x1="5.90335625" y1="0.24243125" x2="6.02125625" y2="0.2555375" layer="51"/>
<rectangle x1="6.51925625" y1="0.24243125" x2="6.67645625" y2="0.2555375" layer="51"/>
<rectangle x1="7.37105625" y1="0.24243125" x2="7.47585625" y2="0.2555375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.255540625" x2="-7.39724375" y2="0.268640625" layer="51"/>
<rectangle x1="-6.71584375" y1="0.255540625" x2="-6.55854375" y2="0.268640625" layer="51"/>
<rectangle x1="-6.03434375" y1="0.255540625" x2="-5.91644375" y2="0.268640625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.255540625" x2="-4.40944375" y2="0.268640625" layer="51"/>
<rectangle x1="-3.88534375" y1="0.255540625" x2="-3.72804375" y2="0.268640625" layer="51"/>
<rectangle x1="-3.24324375" y1="0.255540625" x2="-3.09904375" y2="0.268640625" layer="51"/>
<rectangle x1="-2.56184375" y1="0.255540625" x2="-2.45704375" y2="0.268640625" layer="51"/>
<rectangle x1="-1.05484375" y1="0.255540625" x2="-0.93694375" y2="0.268640625" layer="51"/>
<rectangle x1="-0.41274375" y1="0.255540625" x2="-0.26864375" y2="0.268640625" layer="51"/>
<rectangle x1="0.24245625" y1="0.255540625" x2="0.38655625" y2="0.268640625" layer="51"/>
<rectangle x1="0.92385625" y1="0.255540625" x2="1.02865625" y2="0.268640625" layer="51"/>
<rectangle x1="2.43075625" y1="0.255540625" x2="2.54875625" y2="0.268640625" layer="51"/>
<rectangle x1="3.07285625" y1="0.255540625" x2="3.21705625" y2="0.268640625" layer="51"/>
<rectangle x1="3.70185625" y1="0.255540625" x2="3.85915625" y2="0.268640625" layer="51"/>
<rectangle x1="4.38325625" y1="0.255540625" x2="4.50125625" y2="0.268640625" layer="51"/>
<rectangle x1="5.89025625" y1="0.255540625" x2="6.00815625" y2="0.268640625" layer="51"/>
<rectangle x1="6.53235625" y1="0.255540625" x2="6.68965625" y2="0.268640625" layer="51"/>
<rectangle x1="7.37105625" y1="0.255540625" x2="7.47585625" y2="0.268640625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.268640625" x2="-7.39724375" y2="0.281746875" layer="51"/>
<rectangle x1="-6.72894375" y1="0.268640625" x2="-6.57164375" y2="0.281746875" layer="51"/>
<rectangle x1="-6.03434375" y1="0.268640625" x2="-5.91644375" y2="0.281746875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.268640625" x2="-4.40944375" y2="0.281746875" layer="51"/>
<rectangle x1="-3.87224375" y1="0.268640625" x2="-3.71494375" y2="0.281746875" layer="51"/>
<rectangle x1="-3.25634375" y1="0.268640625" x2="-3.11224375" y2="0.281746875" layer="51"/>
<rectangle x1="-2.56184375" y1="0.268640625" x2="-2.45704375" y2="0.281746875" layer="51"/>
<rectangle x1="-1.05484375" y1="0.268640625" x2="-0.95004375" y2="0.281746875" layer="51"/>
<rectangle x1="-0.39964375" y1="0.268640625" x2="-0.25554375" y2="0.281746875" layer="51"/>
<rectangle x1="0.22935625" y1="0.268640625" x2="0.37345625" y2="0.281746875" layer="51"/>
<rectangle x1="0.92385625" y1="0.268640625" x2="1.02865625" y2="0.281746875" layer="51"/>
<rectangle x1="2.43075625" y1="0.268640625" x2="2.53565625" y2="0.281746875" layer="51"/>
<rectangle x1="3.08595625" y1="0.268640625" x2="3.23015625" y2="0.281746875" layer="51"/>
<rectangle x1="3.68875625" y1="0.268640625" x2="3.84605625" y2="0.281746875" layer="51"/>
<rectangle x1="4.38325625" y1="0.268640625" x2="4.50125625" y2="0.281746875" layer="51"/>
<rectangle x1="5.89025625" y1="0.268640625" x2="6.00815625" y2="0.281746875" layer="51"/>
<rectangle x1="6.54545625" y1="0.268640625" x2="6.70275625" y2="0.281746875" layer="51"/>
<rectangle x1="7.37105625" y1="0.268640625" x2="7.47585625" y2="0.281746875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.28175" x2="-7.39724375" y2="0.29485" layer="51"/>
<rectangle x1="-6.74204375" y1="0.28175" x2="-6.58474375" y2="0.29485" layer="51"/>
<rectangle x1="-6.02124375" y1="0.28175" x2="-5.91644375" y2="0.29485" layer="51"/>
<rectangle x1="-4.52744375" y1="0.28175" x2="-4.42264375" y2="0.29485" layer="51"/>
<rectangle x1="-3.85914375" y1="0.28175" x2="-3.70184375" y2="0.29485" layer="51"/>
<rectangle x1="-3.26944375" y1="0.28175" x2="-3.12534375" y2="0.29485" layer="51"/>
<rectangle x1="-2.56184375" y1="0.28175" x2="-2.44384375" y2="0.29485" layer="51"/>
<rectangle x1="-1.05484375" y1="0.28175" x2="-0.95004375" y2="0.29485" layer="51"/>
<rectangle x1="-0.38654375" y1="0.28175" x2="-0.24244375" y2="0.29485" layer="51"/>
<rectangle x1="0.21625625" y1="0.28175" x2="0.36035625" y2="0.29485" layer="51"/>
<rectangle x1="0.92385625" y1="0.28175" x2="1.04175625" y2="0.29485" layer="51"/>
<rectangle x1="2.43075625" y1="0.28175" x2="2.53565625" y2="0.29485" layer="51"/>
<rectangle x1="3.09915625" y1="0.28175" x2="3.24325625" y2="0.29485" layer="51"/>
<rectangle x1="3.67565625" y1="0.28175" x2="3.83295625" y2="0.29485" layer="51"/>
<rectangle x1="4.39635625" y1="0.28175" x2="4.50125625" y2="0.29485" layer="51"/>
<rectangle x1="5.89025625" y1="0.28175" x2="5.99505625" y2="0.29485" layer="51"/>
<rectangle x1="6.55855625" y1="0.28175" x2="6.71585625" y2="0.29485" layer="51"/>
<rectangle x1="7.37105625" y1="0.28175" x2="7.47585625" y2="0.29485" layer="51"/>
<rectangle x1="-7.50204375" y1="0.29485" x2="-7.39724375" y2="0.30795" layer="51"/>
<rectangle x1="-6.74204375" y1="0.29485" x2="-6.59784375" y2="0.30795" layer="51"/>
<rectangle x1="-6.02124375" y1="0.29485" x2="-5.91644375" y2="0.30795" layer="51"/>
<rectangle x1="-4.52744375" y1="0.29485" x2="-4.42264375" y2="0.30795" layer="51"/>
<rectangle x1="-3.84604375" y1="0.29485" x2="-3.70184375" y2="0.30795" layer="51"/>
<rectangle x1="-3.28254375" y1="0.29485" x2="-3.13844375" y2="0.30795" layer="51"/>
<rectangle x1="-2.54874375" y1="0.29485" x2="-2.44384375" y2="0.30795" layer="51"/>
<rectangle x1="-1.06794375" y1="0.29485" x2="-0.95004375" y2="0.30795" layer="51"/>
<rectangle x1="-0.37344375" y1="0.29485" x2="-0.22934375" y2="0.30795" layer="51"/>
<rectangle x1="0.20315625" y1="0.29485" x2="0.34725625" y2="0.30795" layer="51"/>
<rectangle x1="0.93695625" y1="0.29485" x2="1.04175625" y2="0.30795" layer="51"/>
<rectangle x1="2.41765625" y1="0.29485" x2="2.53565625" y2="0.30795" layer="51"/>
<rectangle x1="3.11225625" y1="0.29485" x2="3.25635625" y2="0.30795" layer="51"/>
<rectangle x1="3.67565625" y1="0.29485" x2="3.81985625" y2="0.30795" layer="51"/>
<rectangle x1="4.39635625" y1="0.29485" x2="4.50125625" y2="0.30795" layer="51"/>
<rectangle x1="5.89025625" y1="0.29485" x2="5.99505625" y2="0.30795" layer="51"/>
<rectangle x1="6.57165625" y1="0.29485" x2="6.71585625" y2="0.30795" layer="51"/>
<rectangle x1="7.37105625" y1="0.29485" x2="7.47585625" y2="0.30795" layer="51"/>
<rectangle x1="-7.50204375" y1="0.30795" x2="-7.39724375" y2="0.32105625" layer="51"/>
<rectangle x1="-6.72894375" y1="0.30795" x2="-6.61094375" y2="0.32105625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.30795" x2="-5.91644375" y2="0.32105625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.30795" x2="-4.42264375" y2="0.32105625" layer="51"/>
<rectangle x1="-3.83294375" y1="0.30795" x2="-3.71494375" y2="0.32105625" layer="51"/>
<rectangle x1="-3.26944375" y1="0.30795" x2="-3.15154375" y2="0.32105625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.30795" x2="-2.44384375" y2="0.32105625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.30795" x2="-0.96314375" y2="0.32105625" layer="51"/>
<rectangle x1="-0.36034375" y1="0.30795" x2="-0.24244375" y2="0.32105625" layer="51"/>
<rectangle x1="0.21625625" y1="0.30795" x2="0.33415625" y2="0.32105625" layer="51"/>
<rectangle x1="0.93695625" y1="0.30795" x2="1.04175625" y2="0.32105625" layer="51"/>
<rectangle x1="2.41765625" y1="0.30795" x2="2.52255625" y2="0.32105625" layer="51"/>
<rectangle x1="3.12535625" y1="0.30795" x2="3.24325625" y2="0.32105625" layer="51"/>
<rectangle x1="3.68875625" y1="0.30795" x2="3.80675625" y2="0.32105625" layer="51"/>
<rectangle x1="4.39635625" y1="0.30795" x2="4.50125625" y2="0.32105625" layer="51"/>
<rectangle x1="5.89025625" y1="0.30795" x2="5.99505625" y2="0.32105625" layer="51"/>
<rectangle x1="6.58475625" y1="0.30795" x2="6.70275625" y2="0.32105625" layer="51"/>
<rectangle x1="7.37105625" y1="0.30795" x2="7.47585625" y2="0.32105625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.321059375" x2="-7.39724375" y2="0.334159375" layer="51"/>
<rectangle x1="-6.71584375" y1="0.321059375" x2="-6.62404375" y2="0.334159375" layer="51"/>
<rectangle x1="-6.02124375" y1="0.321059375" x2="-5.91644375" y2="0.334159375" layer="51"/>
<rectangle x1="-4.52744375" y1="0.321059375" x2="-4.42264375" y2="0.334159375" layer="51"/>
<rectangle x1="-3.81984375" y1="0.321059375" x2="-3.72804375" y2="0.334159375" layer="51"/>
<rectangle x1="-3.25634375" y1="0.321059375" x2="-3.16464375" y2="0.334159375" layer="51"/>
<rectangle x1="-2.54874375" y1="0.321059375" x2="-2.44384375" y2="0.334159375" layer="51"/>
<rectangle x1="-1.06794375" y1="0.321059375" x2="-0.96314375" y2="0.334159375" layer="51"/>
<rectangle x1="-0.34724375" y1="0.321059375" x2="-0.25554375" y2="0.334159375" layer="51"/>
<rectangle x1="0.22935625" y1="0.321059375" x2="0.32105625" y2="0.334159375" layer="51"/>
<rectangle x1="0.93695625" y1="0.321059375" x2="1.04175625" y2="0.334159375" layer="51"/>
<rectangle x1="2.41765625" y1="0.321059375" x2="2.52255625" y2="0.334159375" layer="51"/>
<rectangle x1="3.13845625" y1="0.321059375" x2="3.23015625" y2="0.334159375" layer="51"/>
<rectangle x1="3.70185625" y1="0.321059375" x2="3.79365625" y2="0.334159375" layer="51"/>
<rectangle x1="4.39635625" y1="0.321059375" x2="4.50125625" y2="0.334159375" layer="51"/>
<rectangle x1="5.89025625" y1="0.321059375" x2="5.99505625" y2="0.334159375" layer="51"/>
<rectangle x1="6.59785625" y1="0.321059375" x2="6.68965625" y2="0.334159375" layer="51"/>
<rectangle x1="7.37105625" y1="0.321059375" x2="7.47585625" y2="0.334159375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.334159375" x2="-7.39724375" y2="0.347265625" layer="51"/>
<rectangle x1="-6.70274375" y1="0.334159375" x2="-6.63714375" y2="0.347265625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.334159375" x2="-5.91644375" y2="0.347265625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.334159375" x2="-4.42264375" y2="0.347265625" layer="51"/>
<rectangle x1="-3.80674375" y1="0.334159375" x2="-3.74114375" y2="0.347265625" layer="51"/>
<rectangle x1="-3.24324375" y1="0.334159375" x2="-3.17774375" y2="0.347265625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.334159375" x2="-2.44384375" y2="0.347265625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.334159375" x2="-0.96314375" y2="0.347265625" layer="51"/>
<rectangle x1="-0.33414375" y1="0.334159375" x2="-0.26864375" y2="0.347265625" layer="51"/>
<rectangle x1="0.24245625" y1="0.334159375" x2="0.30795625" y2="0.347265625" layer="51"/>
<rectangle x1="0.93695625" y1="0.334159375" x2="1.04175625" y2="0.347265625" layer="51"/>
<rectangle x1="2.41765625" y1="0.334159375" x2="2.52255625" y2="0.347265625" layer="51"/>
<rectangle x1="3.15155625" y1="0.334159375" x2="3.21705625" y2="0.347265625" layer="51"/>
<rectangle x1="3.71495625" y1="0.334159375" x2="3.78055625" y2="0.347265625" layer="51"/>
<rectangle x1="4.39635625" y1="0.334159375" x2="4.50125625" y2="0.347265625" layer="51"/>
<rectangle x1="5.89025625" y1="0.334159375" x2="5.99505625" y2="0.347265625" layer="51"/>
<rectangle x1="6.61095625" y1="0.334159375" x2="6.67645625" y2="0.347265625" layer="51"/>
<rectangle x1="7.37105625" y1="0.334159375" x2="7.47585625" y2="0.347265625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.34726875" x2="-7.39724375" y2="0.36036875" layer="51"/>
<rectangle x1="-6.68954375" y1="0.34726875" x2="-6.65024375" y2="0.36036875" layer="51"/>
<rectangle x1="-6.02124375" y1="0.34726875" x2="-5.91644375" y2="0.36036875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.34726875" x2="-4.42264375" y2="0.36036875" layer="51"/>
<rectangle x1="-3.79364375" y1="0.34726875" x2="-3.75424375" y2="0.36036875" layer="51"/>
<rectangle x1="-3.23014375" y1="0.34726875" x2="-3.19084375" y2="0.36036875" layer="51"/>
<rectangle x1="-2.54874375" y1="0.34726875" x2="-2.44384375" y2="0.36036875" layer="51"/>
<rectangle x1="-1.06794375" y1="0.34726875" x2="-0.96314375" y2="0.36036875" layer="51"/>
<rectangle x1="-0.32104375" y1="0.34726875" x2="-0.28174375" y2="0.36036875" layer="51"/>
<rectangle x1="0.25555625" y1="0.34726875" x2="0.29485625" y2="0.36036875" layer="51"/>
<rectangle x1="0.93695625" y1="0.34726875" x2="1.04175625" y2="0.36036875" layer="51"/>
<rectangle x1="2.41765625" y1="0.34726875" x2="2.52255625" y2="0.36036875" layer="51"/>
<rectangle x1="3.16465625" y1="0.34726875" x2="3.20395625" y2="0.36036875" layer="51"/>
<rectangle x1="3.72805625" y1="0.34726875" x2="3.76745625" y2="0.36036875" layer="51"/>
<rectangle x1="4.39635625" y1="0.34726875" x2="4.50125625" y2="0.36036875" layer="51"/>
<rectangle x1="5.89025625" y1="0.34726875" x2="5.99505625" y2="0.36036875" layer="51"/>
<rectangle x1="6.62405625" y1="0.34726875" x2="6.66335625" y2="0.36036875" layer="51"/>
<rectangle x1="7.37105625" y1="0.34726875" x2="7.47585625" y2="0.36036875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.36036875" x2="-7.39724375" y2="0.37346875" layer="51"/>
<rectangle x1="-6.02124375" y1="0.36036875" x2="-5.91644375" y2="0.37346875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.36036875" x2="-4.42264375" y2="0.37346875" layer="51"/>
<rectangle x1="-2.54874375" y1="0.36036875" x2="-2.44384375" y2="0.37346875" layer="51"/>
<rectangle x1="-1.06794375" y1="0.36036875" x2="-0.96314375" y2="0.37346875" layer="51"/>
<rectangle x1="0.93695625" y1="0.36036875" x2="1.04175625" y2="0.37346875" layer="51"/>
<rectangle x1="2.41765625" y1="0.36036875" x2="2.52255625" y2="0.37346875" layer="51"/>
<rectangle x1="4.39635625" y1="0.36036875" x2="4.50125625" y2="0.37346875" layer="51"/>
<rectangle x1="5.89025625" y1="0.36036875" x2="5.99505625" y2="0.37346875" layer="51"/>
<rectangle x1="7.37105625" y1="0.36036875" x2="7.47585625" y2="0.37346875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.37346875" x2="-7.39724375" y2="0.38658125" layer="51"/>
<rectangle x1="-6.02124375" y1="0.37346875" x2="-5.91644375" y2="0.38658125" layer="51"/>
<rectangle x1="-4.52744375" y1="0.37346875" x2="-4.42264375" y2="0.38658125" layer="51"/>
<rectangle x1="-2.54874375" y1="0.37346875" x2="-2.44384375" y2="0.38658125" layer="51"/>
<rectangle x1="-1.06794375" y1="0.37346875" x2="-0.96314375" y2="0.38658125" layer="51"/>
<rectangle x1="0.93695625" y1="0.37346875" x2="1.04175625" y2="0.38658125" layer="51"/>
<rectangle x1="2.41765625" y1="0.37346875" x2="2.52255625" y2="0.38658125" layer="51"/>
<rectangle x1="4.39635625" y1="0.37346875" x2="4.50125625" y2="0.38658125" layer="51"/>
<rectangle x1="5.89025625" y1="0.37346875" x2="5.99505625" y2="0.38658125" layer="51"/>
<rectangle x1="7.37105625" y1="0.37346875" x2="7.47585625" y2="0.38658125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.38658125" x2="-7.39724375" y2="0.39968125" layer="51"/>
<rectangle x1="-6.02124375" y1="0.38658125" x2="-5.91644375" y2="0.39968125" layer="51"/>
<rectangle x1="-4.52744375" y1="0.38658125" x2="-4.42264375" y2="0.39968125" layer="51"/>
<rectangle x1="-2.54874375" y1="0.38658125" x2="-2.44384375" y2="0.39968125" layer="51"/>
<rectangle x1="-1.06794375" y1="0.38658125" x2="-0.96314375" y2="0.39968125" layer="51"/>
<rectangle x1="0.93695625" y1="0.38658125" x2="1.04175625" y2="0.39968125" layer="51"/>
<rectangle x1="2.41765625" y1="0.38658125" x2="2.52255625" y2="0.39968125" layer="51"/>
<rectangle x1="4.39635625" y1="0.38658125" x2="4.50125625" y2="0.39968125" layer="51"/>
<rectangle x1="5.89025625" y1="0.38658125" x2="5.99505625" y2="0.39968125" layer="51"/>
<rectangle x1="7.37105625" y1="0.38658125" x2="7.47585625" y2="0.39968125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.39968125" x2="-7.39724375" y2="0.4127875" layer="51"/>
<rectangle x1="-6.02124375" y1="0.39968125" x2="-5.91644375" y2="0.4127875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.39968125" x2="-4.42264375" y2="0.4127875" layer="51"/>
<rectangle x1="-2.54874375" y1="0.39968125" x2="-2.44384375" y2="0.4127875" layer="51"/>
<rectangle x1="-1.06794375" y1="0.39968125" x2="-0.96314375" y2="0.4127875" layer="51"/>
<rectangle x1="0.93695625" y1="0.39968125" x2="1.04175625" y2="0.4127875" layer="51"/>
<rectangle x1="2.41765625" y1="0.39968125" x2="2.52255625" y2="0.4127875" layer="51"/>
<rectangle x1="4.39635625" y1="0.39968125" x2="4.50125625" y2="0.4127875" layer="51"/>
<rectangle x1="5.89025625" y1="0.39968125" x2="5.99505625" y2="0.4127875" layer="51"/>
<rectangle x1="7.37105625" y1="0.39968125" x2="7.47585625" y2="0.4127875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.412790625" x2="-7.39724375" y2="0.425890625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.412790625" x2="-5.91644375" y2="0.425890625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.412790625" x2="-4.42264375" y2="0.425890625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.412790625" x2="-2.44384375" y2="0.425890625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.412790625" x2="-0.96314375" y2="0.425890625" layer="51"/>
<rectangle x1="0.93695625" y1="0.412790625" x2="1.04175625" y2="0.425890625" layer="51"/>
<rectangle x1="2.41765625" y1="0.412790625" x2="2.52255625" y2="0.425890625" layer="51"/>
<rectangle x1="4.39635625" y1="0.412790625" x2="4.50125625" y2="0.425890625" layer="51"/>
<rectangle x1="5.89025625" y1="0.412790625" x2="5.99505625" y2="0.425890625" layer="51"/>
<rectangle x1="7.37105625" y1="0.412790625" x2="7.47585625" y2="0.425890625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.425890625" x2="-7.39724375" y2="0.438990625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.425890625" x2="-5.91644375" y2="0.438990625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.425890625" x2="-4.42264375" y2="0.438990625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.425890625" x2="-2.44384375" y2="0.438990625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.425890625" x2="-0.96314375" y2="0.438990625" layer="51"/>
<rectangle x1="0.93695625" y1="0.425890625" x2="1.04175625" y2="0.438990625" layer="51"/>
<rectangle x1="2.41765625" y1="0.425890625" x2="2.52255625" y2="0.438990625" layer="51"/>
<rectangle x1="4.39635625" y1="0.425890625" x2="4.50125625" y2="0.438990625" layer="51"/>
<rectangle x1="5.89025625" y1="0.425890625" x2="5.99505625" y2="0.438990625" layer="51"/>
<rectangle x1="7.37105625" y1="0.425890625" x2="7.47585625" y2="0.438990625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.438990625" x2="-7.39724375" y2="0.452096875" layer="51"/>
<rectangle x1="-6.02124375" y1="0.438990625" x2="-5.91644375" y2="0.452096875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.438990625" x2="-4.42264375" y2="0.452096875" layer="51"/>
<rectangle x1="-2.54874375" y1="0.438990625" x2="-2.44384375" y2="0.452096875" layer="51"/>
<rectangle x1="-1.06794375" y1="0.438990625" x2="-0.96314375" y2="0.452096875" layer="51"/>
<rectangle x1="0.93695625" y1="0.438990625" x2="1.04175625" y2="0.452096875" layer="51"/>
<rectangle x1="2.41765625" y1="0.438990625" x2="2.52255625" y2="0.452096875" layer="51"/>
<rectangle x1="4.39635625" y1="0.438990625" x2="4.50125625" y2="0.452096875" layer="51"/>
<rectangle x1="5.89025625" y1="0.438990625" x2="5.99505625" y2="0.452096875" layer="51"/>
<rectangle x1="7.37105625" y1="0.438990625" x2="7.47585625" y2="0.452096875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.4521" x2="-7.39724375" y2="0.4652" layer="51"/>
<rectangle x1="-6.02124375" y1="0.4521" x2="-5.91644375" y2="0.4652" layer="51"/>
<rectangle x1="-4.52744375" y1="0.4521" x2="-4.42264375" y2="0.4652" layer="51"/>
<rectangle x1="-2.54874375" y1="0.4521" x2="-2.44384375" y2="0.4652" layer="51"/>
<rectangle x1="-1.06794375" y1="0.4521" x2="-0.96314375" y2="0.4652" layer="51"/>
<rectangle x1="0.93695625" y1="0.4521" x2="1.04175625" y2="0.4652" layer="51"/>
<rectangle x1="2.41765625" y1="0.4521" x2="2.52255625" y2="0.4652" layer="51"/>
<rectangle x1="4.39635625" y1="0.4521" x2="4.50125625" y2="0.4652" layer="51"/>
<rectangle x1="5.89025625" y1="0.4521" x2="5.99505625" y2="0.4652" layer="51"/>
<rectangle x1="7.37105625" y1="0.4521" x2="7.47585625" y2="0.4652" layer="51"/>
<rectangle x1="-7.50204375" y1="0.4652" x2="-7.39724375" y2="0.47830625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.4652" x2="-5.91644375" y2="0.47830625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.4652" x2="-4.42264375" y2="0.47830625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.4652" x2="-2.44384375" y2="0.47830625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.4652" x2="-0.96314375" y2="0.47830625" layer="51"/>
<rectangle x1="0.93695625" y1="0.4652" x2="1.04175625" y2="0.47830625" layer="51"/>
<rectangle x1="2.41765625" y1="0.4652" x2="2.52255625" y2="0.47830625" layer="51"/>
<rectangle x1="4.39635625" y1="0.4652" x2="4.50125625" y2="0.47830625" layer="51"/>
<rectangle x1="5.89025625" y1="0.4652" x2="5.99505625" y2="0.47830625" layer="51"/>
<rectangle x1="7.37105625" y1="0.4652" x2="7.47585625" y2="0.47830625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.478309375" x2="-7.39724375" y2="0.491409375" layer="51"/>
<rectangle x1="-6.02124375" y1="0.478309375" x2="-5.91644375" y2="0.491409375" layer="51"/>
<rectangle x1="-4.52744375" y1="0.478309375" x2="-4.42264375" y2="0.491409375" layer="51"/>
<rectangle x1="-2.54874375" y1="0.478309375" x2="-2.44384375" y2="0.491409375" layer="51"/>
<rectangle x1="-1.06794375" y1="0.478309375" x2="-0.96314375" y2="0.491409375" layer="51"/>
<rectangle x1="0.93695625" y1="0.478309375" x2="1.04175625" y2="0.491409375" layer="51"/>
<rectangle x1="2.41765625" y1="0.478309375" x2="2.52255625" y2="0.491409375" layer="51"/>
<rectangle x1="4.39635625" y1="0.478309375" x2="4.50125625" y2="0.491409375" layer="51"/>
<rectangle x1="5.89025625" y1="0.478309375" x2="5.99505625" y2="0.491409375" layer="51"/>
<rectangle x1="7.37105625" y1="0.478309375" x2="7.47585625" y2="0.491409375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.491409375" x2="-7.39724375" y2="0.504509375" layer="51"/>
<rectangle x1="-6.02124375" y1="0.491409375" x2="-5.91644375" y2="0.504509375" layer="51"/>
<rectangle x1="-4.52744375" y1="0.491409375" x2="-4.42264375" y2="0.504509375" layer="51"/>
<rectangle x1="-2.54874375" y1="0.491409375" x2="-2.44384375" y2="0.504509375" layer="51"/>
<rectangle x1="-1.06794375" y1="0.491409375" x2="-0.96314375" y2="0.504509375" layer="51"/>
<rectangle x1="0.93695625" y1="0.491409375" x2="1.04175625" y2="0.504509375" layer="51"/>
<rectangle x1="2.41765625" y1="0.491409375" x2="2.52255625" y2="0.504509375" layer="51"/>
<rectangle x1="4.39635625" y1="0.491409375" x2="4.50125625" y2="0.504509375" layer="51"/>
<rectangle x1="5.89025625" y1="0.491409375" x2="5.99505625" y2="0.504509375" layer="51"/>
<rectangle x1="7.37105625" y1="0.491409375" x2="7.47585625" y2="0.504509375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.504509375" x2="-7.39724375" y2="0.517615625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.504509375" x2="-5.91644375" y2="0.517615625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.504509375" x2="-4.42264375" y2="0.517615625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.504509375" x2="-2.44384375" y2="0.517615625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.504509375" x2="-0.96314375" y2="0.517615625" layer="51"/>
<rectangle x1="0.93695625" y1="0.504509375" x2="1.04175625" y2="0.517615625" layer="51"/>
<rectangle x1="2.41765625" y1="0.504509375" x2="2.52255625" y2="0.517615625" layer="51"/>
<rectangle x1="4.39635625" y1="0.504509375" x2="4.50125625" y2="0.517615625" layer="51"/>
<rectangle x1="5.89025625" y1="0.504509375" x2="5.99505625" y2="0.517615625" layer="51"/>
<rectangle x1="7.37105625" y1="0.504509375" x2="7.47585625" y2="0.517615625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.51761875" x2="-7.39724375" y2="0.53071875" layer="51"/>
<rectangle x1="-6.02124375" y1="0.51761875" x2="-5.91644375" y2="0.53071875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.51761875" x2="-4.42264375" y2="0.53071875" layer="51"/>
<rectangle x1="-2.54874375" y1="0.51761875" x2="-2.44384375" y2="0.53071875" layer="51"/>
<rectangle x1="-1.06794375" y1="0.51761875" x2="-0.96314375" y2="0.53071875" layer="51"/>
<rectangle x1="0.93695625" y1="0.51761875" x2="1.04175625" y2="0.53071875" layer="51"/>
<rectangle x1="2.41765625" y1="0.51761875" x2="2.52255625" y2="0.53071875" layer="51"/>
<rectangle x1="4.39635625" y1="0.51761875" x2="4.50125625" y2="0.53071875" layer="51"/>
<rectangle x1="5.89025625" y1="0.51761875" x2="5.99505625" y2="0.53071875" layer="51"/>
<rectangle x1="7.37105625" y1="0.51761875" x2="7.47585625" y2="0.53071875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.53071875" x2="-7.39724375" y2="0.54383125" layer="51"/>
<rectangle x1="-6.02124375" y1="0.53071875" x2="-5.91644375" y2="0.54383125" layer="51"/>
<rectangle x1="-5.57574375" y1="0.53071875" x2="-4.86814375" y2="0.54383125" layer="51"/>
<rectangle x1="-4.52744375" y1="0.53071875" x2="-4.42264375" y2="0.54383125" layer="51"/>
<rectangle x1="-2.54874375" y1="0.53071875" x2="-2.44384375" y2="0.54383125" layer="51"/>
<rectangle x1="-2.10314375" y1="0.53071875" x2="-1.40864375" y2="0.54383125" layer="51"/>
<rectangle x1="-1.06794375" y1="0.53071875" x2="-0.96314375" y2="0.54383125" layer="51"/>
<rectangle x1="0.93695625" y1="0.53071875" x2="1.04175625" y2="0.54383125" layer="51"/>
<rectangle x1="1.38245625" y1="0.53071875" x2="2.07695625" y2="0.54383125" layer="51"/>
<rectangle x1="2.41765625" y1="0.53071875" x2="2.52255625" y2="0.54383125" layer="51"/>
<rectangle x1="4.39635625" y1="0.53071875" x2="4.50125625" y2="0.54383125" layer="51"/>
<rectangle x1="4.84195625" y1="0.53071875" x2="5.54955625" y2="0.54383125" layer="51"/>
<rectangle x1="5.89025625" y1="0.53071875" x2="5.99505625" y2="0.54383125" layer="51"/>
<rectangle x1="7.37105625" y1="0.53071875" x2="7.47585625" y2="0.54383125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.54383125" x2="-7.39724375" y2="0.55693125" layer="51"/>
<rectangle x1="-6.02124375" y1="0.54383125" x2="-5.91644375" y2="0.55693125" layer="51"/>
<rectangle x1="-5.60194375" y1="0.54383125" x2="-4.84194375" y2="0.55693125" layer="51"/>
<rectangle x1="-4.52744375" y1="0.54383125" x2="-4.42264375" y2="0.55693125" layer="51"/>
<rectangle x1="-2.54874375" y1="0.54383125" x2="-2.44384375" y2="0.55693125" layer="51"/>
<rectangle x1="-2.14254375" y1="0.54383125" x2="-1.36934375" y2="0.55693125" layer="51"/>
<rectangle x1="-1.06794375" y1="0.54383125" x2="-0.96314375" y2="0.55693125" layer="51"/>
<rectangle x1="0.93695625" y1="0.54383125" x2="1.04175625" y2="0.55693125" layer="51"/>
<rectangle x1="1.34315625" y1="0.54383125" x2="2.11635625" y2="0.55693125" layer="51"/>
<rectangle x1="2.41765625" y1="0.54383125" x2="2.52255625" y2="0.55693125" layer="51"/>
<rectangle x1="4.39635625" y1="0.54383125" x2="4.50125625" y2="0.55693125" layer="51"/>
<rectangle x1="4.81575625" y1="0.54383125" x2="5.57575625" y2="0.55693125" layer="51"/>
<rectangle x1="5.89025625" y1="0.54383125" x2="5.99505625" y2="0.55693125" layer="51"/>
<rectangle x1="7.37105625" y1="0.54383125" x2="7.47585625" y2="0.55693125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.55693125" x2="-7.39724375" y2="0.57003125" layer="51"/>
<rectangle x1="-6.02124375" y1="0.55693125" x2="-5.91644375" y2="0.57003125" layer="51"/>
<rectangle x1="-5.61504375" y1="0.55693125" x2="-4.82884375" y2="0.57003125" layer="51"/>
<rectangle x1="-4.52744375" y1="0.55693125" x2="-4.42264375" y2="0.57003125" layer="51"/>
<rectangle x1="-2.54874375" y1="0.55693125" x2="-2.44384375" y2="0.57003125" layer="51"/>
<rectangle x1="-2.15564375" y1="0.55693125" x2="-1.35624375" y2="0.57003125" layer="51"/>
<rectangle x1="-1.06794375" y1="0.55693125" x2="-0.96314375" y2="0.57003125" layer="51"/>
<rectangle x1="0.93695625" y1="0.55693125" x2="1.04175625" y2="0.57003125" layer="51"/>
<rectangle x1="1.33005625" y1="0.55693125" x2="2.12945625" y2="0.57003125" layer="51"/>
<rectangle x1="2.41765625" y1="0.55693125" x2="2.52255625" y2="0.57003125" layer="51"/>
<rectangle x1="4.39635625" y1="0.55693125" x2="4.50125625" y2="0.57003125" layer="51"/>
<rectangle x1="4.80265625" y1="0.55693125" x2="5.58885625" y2="0.57003125" layer="51"/>
<rectangle x1="5.89025625" y1="0.55693125" x2="5.99505625" y2="0.57003125" layer="51"/>
<rectangle x1="7.37105625" y1="0.55693125" x2="7.47585625" y2="0.57003125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.57003125" x2="-7.39724375" y2="0.5831375" layer="51"/>
<rectangle x1="-6.02124375" y1="0.57003125" x2="-5.91644375" y2="0.5831375" layer="51"/>
<rectangle x1="-5.62814375" y1="0.57003125" x2="-4.81574375" y2="0.5831375" layer="51"/>
<rectangle x1="-4.52744375" y1="0.57003125" x2="-4.42264375" y2="0.5831375" layer="51"/>
<rectangle x1="-2.54874375" y1="0.57003125" x2="-2.44384375" y2="0.5831375" layer="51"/>
<rectangle x1="-2.16874375" y1="0.57003125" x2="-1.34314375" y2="0.5831375" layer="51"/>
<rectangle x1="-1.06794375" y1="0.57003125" x2="-0.96314375" y2="0.5831375" layer="51"/>
<rectangle x1="0.93695625" y1="0.57003125" x2="1.04175625" y2="0.5831375" layer="51"/>
<rectangle x1="1.31695625" y1="0.57003125" x2="2.14255625" y2="0.5831375" layer="51"/>
<rectangle x1="2.41765625" y1="0.57003125" x2="2.52255625" y2="0.5831375" layer="51"/>
<rectangle x1="4.39635625" y1="0.57003125" x2="4.50125625" y2="0.5831375" layer="51"/>
<rectangle x1="4.78955625" y1="0.57003125" x2="5.60195625" y2="0.5831375" layer="51"/>
<rectangle x1="5.89025625" y1="0.57003125" x2="5.99505625" y2="0.5831375" layer="51"/>
<rectangle x1="7.37105625" y1="0.57003125" x2="7.47585625" y2="0.5831375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.583140625" x2="-7.39724375" y2="0.596240625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.583140625" x2="-5.91644375" y2="0.596240625" layer="51"/>
<rectangle x1="-5.64124375" y1="0.583140625" x2="-4.80264375" y2="0.596240625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.583140625" x2="-4.42264375" y2="0.596240625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.583140625" x2="-2.44384375" y2="0.596240625" layer="51"/>
<rectangle x1="-2.18184375" y1="0.583140625" x2="-1.33004375" y2="0.596240625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.583140625" x2="-0.96314375" y2="0.596240625" layer="51"/>
<rectangle x1="0.93695625" y1="0.583140625" x2="1.04175625" y2="0.596240625" layer="51"/>
<rectangle x1="1.30385625" y1="0.583140625" x2="2.15565625" y2="0.596240625" layer="51"/>
<rectangle x1="2.41765625" y1="0.583140625" x2="2.52255625" y2="0.596240625" layer="51"/>
<rectangle x1="4.39635625" y1="0.583140625" x2="4.50125625" y2="0.596240625" layer="51"/>
<rectangle x1="4.77645625" y1="0.583140625" x2="5.61505625" y2="0.596240625" layer="51"/>
<rectangle x1="5.89025625" y1="0.583140625" x2="5.99505625" y2="0.596240625" layer="51"/>
<rectangle x1="7.37105625" y1="0.583140625" x2="7.47585625" y2="0.596240625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.596240625" x2="-7.39724375" y2="0.609346875" layer="51"/>
<rectangle x1="-6.02124375" y1="0.596240625" x2="-5.91644375" y2="0.609346875" layer="51"/>
<rectangle x1="-5.65434375" y1="0.596240625" x2="-4.78954375" y2="0.609346875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.596240625" x2="-4.42264375" y2="0.609346875" layer="51"/>
<rectangle x1="-2.54874375" y1="0.596240625" x2="-2.44384375" y2="0.609346875" layer="51"/>
<rectangle x1="-2.18184375" y1="0.596240625" x2="-1.33004375" y2="0.609346875" layer="51"/>
<rectangle x1="-1.06794375" y1="0.596240625" x2="-0.96314375" y2="0.609346875" layer="51"/>
<rectangle x1="0.93695625" y1="0.596240625" x2="1.04175625" y2="0.609346875" layer="51"/>
<rectangle x1="1.30385625" y1="0.596240625" x2="2.15565625" y2="0.609346875" layer="51"/>
<rectangle x1="2.41765625" y1="0.596240625" x2="2.52255625" y2="0.609346875" layer="51"/>
<rectangle x1="4.39635625" y1="0.596240625" x2="4.50125625" y2="0.609346875" layer="51"/>
<rectangle x1="4.76335625" y1="0.596240625" x2="5.62815625" y2="0.609346875" layer="51"/>
<rectangle x1="5.89025625" y1="0.596240625" x2="5.99505625" y2="0.609346875" layer="51"/>
<rectangle x1="7.37105625" y1="0.596240625" x2="7.47585625" y2="0.609346875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.60935" x2="-7.39724375" y2="0.62245" layer="51"/>
<rectangle x1="-6.02124375" y1="0.60935" x2="-5.91644375" y2="0.62245" layer="51"/>
<rectangle x1="-5.65434375" y1="0.60935" x2="-4.78954375" y2="0.62245" layer="51"/>
<rectangle x1="-4.52744375" y1="0.60935" x2="-4.42264375" y2="0.62245" layer="51"/>
<rectangle x1="-2.54874375" y1="0.60935" x2="-2.44384375" y2="0.62245" layer="51"/>
<rectangle x1="-2.19494375" y1="0.60935" x2="-1.31694375" y2="0.62245" layer="51"/>
<rectangle x1="-1.06794375" y1="0.60935" x2="-0.96314375" y2="0.62245" layer="51"/>
<rectangle x1="0.93695625" y1="0.60935" x2="1.04175625" y2="0.62245" layer="51"/>
<rectangle x1="1.29075625" y1="0.60935" x2="2.16875625" y2="0.62245" layer="51"/>
<rectangle x1="2.41765625" y1="0.60935" x2="2.52255625" y2="0.62245" layer="51"/>
<rectangle x1="4.39635625" y1="0.60935" x2="4.50125625" y2="0.62245" layer="51"/>
<rectangle x1="4.76335625" y1="0.60935" x2="5.62815625" y2="0.62245" layer="51"/>
<rectangle x1="5.89025625" y1="0.60935" x2="5.99505625" y2="0.62245" layer="51"/>
<rectangle x1="7.37105625" y1="0.60935" x2="7.47585625" y2="0.62245" layer="51"/>
<rectangle x1="-7.50204375" y1="0.62245" x2="-7.39724375" y2="0.63555" layer="51"/>
<rectangle x1="-6.02124375" y1="0.62245" x2="-5.91644375" y2="0.63555" layer="51"/>
<rectangle x1="-5.66744375" y1="0.62245" x2="-4.77644375" y2="0.63555" layer="51"/>
<rectangle x1="-4.52744375" y1="0.62245" x2="-4.42264375" y2="0.63555" layer="51"/>
<rectangle x1="-2.54874375" y1="0.62245" x2="-2.44384375" y2="0.63555" layer="51"/>
<rectangle x1="-2.19494375" y1="0.62245" x2="-1.31694375" y2="0.63555" layer="51"/>
<rectangle x1="-1.06794375" y1="0.62245" x2="-0.96314375" y2="0.63555" layer="51"/>
<rectangle x1="0.93695625" y1="0.62245" x2="1.04175625" y2="0.63555" layer="51"/>
<rectangle x1="1.29075625" y1="0.62245" x2="2.16875625" y2="0.63555" layer="51"/>
<rectangle x1="2.41765625" y1="0.62245" x2="2.52255625" y2="0.63555" layer="51"/>
<rectangle x1="4.39635625" y1="0.62245" x2="4.50125625" y2="0.63555" layer="51"/>
<rectangle x1="4.75025625" y1="0.62245" x2="5.64125625" y2="0.63555" layer="51"/>
<rectangle x1="5.89025625" y1="0.62245" x2="5.99505625" y2="0.63555" layer="51"/>
<rectangle x1="7.37105625" y1="0.62245" x2="7.47585625" y2="0.63555" layer="51"/>
<rectangle x1="-7.50204375" y1="0.63555" x2="-7.39724375" y2="0.64865625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.63555" x2="-5.91644375" y2="0.64865625" layer="51"/>
<rectangle x1="-5.66744375" y1="0.63555" x2="-5.54954375" y2="0.64865625" layer="51"/>
<rectangle x1="-4.89434375" y1="0.63555" x2="-4.77644375" y2="0.64865625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.63555" x2="-4.42264375" y2="0.64865625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.63555" x2="-2.44384375" y2="0.64865625" layer="51"/>
<rectangle x1="-2.19494375" y1="0.63555" x2="-2.07694375" y2="0.64865625" layer="51"/>
<rectangle x1="-1.43484375" y1="0.63555" x2="-1.30384375" y2="0.64865625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.63555" x2="-0.96314375" y2="0.64865625" layer="51"/>
<rectangle x1="0.93695625" y1="0.63555" x2="1.04175625" y2="0.64865625" layer="51"/>
<rectangle x1="1.29075625" y1="0.63555" x2="1.40865625" y2="0.64865625" layer="51"/>
<rectangle x1="2.05075625" y1="0.63555" x2="2.18185625" y2="0.64865625" layer="51"/>
<rectangle x1="2.41765625" y1="0.63555" x2="2.52255625" y2="0.64865625" layer="51"/>
<rectangle x1="4.39635625" y1="0.63555" x2="4.50125625" y2="0.64865625" layer="51"/>
<rectangle x1="4.75025625" y1="0.63555" x2="4.86815625" y2="0.64865625" layer="51"/>
<rectangle x1="5.52335625" y1="0.63555" x2="5.64125625" y2="0.64865625" layer="51"/>
<rectangle x1="5.89025625" y1="0.63555" x2="5.99505625" y2="0.64865625" layer="51"/>
<rectangle x1="7.37105625" y1="0.63555" x2="7.47585625" y2="0.64865625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.648659375" x2="-7.39724375" y2="0.661759375" layer="51"/>
<rectangle x1="-6.02124375" y1="0.648659375" x2="-5.91644375" y2="0.661759375" layer="51"/>
<rectangle x1="-5.66744375" y1="0.648659375" x2="-5.56264375" y2="0.661759375" layer="51"/>
<rectangle x1="-4.88124375" y1="0.648659375" x2="-4.77644375" y2="0.661759375" layer="51"/>
<rectangle x1="-4.52744375" y1="0.648659375" x2="-4.42264375" y2="0.661759375" layer="51"/>
<rectangle x1="-2.54874375" y1="0.648659375" x2="-2.44384375" y2="0.661759375" layer="51"/>
<rectangle x1="-2.20804375" y1="0.648659375" x2="-2.09004375" y2="0.661759375" layer="51"/>
<rectangle x1="-1.42174375" y1="0.648659375" x2="-1.30384375" y2="0.661759375" layer="51"/>
<rectangle x1="-1.06794375" y1="0.648659375" x2="-0.96314375" y2="0.661759375" layer="51"/>
<rectangle x1="0.93695625" y1="0.648659375" x2="1.04175625" y2="0.661759375" layer="51"/>
<rectangle x1="1.27765625" y1="0.648659375" x2="1.39555625" y2="0.661759375" layer="51"/>
<rectangle x1="2.06385625" y1="0.648659375" x2="2.18185625" y2="0.661759375" layer="51"/>
<rectangle x1="2.41765625" y1="0.648659375" x2="2.52255625" y2="0.661759375" layer="51"/>
<rectangle x1="4.39635625" y1="0.648659375" x2="4.50125625" y2="0.661759375" layer="51"/>
<rectangle x1="4.75025625" y1="0.648659375" x2="4.85505625" y2="0.661759375" layer="51"/>
<rectangle x1="5.53645625" y1="0.648659375" x2="5.64125625" y2="0.661759375" layer="51"/>
<rectangle x1="5.89025625" y1="0.648659375" x2="5.99505625" y2="0.661759375" layer="51"/>
<rectangle x1="7.37105625" y1="0.648659375" x2="7.47585625" y2="0.661759375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.661759375" x2="-7.39724375" y2="0.674865625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.661759375" x2="-5.91644375" y2="0.674865625" layer="51"/>
<rectangle x1="-5.66744375" y1="0.661759375" x2="-5.56264375" y2="0.674865625" layer="51"/>
<rectangle x1="-4.88124375" y1="0.661759375" x2="-4.77644375" y2="0.674865625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.661759375" x2="-4.42264375" y2="0.674865625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.661759375" x2="-2.44384375" y2="0.674865625" layer="51"/>
<rectangle x1="-2.20804375" y1="0.661759375" x2="-2.10314375" y2="0.674865625" layer="51"/>
<rectangle x1="-1.40864375" y1="0.661759375" x2="-1.30384375" y2="0.674865625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.661759375" x2="-0.96314375" y2="0.674865625" layer="51"/>
<rectangle x1="0.93695625" y1="0.661759375" x2="1.04175625" y2="0.674865625" layer="51"/>
<rectangle x1="1.27765625" y1="0.661759375" x2="1.38245625" y2="0.674865625" layer="51"/>
<rectangle x1="2.07695625" y1="0.661759375" x2="2.18185625" y2="0.674865625" layer="51"/>
<rectangle x1="2.41765625" y1="0.661759375" x2="2.52255625" y2="0.674865625" layer="51"/>
<rectangle x1="4.39635625" y1="0.661759375" x2="4.50125625" y2="0.674865625" layer="51"/>
<rectangle x1="4.75025625" y1="0.661759375" x2="4.85505625" y2="0.674865625" layer="51"/>
<rectangle x1="5.53645625" y1="0.661759375" x2="5.64125625" y2="0.674865625" layer="51"/>
<rectangle x1="5.89025625" y1="0.661759375" x2="5.99505625" y2="0.674865625" layer="51"/>
<rectangle x1="7.37105625" y1="0.661759375" x2="7.47585625" y2="0.674865625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.67486875" x2="-7.39724375" y2="0.68796875" layer="51"/>
<rectangle x1="-6.02124375" y1="0.67486875" x2="-5.91644375" y2="0.68796875" layer="51"/>
<rectangle x1="-5.66744375" y1="0.67486875" x2="-5.56264375" y2="0.68796875" layer="51"/>
<rectangle x1="-4.88124375" y1="0.67486875" x2="-4.77644375" y2="0.68796875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.67486875" x2="-4.42264375" y2="0.68796875" layer="51"/>
<rectangle x1="-2.54874375" y1="0.67486875" x2="-2.44384375" y2="0.68796875" layer="51"/>
<rectangle x1="-2.20804375" y1="0.67486875" x2="-2.10314375" y2="0.68796875" layer="51"/>
<rectangle x1="-1.40864375" y1="0.67486875" x2="-1.30384375" y2="0.68796875" layer="51"/>
<rectangle x1="-1.06794375" y1="0.67486875" x2="-0.96314375" y2="0.68796875" layer="51"/>
<rectangle x1="0.93695625" y1="0.67486875" x2="1.04175625" y2="0.68796875" layer="51"/>
<rectangle x1="1.27765625" y1="0.67486875" x2="1.38245625" y2="0.68796875" layer="51"/>
<rectangle x1="2.07695625" y1="0.67486875" x2="2.18185625" y2="0.68796875" layer="51"/>
<rectangle x1="2.41765625" y1="0.67486875" x2="2.52255625" y2="0.68796875" layer="51"/>
<rectangle x1="4.39635625" y1="0.67486875" x2="4.50125625" y2="0.68796875" layer="51"/>
<rectangle x1="4.75025625" y1="0.67486875" x2="4.85505625" y2="0.68796875" layer="51"/>
<rectangle x1="5.53645625" y1="0.67486875" x2="5.64125625" y2="0.68796875" layer="51"/>
<rectangle x1="5.89025625" y1="0.67486875" x2="5.99505625" y2="0.68796875" layer="51"/>
<rectangle x1="7.37105625" y1="0.67486875" x2="7.47585625" y2="0.68796875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.68796875" x2="-7.39724375" y2="0.70106875" layer="51"/>
<rectangle x1="-6.02124375" y1="0.68796875" x2="-5.91644375" y2="0.70106875" layer="51"/>
<rectangle x1="-5.66744375" y1="0.68796875" x2="-5.56264375" y2="0.70106875" layer="51"/>
<rectangle x1="-4.88124375" y1="0.68796875" x2="-4.77644375" y2="0.70106875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.68796875" x2="-4.42264375" y2="0.70106875" layer="51"/>
<rectangle x1="-2.54874375" y1="0.68796875" x2="-2.44384375" y2="0.70106875" layer="51"/>
<rectangle x1="-2.20804375" y1="0.68796875" x2="-2.10314375" y2="0.70106875" layer="51"/>
<rectangle x1="-1.40864375" y1="0.68796875" x2="-1.30384375" y2="0.70106875" layer="51"/>
<rectangle x1="-1.06794375" y1="0.68796875" x2="-0.96314375" y2="0.70106875" layer="51"/>
<rectangle x1="0.93695625" y1="0.68796875" x2="1.04175625" y2="0.70106875" layer="51"/>
<rectangle x1="1.27765625" y1="0.68796875" x2="1.38245625" y2="0.70106875" layer="51"/>
<rectangle x1="2.07695625" y1="0.68796875" x2="2.18185625" y2="0.70106875" layer="51"/>
<rectangle x1="2.41765625" y1="0.68796875" x2="2.52255625" y2="0.70106875" layer="51"/>
<rectangle x1="4.39635625" y1="0.68796875" x2="4.50125625" y2="0.70106875" layer="51"/>
<rectangle x1="4.75025625" y1="0.68796875" x2="4.85505625" y2="0.70106875" layer="51"/>
<rectangle x1="5.53645625" y1="0.68796875" x2="5.64125625" y2="0.70106875" layer="51"/>
<rectangle x1="5.89025625" y1="0.68796875" x2="5.99505625" y2="0.70106875" layer="51"/>
<rectangle x1="7.37105625" y1="0.68796875" x2="7.47585625" y2="0.70106875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.70106875" x2="-7.39724375" y2="0.71418125" layer="51"/>
<rectangle x1="-6.02124375" y1="0.70106875" x2="-5.91644375" y2="0.71418125" layer="51"/>
<rectangle x1="-5.66744375" y1="0.70106875" x2="-5.56264375" y2="0.71418125" layer="51"/>
<rectangle x1="-4.88124375" y1="0.70106875" x2="-4.77644375" y2="0.71418125" layer="51"/>
<rectangle x1="-4.52744375" y1="0.70106875" x2="-4.42264375" y2="0.71418125" layer="51"/>
<rectangle x1="-2.54874375" y1="0.70106875" x2="-2.44384375" y2="0.71418125" layer="51"/>
<rectangle x1="-2.20804375" y1="0.70106875" x2="-2.10314375" y2="0.71418125" layer="51"/>
<rectangle x1="-1.40864375" y1="0.70106875" x2="-1.30384375" y2="0.71418125" layer="51"/>
<rectangle x1="-1.06794375" y1="0.70106875" x2="-0.96314375" y2="0.71418125" layer="51"/>
<rectangle x1="0.93695625" y1="0.70106875" x2="1.04175625" y2="0.71418125" layer="51"/>
<rectangle x1="1.27765625" y1="0.70106875" x2="1.38245625" y2="0.71418125" layer="51"/>
<rectangle x1="2.07695625" y1="0.70106875" x2="2.18185625" y2="0.71418125" layer="51"/>
<rectangle x1="2.41765625" y1="0.70106875" x2="2.52255625" y2="0.71418125" layer="51"/>
<rectangle x1="4.39635625" y1="0.70106875" x2="4.50125625" y2="0.71418125" layer="51"/>
<rectangle x1="4.75025625" y1="0.70106875" x2="4.85505625" y2="0.71418125" layer="51"/>
<rectangle x1="5.53645625" y1="0.70106875" x2="5.64125625" y2="0.71418125" layer="51"/>
<rectangle x1="5.89025625" y1="0.70106875" x2="5.99505625" y2="0.71418125" layer="51"/>
<rectangle x1="7.37105625" y1="0.70106875" x2="7.47585625" y2="0.71418125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.71418125" x2="-7.39724375" y2="0.72728125" layer="51"/>
<rectangle x1="-6.02124375" y1="0.71418125" x2="-5.91644375" y2="0.72728125" layer="51"/>
<rectangle x1="-5.66744375" y1="0.71418125" x2="-5.56264375" y2="0.72728125" layer="51"/>
<rectangle x1="-4.88124375" y1="0.71418125" x2="-4.77644375" y2="0.72728125" layer="51"/>
<rectangle x1="-4.52744375" y1="0.71418125" x2="-4.42264375" y2="0.72728125" layer="51"/>
<rectangle x1="-2.54874375" y1="0.71418125" x2="-2.44384375" y2="0.72728125" layer="51"/>
<rectangle x1="-2.20804375" y1="0.71418125" x2="-2.10314375" y2="0.72728125" layer="51"/>
<rectangle x1="-1.40864375" y1="0.71418125" x2="-1.30384375" y2="0.72728125" layer="51"/>
<rectangle x1="-1.06794375" y1="0.71418125" x2="-0.96314375" y2="0.72728125" layer="51"/>
<rectangle x1="0.93695625" y1="0.71418125" x2="1.04175625" y2="0.72728125" layer="51"/>
<rectangle x1="1.27765625" y1="0.71418125" x2="1.38245625" y2="0.72728125" layer="51"/>
<rectangle x1="2.07695625" y1="0.71418125" x2="2.18185625" y2="0.72728125" layer="51"/>
<rectangle x1="2.41765625" y1="0.71418125" x2="2.52255625" y2="0.72728125" layer="51"/>
<rectangle x1="4.39635625" y1="0.71418125" x2="4.50125625" y2="0.72728125" layer="51"/>
<rectangle x1="4.75025625" y1="0.71418125" x2="4.85505625" y2="0.72728125" layer="51"/>
<rectangle x1="5.53645625" y1="0.71418125" x2="5.64125625" y2="0.72728125" layer="51"/>
<rectangle x1="5.89025625" y1="0.71418125" x2="5.99505625" y2="0.72728125" layer="51"/>
<rectangle x1="7.37105625" y1="0.71418125" x2="7.47585625" y2="0.72728125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.72728125" x2="-7.39724375" y2="0.7403875" layer="51"/>
<rectangle x1="-6.02124375" y1="0.72728125" x2="-5.91644375" y2="0.7403875" layer="51"/>
<rectangle x1="-5.66744375" y1="0.72728125" x2="-5.56264375" y2="0.7403875" layer="51"/>
<rectangle x1="-4.88124375" y1="0.72728125" x2="-4.77644375" y2="0.7403875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.72728125" x2="-4.42264375" y2="0.7403875" layer="51"/>
<rectangle x1="-2.54874375" y1="0.72728125" x2="-2.44384375" y2="0.7403875" layer="51"/>
<rectangle x1="-2.20804375" y1="0.72728125" x2="-2.10314375" y2="0.7403875" layer="51"/>
<rectangle x1="-1.40864375" y1="0.72728125" x2="-1.30384375" y2="0.7403875" layer="51"/>
<rectangle x1="-1.06794375" y1="0.72728125" x2="-0.96314375" y2="0.7403875" layer="51"/>
<rectangle x1="0.93695625" y1="0.72728125" x2="1.04175625" y2="0.7403875" layer="51"/>
<rectangle x1="1.27765625" y1="0.72728125" x2="1.38245625" y2="0.7403875" layer="51"/>
<rectangle x1="2.07695625" y1="0.72728125" x2="2.18185625" y2="0.7403875" layer="51"/>
<rectangle x1="2.41765625" y1="0.72728125" x2="2.52255625" y2="0.7403875" layer="51"/>
<rectangle x1="4.39635625" y1="0.72728125" x2="4.50125625" y2="0.7403875" layer="51"/>
<rectangle x1="4.75025625" y1="0.72728125" x2="4.85505625" y2="0.7403875" layer="51"/>
<rectangle x1="5.53645625" y1="0.72728125" x2="5.64125625" y2="0.7403875" layer="51"/>
<rectangle x1="5.89025625" y1="0.72728125" x2="5.99505625" y2="0.7403875" layer="51"/>
<rectangle x1="7.37105625" y1="0.72728125" x2="7.47585625" y2="0.7403875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.740390625" x2="-7.39724375" y2="0.753490625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.740390625" x2="-5.91644375" y2="0.753490625" layer="51"/>
<rectangle x1="-5.66744375" y1="0.740390625" x2="-5.56264375" y2="0.753490625" layer="51"/>
<rectangle x1="-4.88124375" y1="0.740390625" x2="-4.77644375" y2="0.753490625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.740390625" x2="-4.42264375" y2="0.753490625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.740390625" x2="-2.44384375" y2="0.753490625" layer="51"/>
<rectangle x1="-2.20804375" y1="0.740390625" x2="-2.10314375" y2="0.753490625" layer="51"/>
<rectangle x1="-1.40864375" y1="0.740390625" x2="-1.30384375" y2="0.753490625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.740390625" x2="-0.96314375" y2="0.753490625" layer="51"/>
<rectangle x1="0.93695625" y1="0.740390625" x2="1.04175625" y2="0.753490625" layer="51"/>
<rectangle x1="1.27765625" y1="0.740390625" x2="1.38245625" y2="0.753490625" layer="51"/>
<rectangle x1="2.07695625" y1="0.740390625" x2="2.18185625" y2="0.753490625" layer="51"/>
<rectangle x1="2.41765625" y1="0.740390625" x2="2.52255625" y2="0.753490625" layer="51"/>
<rectangle x1="4.39635625" y1="0.740390625" x2="4.50125625" y2="0.753490625" layer="51"/>
<rectangle x1="4.75025625" y1="0.740390625" x2="4.85505625" y2="0.753490625" layer="51"/>
<rectangle x1="5.53645625" y1="0.740390625" x2="5.64125625" y2="0.753490625" layer="51"/>
<rectangle x1="5.89025625" y1="0.740390625" x2="5.99505625" y2="0.753490625" layer="51"/>
<rectangle x1="7.37105625" y1="0.740390625" x2="7.47585625" y2="0.753490625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.753490625" x2="-7.39724375" y2="0.766590625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.753490625" x2="-5.91644375" y2="0.766590625" layer="51"/>
<rectangle x1="-5.66744375" y1="0.753490625" x2="-5.56264375" y2="0.766590625" layer="51"/>
<rectangle x1="-4.88124375" y1="0.753490625" x2="-4.77644375" y2="0.766590625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.753490625" x2="-4.42264375" y2="0.766590625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.753490625" x2="-2.44384375" y2="0.766590625" layer="51"/>
<rectangle x1="-2.20804375" y1="0.753490625" x2="-2.10314375" y2="0.766590625" layer="51"/>
<rectangle x1="-1.40864375" y1="0.753490625" x2="-1.30384375" y2="0.766590625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.753490625" x2="-0.96314375" y2="0.766590625" layer="51"/>
<rectangle x1="0.93695625" y1="0.753490625" x2="1.04175625" y2="0.766590625" layer="51"/>
<rectangle x1="1.27765625" y1="0.753490625" x2="1.38245625" y2="0.766590625" layer="51"/>
<rectangle x1="2.07695625" y1="0.753490625" x2="2.18185625" y2="0.766590625" layer="51"/>
<rectangle x1="2.41765625" y1="0.753490625" x2="2.52255625" y2="0.766590625" layer="51"/>
<rectangle x1="4.39635625" y1="0.753490625" x2="4.50125625" y2="0.766590625" layer="51"/>
<rectangle x1="4.75025625" y1="0.753490625" x2="4.85505625" y2="0.766590625" layer="51"/>
<rectangle x1="5.53645625" y1="0.753490625" x2="5.64125625" y2="0.766590625" layer="51"/>
<rectangle x1="5.89025625" y1="0.753490625" x2="5.99505625" y2="0.766590625" layer="51"/>
<rectangle x1="7.37105625" y1="0.753490625" x2="7.47585625" y2="0.766590625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.766590625" x2="-7.39724375" y2="0.779696875" layer="51"/>
<rectangle x1="-6.02124375" y1="0.766590625" x2="-5.91644375" y2="0.779696875" layer="51"/>
<rectangle x1="-5.66744375" y1="0.766590625" x2="-5.56264375" y2="0.779696875" layer="51"/>
<rectangle x1="-4.88124375" y1="0.766590625" x2="-4.77644375" y2="0.779696875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.766590625" x2="-4.42264375" y2="0.779696875" layer="51"/>
<rectangle x1="-2.54874375" y1="0.766590625" x2="-2.44384375" y2="0.779696875" layer="51"/>
<rectangle x1="-2.20804375" y1="0.766590625" x2="-2.10314375" y2="0.779696875" layer="51"/>
<rectangle x1="-1.40864375" y1="0.766590625" x2="-1.30384375" y2="0.779696875" layer="51"/>
<rectangle x1="-1.06794375" y1="0.766590625" x2="-0.96314375" y2="0.779696875" layer="51"/>
<rectangle x1="0.93695625" y1="0.766590625" x2="1.04175625" y2="0.779696875" layer="51"/>
<rectangle x1="1.27765625" y1="0.766590625" x2="1.38245625" y2="0.779696875" layer="51"/>
<rectangle x1="2.07695625" y1="0.766590625" x2="2.18185625" y2="0.779696875" layer="51"/>
<rectangle x1="2.41765625" y1="0.766590625" x2="2.52255625" y2="0.779696875" layer="51"/>
<rectangle x1="4.39635625" y1="0.766590625" x2="4.50125625" y2="0.779696875" layer="51"/>
<rectangle x1="4.75025625" y1="0.766590625" x2="4.85505625" y2="0.779696875" layer="51"/>
<rectangle x1="5.53645625" y1="0.766590625" x2="5.64125625" y2="0.779696875" layer="51"/>
<rectangle x1="5.89025625" y1="0.766590625" x2="5.99505625" y2="0.779696875" layer="51"/>
<rectangle x1="7.37105625" y1="0.766590625" x2="7.47585625" y2="0.779696875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.7797" x2="-7.39724375" y2="0.7928" layer="51"/>
<rectangle x1="-6.02124375" y1="0.7797" x2="-5.91644375" y2="0.7928" layer="51"/>
<rectangle x1="-5.66744375" y1="0.7797" x2="-5.56264375" y2="0.7928" layer="51"/>
<rectangle x1="-4.88124375" y1="0.7797" x2="-4.77644375" y2="0.7928" layer="51"/>
<rectangle x1="-4.52744375" y1="0.7797" x2="-4.42264375" y2="0.7928" layer="51"/>
<rectangle x1="-2.54874375" y1="0.7797" x2="-2.44384375" y2="0.7928" layer="51"/>
<rectangle x1="-2.20804375" y1="0.7797" x2="-2.10314375" y2="0.7928" layer="51"/>
<rectangle x1="-1.40864375" y1="0.7797" x2="-1.30384375" y2="0.7928" layer="51"/>
<rectangle x1="-1.06794375" y1="0.7797" x2="-0.96314375" y2="0.7928" layer="51"/>
<rectangle x1="0.93695625" y1="0.7797" x2="1.04175625" y2="0.7928" layer="51"/>
<rectangle x1="1.27765625" y1="0.7797" x2="1.38245625" y2="0.7928" layer="51"/>
<rectangle x1="2.07695625" y1="0.7797" x2="2.18185625" y2="0.7928" layer="51"/>
<rectangle x1="2.41765625" y1="0.7797" x2="2.52255625" y2="0.7928" layer="51"/>
<rectangle x1="4.39635625" y1="0.7797" x2="4.50125625" y2="0.7928" layer="51"/>
<rectangle x1="4.75025625" y1="0.7797" x2="4.85505625" y2="0.7928" layer="51"/>
<rectangle x1="5.53645625" y1="0.7797" x2="5.64125625" y2="0.7928" layer="51"/>
<rectangle x1="5.89025625" y1="0.7797" x2="5.99505625" y2="0.7928" layer="51"/>
<rectangle x1="7.37105625" y1="0.7797" x2="7.47585625" y2="0.7928" layer="51"/>
<rectangle x1="-7.50204375" y1="0.7928" x2="-7.39724375" y2="0.80590625" layer="51"/>
<rectangle x1="-6.13924375" y1="0.7928" x2="-6.11304375" y2="0.80590625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.7928" x2="-5.91644375" y2="0.80590625" layer="51"/>
<rectangle x1="-5.66744375" y1="0.7928" x2="-5.56264375" y2="0.80590625" layer="51"/>
<rectangle x1="-4.88124375" y1="0.7928" x2="-4.77644375" y2="0.80590625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.7928" x2="-4.42264375" y2="0.80590625" layer="51"/>
<rectangle x1="-4.33084375" y1="0.7928" x2="-4.30464375" y2="0.80590625" layer="51"/>
<rectangle x1="-2.66664375" y1="0.7928" x2="-2.65354375" y2="0.80590625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.7928" x2="-2.44384375" y2="0.80590625" layer="51"/>
<rectangle x1="-2.20804375" y1="0.7928" x2="-2.10314375" y2="0.80590625" layer="51"/>
<rectangle x1="-1.40864375" y1="0.7928" x2="-1.30384375" y2="0.80590625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.7928" x2="-0.96314375" y2="0.80590625" layer="51"/>
<rectangle x1="-0.85834375" y1="0.7928" x2="-0.83214375" y2="0.80590625" layer="51"/>
<rectangle x1="0.81905625" y1="0.7928" x2="0.83215625" y2="0.80590625" layer="51"/>
<rectangle x1="0.93695625" y1="0.7928" x2="1.04175625" y2="0.80590625" layer="51"/>
<rectangle x1="1.27765625" y1="0.7928" x2="1.38245625" y2="0.80590625" layer="51"/>
<rectangle x1="2.07695625" y1="0.7928" x2="2.18185625" y2="0.80590625" layer="51"/>
<rectangle x1="2.41765625" y1="0.7928" x2="2.52255625" y2="0.80590625" layer="51"/>
<rectangle x1="2.62735625" y1="0.7928" x2="2.65355625" y2="0.80590625" layer="51"/>
<rectangle x1="4.27845625" y1="0.7928" x2="4.30465625" y2="0.80590625" layer="51"/>
<rectangle x1="4.39635625" y1="0.7928" x2="4.50125625" y2="0.80590625" layer="51"/>
<rectangle x1="4.75025625" y1="0.7928" x2="4.85505625" y2="0.80590625" layer="51"/>
<rectangle x1="5.53645625" y1="0.7928" x2="5.64125625" y2="0.80590625" layer="51"/>
<rectangle x1="5.89025625" y1="0.7928" x2="5.99505625" y2="0.80590625" layer="51"/>
<rectangle x1="6.08685625" y1="0.7928" x2="6.11305625" y2="0.80590625" layer="51"/>
<rectangle x1="7.37105625" y1="0.7928" x2="7.47585625" y2="0.80590625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.805909375" x2="-7.39724375" y2="0.819009375" layer="51"/>
<rectangle x1="-6.15234375" y1="0.805909375" x2="-6.09994375" y2="0.819009375" layer="51"/>
<rectangle x1="-6.02124375" y1="0.805909375" x2="-5.91644375" y2="0.819009375" layer="51"/>
<rectangle x1="-5.66744375" y1="0.805909375" x2="-5.56264375" y2="0.819009375" layer="51"/>
<rectangle x1="-4.88124375" y1="0.805909375" x2="-4.77644375" y2="0.819009375" layer="51"/>
<rectangle x1="-4.52744375" y1="0.805909375" x2="-4.42264375" y2="0.819009375" layer="51"/>
<rectangle x1="-4.34394375" y1="0.805909375" x2="-4.29154375" y2="0.819009375" layer="51"/>
<rectangle x1="-2.69284375" y1="0.805909375" x2="-2.64044375" y2="0.819009375" layer="51"/>
<rectangle x1="-2.54874375" y1="0.805909375" x2="-2.44384375" y2="0.819009375" layer="51"/>
<rectangle x1="-2.20804375" y1="0.805909375" x2="-2.10314375" y2="0.819009375" layer="51"/>
<rectangle x1="-1.40864375" y1="0.805909375" x2="-1.30384375" y2="0.819009375" layer="51"/>
<rectangle x1="-1.06794375" y1="0.805909375" x2="-0.96314375" y2="0.819009375" layer="51"/>
<rectangle x1="-0.87144375" y1="0.805909375" x2="-0.81894375" y2="0.819009375" layer="51"/>
<rectangle x1="0.79275625" y1="0.805909375" x2="0.84525625" y2="0.819009375" layer="51"/>
<rectangle x1="0.93695625" y1="0.805909375" x2="1.04175625" y2="0.819009375" layer="51"/>
<rectangle x1="1.27765625" y1="0.805909375" x2="1.38245625" y2="0.819009375" layer="51"/>
<rectangle x1="2.07695625" y1="0.805909375" x2="2.18185625" y2="0.819009375" layer="51"/>
<rectangle x1="2.41765625" y1="0.805909375" x2="2.52255625" y2="0.819009375" layer="51"/>
<rectangle x1="2.61425625" y1="0.805909375" x2="2.66665625" y2="0.819009375" layer="51"/>
<rectangle x1="4.26535625" y1="0.805909375" x2="4.31775625" y2="0.819009375" layer="51"/>
<rectangle x1="4.39635625" y1="0.805909375" x2="4.50125625" y2="0.819009375" layer="51"/>
<rectangle x1="4.75025625" y1="0.805909375" x2="4.85505625" y2="0.819009375" layer="51"/>
<rectangle x1="5.53645625" y1="0.805909375" x2="5.64125625" y2="0.819009375" layer="51"/>
<rectangle x1="5.89025625" y1="0.805909375" x2="5.99505625" y2="0.819009375" layer="51"/>
<rectangle x1="6.07375625" y1="0.805909375" x2="6.12615625" y2="0.819009375" layer="51"/>
<rectangle x1="7.37105625" y1="0.805909375" x2="7.47585625" y2="0.819009375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.819009375" x2="-7.39724375" y2="0.832109375" layer="51"/>
<rectangle x1="-6.16544375" y1="0.819009375" x2="-6.09994375" y2="0.832109375" layer="51"/>
<rectangle x1="-6.02124375" y1="0.819009375" x2="-5.91644375" y2="0.832109375" layer="51"/>
<rectangle x1="-5.66744375" y1="0.819009375" x2="-5.56264375" y2="0.832109375" layer="51"/>
<rectangle x1="-4.88124375" y1="0.819009375" x2="-4.77644375" y2="0.832109375" layer="51"/>
<rectangle x1="-4.52744375" y1="0.819009375" x2="-4.42264375" y2="0.832109375" layer="51"/>
<rectangle x1="-4.34394375" y1="0.819009375" x2="-4.27844375" y2="0.832109375" layer="51"/>
<rectangle x1="-2.70594375" y1="0.819009375" x2="-2.62734375" y2="0.832109375" layer="51"/>
<rectangle x1="-2.54874375" y1="0.819009375" x2="-2.44384375" y2="0.832109375" layer="51"/>
<rectangle x1="-2.20804375" y1="0.819009375" x2="-2.10314375" y2="0.832109375" layer="51"/>
<rectangle x1="-1.40864375" y1="0.819009375" x2="-1.30384375" y2="0.832109375" layer="51"/>
<rectangle x1="-1.06794375" y1="0.819009375" x2="-0.96314375" y2="0.832109375" layer="51"/>
<rectangle x1="-0.88454375" y1="0.819009375" x2="-0.80584375" y2="0.832109375" layer="51"/>
<rectangle x1="0.77965625" y1="0.819009375" x2="0.85835625" y2="0.832109375" layer="51"/>
<rectangle x1="0.93695625" y1="0.819009375" x2="1.04175625" y2="0.832109375" layer="51"/>
<rectangle x1="1.27765625" y1="0.819009375" x2="1.38245625" y2="0.832109375" layer="51"/>
<rectangle x1="2.07695625" y1="0.819009375" x2="2.18185625" y2="0.832109375" layer="51"/>
<rectangle x1="2.41765625" y1="0.819009375" x2="2.52255625" y2="0.832109375" layer="51"/>
<rectangle x1="2.60115625" y1="0.819009375" x2="2.67975625" y2="0.832109375" layer="51"/>
<rectangle x1="4.25225625" y1="0.819009375" x2="4.31775625" y2="0.832109375" layer="51"/>
<rectangle x1="4.39635625" y1="0.819009375" x2="4.50125625" y2="0.832109375" layer="51"/>
<rectangle x1="4.75025625" y1="0.819009375" x2="4.85505625" y2="0.832109375" layer="51"/>
<rectangle x1="5.53645625" y1="0.819009375" x2="5.64125625" y2="0.832109375" layer="51"/>
<rectangle x1="5.89025625" y1="0.819009375" x2="5.99505625" y2="0.832109375" layer="51"/>
<rectangle x1="6.07375625" y1="0.819009375" x2="6.15235625" y2="0.832109375" layer="51"/>
<rectangle x1="7.37105625" y1="0.819009375" x2="7.47585625" y2="0.832109375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.832109375" x2="-7.39724375" y2="0.845215625" layer="51"/>
<rectangle x1="-6.19164375" y1="0.832109375" x2="-6.08684375" y2="0.845215625" layer="51"/>
<rectangle x1="-6.02124375" y1="0.832109375" x2="-5.91644375" y2="0.845215625" layer="51"/>
<rectangle x1="-5.66744375" y1="0.832109375" x2="-5.56264375" y2="0.845215625" layer="51"/>
<rectangle x1="-4.88124375" y1="0.832109375" x2="-4.77644375" y2="0.845215625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.832109375" x2="-4.42264375" y2="0.845215625" layer="51"/>
<rectangle x1="-4.35704375" y1="0.832109375" x2="-4.25224375" y2="0.845215625" layer="51"/>
<rectangle x1="-2.71904375" y1="0.832109375" x2="-2.61424375" y2="0.845215625" layer="51"/>
<rectangle x1="-2.54874375" y1="0.832109375" x2="-2.44384375" y2="0.845215625" layer="51"/>
<rectangle x1="-2.20804375" y1="0.832109375" x2="-2.10314375" y2="0.845215625" layer="51"/>
<rectangle x1="-1.40864375" y1="0.832109375" x2="-1.30384375" y2="0.845215625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.832109375" x2="-0.96314375" y2="0.845215625" layer="51"/>
<rectangle x1="-0.89764375" y1="0.832109375" x2="-0.79274375" y2="0.845215625" layer="51"/>
<rectangle x1="0.76655625" y1="0.832109375" x2="0.87145625" y2="0.845215625" layer="51"/>
<rectangle x1="0.93695625" y1="0.832109375" x2="1.04175625" y2="0.845215625" layer="51"/>
<rectangle x1="1.27765625" y1="0.832109375" x2="1.38245625" y2="0.845215625" layer="51"/>
<rectangle x1="2.07695625" y1="0.832109375" x2="2.18185625" y2="0.845215625" layer="51"/>
<rectangle x1="2.41765625" y1="0.832109375" x2="2.52255625" y2="0.845215625" layer="51"/>
<rectangle x1="2.58805625" y1="0.832109375" x2="2.69285625" y2="0.845215625" layer="51"/>
<rectangle x1="4.22605625" y1="0.832109375" x2="4.33085625" y2="0.845215625" layer="51"/>
<rectangle x1="4.39635625" y1="0.832109375" x2="4.50125625" y2="0.845215625" layer="51"/>
<rectangle x1="4.75025625" y1="0.832109375" x2="4.85505625" y2="0.845215625" layer="51"/>
<rectangle x1="5.53645625" y1="0.832109375" x2="5.64125625" y2="0.845215625" layer="51"/>
<rectangle x1="5.89025625" y1="0.832109375" x2="5.99505625" y2="0.845215625" layer="51"/>
<rectangle x1="6.06065625" y1="0.832109375" x2="6.16545625" y2="0.845215625" layer="51"/>
<rectangle x1="7.37105625" y1="0.832109375" x2="7.47585625" y2="0.845215625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.84521875" x2="-7.39724375" y2="0.85831875" layer="51"/>
<rectangle x1="-6.20474375" y1="0.84521875" x2="-6.07374375" y2="0.85831875" layer="51"/>
<rectangle x1="-6.02124375" y1="0.84521875" x2="-5.91644375" y2="0.85831875" layer="51"/>
<rectangle x1="-5.66744375" y1="0.84521875" x2="-5.56264375" y2="0.85831875" layer="51"/>
<rectangle x1="-4.88124375" y1="0.84521875" x2="-4.77644375" y2="0.85831875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.84521875" x2="-4.42264375" y2="0.85831875" layer="51"/>
<rectangle x1="-4.37014375" y1="0.84521875" x2="-4.23914375" y2="0.85831875" layer="51"/>
<rectangle x1="-2.73214375" y1="0.84521875" x2="-2.60114375" y2="0.85831875" layer="51"/>
<rectangle x1="-2.54874375" y1="0.84521875" x2="-2.44384375" y2="0.85831875" layer="51"/>
<rectangle x1="-2.20804375" y1="0.84521875" x2="-2.10314375" y2="0.85831875" layer="51"/>
<rectangle x1="-1.40864375" y1="0.84521875" x2="-1.30384375" y2="0.85831875" layer="51"/>
<rectangle x1="-1.06794375" y1="0.84521875" x2="-0.96314375" y2="0.85831875" layer="51"/>
<rectangle x1="-0.89764375" y1="0.84521875" x2="-0.77964375" y2="0.85831875" layer="51"/>
<rectangle x1="0.75345625" y1="0.84521875" x2="0.88455625" y2="0.85831875" layer="51"/>
<rectangle x1="0.93695625" y1="0.84521875" x2="1.04175625" y2="0.85831875" layer="51"/>
<rectangle x1="1.27765625" y1="0.84521875" x2="1.38245625" y2="0.85831875" layer="51"/>
<rectangle x1="2.07695625" y1="0.84521875" x2="2.18185625" y2="0.85831875" layer="51"/>
<rectangle x1="2.41765625" y1="0.84521875" x2="2.52255625" y2="0.85831875" layer="51"/>
<rectangle x1="2.58805625" y1="0.84521875" x2="2.70595625" y2="0.85831875" layer="51"/>
<rectangle x1="4.21295625" y1="0.84521875" x2="4.34395625" y2="0.85831875" layer="51"/>
<rectangle x1="4.39635625" y1="0.84521875" x2="4.50125625" y2="0.85831875" layer="51"/>
<rectangle x1="4.75025625" y1="0.84521875" x2="4.85505625" y2="0.85831875" layer="51"/>
<rectangle x1="5.53645625" y1="0.84521875" x2="5.64125625" y2="0.85831875" layer="51"/>
<rectangle x1="5.89025625" y1="0.84521875" x2="5.99505625" y2="0.85831875" layer="51"/>
<rectangle x1="6.04755625" y1="0.84521875" x2="6.17855625" y2="0.85831875" layer="51"/>
<rectangle x1="7.37105625" y1="0.84521875" x2="7.47585625" y2="0.85831875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.85831875" x2="-7.39724375" y2="0.87143125" layer="51"/>
<rectangle x1="-6.20474375" y1="0.85831875" x2="-6.06064375" y2="0.87143125" layer="51"/>
<rectangle x1="-6.02124375" y1="0.85831875" x2="-5.91644375" y2="0.87143125" layer="51"/>
<rectangle x1="-5.66744375" y1="0.85831875" x2="-5.56264375" y2="0.87143125" layer="51"/>
<rectangle x1="-4.88124375" y1="0.85831875" x2="-4.77644375" y2="0.87143125" layer="51"/>
<rectangle x1="-4.52744375" y1="0.85831875" x2="-4.42264375" y2="0.87143125" layer="51"/>
<rectangle x1="-4.38324375" y1="0.85831875" x2="-4.23914375" y2="0.87143125" layer="51"/>
<rectangle x1="-2.73214375" y1="0.85831875" x2="-2.60114375" y2="0.87143125" layer="51"/>
<rectangle x1="-2.54874375" y1="0.85831875" x2="-2.44384375" y2="0.87143125" layer="51"/>
<rectangle x1="-2.20804375" y1="0.85831875" x2="-2.10314375" y2="0.87143125" layer="51"/>
<rectangle x1="-1.40864375" y1="0.85831875" x2="-1.30384375" y2="0.87143125" layer="51"/>
<rectangle x1="-1.06794375" y1="0.85831875" x2="-0.96314375" y2="0.87143125" layer="51"/>
<rectangle x1="-0.91074375" y1="0.85831875" x2="-0.77964375" y2="0.87143125" layer="51"/>
<rectangle x1="0.75345625" y1="0.85831875" x2="0.88455625" y2="0.87143125" layer="51"/>
<rectangle x1="0.93695625" y1="0.85831875" x2="1.04175625" y2="0.87143125" layer="51"/>
<rectangle x1="1.27765625" y1="0.85831875" x2="1.38245625" y2="0.87143125" layer="51"/>
<rectangle x1="2.07695625" y1="0.85831875" x2="2.18185625" y2="0.87143125" layer="51"/>
<rectangle x1="2.41765625" y1="0.85831875" x2="2.52255625" y2="0.87143125" layer="51"/>
<rectangle x1="2.57495625" y1="0.85831875" x2="2.70595625" y2="0.87143125" layer="51"/>
<rectangle x1="4.21295625" y1="0.85831875" x2="4.35705625" y2="0.87143125" layer="51"/>
<rectangle x1="4.39635625" y1="0.85831875" x2="4.50125625" y2="0.87143125" layer="51"/>
<rectangle x1="4.75025625" y1="0.85831875" x2="4.85505625" y2="0.87143125" layer="51"/>
<rectangle x1="5.53645625" y1="0.85831875" x2="5.64125625" y2="0.87143125" layer="51"/>
<rectangle x1="5.89025625" y1="0.85831875" x2="5.99505625" y2="0.87143125" layer="51"/>
<rectangle x1="6.03445625" y1="0.85831875" x2="6.17855625" y2="0.87143125" layer="51"/>
<rectangle x1="7.37105625" y1="0.85831875" x2="7.47585625" y2="0.87143125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.87143125" x2="-7.39724375" y2="0.88453125" layer="51"/>
<rectangle x1="-6.19164375" y1="0.87143125" x2="-6.04754375" y2="0.88453125" layer="51"/>
<rectangle x1="-6.02124375" y1="0.87143125" x2="-5.91644375" y2="0.88453125" layer="51"/>
<rectangle x1="-5.66744375" y1="0.87143125" x2="-5.56264375" y2="0.88453125" layer="51"/>
<rectangle x1="-4.88124375" y1="0.87143125" x2="-4.77644375" y2="0.88453125" layer="51"/>
<rectangle x1="-4.52744375" y1="0.87143125" x2="-4.42264375" y2="0.88453125" layer="51"/>
<rectangle x1="-4.39634375" y1="0.87143125" x2="-4.25224375" y2="0.88453125" layer="51"/>
<rectangle x1="-2.71904375" y1="0.87143125" x2="-2.58804375" y2="0.88453125" layer="51"/>
<rectangle x1="-2.54874375" y1="0.87143125" x2="-2.44384375" y2="0.88453125" layer="51"/>
<rectangle x1="-2.20804375" y1="0.87143125" x2="-2.10314375" y2="0.88453125" layer="51"/>
<rectangle x1="-1.40864375" y1="0.87143125" x2="-1.30384375" y2="0.88453125" layer="51"/>
<rectangle x1="-1.06794375" y1="0.87143125" x2="-0.96314375" y2="0.88453125" layer="51"/>
<rectangle x1="-0.92384375" y1="0.87143125" x2="-0.79274375" y2="0.88453125" layer="51"/>
<rectangle x1="0.76655625" y1="0.87143125" x2="0.89765625" y2="0.88453125" layer="51"/>
<rectangle x1="0.93695625" y1="0.87143125" x2="1.04175625" y2="0.88453125" layer="51"/>
<rectangle x1="1.27765625" y1="0.87143125" x2="1.38245625" y2="0.88453125" layer="51"/>
<rectangle x1="2.07695625" y1="0.87143125" x2="2.18185625" y2="0.88453125" layer="51"/>
<rectangle x1="2.41765625" y1="0.87143125" x2="2.52255625" y2="0.88453125" layer="51"/>
<rectangle x1="2.56185625" y1="0.87143125" x2="2.69285625" y2="0.88453125" layer="51"/>
<rectangle x1="4.22605625" y1="0.87143125" x2="4.37015625" y2="0.88453125" layer="51"/>
<rectangle x1="4.39635625" y1="0.87143125" x2="4.50125625" y2="0.88453125" layer="51"/>
<rectangle x1="4.75025625" y1="0.87143125" x2="4.85505625" y2="0.88453125" layer="51"/>
<rectangle x1="5.53645625" y1="0.87143125" x2="5.64125625" y2="0.88453125" layer="51"/>
<rectangle x1="5.89025625" y1="0.87143125" x2="5.99505625" y2="0.88453125" layer="51"/>
<rectangle x1="6.02125625" y1="0.87143125" x2="6.16545625" y2="0.88453125" layer="51"/>
<rectangle x1="7.37105625" y1="0.87143125" x2="7.47585625" y2="0.88453125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.88453125" x2="-7.39724375" y2="0.89763125" layer="51"/>
<rectangle x1="-6.17854375" y1="0.88453125" x2="-6.04754375" y2="0.89763125" layer="51"/>
<rectangle x1="-6.02124375" y1="0.88453125" x2="-5.91644375" y2="0.89763125" layer="51"/>
<rectangle x1="-5.66744375" y1="0.88453125" x2="-5.56264375" y2="0.89763125" layer="51"/>
<rectangle x1="-4.88124375" y1="0.88453125" x2="-4.77644375" y2="0.89763125" layer="51"/>
<rectangle x1="-4.52744375" y1="0.88453125" x2="-4.42264375" y2="0.89763125" layer="51"/>
<rectangle x1="-4.39634375" y1="0.88453125" x2="-4.26534375" y2="0.89763125" layer="51"/>
<rectangle x1="-2.70594375" y1="0.88453125" x2="-2.57494375" y2="0.89763125" layer="51"/>
<rectangle x1="-2.54874375" y1="0.88453125" x2="-2.44384375" y2="0.89763125" layer="51"/>
<rectangle x1="-2.20804375" y1="0.88453125" x2="-2.10314375" y2="0.89763125" layer="51"/>
<rectangle x1="-1.40864375" y1="0.88453125" x2="-1.30384375" y2="0.89763125" layer="51"/>
<rectangle x1="-1.06794375" y1="0.88453125" x2="-0.96314375" y2="0.89763125" layer="51"/>
<rectangle x1="-0.93694375" y1="0.88453125" x2="-0.80584375" y2="0.89763125" layer="51"/>
<rectangle x1="0.77965625" y1="0.88453125" x2="0.91075625" y2="0.89763125" layer="51"/>
<rectangle x1="0.93695625" y1="0.88453125" x2="1.04175625" y2="0.89763125" layer="51"/>
<rectangle x1="1.27765625" y1="0.88453125" x2="1.38245625" y2="0.89763125" layer="51"/>
<rectangle x1="2.07695625" y1="0.88453125" x2="2.18185625" y2="0.89763125" layer="51"/>
<rectangle x1="2.41765625" y1="0.88453125" x2="2.52255625" y2="0.89763125" layer="51"/>
<rectangle x1="2.54875625" y1="0.88453125" x2="2.67975625" y2="0.89763125" layer="51"/>
<rectangle x1="4.23915625" y1="0.88453125" x2="4.37015625" y2="0.89763125" layer="51"/>
<rectangle x1="4.39635625" y1="0.88453125" x2="4.50125625" y2="0.89763125" layer="51"/>
<rectangle x1="4.75025625" y1="0.88453125" x2="4.85505625" y2="0.89763125" layer="51"/>
<rectangle x1="5.53645625" y1="0.88453125" x2="5.64125625" y2="0.89763125" layer="51"/>
<rectangle x1="5.89025625" y1="0.88453125" x2="5.99505625" y2="0.89763125" layer="51"/>
<rectangle x1="6.02125625" y1="0.88453125" x2="6.15235625" y2="0.89763125" layer="51"/>
<rectangle x1="7.37105625" y1="0.88453125" x2="7.47585625" y2="0.89763125" layer="51"/>
<rectangle x1="-7.50204375" y1="0.89763125" x2="-7.39724375" y2="0.9107375" layer="51"/>
<rectangle x1="-6.16544375" y1="0.89763125" x2="-6.03434375" y2="0.9107375" layer="51"/>
<rectangle x1="-6.02124375" y1="0.89763125" x2="-5.91644375" y2="0.9107375" layer="51"/>
<rectangle x1="-5.66744375" y1="0.89763125" x2="-5.56264375" y2="0.9107375" layer="51"/>
<rectangle x1="-4.88124375" y1="0.89763125" x2="-4.77644375" y2="0.9107375" layer="51"/>
<rectangle x1="-4.52744375" y1="0.89763125" x2="-4.42264375" y2="0.9107375" layer="51"/>
<rectangle x1="-4.40944375" y1="0.89763125" x2="-4.27844375" y2="0.9107375" layer="51"/>
<rectangle x1="-2.70594375" y1="0.89763125" x2="-2.56184375" y2="0.9107375" layer="51"/>
<rectangle x1="-2.54874375" y1="0.89763125" x2="-2.44384375" y2="0.9107375" layer="51"/>
<rectangle x1="-2.20804375" y1="0.89763125" x2="-2.10314375" y2="0.9107375" layer="51"/>
<rectangle x1="-1.40864375" y1="0.89763125" x2="-1.30384375" y2="0.9107375" layer="51"/>
<rectangle x1="-1.06794375" y1="0.89763125" x2="-0.96314375" y2="0.9107375" layer="51"/>
<rectangle x1="-0.95004375" y1="0.89763125" x2="-0.80584375" y2="0.9107375" layer="51"/>
<rectangle x1="0.77965625" y1="0.89763125" x2="0.92385625" y2="0.9107375" layer="51"/>
<rectangle x1="0.93695625" y1="0.89763125" x2="1.04175625" y2="0.9107375" layer="51"/>
<rectangle x1="1.27765625" y1="0.89763125" x2="1.38245625" y2="0.9107375" layer="51"/>
<rectangle x1="2.07695625" y1="0.89763125" x2="2.18185625" y2="0.9107375" layer="51"/>
<rectangle x1="2.41765625" y1="0.89763125" x2="2.52255625" y2="0.9107375" layer="51"/>
<rectangle x1="2.53565625" y1="0.89763125" x2="2.67975625" y2="0.9107375" layer="51"/>
<rectangle x1="4.25225625" y1="0.89763125" x2="4.38325625" y2="0.9107375" layer="51"/>
<rectangle x1="4.39635625" y1="0.89763125" x2="4.50125625" y2="0.9107375" layer="51"/>
<rectangle x1="4.75025625" y1="0.89763125" x2="4.85505625" y2="0.9107375" layer="51"/>
<rectangle x1="5.53645625" y1="0.89763125" x2="5.64125625" y2="0.9107375" layer="51"/>
<rectangle x1="5.89025625" y1="0.89763125" x2="5.99505625" y2="0.9107375" layer="51"/>
<rectangle x1="6.00815625" y1="0.89763125" x2="6.13925625" y2="0.9107375" layer="51"/>
<rectangle x1="7.37105625" y1="0.89763125" x2="7.47585625" y2="0.9107375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.910740625" x2="-7.39724375" y2="0.923840625" layer="51"/>
<rectangle x1="-6.15234375" y1="0.910740625" x2="-5.91644375" y2="0.923840625" layer="51"/>
<rectangle x1="-5.66744375" y1="0.910740625" x2="-5.56264375" y2="0.923840625" layer="51"/>
<rectangle x1="-4.88124375" y1="0.910740625" x2="-4.77644375" y2="0.923840625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.910740625" x2="-4.29154375" y2="0.923840625" layer="51"/>
<rectangle x1="-2.69284375" y1="0.910740625" x2="-2.44384375" y2="0.923840625" layer="51"/>
<rectangle x1="-2.20804375" y1="0.910740625" x2="-2.10314375" y2="0.923840625" layer="51"/>
<rectangle x1="-1.40864375" y1="0.910740625" x2="-1.30384375" y2="0.923840625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.910740625" x2="-0.96314375" y2="0.923840625" layer="51"/>
<rectangle x1="-0.95004375" y1="0.910740625" x2="-0.81894375" y2="0.923840625" layer="51"/>
<rectangle x1="0.79275625" y1="0.910740625" x2="1.04175625" y2="0.923840625" layer="51"/>
<rectangle x1="1.27765625" y1="0.910740625" x2="1.38245625" y2="0.923840625" layer="51"/>
<rectangle x1="2.07695625" y1="0.910740625" x2="2.18185625" y2="0.923840625" layer="51"/>
<rectangle x1="2.41765625" y1="0.910740625" x2="2.52255625" y2="0.923840625" layer="51"/>
<rectangle x1="2.53565625" y1="0.910740625" x2="2.66665625" y2="0.923840625" layer="51"/>
<rectangle x1="4.26535625" y1="0.910740625" x2="4.50125625" y2="0.923840625" layer="51"/>
<rectangle x1="4.75025625" y1="0.910740625" x2="4.85505625" y2="0.923840625" layer="51"/>
<rectangle x1="5.53645625" y1="0.910740625" x2="5.64125625" y2="0.923840625" layer="51"/>
<rectangle x1="5.89025625" y1="0.910740625" x2="6.12615625" y2="0.923840625" layer="51"/>
<rectangle x1="7.37105625" y1="0.910740625" x2="7.47585625" y2="0.923840625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.923840625" x2="-7.39724375" y2="0.936946875" layer="51"/>
<rectangle x1="-6.13924375" y1="0.923840625" x2="-5.91644375" y2="0.936946875" layer="51"/>
<rectangle x1="-5.66744375" y1="0.923840625" x2="-5.56264375" y2="0.936946875" layer="51"/>
<rectangle x1="-4.88124375" y1="0.923840625" x2="-4.77644375" y2="0.936946875" layer="51"/>
<rectangle x1="-4.52744375" y1="0.923840625" x2="-4.29154375" y2="0.936946875" layer="51"/>
<rectangle x1="-2.67974375" y1="0.923840625" x2="-2.44384375" y2="0.936946875" layer="51"/>
<rectangle x1="-2.20804375" y1="0.923840625" x2="-2.10314375" y2="0.936946875" layer="51"/>
<rectangle x1="-1.40864375" y1="0.923840625" x2="-1.30384375" y2="0.936946875" layer="51"/>
<rectangle x1="-1.06794375" y1="0.923840625" x2="-0.83214375" y2="0.936946875" layer="51"/>
<rectangle x1="0.80585625" y1="0.923840625" x2="1.04175625" y2="0.936946875" layer="51"/>
<rectangle x1="1.27765625" y1="0.923840625" x2="1.38245625" y2="0.936946875" layer="51"/>
<rectangle x1="2.07695625" y1="0.923840625" x2="2.18185625" y2="0.936946875" layer="51"/>
<rectangle x1="2.41765625" y1="0.923840625" x2="2.65355625" y2="0.936946875" layer="51"/>
<rectangle x1="4.27845625" y1="0.923840625" x2="4.50125625" y2="0.936946875" layer="51"/>
<rectangle x1="4.75025625" y1="0.923840625" x2="4.85505625" y2="0.936946875" layer="51"/>
<rectangle x1="5.53645625" y1="0.923840625" x2="5.64125625" y2="0.936946875" layer="51"/>
<rectangle x1="5.89025625" y1="0.923840625" x2="6.12615625" y2="0.936946875" layer="51"/>
<rectangle x1="7.37105625" y1="0.923840625" x2="7.47585625" y2="0.936946875" layer="51"/>
<rectangle x1="-7.50204375" y1="0.93695" x2="-7.39724375" y2="0.95005" layer="51"/>
<rectangle x1="-6.13924375" y1="0.93695" x2="-5.91644375" y2="0.95005" layer="51"/>
<rectangle x1="-5.66744375" y1="0.93695" x2="-5.56264375" y2="0.95005" layer="51"/>
<rectangle x1="-4.88124375" y1="0.93695" x2="-4.77644375" y2="0.95005" layer="51"/>
<rectangle x1="-4.52744375" y1="0.93695" x2="-4.30464375" y2="0.95005" layer="51"/>
<rectangle x1="-2.66664375" y1="0.93695" x2="-2.44384375" y2="0.95005" layer="51"/>
<rectangle x1="-2.20804375" y1="0.93695" x2="-2.10314375" y2="0.95005" layer="51"/>
<rectangle x1="-1.40864375" y1="0.93695" x2="-1.30384375" y2="0.95005" layer="51"/>
<rectangle x1="-1.06794375" y1="0.93695" x2="-0.84524375" y2="0.95005" layer="51"/>
<rectangle x1="0.81905625" y1="0.93695" x2="1.04175625" y2="0.95005" layer="51"/>
<rectangle x1="1.27765625" y1="0.93695" x2="1.38245625" y2="0.95005" layer="51"/>
<rectangle x1="2.07695625" y1="0.93695" x2="2.18185625" y2="0.95005" layer="51"/>
<rectangle x1="2.41765625" y1="0.93695" x2="2.64045625" y2="0.95005" layer="51"/>
<rectangle x1="4.27845625" y1="0.93695" x2="4.50125625" y2="0.95005" layer="51"/>
<rectangle x1="4.75025625" y1="0.93695" x2="4.85505625" y2="0.95005" layer="51"/>
<rectangle x1="5.53645625" y1="0.93695" x2="5.64125625" y2="0.95005" layer="51"/>
<rectangle x1="5.89025625" y1="0.93695" x2="6.11305625" y2="0.95005" layer="51"/>
<rectangle x1="7.37105625" y1="0.93695" x2="7.47585625" y2="0.95005" layer="51"/>
<rectangle x1="-7.50204375" y1="0.95005" x2="-7.39724375" y2="0.96315" layer="51"/>
<rectangle x1="-6.12614375" y1="0.95005" x2="-5.91644375" y2="0.96315" layer="51"/>
<rectangle x1="-5.68054375" y1="0.95005" x2="-5.56264375" y2="0.96315" layer="51"/>
<rectangle x1="-4.88124375" y1="0.95005" x2="-4.76334375" y2="0.96315" layer="51"/>
<rectangle x1="-4.52744375" y1="0.95005" x2="-4.31774375" y2="0.96315" layer="51"/>
<rectangle x1="-2.65354375" y1="0.95005" x2="-2.44384375" y2="0.96315" layer="51"/>
<rectangle x1="-2.20804375" y1="0.95005" x2="-2.10314375" y2="0.96315" layer="51"/>
<rectangle x1="-1.40864375" y1="0.95005" x2="-1.30384375" y2="0.96315" layer="51"/>
<rectangle x1="-1.06794375" y1="0.95005" x2="-0.85834375" y2="0.96315" layer="51"/>
<rectangle x1="0.83215625" y1="0.95005" x2="1.04175625" y2="0.96315" layer="51"/>
<rectangle x1="1.27765625" y1="0.95005" x2="1.38245625" y2="0.96315" layer="51"/>
<rectangle x1="2.07695625" y1="0.95005" x2="2.18185625" y2="0.96315" layer="51"/>
<rectangle x1="2.41765625" y1="0.95005" x2="2.62735625" y2="0.96315" layer="51"/>
<rectangle x1="4.29155625" y1="0.95005" x2="4.50125625" y2="0.96315" layer="51"/>
<rectangle x1="4.73715625" y1="0.95005" x2="4.85505625" y2="0.96315" layer="51"/>
<rectangle x1="5.53645625" y1="0.95005" x2="5.65435625" y2="0.96315" layer="51"/>
<rectangle x1="5.89025625" y1="0.95005" x2="6.09995625" y2="0.96315" layer="51"/>
<rectangle x1="7.37105625" y1="0.95005" x2="7.47585625" y2="0.96315" layer="51"/>
<rectangle x1="-7.50204375" y1="0.96315" x2="-7.39724375" y2="0.97625625" layer="51"/>
<rectangle x1="-6.11304375" y1="0.96315" x2="-5.91644375" y2="0.97625625" layer="51"/>
<rectangle x1="-5.68054375" y1="0.96315" x2="-5.57574375" y2="0.97625625" layer="51"/>
<rectangle x1="-4.86814375" y1="0.96315" x2="-4.76334375" y2="0.97625625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.96315" x2="-4.33084375" y2="0.97625625" layer="51"/>
<rectangle x1="-2.65354375" y1="0.96315" x2="-2.44384375" y2="0.97625625" layer="51"/>
<rectangle x1="-2.20804375" y1="0.96315" x2="-2.10314375" y2="0.97625625" layer="51"/>
<rectangle x1="-1.40864375" y1="0.96315" x2="-1.30384375" y2="0.97625625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.96315" x2="-0.85834375" y2="0.97625625" layer="51"/>
<rectangle x1="0.83215625" y1="0.96315" x2="1.04175625" y2="0.97625625" layer="51"/>
<rectangle x1="1.27765625" y1="0.96315" x2="1.38245625" y2="0.97625625" layer="51"/>
<rectangle x1="2.07695625" y1="0.96315" x2="2.18185625" y2="0.97625625" layer="51"/>
<rectangle x1="2.41765625" y1="0.96315" x2="2.62735625" y2="0.97625625" layer="51"/>
<rectangle x1="4.30465625" y1="0.96315" x2="4.50125625" y2="0.97625625" layer="51"/>
<rectangle x1="4.73715625" y1="0.96315" x2="4.84195625" y2="0.97625625" layer="51"/>
<rectangle x1="5.54955625" y1="0.96315" x2="5.65435625" y2="0.97625625" layer="51"/>
<rectangle x1="5.89025625" y1="0.96315" x2="6.08685625" y2="0.97625625" layer="51"/>
<rectangle x1="7.37105625" y1="0.96315" x2="7.47585625" y2="0.97625625" layer="51"/>
<rectangle x1="-7.50204375" y1="0.976259375" x2="-7.39724375" y2="0.989359375" layer="51"/>
<rectangle x1="-6.09994375" y1="0.976259375" x2="-5.91644375" y2="0.989359375" layer="51"/>
<rectangle x1="-5.68054375" y1="0.976259375" x2="-5.57574375" y2="0.989359375" layer="51"/>
<rectangle x1="-4.86814375" y1="0.976259375" x2="-4.76334375" y2="0.989359375" layer="51"/>
<rectangle x1="-4.52744375" y1="0.976259375" x2="-4.34394375" y2="0.989359375" layer="51"/>
<rectangle x1="-2.64044375" y1="0.976259375" x2="-2.44384375" y2="0.989359375" layer="51"/>
<rectangle x1="-2.22114375" y1="0.976259375" x2="-2.10314375" y2="0.989359375" layer="51"/>
<rectangle x1="-1.40864375" y1="0.976259375" x2="-1.29074375" y2="0.989359375" layer="51"/>
<rectangle x1="-1.06794375" y1="0.976259375" x2="-0.87144375" y2="0.989359375" layer="51"/>
<rectangle x1="0.84525625" y1="0.976259375" x2="1.04175625" y2="0.989359375" layer="51"/>
<rectangle x1="1.26455625" y1="0.976259375" x2="1.38245625" y2="0.989359375" layer="51"/>
<rectangle x1="2.07695625" y1="0.976259375" x2="2.19495625" y2="0.989359375" layer="51"/>
<rectangle x1="2.41765625" y1="0.976259375" x2="2.61425625" y2="0.989359375" layer="51"/>
<rectangle x1="4.31775625" y1="0.976259375" x2="4.50125625" y2="0.989359375" layer="51"/>
<rectangle x1="4.73715625" y1="0.976259375" x2="4.84195625" y2="0.989359375" layer="51"/>
<rectangle x1="5.54955625" y1="0.976259375" x2="5.65435625" y2="0.989359375" layer="51"/>
<rectangle x1="5.89025625" y1="0.976259375" x2="6.07375625" y2="0.989359375" layer="51"/>
<rectangle x1="7.37105625" y1="0.976259375" x2="7.47585625" y2="0.989359375" layer="51"/>
<rectangle x1="-7.50204375" y1="0.989359375" x2="-7.39724375" y2="1.002465625" layer="51"/>
<rectangle x1="-6.09994375" y1="0.989359375" x2="-5.91644375" y2="1.002465625" layer="51"/>
<rectangle x1="-5.69364375" y1="0.989359375" x2="-5.57574375" y2="1.002465625" layer="51"/>
<rectangle x1="-4.86814375" y1="0.989359375" x2="-4.75024375" y2="1.002465625" layer="51"/>
<rectangle x1="-4.52744375" y1="0.989359375" x2="-4.34394375" y2="1.002465625" layer="51"/>
<rectangle x1="-2.62734375" y1="0.989359375" x2="-2.44384375" y2="1.002465625" layer="51"/>
<rectangle x1="-2.22114375" y1="0.989359375" x2="-2.10314375" y2="1.002465625" layer="51"/>
<rectangle x1="-1.39554375" y1="0.989359375" x2="-1.29074375" y2="1.002465625" layer="51"/>
<rectangle x1="-1.06794375" y1="0.989359375" x2="-0.88454375" y2="1.002465625" layer="51"/>
<rectangle x1="0.85835625" y1="0.989359375" x2="1.04175625" y2="1.002465625" layer="51"/>
<rectangle x1="1.26455625" y1="0.989359375" x2="1.38245625" y2="1.002465625" layer="51"/>
<rectangle x1="2.09005625" y1="0.989359375" x2="2.19495625" y2="1.002465625" layer="51"/>
<rectangle x1="2.41765625" y1="0.989359375" x2="2.60115625" y2="1.002465625" layer="51"/>
<rectangle x1="4.33085625" y1="0.989359375" x2="4.50125625" y2="1.002465625" layer="51"/>
<rectangle x1="4.72395625" y1="0.989359375" x2="4.84195625" y2="1.002465625" layer="51"/>
<rectangle x1="5.54955625" y1="0.989359375" x2="5.66745625" y2="1.002465625" layer="51"/>
<rectangle x1="5.89025625" y1="0.989359375" x2="6.07375625" y2="1.002465625" layer="51"/>
<rectangle x1="7.37105625" y1="0.989359375" x2="7.47585625" y2="1.002465625" layer="51"/>
<rectangle x1="-7.50204375" y1="1.00246875" x2="-7.39724375" y2="1.01556875" layer="51"/>
<rectangle x1="-6.08684375" y1="1.00246875" x2="-5.91644375" y2="1.01556875" layer="51"/>
<rectangle x1="-5.69364375" y1="1.00246875" x2="-5.57574375" y2="1.01556875" layer="51"/>
<rectangle x1="-4.86814375" y1="1.00246875" x2="-4.75024375" y2="1.01556875" layer="51"/>
<rectangle x1="-4.52744375" y1="1.00246875" x2="-4.35704375" y2="1.01556875" layer="51"/>
<rectangle x1="-2.61424375" y1="1.00246875" x2="-2.44384375" y2="1.01556875" layer="51"/>
<rectangle x1="-2.23424375" y1="1.00246875" x2="-2.11624375" y2="1.01556875" layer="51"/>
<rectangle x1="-1.39554375" y1="1.00246875" x2="-1.27764375" y2="1.01556875" layer="51"/>
<rectangle x1="-1.06794375" y1="1.00246875" x2="-0.89764375" y2="1.01556875" layer="51"/>
<rectangle x1="0.87145625" y1="1.00246875" x2="1.04175625" y2="1.01556875" layer="51"/>
<rectangle x1="1.25145625" y1="1.00246875" x2="1.36935625" y2="1.01556875" layer="51"/>
<rectangle x1="2.09005625" y1="1.00246875" x2="2.20805625" y2="1.01556875" layer="51"/>
<rectangle x1="2.41765625" y1="1.00246875" x2="2.58805625" y2="1.01556875" layer="51"/>
<rectangle x1="4.33085625" y1="1.00246875" x2="4.50125625" y2="1.01556875" layer="51"/>
<rectangle x1="4.72395625" y1="1.00246875" x2="4.84195625" y2="1.01556875" layer="51"/>
<rectangle x1="5.54955625" y1="1.00246875" x2="5.66745625" y2="1.01556875" layer="51"/>
<rectangle x1="5.89025625" y1="1.00246875" x2="6.06065625" y2="1.01556875" layer="51"/>
<rectangle x1="7.37105625" y1="1.00246875" x2="7.47585625" y2="1.01556875" layer="51"/>
<rectangle x1="-7.50204375" y1="1.01556875" x2="-7.39724375" y2="1.02868125" layer="51"/>
<rectangle x1="-6.07374375" y1="1.01556875" x2="-5.91644375" y2="1.02868125" layer="51"/>
<rectangle x1="-5.70674375" y1="1.01556875" x2="-5.58884375" y2="1.02868125" layer="51"/>
<rectangle x1="-4.85504375" y1="1.01556875" x2="-4.73704375" y2="1.02868125" layer="51"/>
<rectangle x1="-4.52744375" y1="1.01556875" x2="-4.37014375" y2="1.02868125" layer="51"/>
<rectangle x1="-2.60114375" y1="1.01556875" x2="-2.44384375" y2="1.02868125" layer="51"/>
<rectangle x1="-2.24734375" y1="1.01556875" x2="-2.11624375" y2="1.02868125" layer="51"/>
<rectangle x1="-1.39554375" y1="1.01556875" x2="-1.26454375" y2="1.02868125" layer="51"/>
<rectangle x1="-1.06794375" y1="1.01556875" x2="-0.89764375" y2="1.02868125" layer="51"/>
<rectangle x1="0.88455625" y1="1.01556875" x2="1.04175625" y2="1.02868125" layer="51"/>
<rectangle x1="1.23835625" y1="1.01556875" x2="1.36935625" y2="1.02868125" layer="51"/>
<rectangle x1="2.09005625" y1="1.01556875" x2="2.22115625" y2="1.02868125" layer="51"/>
<rectangle x1="2.41765625" y1="1.01556875" x2="2.58805625" y2="1.02868125" layer="51"/>
<rectangle x1="4.34395625" y1="1.01556875" x2="4.50125625" y2="1.02868125" layer="51"/>
<rectangle x1="4.71085625" y1="1.01556875" x2="4.82885625" y2="1.02868125" layer="51"/>
<rectangle x1="5.56265625" y1="1.01556875" x2="5.68055625" y2="1.02868125" layer="51"/>
<rectangle x1="5.89025625" y1="1.01556875" x2="6.04755625" y2="1.02868125" layer="51"/>
<rectangle x1="7.37105625" y1="1.01556875" x2="7.47585625" y2="1.02868125" layer="51"/>
<rectangle x1="-7.50204375" y1="1.02868125" x2="-7.39724375" y2="1.04178125" layer="51"/>
<rectangle x1="-6.06064375" y1="1.02868125" x2="-5.91644375" y2="1.04178125" layer="51"/>
<rectangle x1="-5.71984375" y1="1.02868125" x2="-5.58884375" y2="1.04178125" layer="51"/>
<rectangle x1="-4.85504375" y1="1.02868125" x2="-4.72394375" y2="1.04178125" layer="51"/>
<rectangle x1="-4.52744375" y1="1.02868125" x2="-4.38324375" y2="1.04178125" layer="51"/>
<rectangle x1="-2.60114375" y1="1.02868125" x2="-2.44384375" y2="1.04178125" layer="51"/>
<rectangle x1="-2.26044375" y1="1.02868125" x2="-2.12944375" y2="1.04178125" layer="51"/>
<rectangle x1="-1.38244375" y1="1.02868125" x2="-1.25144375" y2="1.04178125" layer="51"/>
<rectangle x1="-1.06794375" y1="1.02868125" x2="-0.91074375" y2="1.04178125" layer="51"/>
<rectangle x1="0.88455625" y1="1.02868125" x2="1.04175625" y2="1.04178125" layer="51"/>
<rectangle x1="1.22525625" y1="1.02868125" x2="1.35625625" y2="1.04178125" layer="51"/>
<rectangle x1="2.10315625" y1="1.02868125" x2="2.23425625" y2="1.04178125" layer="51"/>
<rectangle x1="2.41765625" y1="1.02868125" x2="2.57495625" y2="1.04178125" layer="51"/>
<rectangle x1="4.35705625" y1="1.02868125" x2="4.50125625" y2="1.04178125" layer="51"/>
<rectangle x1="4.69775625" y1="1.02868125" x2="4.82885625" y2="1.04178125" layer="51"/>
<rectangle x1="5.56265625" y1="1.02868125" x2="5.69365625" y2="1.04178125" layer="51"/>
<rectangle x1="5.89025625" y1="1.02868125" x2="6.03445625" y2="1.04178125" layer="51"/>
<rectangle x1="7.37105625" y1="1.02868125" x2="7.47585625" y2="1.04178125" layer="51"/>
<rectangle x1="-7.50204375" y1="1.04178125" x2="-7.39724375" y2="1.05488125" layer="51"/>
<rectangle x1="-6.06064375" y1="1.04178125" x2="-5.91644375" y2="1.05488125" layer="51"/>
<rectangle x1="-5.74614375" y1="1.04178125" x2="-5.60194375" y2="1.05488125" layer="51"/>
<rectangle x1="-4.84194375" y1="1.04178125" x2="-4.69774375" y2="1.05488125" layer="51"/>
<rectangle x1="-4.52744375" y1="1.04178125" x2="-4.38324375" y2="1.05488125" layer="51"/>
<rectangle x1="-2.58804375" y1="1.04178125" x2="-2.44384375" y2="1.05488125" layer="51"/>
<rectangle x1="-2.27354375" y1="1.04178125" x2="-2.12944375" y2="1.05488125" layer="51"/>
<rectangle x1="-1.38244375" y1="1.04178125" x2="-1.23834375" y2="1.05488125" layer="51"/>
<rectangle x1="-1.06794375" y1="1.04178125" x2="-0.92384375" y2="1.05488125" layer="51"/>
<rectangle x1="0.89765625" y1="1.04178125" x2="1.04175625" y2="1.05488125" layer="51"/>
<rectangle x1="1.21215625" y1="1.04178125" x2="1.35625625" y2="1.05488125" layer="51"/>
<rectangle x1="2.10315625" y1="1.04178125" x2="2.24735625" y2="1.05488125" layer="51"/>
<rectangle x1="2.41765625" y1="1.04178125" x2="2.56185625" y2="1.05488125" layer="51"/>
<rectangle x1="4.37015625" y1="1.04178125" x2="4.50125625" y2="1.05488125" layer="51"/>
<rectangle x1="4.67155625" y1="1.04178125" x2="4.81575625" y2="1.05488125" layer="51"/>
<rectangle x1="5.57575625" y1="1.04178125" x2="5.71995625" y2="1.05488125" layer="51"/>
<rectangle x1="5.89025625" y1="1.04178125" x2="6.03445625" y2="1.05488125" layer="51"/>
<rectangle x1="7.37105625" y1="1.04178125" x2="7.47585625" y2="1.05488125" layer="51"/>
<rectangle x1="-7.50204375" y1="1.05488125" x2="-7.39724375" y2="1.0679875" layer="51"/>
<rectangle x1="-6.04754375" y1="1.05488125" x2="-5.91644375" y2="1.0679875" layer="51"/>
<rectangle x1="-5.77234375" y1="1.05488125" x2="-5.60194375" y2="1.0679875" layer="51"/>
<rectangle x1="-4.84194375" y1="1.05488125" x2="-4.67154375" y2="1.0679875" layer="51"/>
<rectangle x1="-4.52744375" y1="1.05488125" x2="-4.39634375" y2="1.0679875" layer="51"/>
<rectangle x1="-2.57494375" y1="1.05488125" x2="-2.44384375" y2="1.0679875" layer="51"/>
<rectangle x1="-2.29974375" y1="1.05488125" x2="-2.14254375" y2="1.0679875" layer="51"/>
<rectangle x1="-1.36934375" y1="1.05488125" x2="-1.21214375" y2="1.0679875" layer="51"/>
<rectangle x1="-1.06794375" y1="1.05488125" x2="-0.93694375" y2="1.0679875" layer="51"/>
<rectangle x1="0.91075625" y1="1.05488125" x2="1.04175625" y2="1.0679875" layer="51"/>
<rectangle x1="1.18595625" y1="1.05488125" x2="1.34315625" y2="1.0679875" layer="51"/>
<rectangle x1="2.11635625" y1="1.05488125" x2="2.27355625" y2="1.0679875" layer="51"/>
<rectangle x1="2.41765625" y1="1.05488125" x2="2.54875625" y2="1.0679875" layer="51"/>
<rectangle x1="4.37015625" y1="1.05488125" x2="4.50125625" y2="1.0679875" layer="51"/>
<rectangle x1="4.64535625" y1="1.05488125" x2="4.81575625" y2="1.0679875" layer="51"/>
<rectangle x1="5.57575625" y1="1.05488125" x2="5.74615625" y2="1.0679875" layer="51"/>
<rectangle x1="5.89025625" y1="1.05488125" x2="6.02125625" y2="1.0679875" layer="51"/>
<rectangle x1="7.37105625" y1="1.05488125" x2="7.47585625" y2="1.0679875" layer="51"/>
<rectangle x1="-7.50204375" y1="1.067990625" x2="-5.61504375" y2="1.081090625" layer="51"/>
<rectangle x1="-4.82884375" y1="1.067990625" x2="-2.14254375" y2="1.081090625" layer="51"/>
<rectangle x1="-1.35624375" y1="1.067990625" x2="1.34315625" y2="1.081090625" layer="51"/>
<rectangle x1="2.12945625" y1="1.067990625" x2="4.80265625" y2="1.081090625" layer="51"/>
<rectangle x1="5.58885625" y1="1.067990625" x2="7.47585625" y2="1.081090625" layer="51"/>
<rectangle x1="-7.50204375" y1="1.081090625" x2="-5.62814375" y2="1.094196875" layer="51"/>
<rectangle x1="-4.81574375" y1="1.081090625" x2="-2.15564375" y2="1.094196875" layer="51"/>
<rectangle x1="-1.35624375" y1="1.081090625" x2="1.33005625" y2="1.094196875" layer="51"/>
<rectangle x1="2.12945625" y1="1.081090625" x2="4.78955625" y2="1.094196875" layer="51"/>
<rectangle x1="5.60195625" y1="1.081090625" x2="7.47585625" y2="1.094196875" layer="51"/>
<rectangle x1="-7.50204375" y1="1.0942" x2="-5.64124375" y2="1.1073" layer="51"/>
<rectangle x1="-4.80264375" y1="1.0942" x2="-2.16874375" y2="1.1073" layer="51"/>
<rectangle x1="-1.34314375" y1="1.0942" x2="1.31695625" y2="1.1073" layer="51"/>
<rectangle x1="2.14255625" y1="1.0942" x2="4.77645625" y2="1.1073" layer="51"/>
<rectangle x1="5.61505625" y1="1.0942" x2="7.47585625" y2="1.1073" layer="51"/>
<rectangle x1="-7.50204375" y1="1.1073" x2="-5.65434375" y2="1.1204" layer="51"/>
<rectangle x1="-4.78954375" y1="1.1073" x2="-2.18184375" y2="1.1204" layer="51"/>
<rectangle x1="-1.33004375" y1="1.1073" x2="1.30385625" y2="1.1204" layer="51"/>
<rectangle x1="2.15565625" y1="1.1073" x2="4.76335625" y2="1.1204" layer="51"/>
<rectangle x1="5.62815625" y1="1.1073" x2="7.47585625" y2="1.1204" layer="51"/>
<rectangle x1="-7.50204375" y1="1.1204" x2="-5.66744375" y2="1.13350625" layer="51"/>
<rectangle x1="-4.77644375" y1="1.1204" x2="-2.20804375" y2="1.13350625" layer="51"/>
<rectangle x1="-1.30384375" y1="1.1204" x2="1.27765625" y2="1.13350625" layer="51"/>
<rectangle x1="2.18185625" y1="1.1204" x2="4.75025625" y2="1.13350625" layer="51"/>
<rectangle x1="5.64125625" y1="1.1204" x2="7.47585625" y2="1.13350625" layer="51"/>
<rectangle x1="-7.44964375" y1="1.133509375" x2="-5.69364375" y2="1.146609375" layer="51"/>
<rectangle x1="-4.75024375" y1="1.133509375" x2="-2.22114375" y2="1.146609375" layer="51"/>
<rectangle x1="-1.29074375" y1="1.133509375" x2="1.26455625" y2="1.146609375" layer="51"/>
<rectangle x1="2.19495625" y1="1.133509375" x2="4.73715625" y2="1.146609375" layer="51"/>
<rectangle x1="5.66745625" y1="1.133509375" x2="7.43655625" y2="1.146609375" layer="51"/>
<rectangle x1="-7.44964375" y1="1.146609375" x2="-5.71984375" y2="1.159715625" layer="51"/>
<rectangle x1="-4.72394375" y1="1.146609375" x2="-2.24734375" y2="1.159715625" layer="51"/>
<rectangle x1="-1.26454375" y1="1.146609375" x2="1.23835625" y2="1.159715625" layer="51"/>
<rectangle x1="2.22115625" y1="1.146609375" x2="4.71085625" y2="1.159715625" layer="51"/>
<rectangle x1="5.69365625" y1="1.146609375" x2="7.43655625" y2="1.159715625" layer="51"/>
<rectangle x1="-7.44964375" y1="1.15971875" x2="-5.74614375" y2="1.17281875" layer="51"/>
<rectangle x1="-4.69774375" y1="1.15971875" x2="-2.28664375" y2="1.17281875" layer="51"/>
<rectangle x1="-1.22524375" y1="1.15971875" x2="1.19905625" y2="1.17281875" layer="51"/>
<rectangle x1="2.26045625" y1="1.15971875" x2="4.67155625" y2="1.17281875" layer="51"/>
<rectangle x1="5.71995625" y1="1.15971875" x2="7.43655625" y2="1.17281875" layer="51"/>
</package>
<package name="KK6410-2021_2-PIN">
<description>&lt;h1&gt;KK&amp;reg; 6410 Series 2-Pin Vertical Header with Friction Lock&lt;/h1&gt;
&lt;h2&gt;Single Row, 0.100" (2.54mm) Pitch&lt;/h2&gt;</description>
<pad name="1" x="-1.27" y="0" drill="1.2" shape="square"/>
<pad name="2" x="1.27" y="0" drill="1.2"/>
<wire x1="-2.54" y1="-2.921" x2="-2.54" y2="2.921" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="-1.27" y2="2.921" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.921" x2="1.27" y2="2.921" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.921" x2="2.54" y2="2.921" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.921" x2="2.54" y2="-2.921" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.921" x2="-2.54" y2="-2.921" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.921" x2="-1.27" y2="1.9304" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.9304" x2="1.27" y2="1.9304" width="0.127" layer="21"/>
<wire x1="1.27" y1="1.9304" x2="1.27" y2="2.921" width="0.127" layer="21"/>
<text x="-2.54" y="3.81" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KK6410-2031_3-PIN">
<description>&lt;h1&gt;KK&amp;reg; 6410 Series 3-Pin Vertical Header with Fricton Lock&lt;/h1&gt;
&lt;h2&gt;Single Row, 0.100" (2.54mm) Pitch&lt;/h2&gt;</description>
<pad name="1" x="-2.54" y="0" drill="1.2" shape="square"/>
<pad name="2" x="0" y="0" drill="1.2"/>
<pad name="3" x="2.54" y="0" drill="1.2"/>
<wire x1="-3.81" y1="-2.921" x2="-3.81" y2="2.921" width="0.127" layer="21"/>
<wire x1="-3.81" y1="2.921" x2="-2.54" y2="2.921" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="2.54" y2="2.921" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.921" x2="3.81" y2="2.921" width="0.127" layer="21"/>
<wire x1="3.81" y1="2.921" x2="3.81" y2="-2.921" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.921" x2="-3.81" y2="-2.921" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="-2.54" y2="1.9304" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.9304" x2="2.54" y2="1.9304" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.9304" x2="2.54" y2="2.921" width="0.127" layer="21"/>
<text x="-3.81" y="3.81" size="1.27" layer="21">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="KK6410-2061_6-PIN">
<description>&lt;h1&gt;KK 6410 Series 6-Pin Vertical Header with Friction Lock&lt;/h1&gt;
&lt;h2&gt;Single Row, 0.100" (2.54mm) Pitch&lt;/h2&gt;</description>
<pad name="1" x="-6.35" y="0" drill="1.2" shape="square"/>
<pad name="2" x="-3.81" y="0" drill="1.2"/>
<pad name="3" x="-1.27" y="0" drill="1.2"/>
<wire x1="-7.62" y1="-2.921" x2="-7.62" y2="2.921" width="0.127" layer="21"/>
<wire x1="-7.62" y1="2.921" x2="-6.35" y2="2.921" width="0.127" layer="21"/>
<wire x1="-6.35" y1="2.921" x2="6.35" y2="2.921" width="0.127" layer="21"/>
<wire x1="6.35" y1="2.921" x2="7.62" y2="2.921" width="0.127" layer="21"/>
<wire x1="7.62" y1="2.921" x2="7.62" y2="-2.921" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.921" x2="-7.62" y2="-2.921" width="0.127" layer="21"/>
<wire x1="-6.35" y1="2.921" x2="-6.35" y2="1.9304" width="0.127" layer="21"/>
<wire x1="-6.35" y1="1.9304" x2="6.35" y2="1.9304" width="0.127" layer="21"/>
<wire x1="6.35" y1="1.9304" x2="6.35" y2="2.921" width="0.127" layer="21"/>
<pad name="4" x="1.27" y="0" drill="1.2"/>
<pad name="5" x="3.81" y="0" drill="1.2"/>
<text x="-7.62" y="3.81" size="1.27" layer="21">&gt;NAME</text>
<text x="-7.62" y="-5.08" size="1.27" layer="21">&gt;VALUE</text>
<pad name="6" x="6.35" y="0" drill="1.2"/>
</package>
<package name="734120110">
<description>&lt;b&gt;MICRO COAXIAL CONNECTOR RECEPTACLE VERTICAL&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.molex.com/pdm_docs/sd/734120110_sd.pdf"&gt; Data sheet &lt;/a&gt;</description>
<smd name="S@1" x="0" y="1.5" dx="1" dy="1" layer="1"/>
<smd name="S@2" x="-1.475" y="0" dx="1.05" dy="2.2" layer="1"/>
<smd name="S@3" x="1.475" y="0" dx="1.05" dy="2.2" layer="1"/>
<smd name="1" x="0" y="-1.5" dx="1" dy="1" layer="1"/>
<wire x1="-1.4" y1="1.4" x2="1.4" y2="1.4" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.4" x2="1.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.4" x2="-1.15" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-1.15" y1="-1.4" x2="-1.4" y2="-1.15" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.15" x2="-1.4" y2="1.4" width="0.2032" layer="51"/>
<wire x1="-1.15" y1="-1.4" x2="-1.2625" y2="-1.2875" width="0.2032" layer="21"/>
<wire x1="-0.75" y1="-1.4" x2="-1.15" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="0.7875" y1="1.4" x2="1.4" y2="1.4" width="0.2032" layer="21"/>
<wire x1="1.4" y1="-1.4" x2="0.775" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="1.4" x2="-0.7375" y2="1.4" width="0.2032" layer="21"/>
<text x="-1.75" y="-3.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.75" y="2.25" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8" y1="-0.9" x2="-1.35" y2="0.9" layer="51"/>
<rectangle x1="1.35" y1="-0.9" x2="1.8" y2="0.9" layer="51"/>
<rectangle x1="-0.3" y1="1.5" x2="0.3" y2="1.8" layer="51"/>
<rectangle x1="-0.3" y1="-1.8" x2="0.3" y2="-1.5" layer="51"/>
<circle x="0" y="0" radius="0.95" width="0.1016" layer="51"/>
<wire x1="-0.8" y1="0.5125" x2="-0.3375" y2="0.8875" width="0.1016" layer="21" curve="-37.899121"/>
<wire x1="-0.5125" y1="-0.8" x2="-0.8875" y2="-0.3375" width="0.1016" layer="21" curve="-37.899121"/>
<wire x1="0.8" y1="-0.5125" x2="0.3375" y2="-0.8875" width="0.1016" layer="21" curve="-37.899121"/>
<wire x1="0.5125" y1="0.8" x2="0.8875" y2="0.3375" width="0.1016" layer="21" curve="-37.899121"/>
<circle x="0" y="0" radius="0.200059375" width="0.1016" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="284512-4">
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<circle x="3.81" y="5.08" radius="0.6839" width="0.254" layer="94"/>
<text x="-5.08" y="8.001" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-7.493" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="5.08" length="short" direction="pas"/>
<pin name="2" x="-7.62" y="2.54" length="short" direction="pas"/>
<pin name="3" x="-7.62" y="0" length="short" direction="pas"/>
<pin name="4" x="-7.62" y="-2.54" length="short" direction="pas"/>
</symbol>
<symbol name="MALE_CONNECTOR">
<pin name="P$1" x="-5.08" y="0" visible="pad" length="middle"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="BNC-MGND">
<wire x1="0" y1="-2.54" x2="-0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="0.508" width="0.3048" layer="94" curve="-79.611142" cap="flat"/>
<wire x1="-2.54" y1="-2.54" x2="0" y2="-0.508" width="0.3048" layer="94" curve="79.611142" cap="flat"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<rectangle x1="-2.54" y1="-0.254" x2="-0.254" y2="0.254" layer="94"/>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="284512-4" prefix="CON">
<description>TE Connectivity Buchanan Series, 3.5mm Pitch 4 Way 1 Row Right Angle PCB Terminal Block Header, Through Hole

Mating Product 
284506-4</description>
<gates>
<gate name="G$1" symbol="284512-4" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="284512-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="284512-4" constant="no"/>
<attribute name="OC_FARNELL" value="2059806" constant="no"/>
<attribute name="OC_RS" value="710-0126" constant="no"/>
<attribute name="OC_TME" value="TBG-3.5-KW-4P" constant="no"/>
<attribute name="PACKAGE" value="DIP 3.5mm" constant="no"/>
<attribute name="SUPPLIER" value="TE" constant="no"/>
<attribute name="VALUE" value="300V AC 11A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0022272021" prefix="J">
<description>&lt;h1&gt;KK® 254 Wire-to-Board Header, Vertical, with Friction Lock, 2 Circuits, Tin (Sn) Plating&lt;/h1&gt;
&lt;h2&gt;KK® Interconnect System&lt;/h2&gt;
&lt;h3&gt;Documentation&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="http://www.molex.com/pdm_docs/sd/022272021_sd.pdf"&gt;Drawing&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="http://www.molex.com/pdm_docs/ps/PS-99020-0088.pdf"&gt;Product Specification&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;h3&gt;Matings&lt;/h3&gt;
&lt;table&gt;
&lt;tr&gt;&lt;th&gt;Part Number&lt;/th&gt;&lt;th&gt;Description&lt;/th&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;0022013027&lt;/td&gt;&lt;td&gt;CONN HOUS 2POS .100 W/RAMP/RIB&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;0010112023&lt;/td&gt;&lt;td&gt;CONN HOUSING 2POS .100 HI PRESS&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;0022012027&lt;/td&gt;&lt;td&gt;CONN HOUSING 2POS .100 W/RAMP&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;14-56-2021&lt;/td&gt;&lt;td&gt;CONN IDC 2POS .100 GOLD 24AWG&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;14-60-0021&lt;/td&gt;&lt;td&gt;CONN IDC 2POS .100 TIN 24AWG&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;0022012021&lt;/td&gt;&lt;td&gt;CONN HOUSING 2POS .100 W/O RAMP&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;0022012025&lt;/td&gt;&lt;td&gt;CONN HSNG 2POS .100 W/RAMP/RIB&lt;/td&gt;&lt;/tr&gt;</description>
<gates>
<gate name="-1" symbol="MALE_CONNECTOR" x="0" y="5.08"/>
<gate name="-2" symbol="MALE_CONNECTOR" x="0" y="-5.08"/>
</gates>
<devices>
<device name="" package="KK6410-2021_2-PIN">
<connects>
<connect gate="-1" pin="P$1" pad="1"/>
<connect gate="-2" pin="P$1" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="22-23-2021" constant="no"/>
<attribute name="OC_MOUSER" value="538-22-23-2021 " constant="no"/>
<attribute name="OC_RS" value="679-5583" constant="no"/>
<attribute name="OC_TME" value="MX-22-23-2021 " constant="no"/>
<attribute name="PACKAGE" value="THT" constant="no"/>
<attribute name="SUPPLIER" value="MOLEX" constant="no"/>
<attribute name="VALUE" value="2pin 2.54mm" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0022272031" prefix="J">
<description>&lt;h1&gt;KK® 254 Wire-to-Board Header, Vertical, with Friction Lock, 3 Circuits, Tin (Sn) Plating&lt;/h1&gt;
&lt;h2&gt;KK® Interconnect System&lt;/h2&gt;
&lt;h3&gt;Documentation&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="http://www.molex.com/pdm_docs/sd/022272031_sd.pdf"&gt;Drawing&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="http://www.molex.com/pdm_docs/ps/PS-99020-0088.pdf"&gt;Product Specification&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="-1" symbol="MALE_CONNECTOR" x="0" y="12.7"/>
<gate name="-2" symbol="MALE_CONNECTOR" x="0" y="0"/>
<gate name="-3" symbol="MALE_CONNECTOR" x="0" y="-12.7"/>
</gates>
<devices>
<device name="" package="KK6410-2031_3-PIN">
<connects>
<connect gate="-1" pin="P$1" pad="1"/>
<connect gate="-2" pin="P$1" pad="2"/>
<connect gate="-3" pin="P$1" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="22-23-2031" constant="no"/>
<attribute name="OC_MOUSER" value="538-22-23-2031" constant="no"/>
<attribute name="OC_RS" value="679-5587" constant="no"/>
<attribute name="OC_TME" value="MX-22-23-2031 " constant="no"/>
<attribute name="PACKAGE" value="THT" constant="no"/>
<attribute name="SUPPLIER" value="MOLEX" constant="no"/>
<attribute name="VALUE" value="3pin 2.54mm" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0022272061" prefix="J">
<description>&lt;h1&gt;KK® 254 Wire-to-Board Header, Vertical, with Friction Lock, 6 Circuits, Tin (Sn) Plating&lt;/h1&gt;
&lt;h2&gt;KK® Interconnect System&lt;/h2&gt;
&lt;h3&gt;Documentation&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="http://www.molex.com/pdm_docs/sd/022272061_sd.pdf"&gt;Drawing&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="http://www.molex.com/pdm_docs/ps/PS-99020-0088.pdf"&gt;Product Specification&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="-1" symbol="MALE_CONNECTOR" x="0" y="10.16"/>
<gate name="-2" symbol="MALE_CONNECTOR" x="0" y="0"/>
<gate name="-3" symbol="MALE_CONNECTOR" x="0" y="-10.16"/>
<gate name="-4" symbol="MALE_CONNECTOR" x="0" y="-20.32"/>
<gate name="-5" symbol="MALE_CONNECTOR" x="0" y="-30.48"/>
<gate name="-6" symbol="MALE_CONNECTOR" x="0" y="-40.64"/>
</gates>
<devices>
<device name="" package="KK6410-2061_6-PIN">
<connects>
<connect gate="-1" pin="P$1" pad="1"/>
<connect gate="-2" pin="P$1" pad="2"/>
<connect gate="-3" pin="P$1" pad="3"/>
<connect gate="-4" pin="P$1" pad="4"/>
<connect gate="-5" pin="P$1" pad="5"/>
<connect gate="-6" pin="P$1" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="22-23-2061" constant="no"/>
<attribute name="OC_MOUSER" value="538-22-23-2061" constant="no"/>
<attribute name="OC_RS" value="679-5593" constant="no"/>
<attribute name="OC_TME" value="MX-22-23-2061" constant="no"/>
<attribute name="PACKAGE" value="THT" constant="no"/>
<attribute name="SUPPLIER" value="MOLEX" constant="no"/>
<attribute name="VALUE" value="6pin 2.54mm" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="734120110" prefix="CON">
<description>&lt;b&gt;MICRO COAXIAL CONNECTOR RECEPTACLE VERTICAL&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.molex.com/pdm_docs/sd/734120110_sd.pdf"&gt; Data sheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="BNC-MGND" x="0" y="0"/>
</gates>
<devices>
<device name="" package="734120110">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="S@1 S@2 S@3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="734120110" constant="no"/>
<attribute name="OC_MOUSER" value="538-73412-0110" constant="no"/>
<attribute name="OC_RS" value="702-5425" constant="no"/>
<attribute name="OC_TME" value="MX-73412-0110 " constant="no"/>
<attribute name="PACKAGE" value="SMD" constant="no"/>
<attribute name="SUPPLIER" value="MOLEX" constant="no"/>
<attribute name="VALUE" value="50R" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="AC" width="1" drill="0.8">
<clearance class="1" value="1"/>
</class>
</classes>
<parts>
<part name="FRAME2" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="U6" library="Marcin_LIB" deviceset="PCA9685" device="TSSOP28" value="16-ch PWM"/>
<part name="U7" library="Marcin_LIB" deviceset="PCF8574" device="SO-16" value="PCF8574"/>
<part name="FRAME3" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="FRAME4" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="U10" library="Marcin_LIB" deviceset="BH1750FVI" device=""/>
<part name="U8" library="Marcin_LIB" deviceset="BME280" device=""/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="U2" library="Marcin_LIB" deviceset="INA219" device=""/>
<part name="U11" library="Marcin_LIB" deviceset="UBLOX_NEO-6/7" device=""/>
<part name="FB1" library="Marcin_LIB" deviceset="L" device="0603" value="22uH"/>
<part name="R15" library="ROMAX_RLC" deviceset="R" device="R0805" technology="1K" value="22R"/>
<part name="U1" library="Marcin_LIB" deviceset="IRM_05/10" device="" technology="-12" value="10W 12V"/>
<part name="R1" library="Marcin_R" deviceset="RES" device="R0805" value="0.1R 1%"/>
<part name="P+1" library="Marcin_Supply" deviceset="+12V" device=""/>
<part name="GND1" library="Marcin_Supply" deviceset="0V" device=""/>
<part name="GND2" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND3" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="+3V31" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="+3V33" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="P+3" library="Marcin_Supply" deviceset="+5V" device=""/>
<part name="U3" library="Marcin_LIB" deviceset="DCDC_OKI-78SR" device="" value="OKI-78SR-5"/>
<part name="GND6" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="U4" library="Marcin_LIB" deviceset="NCP1117ST33T3G" device=""/>
<part name="GND12" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="C1" library="Marcin_C" deviceset="EL0810" device="" technology="_330UF" value="330uF"/>
<part name="C4" library="Marcin_C" deviceset="EL0810" device="" technology="_330UF" value="330uF"/>
<part name="C5" library="Marcin_C" deviceset="EL0810" device="" technology="_330UF" value="330uF"/>
<part name="C7" library="Marcin_C" deviceset="EL0810" device="" technology="_330UF" value="330uF"/>
<part name="C2" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="C3" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="C6" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="C8" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND4" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND5" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND7" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND8" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND13" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND14" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="CON1" library="Marcin_conn" deviceset="284512-4" device="" value="AC INPUT"/>
<part name="PE1" library="Marcin_Supply" deviceset="PE" device=""/>
<part name="+3V35" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="C10" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND17" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND19" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="+3V36" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="C11" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND18" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND20" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="U9" library="Marcin_LIB" deviceset="HC-SR501" device=""/>
<part name="P+13" library="Marcin_Supply" deviceset="+5V" device=""/>
<part name="C13" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND49" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND50" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="U13" library="Marcin_LIB" deviceset="HC-SR501" device=""/>
<part name="P+14" library="Marcin_Supply" deviceset="+5V" device=""/>
<part name="C18" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND60" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND61" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="+3V39" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="C12" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND43" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND44" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND47" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="U12" library="Marcin_LIB" deviceset="BME280" device=""/>
<part name="+3V315" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="C17" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND58" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND59" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="+3V316" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="+3V313" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="C14" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND51" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND52" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="C16" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND56" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="D14" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KRKT" value="GPS"/>
<part name="R16" library="Marcin_R" deviceset="RES" device="R0805" value="220R"/>
<part name="GND54" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND57" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="+3V314" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="C15" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND55" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="U5" library="Marcin_LIB" deviceset="ADS1X15" device="" value="12bit 3.3kSps"/>
<part name="+3V34" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="C9" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND15" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND16" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="POT1" library="Marcin_LIB" deviceset="RTRIM" device="" technology="3364W-1-203E" value="20k"/>
<part name="GND21" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="+3V37" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="K1" library="Marcin_LIB" deviceset="RM51-3021-85-1012" device="" value="230V 10A"/>
<part name="P+6" library="Marcin_Supply" deviceset="+12V" device=""/>
<part name="GND37" library="Marcin_Supply" deviceset="0V" device=""/>
<part name="CON2" library="Marcin_conn" deviceset="284512-4" device="" value="REL1"/>
<part name="PE2" library="Marcin_Supply" deviceset="PE" device=""/>
<part name="Q3" library="Marcin_LIB" deviceset="MOSFET-N" device="" technology="NTR4003NT1G" value="MOSFET-NNTR4003NT1G"/>
<part name="R6" library="Marcin_R" deviceset="RES" device="R0805" value="10k"/>
<part name="GND29" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND26" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="D6" library="Marcin_LIB" deviceset="SCHOTTKY" device="" technology="BAT54C.215" value="30V 200mA"/>
<part name="K2" library="Marcin_LIB" deviceset="RM51-3021-85-1012" device="" value="230V 10A"/>
<part name="P+7" library="Marcin_Supply" deviceset="+12V" device=""/>
<part name="GND38" library="Marcin_Supply" deviceset="0V" device=""/>
<part name="CON3" library="Marcin_conn" deviceset="284512-4" device="" value="REL2"/>
<part name="PE3" library="Marcin_Supply" deviceset="PE" device=""/>
<part name="Q4" library="Marcin_LIB" deviceset="MOSFET-N" device="" technology="NTR4003NT1G" value="30V 500mA"/>
<part name="R7" library="Marcin_R" deviceset="RES" device="R0805" value="10k"/>
<part name="GND30" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND27" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="D7" library="Marcin_LIB" deviceset="SCHOTTKY" device="" technology="BAT54C.215" value="30V 200mA"/>
<part name="Q1" library="Marcin_LIB" deviceset="MOSFET-N" device="" technology="NTR4003NT1G" value="30V 500mA"/>
<part name="R5" library="Marcin_R" deviceset="RES" device="R0805" value="10k"/>
<part name="GND24" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND23" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="P+5" library="Marcin_Supply" deviceset="+12V" device=""/>
<part name="D4" library="Marcin_LIB" deviceset="SCHOTTKY" device="" technology="BAT54C.215" value="30V 200mA"/>
<part name="Q2" library="Marcin_LIB" deviceset="MOSFET-N" device="" technology="NTR4003NT1G" value="30V 500mA"/>
<part name="GND25" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="J2" library="Marcin_conn" deviceset="0022272021" device="" value="PWM0"/>
<part name="Q5" library="Marcin_LIB" deviceset="MOSFET-N" device="" technology="NTR4003NT1G" value="30V 500mA"/>
<part name="R11" library="Marcin_R" deviceset="RES" device="R0805" value="10k"/>
<part name="GND34" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND33" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="P+8" library="Marcin_Supply" deviceset="+12V" device=""/>
<part name="D10" library="Marcin_LIB" deviceset="SCHOTTKY" device="" technology="BAT54C.215" value="30V 200mA"/>
<part name="Q6" library="Marcin_LIB" deviceset="MOSFET-N" device="" technology="NTR4003NT1G" value="30V 500mA"/>
<part name="GND35" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="J3" library="Marcin_conn" deviceset="0022272021" device="" value="PWM1"/>
<part name="Q7" library="Marcin_LIB" deviceset="MOSFET-N" device="" technology="NTR4003NT1G" value="30V 500mA"/>
<part name="R13" library="Marcin_R" deviceset="RES" device="R0805" value="10k"/>
<part name="GND40" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND39" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="P+9" library="Marcin_Supply" deviceset="+12V" device=""/>
<part name="D12" library="Marcin_LIB" deviceset="SCHOTTKY" device="" technology="BAT54C.215" value="30V 200mA"/>
<part name="Q8" library="Marcin_LIB" deviceset="MOSFET-N" device="" technology="NTR4003NT1G" value="30V 500mA"/>
<part name="GND41" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="J4" library="Marcin_conn" deviceset="0022272021" device="" value="PWM2"/>
<part name="R8" library="Marcin_R" deviceset="RES" device="R0805" value="1k"/>
<part name="R12" library="Marcin_R" deviceset="RES" device="R0805" value="1k"/>
<part name="R14" library="Marcin_R" deviceset="RES" device="R0805" value="1k"/>
<part name="D13" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KGKT" value="PWM2"/>
<part name="D11" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KGKT" value="PWM1"/>
<part name="D5" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KGKT" value="PWM0"/>
<part name="GND42" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND36" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND28" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="J1" library="Marcin_conn" deviceset="0022272031" device="" value="AIN"/>
<part name="+3V38" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="GND22" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="J5" library="Marcin_conn" deviceset="0022272061" device="" value="I2C 0"/>
<part name="P+10" library="Marcin_Supply" deviceset="+5V" device=""/>
<part name="GND45" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="+3V310" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="J6" library="Marcin_conn" deviceset="0022272061" device="" value="I2C 1"/>
<part name="P+11" library="Marcin_Supply" deviceset="+5V" device=""/>
<part name="GND46" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="+3V311" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="J7" library="Marcin_conn" deviceset="0022272061" device="" value="I2C 2"/>
<part name="P+12" library="Marcin_Supply" deviceset="+5V" device=""/>
<part name="GND48" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="+3V312" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="FRAME5" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="U14" library="Marcin_LIB" deviceset="RPI_ZERO" device=""/>
<part name="C19" library="Marcin_C" deviceset="C0805" device="" technology="_100NF" value="100nF"/>
<part name="GND64" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="R23" library="Marcin_R" deviceset="RES" device="R0805"/>
<part name="R24" library="Marcin_R" deviceset="RES" device="R0805"/>
<part name="R25" library="Marcin_R" deviceset="RES" device="R0805"/>
<part name="R26" library="Marcin_R" deviceset="RES" device="R0805"/>
<part name="R27" library="Marcin_R" deviceset="RES" device="R0805"/>
<part name="R28" library="Marcin_R" deviceset="RES" device="R0805"/>
<part name="R29" library="Marcin_R" deviceset="RES" device="R0805"/>
<part name="GND66" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="P+15" library="Marcin_Supply" deviceset="+5V" device=""/>
<part name="R2" library="Marcin_R" deviceset="RES" device="R0805" value="1k"/>
<part name="R3" library="Marcin_R" deviceset="RES" device="R0805" value="1k"/>
<part name="R4" library="Marcin_R" deviceset="RES" device="R0805" value="1k"/>
<part name="D1" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KGKT" value="3.3V"/>
<part name="D2" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KGKT" value="5V"/>
<part name="D3" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KGKT" value="12V"/>
<part name="GND9" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND10" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND11" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="P+4" library="Marcin_Supply" deviceset="+12V" device=""/>
<part name="P+2" library="Marcin_Supply" deviceset="+5V" device=""/>
<part name="+3V32" library="Marcin_Supply" deviceset="+3V3" device=""/>
<part name="R9" library="Marcin_R" deviceset="RES" device="R0805" value="1k"/>
<part name="D8" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KGKT" value="REL1"/>
<part name="GND31" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="R10" library="Marcin_R" deviceset="RES" device="R0805" value="1k"/>
<part name="D9" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KGKT" value="REL2"/>
<part name="GND32" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="R18" library="Marcin_R" deviceset="RES" device="R0805" value="1k"/>
<part name="D15" library="Marcin_LIB" deviceset="LED" device="" technology="LTST-C930KGKT" value="RP3"/>
<part name="GND63" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="CON4" library="Marcin_conn" deviceset="734120110" device="" value="50R"/>
<part name="GND53" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="R19" library="Marcin_R" deviceset="RES" device="R0805"/>
<part name="R20" library="Marcin_R" deviceset="RES" device="R0805"/>
<part name="R21" library="Marcin_R" deviceset="RES" device="R0805"/>
<part name="R22" library="Marcin_R" deviceset="RES" device="R0805"/>
<part name="SW1" library="Marcin_LIB" deviceset="SWITCH" device="" technology="DTSM6" value="BT1"/>
<part name="R17" library="Marcin_R" deviceset="RES" device="R0805" value="1k"/>
<part name="GND62" library="Marcin_Supply" deviceset="GND" device=""/>
<part name="GND65" library="Marcin_Supply" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<description>Power Supply</description>
<plain>
<text x="108.204" y="93.98" size="1.778" layer="97">I2C 0x44</text>
<text x="38.1" y="48.26" size="1.778" layer="97">Corrections:
1. Pinout of NCP1117
2. Address of INA219 is 0x48 instead of 0x44.
The 8pin must be tieded to VCC instead of SDA
3. Relay pinout wrong
4. The 230V socket must be angled.Otherwise it sticks below the edge of PCB and causing issues during instalation.
5. There must be way of hard-reset of SBC, e.g. Hardware watchdog circuit. Happens the situation where there is no I2C comms. Only power cycle brings that back.</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="U2" gate="G$1" x="101.6" y="101.6"/>
<instance part="U1" gate="G$1" x="63.5" y="139.7" smashed="yes">
<attribute name="NAME" x="53.34" y="151.384" size="1.778" layer="95"/>
<attribute name="VALUE" x="53.594" y="127" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="104.14" y="147.32"/>
<instance part="P+1" gate="1" x="124.46" y="152.4" smashed="yes">
<attribute name="VALUE" x="121.92" y="154.94" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="48.26" y="127"/>
<instance part="GND2" gate="1" x="73.66" y="96.52"/>
<instance part="GND3" gate="1" x="83.82" y="127"/>
<instance part="+3V31" gate="G$1" x="81.28" y="114.3" smashed="yes">
<attribute name="VALUE" x="78.74" y="114.3" size="1.778" layer="96"/>
</instance>
<instance part="+3V33" gate="G$1" x="231.14" y="152.4" smashed="yes">
<attribute name="VALUE" x="228.6" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="P+3" gate="1" x="172.72" y="152.4" smashed="yes">
<attribute name="VALUE" x="170.18" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="U3" gate="G$1" x="142.24" y="147.32" smashed="yes">
<attribute name="NAME" x="137.16" y="155.194" size="1.778" layer="95"/>
<attribute name="MPN" x="137.16" y="152.908" size="1.778" layer="96"/>
</instance>
<instance part="GND6" gate="1" x="144.78" y="134.62"/>
<instance part="U4" gate="G$1" x="203.2" y="147.32" smashed="yes">
<attribute name="NAME" x="195.326" y="155.829" size="1.778" layer="95" ratio="10"/>
<attribute name="VALUE" x="195.58" y="153.162" size="1.778" layer="96" ratio="10"/>
</instance>
<instance part="GND12" gate="1" x="203.2" y="134.62"/>
<instance part="C1" gate="G$1" x="83.82" y="139.7" smashed="yes">
<attribute name="NAME" x="80.518" y="143.891" size="1.778" layer="95"/>
<attribute name="VALUE" x="75.692" y="137.287" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="124.46" y="142.24" smashed="yes">
<attribute name="NAME" x="125.984" y="145.161" size="1.778" layer="95"/>
<attribute name="VALUE" x="125.984" y="140.081" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="172.72" y="142.24" smashed="yes">
<attribute name="NAME" x="169.926" y="144.78" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="164.338" y="139.827" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="220.98" y="142.24" smashed="yes">
<attribute name="NAME" x="215.138" y="144.653" size="1.778" layer="95"/>
<attribute name="VALUE" x="212.852" y="140.081" size="1.778" layer="96"/>
</instance>
<instance part="C2" gate="G$1" x="88.9" y="139.7" smashed="yes">
<attribute name="NAME" x="89.408" y="143.637" size="1.778" layer="95"/>
<attribute name="VALUE" x="89.662" y="138.811" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="C3" gate="G$1" x="119.38" y="142.24" smashed="yes">
<attribute name="NAME" x="115.824" y="145.161" size="1.778" layer="95"/>
<attribute name="VALUE" x="110.744" y="140.081" size="1.778" layer="96"/>
</instance>
<instance part="C6" gate="G$1" x="177.8" y="142.24" smashed="yes">
<attribute name="NAME" x="178.816" y="144.78" size="1.778" layer="95"/>
<attribute name="VALUE" x="178.562" y="140.335" size="1.778" layer="96"/>
</instance>
<instance part="C8" gate="G$1" x="228.6" y="142.24" smashed="yes">
<attribute name="NAME" x="229.616" y="144.78" size="1.778" layer="95"/>
<attribute name="VALUE" x="229.362" y="140.462" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="119.38" y="137.16"/>
<instance part="GND5" gate="1" x="124.46" y="137.16"/>
<instance part="GND7" gate="1" x="172.72" y="137.16"/>
<instance part="GND8" gate="1" x="177.8" y="137.16"/>
<instance part="GND13" gate="1" x="220.98" y="137.16"/>
<instance part="GND14" gate="1" x="228.6" y="137.16"/>
<instance part="CON1" gate="G$1" x="20.32" y="139.7" rot="MR0"/>
<instance part="PE1" gate="M" x="27.94" y="132.08" smashed="yes">
<attribute name="VALUE" x="26.416" y="128.27" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="175.26" y="116.84" smashed="yes" rot="R180">
<attribute name="NAME" x="174.244" y="117.602" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="177.292" y="117.602" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R3" gate="G$1" x="175.26" y="114.3" smashed="yes" rot="R180">
<attribute name="NAME" x="173.736" y="115.062" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="177.546" y="115.062" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R4" gate="G$1" x="175.26" y="111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="173.736" y="112.522" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="179.578" y="113.792" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="D1" gate="G$1" x="180.34" y="109.22" smashed="yes">
<attribute name="NAME" x="180.848" y="103.632" size="1.778" layer="95"/>
<attribute name="VALUE" x="186.055" y="104.648" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="D2" gate="G$1" x="185.42" y="109.22" smashed="yes">
<attribute name="NAME" x="185.928" y="103.886" size="1.778" layer="95"/>
<attribute name="VALUE" x="191.135" y="104.648" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="D3" gate="G$1" x="190.5" y="109.22" smashed="yes">
<attribute name="NAME" x="191.516" y="103.886" size="1.778" layer="95"/>
<attribute name="VALUE" x="196.215" y="104.648" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND9" gate="1" x="180.34" y="101.6"/>
<instance part="GND10" gate="1" x="185.42" y="101.6"/>
<instance part="GND11" gate="1" x="190.5" y="101.6"/>
<instance part="P+4" gate="1" x="172.72" y="121.92" smashed="yes">
<attribute name="VALUE" x="170.18" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="P+2" gate="1" x="167.64" y="121.92" smashed="yes">
<attribute name="VALUE" x="165.1" y="121.92" size="1.778" layer="96"/>
</instance>
<instance part="+3V32" gate="G$1" x="162.56" y="119.38" smashed="yes">
<attribute name="VALUE" x="160.02" y="119.38" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$5" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="V+"/>
<wire x1="99.06" y1="147.32" x2="88.9" y2="147.32" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="+"/>
<wire x1="88.9" y1="147.32" x2="83.82" y2="147.32" width="0.1524" layer="91"/>
<wire x1="83.82" y1="147.32" x2="76.2" y2="147.32" width="0.1524" layer="91"/>
<wire x1="83.82" y1="144.78" x2="83.82" y2="147.32" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="88.9" y1="144.78" x2="88.9" y2="147.32" width="0.1524" layer="91"/>
<junction x="88.9" y="147.32"/>
<junction x="83.82" y="147.32"/>
<pinref part="U2" gate="G$1" pin="VIN+"/>
<wire x1="99.06" y1="116.84" x2="99.06" y2="147.32" width="0.1524" layer="91"/>
<junction x="99.06" y="147.32"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SDA"/>
<wire x1="121.92" y1="101.6" x2="132.08" y2="101.6" width="0.1524" layer="91"/>
<label x="124.46" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="A1"/>
<wire x1="83.82" y1="104.14" x2="76.2" y2="104.14" width="0.1524" layer="91"/>
<label x="76.2" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SCL"/>
<wire x1="121.92" y1="99.06" x2="132.08" y2="99.06" width="0.1524" layer="91"/>
<label x="124.46" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="+12V" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="106.68" y1="147.32" x2="119.38" y2="147.32" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+12V"/>
<wire x1="119.38" y1="147.32" x2="124.46" y2="147.32" width="0.1524" layer="91"/>
<wire x1="124.46" y1="147.32" x2="124.46" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VI"/>
<wire x1="134.62" y1="147.32" x2="124.46" y2="147.32" width="0.1524" layer="91"/>
<junction x="124.46" y="147.32"/>
<pinref part="C4" gate="G$1" pin="+"/>
<pinref part="C3" gate="G$1" pin="1"/>
<junction x="119.38" y="147.32"/>
<pinref part="U2" gate="G$1" pin="VIN-"/>
<wire x1="106.68" y1="116.84" x2="106.68" y2="147.32" width="0.1524" layer="91"/>
<junction x="106.68" y="147.32"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="P+4" gate="1" pin="+12V"/>
<wire x1="172.72" y1="116.84" x2="172.72" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N" class="1">
<segment>
<pinref part="GND1" gate="1" pin="0V"/>
<wire x1="48.26" y1="132.08" x2="48.26" y2="129.54" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="2"/>
<wire x1="27.94" y1="142.24" x2="40.64" y2="142.24" width="0.1524" layer="91"/>
<wire x1="40.64" y1="142.24" x2="40.64" y2="132.08" width="0.1524" layer="91"/>
<wire x1="40.64" y1="132.08" x2="48.26" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="AC/N"/>
<wire x1="50.8" y1="132.08" x2="48.26" y2="132.08" width="0.1524" layer="91"/>
<junction x="48.26" y="132.08"/>
<label x="30.48" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="83.82" y1="129.54" x2="83.82" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="V-"/>
<wire x1="83.82" y1="132.08" x2="76.2" y2="132.08" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="-"/>
<wire x1="83.82" y1="132.08" x2="83.82" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="88.9" y1="137.16" x2="88.9" y2="132.08" width="0.1524" layer="91"/>
<wire x1="88.9" y1="132.08" x2="83.82" y2="132.08" width="0.1524" layer="91"/>
<junction x="83.82" y="132.08"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="83.82" y1="101.6" x2="73.66" y2="101.6" width="0.1524" layer="91"/>
<wire x1="73.66" y1="101.6" x2="73.66" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="A0"/>
<wire x1="83.82" y1="106.68" x2="73.66" y2="106.68" width="0.1524" layer="91"/>
<wire x1="73.66" y1="106.68" x2="73.66" y2="101.6" width="0.1524" layer="91"/>
<junction x="73.66" y="101.6"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="-"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="-"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="-"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VS"/>
<pinref part="+3V31" gate="G$1" pin="+3V3"/>
<wire x1="83.82" y1="109.22" x2="81.28" y2="109.22" width="0.1524" layer="91"/>
<wire x1="81.28" y1="109.22" x2="81.28" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="VOUT"/>
<pinref part="+3V33" gate="G$1" pin="+3V3"/>
<wire x1="213.36" y1="147.32" x2="220.98" y2="147.32" width="0.1524" layer="91"/>
<wire x1="220.98" y1="147.32" x2="228.6" y2="147.32" width="0.1524" layer="91"/>
<wire x1="228.6" y1="147.32" x2="231.14" y2="147.32" width="0.1524" layer="91"/>
<wire x1="231.14" y1="147.32" x2="231.14" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="+"/>
<junction x="220.98" y="147.32"/>
<pinref part="C8" gate="G$1" pin="1"/>
<junction x="228.6" y="147.32"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="+3V32" gate="G$1" pin="+3V3"/>
<wire x1="172.72" y1="111.76" x2="162.56" y2="111.76" width="0.1524" layer="91"/>
<wire x1="162.56" y1="111.76" x2="162.56" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VO"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="154.94" y1="147.32" x2="172.72" y2="147.32" width="0.1524" layer="91"/>
<wire x1="172.72" y1="147.32" x2="172.72" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VIN"/>
<wire x1="172.72" y1="147.32" x2="177.8" y2="147.32" width="0.1524" layer="91"/>
<junction x="172.72" y="147.32"/>
<pinref part="C5" gate="G$1" pin="+"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="177.8" y1="147.32" x2="193.04" y2="147.32" width="0.1524" layer="91"/>
<junction x="177.8" y="147.32"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="172.72" y1="114.3" x2="167.64" y2="114.3" width="0.1524" layer="91"/>
<wire x1="167.64" y1="114.3" x2="167.64" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L" class="1">
<segment>
<wire x1="40.64" y1="147.32" x2="40.64" y2="144.78" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="1"/>
<wire x1="40.64" y1="144.78" x2="27.94" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="AC/L"/>
<wire x1="40.64" y1="147.32" x2="50.8" y2="147.32" width="0.1524" layer="91"/>
<label x="30.48" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="PE" class="0">
<segment>
<pinref part="PE1" gate="M" pin="PE"/>
<pinref part="CON1" gate="G$1" pin="4"/>
<wire x1="27.94" y1="134.62" x2="27.94" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="185.42" y1="111.76" x2="185.42" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="185.42" y1="114.3" x2="180.34" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="180.34" y1="116.84" x2="190.5" y2="116.84" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="190.5" y1="116.84" x2="190.5" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>PWM &amp; I/O</description>
<plain>
<text x="98.044" y="93.98" size="1.778" layer="97">I2C 0x40</text>
<text x="98.044" y="50.8" size="1.778" layer="97">I2C 0x20</text>
<text x="54.864" y="15.24" size="1.778" layer="97">I2C 0x48</text>
<text x="53.34" y="53.34" size="1.778" layer="97" rot="R90">25mA max output current!</text>
<text x="104.14" y="154.94" size="1.778" layer="97">N-MOSDET
FDV303N is substitude</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="U6" gate="G$1" x="93.98" y="119.38"/>
<instance part="U7" gate="G$1" x="93.98" y="60.96"/>
<instance part="+3V35" gate="G$1" x="60.96" y="142.24" smashed="yes">
<attribute name="VALUE" x="58.42" y="142.24" size="1.778" layer="96"/>
</instance>
<instance part="C10" gate="G$1" x="60.96" y="132.08" smashed="yes">
<attribute name="NAME" x="56.896" y="134.62" size="1.778" layer="95"/>
<attribute name="VALUE" x="51.562" y="130.302" size="1.778" layer="96"/>
</instance>
<instance part="GND17" gate="1" x="60.96" y="127"/>
<instance part="GND19" gate="1" x="76.2" y="96.52"/>
<instance part="+3V36" gate="G$1" x="66.04" y="83.82" smashed="yes">
<attribute name="VALUE" x="63.5" y="83.82" size="1.778" layer="96"/>
</instance>
<instance part="C11" gate="G$1" x="66.04" y="73.66" smashed="yes">
<attribute name="NAME" x="59.436" y="76.2" size="1.778" layer="95"/>
<attribute name="VALUE" x="56.642" y="71.882" size="1.778" layer="96"/>
</instance>
<instance part="GND18" gate="1" x="66.04" y="68.58"/>
<instance part="GND20" gate="1" x="76.2" y="50.8"/>
<instance part="U5" gate="G$1" x="48.26" y="30.48"/>
<instance part="+3V34" gate="G$1" x="15.24" y="45.72" smashed="yes">
<attribute name="VALUE" x="12.7" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="15.24" y="35.56" smashed="yes">
<attribute name="NAME" x="8.636" y="38.1" size="1.778" layer="95"/>
<attribute name="VALUE" x="5.842" y="33.782" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="15.24" y="30.48"/>
<instance part="GND16" gate="1" x="22.86" y="17.78"/>
<instance part="POT1" gate="G$1" x="93.98" y="22.86" rot="MR0"/>
<instance part="GND21" gate="1" x="93.98" y="15.24"/>
<instance part="+3V37" gate="G$1" x="93.98" y="30.48" smashed="yes">
<attribute name="VALUE" x="91.44" y="30.48" size="1.778" layer="96"/>
</instance>
<instance part="K1" gate="G$1" x="187.96" y="88.9"/>
<instance part="P+6" gate="1" x="172.72" y="101.6" smashed="yes">
<attribute name="VALUE" x="170.18" y="104.14" size="1.778" layer="96"/>
</instance>
<instance part="GND37" gate="1" x="200.66" y="78.74" rot="MR0"/>
<instance part="CON2" gate="G$1" x="228.6" y="91.44"/>
<instance part="PE2" gate="M" x="220.98" y="83.82" smashed="yes" rot="MR0">
<attribute name="VALUE" x="222.504" y="80.01" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="Q3" gate="Q" x="154.94" y="73.66" smashed="yes">
<attribute name="NAME" x="151.13" y="76.2" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="158.75" y="67.818" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R6" gate="G$1" x="147.32" y="71.12" rot="R90"/>
<instance part="GND29" gate="1" x="154.94" y="63.5"/>
<instance part="GND26" gate="1" x="147.32" y="63.5"/>
<instance part="D6" gate="G$1" x="154.94" y="93.98" rot="MR90"/>
<instance part="K2" gate="G$1" x="187.96" y="45.72"/>
<instance part="P+7" gate="1" x="172.72" y="58.42" smashed="yes">
<attribute name="VALUE" x="170.18" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="GND38" gate="1" x="200.66" y="35.56" rot="MR0"/>
<instance part="CON3" gate="G$1" x="228.6" y="48.26"/>
<instance part="PE3" gate="M" x="220.98" y="40.64" smashed="yes" rot="MR0">
<attribute name="VALUE" x="222.504" y="36.83" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="Q4" gate="Q" x="154.94" y="30.48" smashed="yes">
<attribute name="NAME" x="151.13" y="33.02" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="159.004" y="26.162" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R7" gate="G$1" x="147.32" y="27.94" rot="R90"/>
<instance part="GND30" gate="1" x="154.94" y="20.32"/>
<instance part="GND27" gate="1" x="147.32" y="20.32"/>
<instance part="D7" gate="G$1" x="154.94" y="50.8" rot="MR90"/>
<instance part="Q1" gate="Q" x="139.7" y="149.86" smashed="yes">
<attribute name="NAME" x="135.89" y="152.4" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="139.7" y="152.4" size="1.27" layer="96" ratio="10" display="off"/>
</instance>
<instance part="R5" gate="G$1" x="132.08" y="147.32" rot="R90"/>
<instance part="GND24" gate="1" x="139.7" y="139.7"/>
<instance part="GND23" gate="1" x="132.08" y="139.7"/>
<instance part="P+5" gate="1" x="139.7" y="170.18" smashed="yes">
<attribute name="VALUE" x="137.16" y="172.72" size="1.778" layer="96"/>
</instance>
<instance part="D4" gate="G$1" x="134.62" y="162.56" rot="MR90"/>
<instance part="Q2" gate="Q" x="144.78" y="149.86" smashed="yes" rot="MR0">
<attribute name="NAME" x="148.59" y="152.4" size="1.27" layer="95" ratio="10" rot="MR0"/>
<attribute name="VALUE" x="144.78" y="152.4" size="1.27" layer="96" ratio="10" rot="MR0" display="off"/>
</instance>
<instance part="GND25" gate="1" x="144.78" y="139.7"/>
<instance part="J2" gate="-1" x="160.02" y="162.56" smashed="yes">
<attribute name="NAME" x="160.02" y="164.338" size="1.778" layer="95"/>
<attribute name="VALUE" x="160.02" y="157.48" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J2" gate="-2" x="160.02" y="157.48" smashed="yes">
<attribute name="NAME" x="160.02" y="159.004" size="1.778" layer="95"/>
<attribute name="VALUE" x="160.02" y="152.4" size="1.778" layer="96" display="off"/>
</instance>
<instance part="Q5" gate="Q" x="177.8" y="149.86" smashed="yes">
<attribute name="NAME" x="173.99" y="152.4" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="177.8" y="152.4" size="1.27" layer="96" ratio="10" display="off"/>
</instance>
<instance part="R11" gate="G$1" x="170.18" y="147.32" rot="R90"/>
<instance part="GND34" gate="1" x="177.8" y="139.7"/>
<instance part="GND33" gate="1" x="170.18" y="139.7"/>
<instance part="P+8" gate="1" x="177.8" y="170.18" smashed="yes">
<attribute name="VALUE" x="175.26" y="172.72" size="1.778" layer="96"/>
</instance>
<instance part="D10" gate="G$1" x="172.72" y="162.56" rot="MR90"/>
<instance part="Q6" gate="Q" x="182.88" y="149.86" smashed="yes" rot="MR0">
<attribute name="NAME" x="186.69" y="152.4" size="1.27" layer="95" ratio="10" rot="MR0"/>
<attribute name="VALUE" x="182.88" y="152.4" size="1.27" layer="96" ratio="10" rot="MR0" display="off"/>
</instance>
<instance part="GND35" gate="1" x="182.88" y="139.7"/>
<instance part="J3" gate="-1" x="198.12" y="162.56" smashed="yes">
<attribute name="NAME" x="198.12" y="164.338" size="1.778" layer="95"/>
<attribute name="VALUE" x="198.12" y="157.48" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J3" gate="-2" x="198.12" y="157.48" smashed="yes">
<attribute name="NAME" x="198.12" y="159.004" size="1.778" layer="95"/>
<attribute name="VALUE" x="198.12" y="152.4" size="1.778" layer="96" display="off"/>
</instance>
<instance part="Q7" gate="Q" x="220.98" y="149.86" smashed="yes">
<attribute name="NAME" x="217.17" y="152.4" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="220.98" y="152.4" size="1.27" layer="96" ratio="10" display="off"/>
</instance>
<instance part="R13" gate="G$1" x="213.36" y="147.32" rot="R90"/>
<instance part="GND40" gate="1" x="220.98" y="139.7"/>
<instance part="GND39" gate="1" x="213.36" y="139.7"/>
<instance part="P+9" gate="1" x="220.98" y="170.18" smashed="yes">
<attribute name="VALUE" x="218.44" y="172.72" size="1.778" layer="96"/>
</instance>
<instance part="D12" gate="G$1" x="215.9" y="162.56" rot="MR90"/>
<instance part="Q8" gate="Q" x="226.06" y="149.86" smashed="yes" rot="MR0">
<attribute name="NAME" x="229.87" y="152.4" size="1.27" layer="95" ratio="10" rot="MR0"/>
<attribute name="VALUE" x="226.06" y="152.4" size="1.27" layer="96" ratio="10" rot="MR0" display="off"/>
</instance>
<instance part="GND41" gate="1" x="226.06" y="139.7"/>
<instance part="J4" gate="-1" x="241.3" y="162.56" smashed="yes">
<attribute name="NAME" x="241.3" y="164.338" size="1.778" layer="95"/>
<attribute name="VALUE" x="241.3" y="157.48" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J4" gate="-2" x="241.3" y="157.48" smashed="yes">
<attribute name="NAME" x="241.3" y="159.004" size="1.778" layer="95"/>
<attribute name="VALUE" x="241.3" y="152.4" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R8" gate="G$1" x="149.86" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="148.844" y="125.222" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="151.892" y="125.222" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R12" gate="G$1" x="195.58" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="194.056" y="125.222" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="197.866" y="125.222" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R14" gate="G$1" x="236.22" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="234.696" y="125.222" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="240.538" y="126.492" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="D13" gate="G$1" x="241.3" y="121.92" smashed="yes">
<attribute name="NAME" x="241.808" y="116.332" size="1.778" layer="95"/>
<attribute name="VALUE" x="247.015" y="117.348" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="D11" gate="G$1" x="200.66" y="121.92" smashed="yes">
<attribute name="NAME" x="201.168" y="116.586" size="1.778" layer="95"/>
<attribute name="VALUE" x="206.375" y="117.348" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="D5" gate="G$1" x="154.94" y="121.92" smashed="yes">
<attribute name="NAME" x="155.956" y="116.586" size="1.778" layer="95"/>
<attribute name="VALUE" x="160.655" y="117.348" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND42" gate="1" x="241.3" y="114.3"/>
<instance part="GND36" gate="1" x="200.66" y="114.3"/>
<instance part="GND28" gate="1" x="154.94" y="114.3"/>
<instance part="J1" gate="-1" x="111.76" y="40.64" smashed="yes">
<attribute name="NAME" x="111.76" y="43.18" size="1.778" layer="95"/>
<attribute name="VALUE" x="111.76" y="35.56" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J1" gate="-2" x="111.76" y="33.02" smashed="yes">
<attribute name="NAME" x="111.76" y="35.56" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="111.76" y="27.94" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J1" gate="-3" x="111.76" y="25.4" smashed="yes">
<attribute name="NAME" x="111.76" y="27.94" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="111.76" y="20.32" size="1.778" layer="96" display="off"/>
</instance>
<instance part="+3V38" gate="G$1" x="106.68" y="45.72" smashed="yes">
<attribute name="VALUE" x="104.14" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="GND22" gate="1" x="106.68" y="20.32"/>
<instance part="R9" gate="G$1" x="162.56" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="163.322" y="80.264" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="164.592" y="74.422" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D8" gate="G$1" x="162.56" y="71.12" smashed="yes">
<attribute name="NAME" x="163.068" y="65.532" size="1.778" layer="95"/>
<attribute name="VALUE" x="168.275" y="66.548" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND31" gate="1" x="162.56" y="63.5"/>
<instance part="R10" gate="G$1" x="162.56" y="35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="163.322" y="37.084" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="164.592" y="31.242" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D9" gate="G$1" x="162.56" y="27.94" smashed="yes">
<attribute name="NAME" x="163.068" y="22.352" size="1.778" layer="95"/>
<attribute name="VALUE" x="168.275" y="23.368" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND32" gate="1" x="162.56" y="20.32"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="GND"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="78.74" y1="99.06" x2="76.2" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="!OE!"/>
<wire x1="78.74" y1="101.6" x2="76.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="76.2" y1="101.6" x2="76.2" y2="99.06" width="0.1524" layer="91"/>
<junction x="76.2" y="99.06"/>
<pinref part="U6" gate="G$1" pin="A0"/>
<wire x1="78.74" y1="121.92" x2="76.2" y2="121.92" width="0.1524" layer="91"/>
<wire x1="76.2" y1="121.92" x2="76.2" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="A1"/>
<wire x1="76.2" y1="119.38" x2="76.2" y2="116.84" width="0.1524" layer="91"/>
<wire x1="76.2" y1="116.84" x2="76.2" y2="114.3" width="0.1524" layer="91"/>
<wire x1="76.2" y1="114.3" x2="76.2" y2="111.76" width="0.1524" layer="91"/>
<wire x1="76.2" y1="111.76" x2="76.2" y2="109.22" width="0.1524" layer="91"/>
<wire x1="76.2" y1="109.22" x2="76.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="119.38" x2="76.2" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="A2"/>
<wire x1="78.74" y1="116.84" x2="76.2" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="A3"/>
<wire x1="78.74" y1="114.3" x2="76.2" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="A4"/>
<wire x1="78.74" y1="111.76" x2="76.2" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="A5"/>
<wire x1="78.74" y1="109.22" x2="76.2" y2="109.22" width="0.1524" layer="91"/>
<junction x="76.2" y="119.38"/>
<junction x="76.2" y="116.84"/>
<junction x="76.2" y="114.3"/>
<junction x="76.2" y="111.76"/>
<junction x="76.2" y="109.22"/>
<junction x="76.2" y="101.6"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="GND18" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="GND"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="78.74" y1="55.88" x2="76.2" y2="55.88" width="0.1524" layer="91"/>
<wire x1="76.2" y1="55.88" x2="76.2" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="A0"/>
<wire x1="78.74" y1="66.04" x2="76.2" y2="66.04" width="0.1524" layer="91"/>
<wire x1="76.2" y1="66.04" x2="76.2" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="A1"/>
<wire x1="76.2" y1="63.5" x2="76.2" y2="60.96" width="0.1524" layer="91"/>
<wire x1="76.2" y1="60.96" x2="76.2" y2="55.88" width="0.1524" layer="91"/>
<wire x1="78.74" y1="63.5" x2="76.2" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="A2"/>
<wire x1="78.74" y1="60.96" x2="76.2" y2="60.96" width="0.1524" layer="91"/>
<junction x="76.2" y="63.5"/>
<junction x="76.2" y="60.96"/>
<junction x="76.2" y="55.88"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GND"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="22.86" y1="20.32" x2="25.4" y2="20.32" width="0.1524" layer="91"/>
<wire x1="22.86" y1="20.32" x2="22.86" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="ADDR"/>
<wire x1="25.4" y1="25.4" x2="22.86" y2="25.4" width="0.1524" layer="91"/>
<junction x="22.86" y="20.32"/>
</segment>
<segment>
<pinref part="POT1" gate="G$1" pin="A"/>
<pinref part="GND21" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q3" gate="Q" pin="S"/>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="154.94" y1="66.04" x2="154.94" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="GND26" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q4" gate="Q" pin="S"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="154.94" y1="22.86" x2="154.94" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="GND27" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q1" gate="Q" pin="S"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="139.7" y1="142.24" x2="139.7" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q2" gate="Q" pin="S"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="144.78" y1="144.78" x2="144.78" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q5" gate="Q" pin="S"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="177.8" y1="142.24" x2="177.8" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="GND33" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q6" gate="Q" pin="S"/>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="182.88" y1="144.78" x2="182.88" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q7" gate="Q" pin="S"/>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="220.98" y1="142.24" x2="220.98" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="GND39" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q8" gate="Q" pin="S"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="226.06" y1="144.78" x2="226.06" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D13" gate="G$1" pin="C"/>
<pinref part="GND42" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D11" gate="G$1" pin="C"/>
<pinref part="GND36" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="C"/>
<pinref part="GND28" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND22" gate="1" pin="GND"/>
<pinref part="J1" gate="-3" pin="P$1"/>
<wire x1="106.68" y1="22.86" x2="106.68" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D8" gate="G$1" pin="C"/>
<pinref part="GND31" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D9" gate="G$1" pin="C"/>
<pinref part="GND32" gate="1" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V35" gate="G$1" pin="+3V3"/>
<wire x1="60.96" y1="137.16" x2="60.96" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<pinref part="U6" gate="G$1" pin="VCC"/>
<wire x1="60.96" y1="137.16" x2="78.74" y2="137.16" width="0.1524" layer="91"/>
<junction x="60.96" y="137.16"/>
</segment>
<segment>
<pinref part="+3V36" gate="G$1" pin="+3V3"/>
<wire x1="66.04" y1="78.74" x2="66.04" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<pinref part="U7" gate="G$1" pin="VCC"/>
<wire x1="66.04" y1="78.74" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<junction x="66.04" y="78.74"/>
</segment>
<segment>
<pinref part="+3V34" gate="G$1" pin="+3V3"/>
<wire x1="15.24" y1="40.64" x2="15.24" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="VDD"/>
<wire x1="25.4" y1="40.64" x2="15.24" y2="40.64" width="0.1524" layer="91"/>
<junction x="15.24" y="40.64"/>
</segment>
<segment>
<pinref part="POT1" gate="G$1" pin="E"/>
<pinref part="+3V37" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V38" gate="G$1" pin="+3V3"/>
<pinref part="J1" gate="-1" pin="P$1"/>
<wire x1="106.68" y1="43.18" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="SDA"/>
<wire x1="78.74" y1="129.54" x2="71.12" y2="129.54" width="0.1524" layer="91"/>
<label x="71.12" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="SDA"/>
<wire x1="78.74" y1="73.66" x2="71.12" y2="73.66" width="0.1524" layer="91"/>
<label x="71.12" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SDA"/>
<wire x1="25.4" y1="35.56" x2="20.32" y2="35.56" width="0.1524" layer="91"/>
<label x="20.32" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="SCL"/>
<wire x1="78.74" y1="127" x2="71.12" y2="127" width="0.1524" layer="91"/>
<label x="71.12" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="SCL"/>
<wire x1="78.74" y1="71.12" x2="71.12" y2="71.12" width="0.1524" layer="91"/>
<label x="71.12" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SCL"/>
<wire x1="25.4" y1="33.02" x2="20.32" y2="33.02" width="0.1524" layer="91"/>
<label x="20.32" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM0" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="LED0"/>
<wire x1="109.22" y1="137.16" x2="124.46" y2="137.16" width="0.1524" layer="91"/>
<label x="111.76" y="137.16" size="1.778" layer="95"/>
<wire x1="124.46" y1="137.16" x2="124.46" y2="149.86" width="0.1524" layer="91"/>
<pinref part="Q1" gate="Q" pin="G"/>
<wire x1="134.62" y1="149.86" x2="132.08" y2="149.86" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<junction x="132.08" y="149.86"/>
<wire x1="124.46" y1="149.86" x2="132.08" y2="149.86" width="0.1524" layer="91"/>
<label x="124.46" y="149.86" size="1.778" layer="95"/>
<label x="149.86" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q2" gate="Q" pin="G"/>
<wire x1="149.86" y1="149.86" x2="157.48" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM1" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="LED1"/>
<label x="111.76" y="134.62" size="1.778" layer="95"/>
<pinref part="Q5" gate="Q" pin="G"/>
<wire x1="172.72" y1="149.86" x2="170.18" y2="149.86" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<junction x="170.18" y="149.86"/>
<wire x1="162.56" y1="149.86" x2="170.18" y2="149.86" width="0.1524" layer="91"/>
<label x="187.96" y="149.86" size="1.778" layer="95"/>
<wire x1="109.22" y1="134.62" x2="162.56" y2="134.62" width="0.1524" layer="91"/>
<wire x1="162.56" y1="134.62" x2="162.56" y2="149.86" width="0.1524" layer="91"/>
<label x="162.56" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q6" gate="Q" pin="G"/>
<wire x1="187.96" y1="149.86" x2="195.58" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="LED2"/>
<label x="111.76" y="132.08" size="1.778" layer="95"/>
<pinref part="Q7" gate="Q" pin="G"/>
<wire x1="215.9" y1="149.86" x2="213.36" y2="149.86" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<junction x="213.36" y="149.86"/>
<wire x1="205.74" y1="149.86" x2="213.36" y2="149.86" width="0.1524" layer="91"/>
<label x="231.14" y="149.86" size="1.778" layer="95"/>
<wire x1="109.22" y1="132.08" x2="205.74" y2="132.08" width="0.1524" layer="91"/>
<wire x1="205.74" y1="132.08" x2="205.74" y2="149.86" width="0.1524" layer="91"/>
<label x="205.74" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q8" gate="Q" pin="G"/>
<wire x1="231.14" y1="149.86" x2="238.76" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PIR1" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="P0"/>
<wire x1="109.22" y1="78.74" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<label x="111.76" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="PIR2" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="P1"/>
<wire x1="109.22" y1="76.2" x2="119.38" y2="76.2" width="0.1524" layer="91"/>
<label x="111.76" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="REL_HEATER" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="P2"/>
<label x="111.76" y="73.66" size="1.778" layer="95"/>
<pinref part="Q3" gate="Q" pin="G"/>
<wire x1="149.86" y1="73.66" x2="147.32" y2="73.66" width="0.1524" layer="91"/>
<wire x1="147.32" y1="73.66" x2="109.22" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<junction x="147.32" y="73.66"/>
</segment>
</net>
<net name="BH1750_DVI" class="0">
<segment>
<wire x1="109.22" y1="66.04" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="P5"/>
<label x="111.76" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADS1X15_ALERT" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="ALERT"/>
<wire x1="73.66" y1="40.64" x2="99.06" y2="40.64" width="0.1524" layer="91"/>
<label x="76.2" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="109.22" y1="68.58" x2="132.08" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="P4"/>
<label x="111.76" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="AIN0" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="AIN0"/>
<label x="76.2" y="33.02" size="1.778" layer="95"/>
<pinref part="J1" gate="-2" pin="P$1"/>
<wire x1="106.68" y1="33.02" x2="73.66" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AIN1" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="AIN1"/>
<label x="76.2" y="30.48" size="1.778" layer="95"/>
<pinref part="POT1" gate="G$1" pin="S"/>
<wire x1="91.44" y1="22.86" x2="86.36" y2="22.86" width="0.1524" layer="91"/>
<wire x1="86.36" y1="22.86" x2="86.36" y2="30.48" width="0.1524" layer="91"/>
<wire x1="86.36" y1="30.48" x2="73.66" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AIN2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="AIN2"/>
<wire x1="73.66" y1="27.94" x2="83.82" y2="27.94" width="0.1524" layer="91"/>
<label x="76.2" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="AIN3" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="AIN3"/>
<wire x1="73.66" y1="25.4" x2="83.82" y2="25.4" width="0.1524" layer="91"/>
<label x="76.2" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="REL_SPARE1" class="0">
<segment>
<wire x1="109.22" y1="71.12" x2="142.24" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="P3"/>
<label x="111.76" y="71.12" size="1.778" layer="95"/>
<wire x1="142.24" y1="71.12" x2="142.24" y2="30.48" width="0.1524" layer="91"/>
<pinref part="Q4" gate="Q" pin="G"/>
<wire x1="149.86" y1="30.48" x2="147.32" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="142.24" y1="30.48" x2="147.32" y2="30.48" width="0.1524" layer="91"/>
<junction x="147.32" y="30.48"/>
</segment>
</net>
<net name="L" class="1">
<segment>
<pinref part="K2" gate="G$1" pin="COM"/>
<wire x1="175.26" y1="43.18" x2="162.56" y2="43.18" width="0.1524" layer="91"/>
<label x="170.18" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="K1" gate="G$1" pin="COM"/>
<wire x1="175.26" y1="86.36" x2="165.1" y2="86.36" width="0.1524" layer="91"/>
<label x="170.18" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="+12V" class="0">
<segment>
<pinref part="P+6" gate="1" pin="+12V"/>
<pinref part="K1" gate="G$1" pin="A1"/>
<wire x1="175.26" y1="93.98" x2="172.72" y2="93.98" width="0.1524" layer="91"/>
<wire x1="172.72" y1="93.98" x2="172.72" y2="99.06" width="0.1524" layer="91"/>
<junction x="172.72" y="93.98"/>
<pinref part="D6" gate="G$1" pin="3"/>
<wire x1="172.72" y1="93.98" x2="157.48" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+7" gate="1" pin="+12V"/>
<pinref part="K2" gate="G$1" pin="A1"/>
<wire x1="175.26" y1="50.8" x2="172.72" y2="50.8" width="0.1524" layer="91"/>
<wire x1="172.72" y1="50.8" x2="172.72" y2="55.88" width="0.1524" layer="91"/>
<junction x="172.72" y="50.8"/>
<pinref part="D7" gate="G$1" pin="3"/>
<wire x1="172.72" y1="50.8" x2="157.48" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+5" gate="1" pin="+12V"/>
<wire x1="139.7" y1="167.64" x2="139.7" y2="162.56" width="0.1524" layer="91"/>
<pinref part="J2" gate="-1" pin="P$1"/>
<wire x1="139.7" y1="162.56" x2="154.94" y2="162.56" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="3"/>
<wire x1="139.7" y1="162.56" x2="137.16" y2="162.56" width="0.1524" layer="91"/>
<junction x="139.7" y="162.56"/>
</segment>
<segment>
<pinref part="P+8" gate="1" pin="+12V"/>
<wire x1="177.8" y1="167.64" x2="177.8" y2="162.56" width="0.1524" layer="91"/>
<pinref part="J3" gate="-1" pin="P$1"/>
<wire x1="177.8" y1="162.56" x2="193.04" y2="162.56" width="0.1524" layer="91"/>
<pinref part="D10" gate="G$1" pin="3"/>
<wire x1="177.8" y1="162.56" x2="175.26" y2="162.56" width="0.1524" layer="91"/>
<junction x="177.8" y="162.56"/>
</segment>
<segment>
<pinref part="P+9" gate="1" pin="+12V"/>
<wire x1="220.98" y1="167.64" x2="220.98" y2="162.56" width="0.1524" layer="91"/>
<pinref part="J4" gate="-1" pin="P$1"/>
<wire x1="220.98" y1="162.56" x2="236.22" y2="162.56" width="0.1524" layer="91"/>
<pinref part="D12" gate="G$1" pin="3"/>
<wire x1="220.98" y1="162.56" x2="218.44" y2="162.56" width="0.1524" layer="91"/>
<junction x="220.98" y="162.56"/>
</segment>
</net>
<net name="L_REL2" class="1">
<segment>
<pinref part="K2" gate="G$1" pin="NO"/>
<wire x1="193.04" y1="55.88" x2="208.28" y2="55.88" width="0.1524" layer="91"/>
<wire x1="208.28" y1="55.88" x2="208.28" y2="53.34" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="1"/>
<wire x1="208.28" y1="53.34" x2="220.98" y2="53.34" width="0.1524" layer="91"/>
<label x="218.44" y="53.34" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="N" class="1">
<segment>
<pinref part="GND37" gate="1" pin="0V"/>
<wire x1="200.66" y1="83.82" x2="200.66" y2="81.28" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="2"/>
<wire x1="220.98" y1="93.98" x2="208.28" y2="93.98" width="0.1524" layer="91"/>
<wire x1="208.28" y1="93.98" x2="208.28" y2="83.82" width="0.1524" layer="91"/>
<wire x1="208.28" y1="83.82" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
<label x="218.44" y="93.98" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="GND38" gate="1" pin="0V"/>
<wire x1="200.66" y1="40.64" x2="200.66" y2="38.1" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="2"/>
<wire x1="220.98" y1="50.8" x2="208.28" y2="50.8" width="0.1524" layer="91"/>
<wire x1="208.28" y1="50.8" x2="208.28" y2="40.64" width="0.1524" layer="91"/>
<wire x1="208.28" y1="40.64" x2="200.66" y2="40.64" width="0.1524" layer="91"/>
<label x="218.44" y="50.8" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="PE" class="0">
<segment>
<pinref part="PE2" gate="M" pin="PE"/>
<pinref part="CON2" gate="G$1" pin="4"/>
<wire x1="220.98" y1="86.36" x2="220.98" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PE3" gate="M" pin="PE"/>
<pinref part="CON3" gate="G$1" pin="4"/>
<wire x1="220.98" y1="43.18" x2="220.98" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L_REL1" class="1">
<segment>
<pinref part="K1" gate="G$1" pin="NO"/>
<wire x1="193.04" y1="99.06" x2="208.28" y2="99.06" width="0.1524" layer="91"/>
<wire x1="208.28" y1="99.06" x2="208.28" y2="96.52" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="1"/>
<wire x1="208.28" y1="96.52" x2="220.98" y2="96.52" width="0.1524" layer="91"/>
<label x="218.44" y="96.52" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="PWM0_NEG" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="1"/>
<pinref part="Q1" gate="Q" pin="D"/>
<wire x1="134.62" y1="157.48" x2="139.7" y2="157.48" width="0.1524" layer="91"/>
<wire x1="139.7" y1="157.48" x2="139.7" y2="154.94" width="0.1524" layer="91"/>
<junction x="139.7" y="157.48"/>
<label x="142.24" y="157.48" size="1.778" layer="95"/>
<wire x1="139.7" y1="157.48" x2="144.78" y2="157.48" width="0.1524" layer="91"/>
<pinref part="Q2" gate="Q" pin="D"/>
<wire x1="144.78" y1="157.48" x2="154.94" y2="157.48" width="0.1524" layer="91"/>
<wire x1="144.78" y1="154.94" x2="144.78" y2="157.48" width="0.1524" layer="91"/>
<junction x="144.78" y="157.48"/>
<pinref part="J2" gate="-2" pin="P$1"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="147.32" y1="124.46" x2="132.08" y2="124.46" width="0.1524" layer="91"/>
<label x="132.08" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM1_NEG" class="0">
<segment>
<pinref part="D10" gate="G$1" pin="1"/>
<pinref part="Q5" gate="Q" pin="D"/>
<wire x1="172.72" y1="157.48" x2="177.8" y2="157.48" width="0.1524" layer="91"/>
<wire x1="177.8" y1="157.48" x2="177.8" y2="154.94" width="0.1524" layer="91"/>
<junction x="177.8" y="157.48"/>
<label x="180.34" y="157.48" size="1.778" layer="95"/>
<wire x1="177.8" y1="157.48" x2="182.88" y2="157.48" width="0.1524" layer="91"/>
<pinref part="Q6" gate="Q" pin="D"/>
<wire x1="182.88" y1="157.48" x2="193.04" y2="157.48" width="0.1524" layer="91"/>
<wire x1="182.88" y1="154.94" x2="182.88" y2="157.48" width="0.1524" layer="91"/>
<junction x="182.88" y="157.48"/>
<pinref part="J3" gate="-2" pin="P$1"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="193.04" y1="124.46" x2="177.8" y2="124.46" width="0.1524" layer="91"/>
<label x="177.8" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM2_NEG" class="0">
<segment>
<pinref part="D12" gate="G$1" pin="1"/>
<pinref part="Q7" gate="Q" pin="D"/>
<wire x1="215.9" y1="157.48" x2="220.98" y2="157.48" width="0.1524" layer="91"/>
<wire x1="220.98" y1="157.48" x2="220.98" y2="154.94" width="0.1524" layer="91"/>
<junction x="220.98" y="157.48"/>
<label x="223.52" y="157.48" size="1.778" layer="95"/>
<wire x1="220.98" y1="157.48" x2="226.06" y2="157.48" width="0.1524" layer="91"/>
<pinref part="Q8" gate="Q" pin="D"/>
<wire x1="226.06" y1="157.48" x2="236.22" y2="157.48" width="0.1524" layer="91"/>
<wire x1="226.06" y1="154.94" x2="226.06" y2="157.48" width="0.1524" layer="91"/>
<junction x="226.06" y="157.48"/>
<pinref part="J4" gate="-2" pin="P$1"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="233.68" y1="124.46" x2="218.44" y2="124.46" width="0.1524" layer="91"/>
<label x="218.44" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="D13" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="D11" gate="G$1" pin="A"/>
<pinref part="R12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="D5" gate="G$1" pin="A"/>
</segment>
</net>
<net name="INT0" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="P6"/>
<wire x1="109.22" y1="63.5" x2="127" y2="63.5" width="0.1524" layer="91"/>
<label x="111.76" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="INT1" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="P7"/>
<wire x1="109.22" y1="60.96" x2="127" y2="60.96" width="0.1524" layer="91"/>
<label x="111.76" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="1"/>
<pinref part="Q3" gate="Q" pin="D"/>
<wire x1="154.94" y1="88.9" x2="154.94" y2="81.28" width="0.1524" layer="91"/>
<pinref part="K1" gate="G$1" pin="A2"/>
<wire x1="154.94" y1="81.28" x2="154.94" y2="78.74" width="0.1524" layer="91"/>
<wire x1="175.26" y1="81.28" x2="162.56" y2="81.28" width="0.1524" layer="91"/>
<junction x="154.94" y="81.28"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="162.56" y1="81.28" x2="154.94" y2="81.28" width="0.1524" layer="91"/>
<junction x="162.56" y="81.28"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="Q4" gate="Q" pin="D"/>
<pinref part="D7" gate="G$1" pin="1"/>
<wire x1="154.94" y1="35.56" x2="154.94" y2="38.1" width="0.1524" layer="91"/>
<pinref part="K2" gate="G$1" pin="A2"/>
<wire x1="154.94" y1="38.1" x2="154.94" y2="45.72" width="0.1524" layer="91"/>
<wire x1="175.26" y1="38.1" x2="162.56" y2="38.1" width="0.1524" layer="91"/>
<junction x="154.94" y="38.1"/>
<pinref part="R10" gate="G$1" pin="2"/>
<junction x="162.56" y="38.1"/>
<wire x1="162.56" y1="38.1" x2="154.94" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="D8" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="D9" gate="G$1" pin="A"/>
</segment>
</net>
<net name="!GPIO_INT!" class="0">
<segment>
<wire x1="109.22" y1="55.88" x2="127" y2="55.88" width="0.1524" layer="91"/>
<label x="111.76" y="55.88" size="1.778" layer="95"/>
<pinref part="U7" gate="G$1" pin="!INT!"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>In Panel</description>
<plain>
<text x="59.944" y="121.92" size="1.778" layer="97">I2C 0x76</text>
</plain>
<instances>
<instance part="FRAME3" gate="G$1" x="0" y="0"/>
<instance part="U8" gate="G$1" x="55.88" y="134.62"/>
<instance part="U9" gate="G$1" x="162.56" y="127" rot="MR90"/>
<instance part="P+13" gate="1" x="121.92" y="134.62" smashed="yes">
<attribute name="VALUE" x="119.38" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="C13" gate="G$1" x="121.92" y="124.46" smashed="yes">
<attribute name="NAME" x="122.936" y="127" size="1.778" layer="95"/>
<attribute name="VALUE" x="122.682" y="122.555" size="1.778" layer="96"/>
</instance>
<instance part="GND49" gate="1" x="121.92" y="119.38"/>
<instance part="GND50" gate="1" x="142.24" y="121.92"/>
<instance part="+3V39" gate="G$1" x="25.4" y="147.32" smashed="yes">
<attribute name="VALUE" x="22.86" y="147.32" size="1.778" layer="96"/>
</instance>
<instance part="C12" gate="G$1" x="25.4" y="137.16" smashed="yes">
<attribute name="NAME" x="21.336" y="139.7" size="1.778" layer="95"/>
<attribute name="VALUE" x="16.002" y="135.382" size="1.778" layer="96"/>
</instance>
<instance part="GND43" gate="1" x="25.4" y="132.08"/>
<instance part="GND44" gate="1" x="30.48" y="121.92"/>
<instance part="GND47" gate="1" x="81.28" y="124.46"/>
<instance part="J5" gate="-1" x="43.18" y="73.66" smashed="yes">
<attribute name="NAME" x="43.18" y="76.2" size="1.778" layer="95"/>
<attribute name="VALUE" x="43.18" y="68.58" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J5" gate="-2" x="43.18" y="68.58" smashed="yes">
<attribute name="NAME" x="43.18" y="71.12" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="43.18" y="63.5" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J5" gate="-3" x="43.18" y="63.5" smashed="yes">
<attribute name="NAME" x="43.18" y="66.04" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="43.18" y="58.42" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J5" gate="-4" x="43.18" y="58.42" smashed="yes">
<attribute name="NAME" x="43.18" y="60.96" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="43.18" y="53.34" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J5" gate="-5" x="43.18" y="53.34" smashed="yes">
<attribute name="NAME" x="43.18" y="55.88" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="43.18" y="48.26" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J5" gate="-6" x="43.18" y="48.26" smashed="yes">
<attribute name="NAME" x="43.18" y="50.8" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="43.18" y="43.18" size="1.778" layer="96" display="off"/>
</instance>
<instance part="P+10" gate="1" x="38.1" y="76.2" smashed="yes">
<attribute name="VALUE" x="35.56" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="GND45" gate="1" x="38.1" y="45.72"/>
<instance part="+3V310" gate="G$1" x="38.1" y="66.04" smashed="yes">
<attribute name="VALUE" x="35.56" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="J6" gate="-1" x="73.66" y="73.66" smashed="yes">
<attribute name="NAME" x="73.66" y="76.2" size="1.778" layer="95"/>
<attribute name="VALUE" x="73.66" y="68.58" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J6" gate="-2" x="73.66" y="68.58" smashed="yes">
<attribute name="NAME" x="73.66" y="71.12" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="73.66" y="63.5" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J6" gate="-3" x="73.66" y="63.5" smashed="yes">
<attribute name="NAME" x="73.66" y="66.04" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="73.66" y="58.42" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J6" gate="-4" x="73.66" y="58.42" smashed="yes">
<attribute name="NAME" x="73.66" y="60.96" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="73.66" y="53.34" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J6" gate="-5" x="73.66" y="53.34" smashed="yes">
<attribute name="NAME" x="73.66" y="55.88" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="73.66" y="48.26" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J6" gate="-6" x="73.66" y="48.26" smashed="yes">
<attribute name="NAME" x="73.66" y="50.8" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="73.66" y="43.18" size="1.778" layer="96" display="off"/>
</instance>
<instance part="P+11" gate="1" x="68.58" y="76.2" smashed="yes">
<attribute name="VALUE" x="66.04" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="GND46" gate="1" x="68.58" y="45.72"/>
<instance part="+3V311" gate="G$1" x="68.58" y="66.04" smashed="yes">
<attribute name="VALUE" x="66.04" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="J7" gate="-1" x="104.14" y="73.66" smashed="yes">
<attribute name="NAME" x="104.14" y="76.2" size="1.778" layer="95"/>
<attribute name="VALUE" x="104.14" y="68.58" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J7" gate="-2" x="104.14" y="68.58" smashed="yes">
<attribute name="NAME" x="104.14" y="71.12" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="104.14" y="63.5" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J7" gate="-3" x="104.14" y="63.5" smashed="yes">
<attribute name="NAME" x="104.14" y="66.04" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="104.14" y="58.42" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J7" gate="-4" x="104.14" y="58.42" smashed="yes">
<attribute name="NAME" x="104.14" y="60.96" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="104.14" y="53.34" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J7" gate="-5" x="104.14" y="53.34" smashed="yes">
<attribute name="NAME" x="104.14" y="55.88" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="104.14" y="48.26" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J7" gate="-6" x="104.14" y="48.26" smashed="yes">
<attribute name="NAME" x="104.14" y="50.8" size="1.778" layer="95" display="off"/>
<attribute name="VALUE" x="104.14" y="43.18" size="1.778" layer="96" display="off"/>
</instance>
<instance part="P+12" gate="1" x="99.06" y="76.2" smashed="yes">
<attribute name="VALUE" x="96.52" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="GND48" gate="1" x="99.06" y="45.72"/>
<instance part="+3V312" gate="G$1" x="99.06" y="66.04" smashed="yes">
<attribute name="VALUE" x="96.52" y="66.04" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="PIR1" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="OUT"/>
<wire x1="142.24" y1="127" x2="134.62" y2="127" width="0.1524" layer="91"/>
<label x="134.62" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="GND49" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="GND"/>
<pinref part="GND50" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND43" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="GND"/>
<pinref part="GND44" gate="1" pin="GND"/>
<wire x1="33.02" y1="129.54" x2="30.48" y2="129.54" width="0.1524" layer="91"/>
<wire x1="30.48" y1="129.54" x2="30.48" y2="127" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="GND@1"/>
<wire x1="30.48" y1="127" x2="30.48" y2="124.46" width="0.1524" layer="91"/>
<wire x1="33.02" y1="127" x2="30.48" y2="127" width="0.1524" layer="91"/>
<junction x="30.48" y="127"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="SDO"/>
<pinref part="GND47" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J5" gate="-6" pin="P$1"/>
<pinref part="GND45" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J6" gate="-6" pin="P$1"/>
<pinref part="GND46" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J7" gate="-6" pin="P$1"/>
<pinref part="GND48" gate="1" pin="GND"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P+13" gate="1" pin="+5V"/>
<wire x1="121.92" y1="129.54" x2="121.92" y2="132.08" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<pinref part="U9" gate="G$1" pin="VCC"/>
<wire x1="142.24" y1="129.54" x2="121.92" y2="129.54" width="0.1524" layer="91"/>
<junction x="121.92" y="129.54"/>
</segment>
<segment>
<pinref part="J5" gate="-1" pin="P$1"/>
<pinref part="P+10" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="J6" gate="-1" pin="P$1"/>
<pinref part="P+11" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="J7" gate="-1" pin="P$1"/>
<pinref part="P+12" gate="1" pin="+5V"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V39" gate="G$1" pin="+3V3"/>
<wire x1="25.4" y1="142.24" x2="25.4" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<pinref part="U8" gate="G$1" pin="VDD"/>
<wire x1="33.02" y1="142.24" x2="30.48" y2="142.24" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="VDDIO"/>
<wire x1="30.48" y1="142.24" x2="25.4" y2="142.24" width="0.1524" layer="91"/>
<wire x1="33.02" y1="139.7" x2="30.48" y2="139.7" width="0.1524" layer="91"/>
<wire x1="30.48" y1="139.7" x2="30.48" y2="142.24" width="0.1524" layer="91"/>
<junction x="30.48" y="142.24"/>
<junction x="25.4" y="142.24"/>
<wire x1="25.4" y1="144.78" x2="25.4" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J5" gate="-3" pin="P$1"/>
<pinref part="+3V310" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J6" gate="-3" pin="P$1"/>
<pinref part="+3V311" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J7" gate="-3" pin="P$1"/>
<pinref part="+3V312" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="SDA"/>
<wire x1="81.28" y1="142.24" x2="86.36" y2="142.24" width="0.1524" layer="91"/>
<label x="81.28" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J5" gate="-4" pin="P$1"/>
<wire x1="38.1" y1="58.42" x2="30.48" y2="58.42" width="0.1524" layer="91"/>
<label x="30.48" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J6" gate="-4" pin="P$1"/>
<wire x1="68.58" y1="58.42" x2="60.96" y2="58.42" width="0.1524" layer="91"/>
<label x="60.96" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J7" gate="-4" pin="P$1"/>
<wire x1="99.06" y1="58.42" x2="91.44" y2="58.42" width="0.1524" layer="91"/>
<label x="91.44" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="SCL"/>
<wire x1="81.28" y1="139.7" x2="86.36" y2="139.7" width="0.1524" layer="91"/>
<label x="81.28" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J5" gate="-5" pin="P$1"/>
<wire x1="38.1" y1="53.34" x2="30.48" y2="53.34" width="0.1524" layer="91"/>
<label x="30.48" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J6" gate="-5" pin="P$1"/>
<wire x1="68.58" y1="53.34" x2="60.96" y2="53.34" width="0.1524" layer="91"/>
<label x="60.96" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J7" gate="-5" pin="P$1"/>
<wire x1="99.06" y1="53.34" x2="91.44" y2="53.34" width="0.1524" layer="91"/>
<label x="91.44" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="INT0" class="0">
<segment>
<pinref part="J5" gate="-2" pin="P$1"/>
<wire x1="38.1" y1="68.58" x2="30.48" y2="68.58" width="0.1524" layer="91"/>
<label x="30.48" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="INT1" class="0">
<segment>
<pinref part="J6" gate="-2" pin="P$1"/>
<wire x1="68.58" y1="68.58" x2="60.96" y2="68.58" width="0.1524" layer="91"/>
<label x="60.96" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="INT2" class="0">
<segment>
<pinref part="J7" gate="-2" pin="P$1"/>
<wire x1="99.06" y1="68.58" x2="91.44" y2="68.58" width="0.1524" layer="91"/>
<label x="91.44" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Out Panel</description>
<plain>
<text x="123.444" y="134.62" size="1.778" layer="97">I2C 0x40</text>
<text x="32.004" y="132.08" size="1.778" layer="97">I2C 0x23</text>
<text x="87.884" y="43.18" size="1.778" layer="97">I2C 0x42</text>
</plain>
<instances>
<instance part="FRAME4" gate="G$1" x="0" y="0"/>
<instance part="U10" gate="G$1" x="38.1" y="147.32"/>
<instance part="U11" gate="G$1" x="88.9" y="78.74"/>
<instance part="FB1" gate="G$1" x="40.64" y="66.04"/>
<instance part="R15" gate="G$1" x="50.8" y="66.04"/>
<instance part="U13" gate="G$1" x="195.58" y="99.06" rot="MR90"/>
<instance part="P+14" gate="1" x="154.94" y="106.68" smashed="yes">
<attribute name="VALUE" x="152.4" y="106.68" size="1.778" layer="96"/>
</instance>
<instance part="C18" gate="G$1" x="154.94" y="96.52" smashed="yes">
<attribute name="NAME" x="155.956" y="99.06" size="1.778" layer="95"/>
<attribute name="VALUE" x="155.702" y="94.615" size="1.778" layer="96"/>
</instance>
<instance part="GND60" gate="1" x="154.94" y="91.44"/>
<instance part="GND61" gate="1" x="175.26" y="93.98"/>
<instance part="U12" gate="G$1" x="116.84" y="147.32"/>
<instance part="+3V315" gate="G$1" x="86.36" y="160.02" smashed="yes">
<attribute name="VALUE" x="83.82" y="160.02" size="1.778" layer="96"/>
</instance>
<instance part="C17" gate="G$1" x="86.36" y="149.86" smashed="yes">
<attribute name="NAME" x="82.296" y="152.4" size="1.778" layer="95"/>
<attribute name="VALUE" x="76.962" y="148.082" size="1.778" layer="96"/>
</instance>
<instance part="GND58" gate="1" x="86.36" y="144.78"/>
<instance part="GND59" gate="1" x="91.44" y="134.62"/>
<instance part="+3V316" gate="G$1" x="142.24" y="142.24" smashed="yes">
<attribute name="VALUE" x="139.7" y="142.24" size="1.778" layer="96"/>
</instance>
<instance part="+3V313" gate="G$1" x="15.24" y="160.02" smashed="yes">
<attribute name="VALUE" x="12.7" y="160.02" size="1.778" layer="96"/>
</instance>
<instance part="C14" gate="G$1" x="15.24" y="149.86" smashed="yes">
<attribute name="NAME" x="11.176" y="152.4" size="1.778" layer="95"/>
<attribute name="VALUE" x="5.842" y="148.082" size="1.778" layer="96"/>
</instance>
<instance part="GND51" gate="1" x="15.24" y="144.78"/>
<instance part="GND52" gate="1" x="22.86" y="137.16"/>
<instance part="C16" gate="G$1" x="58.42" y="134.62" smashed="yes">
<attribute name="NAME" x="54.356" y="137.16" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.022" y="132.842" size="1.778" layer="96"/>
</instance>
<instance part="GND56" gate="1" x="58.42" y="129.54"/>
<instance part="D14" gate="G$1" x="48.26" y="93.98" rot="R270"/>
<instance part="R16" gate="G$1" x="58.42" y="93.98"/>
<instance part="GND54" gate="1" x="40.64" y="91.44"/>
<instance part="GND57" gate="1" x="58.42" y="43.18"/>
<instance part="+3V314" gate="G$1" x="53.34" y="116.84" smashed="yes">
<attribute name="VALUE" x="50.8" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="C15" gate="G$1" x="53.34" y="106.68" smashed="yes">
<attribute name="NAME" x="49.276" y="109.22" size="1.778" layer="95"/>
<attribute name="VALUE" x="43.942" y="104.902" size="1.778" layer="96"/>
</instance>
<instance part="GND55" gate="1" x="53.34" y="101.6"/>
<instance part="CON4" gate="G$1" x="22.86" y="63.5"/>
<instance part="GND53" gate="1" x="25.4" y="58.42"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="FB1" gate="G$1" pin="2"/>
<pinref part="R15" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ANT" class="0">
<segment>
<pinref part="FB1" gate="G$1" pin="1"/>
<wire x1="35.56" y1="66.04" x2="35.56" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="11_RF_IN"/>
<wire x1="35.56" y1="63.5" x2="60.96" y2="63.5" width="0.1524" layer="91"/>
<wire x1="35.56" y1="63.5" x2="25.4" y2="63.5" width="0.1524" layer="91"/>
<label x="25.4" y="63.5" size="1.778" layer="95"/>
<junction x="35.56" y="63.5"/>
<pinref part="CON4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="8_RESERVED"/>
<wire x1="60.96" y1="68.58" x2="58.42" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="9_VCC_RF"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="60.96" y1="66.04" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<wire x1="58.42" y1="66.04" x2="55.88" y2="66.04" width="0.1524" layer="91"/>
<wire x1="58.42" y1="68.58" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<junction x="58.42" y="66.04"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="GND60" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U13" gate="G$1" pin="GND"/>
<pinref part="GND61" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="GND58" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="GND"/>
<pinref part="GND59" gate="1" pin="GND"/>
<wire x1="93.98" y1="142.24" x2="91.44" y2="142.24" width="0.1524" layer="91"/>
<wire x1="91.44" y1="142.24" x2="91.44" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U12" gate="G$1" pin="GND@1"/>
<wire x1="91.44" y1="139.7" x2="91.44" y2="137.16" width="0.1524" layer="91"/>
<wire x1="93.98" y1="139.7" x2="91.44" y2="139.7" width="0.1524" layer="91"/>
<junction x="91.44" y="139.7"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="GND51" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="GND"/>
<pinref part="GND52" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND56" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D14" gate="G$1" pin="C"/>
<pinref part="GND54" gate="1" pin="GND"/>
<wire x1="43.18" y1="93.98" x2="40.64" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND57" gate="1" pin="GND"/>
<wire x1="58.42" y1="45.72" x2="58.42" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="10_GND"/>
<wire x1="58.42" y1="48.26" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="58.42" y1="50.8" x2="58.42" y2="53.34" width="0.1524" layer="91"/>
<wire x1="58.42" y1="53.34" x2="58.42" y2="55.88" width="0.1524" layer="91"/>
<wire x1="58.42" y1="55.88" x2="60.96" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="12_GND"/>
<wire x1="60.96" y1="53.34" x2="58.42" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="13_GND"/>
<wire x1="60.96" y1="50.8" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="24_GND"/>
<wire x1="60.96" y1="48.26" x2="58.42" y2="48.26" width="0.1524" layer="91"/>
<junction x="58.42" y="48.26"/>
<junction x="58.42" y="50.8"/>
<junction x="58.42" y="53.34"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="GND55" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="CON4" gate="G$1" pin="2"/>
<pinref part="GND53" gate="1" pin="GND"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P+14" gate="1" pin="+5V"/>
<wire x1="154.94" y1="101.6" x2="154.94" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="U13" gate="G$1" pin="VCC"/>
<wire x1="175.26" y1="101.6" x2="154.94" y2="101.6" width="0.1524" layer="91"/>
<junction x="154.94" y="101.6"/>
</segment>
</net>
<net name="PIR2" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="OUT"/>
<wire x1="175.26" y1="99.06" x2="167.64" y2="99.06" width="0.1524" layer="91"/>
<label x="167.64" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V315" gate="G$1" pin="+3V3"/>
<wire x1="86.36" y1="154.94" x2="86.36" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<pinref part="U12" gate="G$1" pin="VDD"/>
<wire x1="93.98" y1="154.94" x2="91.44" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U12" gate="G$1" pin="VDDIO"/>
<wire x1="91.44" y1="154.94" x2="86.36" y2="154.94" width="0.1524" layer="91"/>
<wire x1="93.98" y1="152.4" x2="91.44" y2="152.4" width="0.1524" layer="91"/>
<wire x1="91.44" y1="152.4" x2="91.44" y2="154.94" width="0.1524" layer="91"/>
<junction x="91.44" y="154.94"/>
<junction x="86.36" y="154.94"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="SDO"/>
<pinref part="+3V316" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V313" gate="G$1" pin="+3V3"/>
<wire x1="15.24" y1="154.94" x2="15.24" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="22.86" y1="154.94" x2="15.24" y2="154.94" width="0.1524" layer="91"/>
<junction x="15.24" y="154.94"/>
<pinref part="U10" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="+3V314" gate="G$1" pin="+3V3"/>
<wire x1="53.34" y1="111.76" x2="53.34" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="60.96" y1="111.76" x2="53.34" y2="111.76" width="0.1524" layer="91"/>
<junction x="53.34" y="111.76"/>
<pinref part="U11" gate="G$1" pin="23_VCC"/>
<wire x1="60.96" y1="111.76" x2="60.96" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="SDA"/>
<wire x1="142.24" y1="154.94" x2="147.32" y2="154.94" width="0.1524" layer="91"/>
<label x="142.24" y="154.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="SDA"/>
<wire x1="53.34" y1="154.94" x2="60.96" y2="154.94" width="0.1524" layer="91"/>
<label x="53.34" y="154.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="18_SDA2"/>
<wire x1="60.96" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<label x="53.34" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="SCL"/>
<wire x1="142.24" y1="152.4" x2="147.32" y2="152.4" width="0.1524" layer="91"/>
<label x="142.24" y="152.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="SCL"/>
<wire x1="53.34" y1="152.4" x2="60.96" y2="152.4" width="0.1524" layer="91"/>
<label x="53.34" y="152.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="19_SCL2"/>
<wire x1="60.96" y1="78.74" x2="53.34" y2="78.74" width="0.1524" layer="91"/>
<label x="53.34" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="BH1750_DVI" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="DVI"/>
<wire x1="53.34" y1="139.7" x2="58.42" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<junction x="58.42" y="139.7"/>
<wire x1="58.42" y1="139.7" x2="68.58" y2="139.7" width="0.1524" layer="91"/>
<label x="53.34" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="3_TIMEPULSE"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="D14" gate="G$1" pin="A"/>
<wire x1="53.34" y1="93.98" x2="50.8" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="FRAME5" gate="G$1" x="0" y="0"/>
<instance part="U14" gate="G$1" x="119.38" y="101.6"/>
<instance part="C19" gate="G$1" x="154.94" y="129.54" smashed="yes">
<attribute name="NAME" x="150.876" y="132.08" size="1.778" layer="95"/>
<attribute name="VALUE" x="145.542" y="127.762" size="1.778" layer="96"/>
</instance>
<instance part="GND64" gate="1" x="71.12" y="78.74"/>
<instance part="R23" gate="G$1" x="149.86" y="119.38"/>
<instance part="R24" gate="G$1" x="149.86" y="114.3"/>
<instance part="R25" gate="G$1" x="149.86" y="111.76"/>
<instance part="R26" gate="G$1" x="149.86" y="93.98"/>
<instance part="R27" gate="G$1" x="149.86" y="88.9"/>
<instance part="R28" gate="G$1" x="149.86" y="86.36"/>
<instance part="R29" gate="G$1" x="149.86" y="83.82"/>
<instance part="GND66" gate="1" x="180.34" y="86.36"/>
<instance part="P+15" gate="1" x="154.94" y="142.24" smashed="yes">
<attribute name="VALUE" x="152.4" y="142.24" size="1.778" layer="96"/>
</instance>
<instance part="R18" gate="G$1" x="66.04" y="129.54" smashed="yes" rot="R90">
<attribute name="NAME" x="66.802" y="131.064" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="68.072" y="125.222" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D15" gate="G$1" x="66.04" y="121.92" smashed="yes">
<attribute name="NAME" x="66.548" y="116.332" size="1.778" layer="95"/>
<attribute name="VALUE" x="71.755" y="117.348" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND63" gate="1" x="66.04" y="111.76"/>
<instance part="R19" gate="G$1" x="93.98" y="119.38"/>
<instance part="R20" gate="G$1" x="93.98" y="116.84"/>
<instance part="R21" gate="G$1" x="93.98" y="114.3"/>
<instance part="R22" gate="G$1" x="93.98" y="96.52"/>
<instance part="SW1" gate="G$1" x="50.8" y="119.38" rot="MR90"/>
<instance part="R17" gate="G$1" x="48.26" y="127" rot="R270"/>
<instance part="GND62" gate="1" x="48.26" y="111.76"/>
<instance part="GND65" gate="1" x="154.94" y="124.46"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="D15" gate="G$1" pin="C"/>
<pinref part="GND63" gate="1" pin="GND"/>
<wire x1="66.04" y1="114.3" x2="66.04" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND64" gate="1" pin="GND"/>
<wire x1="71.12" y1="81.28" x2="71.12" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U14" gate="G$1" pin="GND@1"/>
<wire x1="71.12" y1="83.82" x2="71.12" y2="101.6" width="0.1524" layer="91"/>
<wire x1="71.12" y1="101.6" x2="71.12" y2="121.92" width="0.1524" layer="91"/>
<wire x1="71.12" y1="121.92" x2="96.52" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U14" gate="G$1" pin="GND@2"/>
<wire x1="71.12" y1="101.6" x2="96.52" y2="101.6" width="0.1524" layer="91"/>
<junction x="71.12" y="101.6"/>
<pinref part="U14" gate="G$1" pin="GND@3"/>
<wire x1="71.12" y1="83.82" x2="96.52" y2="83.82" width="0.1524" layer="91"/>
<junction x="71.12" y="83.82"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="T1"/>
<pinref part="GND62" gate="1" pin="GND"/>
<pinref part="SW1" gate="G$1" pin="T2"/>
<wire x1="50.8" y1="114.3" x2="48.26" y2="114.3" width="0.1524" layer="91"/>
<junction x="48.26" y="114.3"/>
</segment>
<segment>
<pinref part="U14" gate="G$1" pin="GND@4"/>
<wire x1="180.34" y1="116.84" x2="144.78" y2="116.84" width="0.1524" layer="91"/>
<pinref part="GND66" gate="1" pin="GND"/>
<wire x1="180.34" y1="88.9" x2="180.34" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U14" gate="G$1" pin="GND@5"/>
<wire x1="180.34" y1="91.44" x2="180.34" y2="96.52" width="0.1524" layer="91"/>
<wire x1="180.34" y1="96.52" x2="180.34" y2="109.22" width="0.1524" layer="91"/>
<wire x1="180.34" y1="109.22" x2="180.34" y2="116.84" width="0.1524" layer="91"/>
<wire x1="180.34" y1="109.22" x2="144.78" y2="109.22" width="0.1524" layer="91"/>
<junction x="180.34" y="109.22"/>
<pinref part="U14" gate="G$1" pin="GND@6"/>
<wire x1="180.34" y1="96.52" x2="144.78" y2="96.52" width="0.1524" layer="91"/>
<junction x="180.34" y="96.52"/>
<pinref part="U14" gate="G$1" pin="GND@7"/>
<wire x1="180.34" y1="91.44" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
<junction x="180.34" y="91.44"/>
<pinref part="U14" gate="G$1" pin="GND@8"/>
<wire x1="144.78" y1="127" x2="154.94" y2="127" width="0.1524" layer="91"/>
<wire x1="154.94" y1="127" x2="180.34" y2="127" width="0.1524" layer="91"/>
<wire x1="180.34" y1="127" x2="180.34" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="2"/>
<junction x="154.94" y="127"/>
<junction x="180.34" y="116.84"/>
<pinref part="GND65" gate="1" pin="GND"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="GPIO2/SDA1"/>
<wire x1="96.52" y1="129.54" x2="78.74" y2="129.54" width="0.1524" layer="91"/>
<label x="78.74" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="GPIO3/SCL1"/>
<wire x1="96.52" y1="127" x2="78.74" y2="127" width="0.1524" layer="91"/>
<label x="78.74" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="GPIO18"/>
<pinref part="R23" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="GEN4/GPIO23"/>
<pinref part="R24" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="GEN5/GPIO24"/>
<pinref part="R25" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="GPIO12"/>
<pinref part="R26" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="GPIO16"/>
<pinref part="R27" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="GPIO20"/>
<pinref part="R28" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="GPIO21"/>
<pinref part="R29" gate="G$1" pin="1"/>
</segment>
</net>
<net name="PIR1" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="152.4" y1="119.38" x2="165.1" y2="119.38" width="0.1524" layer="91"/>
<label x="152.4" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="PIR2" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="152.4" y1="114.3" x2="165.1" y2="114.3" width="0.1524" layer="91"/>
<label x="152.4" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="REL_HEATER" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="152.4" y1="111.76" x2="165.1" y2="111.76" width="0.1524" layer="91"/>
<label x="152.4" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="REL_SPARE1" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="152.4" y1="93.98" x2="165.1" y2="93.98" width="0.1524" layer="91"/>
<label x="152.4" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADS1X15_ALERT" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="152.4" y1="88.9" x2="165.1" y2="88.9" width="0.1524" layer="91"/>
<label x="152.4" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="BH1750_DVI" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="152.4" y1="86.36" x2="165.1" y2="86.36" width="0.1524" layer="91"/>
<label x="152.4" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="BT1" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="152.4" y1="83.82" x2="165.1" y2="83.82" width="0.1524" layer="91"/>
<label x="152.4" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="T3"/>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="SW1" gate="G$1" pin="T4"/>
<wire x1="50.8" y1="124.46" x2="48.26" y2="124.46" width="0.1524" layer="91"/>
<junction x="48.26" y="124.46"/>
<wire x1="48.26" y1="124.46" x2="38.1" y2="124.46" width="0.1524" layer="91"/>
<label x="38.1" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P+15" gate="1" pin="+5V"/>
<pinref part="U14" gate="G$1" pin="5V0@1"/>
<wire x1="144.78" y1="132.08" x2="147.32" y2="132.08" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="154.94" y1="134.62" x2="154.94" y2="137.16" width="0.1524" layer="91"/>
<wire x1="154.94" y1="137.16" x2="147.32" y2="137.16" width="0.1524" layer="91"/>
<wire x1="147.32" y1="137.16" x2="147.32" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U14" gate="G$1" pin="5V0@2"/>
<wire x1="144.78" y1="129.54" x2="147.32" y2="129.54" width="0.1524" layer="91"/>
<wire x1="147.32" y1="129.54" x2="147.32" y2="132.08" width="0.1524" layer="91"/>
<junction x="147.32" y="132.08"/>
<wire x1="154.94" y1="139.7" x2="154.94" y2="137.16" width="0.1524" layer="91"/>
<junction x="154.94" y="137.16"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="D15" gate="G$1" pin="A"/>
</segment>
</net>
<net name="3V3@1" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="3V3@1"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="96.52" y1="132.08" x2="66.04" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="48.26" y1="132.08" x2="66.04" y2="132.08" width="0.1524" layer="91"/>
<junction x="66.04" y="132.08"/>
</segment>
</net>
<net name="PWM0" class="0">
<segment>
<wire x1="78.74" y1="119.38" x2="88.9" y2="119.38" width="0.1524" layer="91"/>
<label x="78.74" y="119.38" size="1.778" layer="95"/>
<pinref part="R19" gate="G$1" pin="1"/>
</segment>
</net>
<net name="PWM1" class="0">
<segment>
<label x="78.74" y="116.84" size="1.778" layer="95"/>
<wire x1="78.74" y1="116.84" x2="88.9" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<label x="78.74" y="114.3" size="1.778" layer="95"/>
<wire x1="78.74" y1="114.3" x2="88.9" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="U14" gate="G$1" pin="GPIO17/GEN0"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="U14" gate="G$1" pin="GPIO27/GEN2"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="U14" gate="G$1" pin="GPIO22/GEN3"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="U14" gate="G$1" pin="GPIO5"/>
</segment>
</net>
<net name="!GPIO_INT!" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="88.9" y1="96.52" x2="76.2" y2="96.52" width="0.1524" layer="91"/>
<label x="76.2" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,4,22.86,154.94,U8,VCC,+3V3,,,"/>
<approved hash="104,3,33.02,142.24,U7,VDD,+3V3,,,"/>
<approved hash="104,3,33.02,139.7,U7,VDDIO,+3V3,,,"/>
<approved hash="104,1,83.82,109.22,U2,VS,+3V3,,,"/>
<approved hash="104,1,193.04,147.32,U4,VIN,+5V,,,"/>
<approved hash="104,1,213.36,147.32,U4,VOUT,+3V3,,,"/>
<approved hash="104,3,142.24,129.54,U$1,VCC,+5V,,,"/>
<approved hash="104,4,175.26,101.6,U$2,VCC,+5V,,,"/>
<approved hash="104,4,93.98,154.94,U10,VDD,+3V3,,,"/>
<approved hash="104,4,93.98,152.4,U10,VDDIO,+3V3,,,"/>
<approved hash="104,2,25.4,40.64,U11,VDD,+3V3,,,"/>
<approved hash="208,1,83.82,129.54,GND,sup,,,,"/>
<approved hash="208,1,76.2,132.08,GND,out,,,,"/>
<approved hash="208,1,144.78,137.16,GND,sup,,,,"/>
<approved hash="208,1,203.2,137.16,GND,sup,,,,"/>
<approved hash="208,1,73.66,99.06,GND,sup,,,,"/>
<approved hash="208,1,119.38,139.7,GND,sup,,,,"/>
<approved hash="208,1,124.46,139.7,GND,sup,,,,"/>
<approved hash="208,1,172.72,139.7,GND,sup,,,,"/>
<approved hash="208,1,177.8,139.7,GND,sup,,,,"/>
<approved hash="208,1,220.98,139.7,GND,sup,,,,"/>
<approved hash="208,1,228.6,139.7,GND,sup,,,,"/>
<approved hash="208,2,60.96,129.54,GND,sup,,,,"/>
<approved hash="208,2,78.74,99.06,GND,sup,,,,"/>
<approved hash="208,2,76.2,99.06,GND,sup,,,,"/>
<approved hash="208,2,66.04,71.12,GND,sup,,,,"/>
<approved hash="208,2,78.74,55.88,GND,sup,,,,"/>
<approved hash="208,2,76.2,53.34,GND,sup,,,,"/>
<approved hash="208,2,15.24,33.02,GND,sup,,,,"/>
<approved hash="208,2,22.86,20.32,GND,sup,,,,"/>
<approved hash="208,2,93.98,17.78,GND,sup,,,,"/>
<approved hash="208,2,154.94,66.04,GND,sup,,,,"/>
<approved hash="208,2,147.32,66.04,GND,sup,,,,"/>
<approved hash="208,2,154.94,22.86,GND,sup,,,,"/>
<approved hash="208,2,147.32,22.86,GND,sup,,,,"/>
<approved hash="208,2,139.7,142.24,GND,sup,,,,"/>
<approved hash="208,2,132.08,142.24,GND,sup,,,,"/>
<approved hash="208,2,144.78,142.24,GND,sup,,,,"/>
<approved hash="208,2,177.8,142.24,GND,sup,,,,"/>
<approved hash="208,2,170.18,142.24,GND,sup,,,,"/>
<approved hash="208,2,182.88,142.24,GND,sup,,,,"/>
<approved hash="208,2,220.98,142.24,GND,sup,,,,"/>
<approved hash="208,2,213.36,142.24,GND,sup,,,,"/>
<approved hash="208,2,226.06,142.24,GND,sup,,,,"/>
<approved hash="208,2,119.38,116.84,GND,sup,,,,"/>
<approved hash="208,2,124.46,116.84,GND,sup,,,,"/>
<approved hash="208,2,129.54,116.84,GND,sup,,,,"/>
<approved hash="208,2,106.68,22.86,GND,sup,,,,"/>
<approved hash="208,3,121.92,121.92,GND,sup,,,,"/>
<approved hash="208,3,142.24,124.46,GND,sup,,,,"/>
<approved hash="208,3,25.4,134.62,GND,sup,,,,"/>
<approved hash="208,3,30.48,124.46,GND,sup,,,,"/>
<approved hash="208,3,81.28,127,GND,sup,,,,"/>
<approved hash="208,3,38.1,48.26,GND,sup,,,,"/>
<approved hash="208,3,68.58,48.26,GND,sup,,,,"/>
<approved hash="208,3,99.06,48.26,GND,sup,,,,"/>
<approved hash="208,4,154.94,93.98,GND,sup,,,,"/>
<approved hash="208,4,175.26,96.52,GND,sup,,,,"/>
<approved hash="208,4,86.36,147.32,GND,sup,,,,"/>
<approved hash="208,4,91.44,137.16,GND,sup,,,,"/>
<approved hash="208,4,15.24,147.32,GND,sup,,,,"/>
<approved hash="208,4,22.86,139.7,GND,sup,,,,"/>
<approved hash="208,4,58.42,132.08,GND,sup,,,,"/>
<approved hash="208,4,40.64,93.98,GND,sup,,,,"/>
<approved hash="208,4,58.42,45.72,GND,sup,,,,"/>
<approved hash="208,4,53.34,104.14,GND,sup,,,,"/>
<approved hash="113,2,130.071,89.431,FRAME2,,,,,"/>
<approved hash="113,3,130.071,89.431,FRAME3,,,,,"/>
<approved hash="113,4,130.071,89.431,FRAME4,,,,,"/>
<approved hash="113,1,130.071,89.431,FRAME1,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
</compatibility>
</eagle>
